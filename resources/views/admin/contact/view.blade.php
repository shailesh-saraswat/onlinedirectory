@extends('admin.index')

@section('after-style')

<link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endsection

@section('content')

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                  
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">Enquiry List</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Enquiry List</h3>
                            </div>
                           
                            <div class="card-body">
                                <table id="page-list" class="table table-bordered table-striped">
                                    <thead>
                                       <tr>
                                        <th>S.No</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Type</th>
                                        <th>Subject</th>
                                        <th>Message</th>
                                        <th>Date</th>
                                        </tr>
                                    </thead>
                                   <tbody>
                                    
                                        <tr>
                                        <td>{{$showcontact->id}}</td>
                                        <td>{{$showcontact->name}}</td>
                                        <td>{{$showcontact->email}}</td>
                                        @if($showcontact->usercontact[0]['roles_id'] == 3)
                                        <td>{{ "User"}}</td>
                                        @elseif($showcontact->usercontact[0]['roles_id'] == 2)
                                        <td>{{ "Professional"}}</td>
                                        @else
                                        <td>{{ "NA"}}</td>
                                        @endif
                                        <td>{{$showcontact->subject}}</td>
                                        <td>{!! $showcontact->message !!}</td>
                                        <td>{{date('d M Y',strtotime($showcontact->created_at))}}</td>
                                        </tr>
            
                                    </tbody>
                                </table>
                            </div> 
                        </div>
                    </div>  
                </div>
            </div>
        </section>
    </div>
@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

@endsection