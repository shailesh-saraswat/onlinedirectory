<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        User::create([
            'name'          => Str::random(5),
            'slug'          => Str::slug('-'),
            'first_name'    => Str::random(5),
            'last_name'     => Str::random(5),
            'email'         => Str::random(5)."ds@gmail.com",
            'password'      => bcrypt('hello123'),
            'verified'      => 1,
            'business_name' => Str::random(5),
            'location'      => '',
            'latitude'      => '',
            'longitude'     => '',
            'category_id'   => 1,
            'template_id'   => 1,
            'mobile'        => "1234566789",
            'roles_id'      => 2,
            'status'        => 2,
        ]);
    }
}
