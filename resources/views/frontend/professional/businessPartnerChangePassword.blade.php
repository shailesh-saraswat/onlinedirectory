@extends('frontend.professional.partials.index')

@section('content')

<main class="content">
<div class="container-fluid p-0">

	<div class="row mb-2 mb-xl-3">
		<div class="col-auto d-none d-sm-block">
			<h3><strong> {{auth()->user()->name ?? ""}}</strong> Change Password</h3>
		</div>
		<div class="col-auto ms-auto text-end mt-n1">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb bg-transparent p-0 mt-1 mb-0">
					<li class="breadcrumb-item"><a href="{{route('business.partner.profile')}}">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Change Password</li>
				</ol>
			</nav>
		</div>						
	</div>
	<div class="row">
		<div class="col-xl-7 col-xxl-5 d-flex">
			<div class="card w-100">
				<div class="card-body">
				<h2> Change Password </h2>
				@include('frontend.partials.messages')
				<form action="{{route('business.partner.updatepassword')}}" method="post">
				@csrf
				<div class="panel-body">
					<div class="profile-tab profile-content changepwdPT">										
						<div class="form-group">
							<label for="">Current Password</label>
							
							<input type="password" name="old_password" class="form-control" id="" value="{{old('old_password')}}" placeholder="Enter Old Password">
						</div>
						<div class="form-group">
							<label for="">New Password</label>
							<input type="password" name="new_password" class="form-control" id="" placeholder="Enter New Password">
							
						</div>
						<div class="form-group">
							<label for="">Confirm Password</label>
							<input type="password" name="confirm_password" class="form-control" id="" placeholder="Enter Confirm Password">
						</div>										
					</div>
					<button type="submit" class="btn btn-default btnCustom">Submit</button>									
				</div>
				</form>
			</div>
		</div>
	</div>
</div>				
</div>
</main>
@endsection