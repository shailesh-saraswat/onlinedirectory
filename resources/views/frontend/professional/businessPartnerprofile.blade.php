@extends('frontend.professional.partials.index')

@section('content')

<main class="content">
	<div class="container-fluid p-0">

		<div class="row mb-2 mb-xl-3">
			<div class="col-auto d-none d-sm-block">
				<h3><strong>{{auth()->user()->name ?? ""}}</strong> Profile</h3>
			</div>
			<div class="col-auto ms-auto text-end mt-n1">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb bg-transparent p-0 mt-1 mb-0">
						<li class="breadcrumb-item"><a href="{{route('business.partner.home')}}">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">User Profile</li>
					</ol>
				</nav>
			</div>						
		</div>
		<div class="row">
			<div class="col-xl-7 col-xxl-5 d-flex">
				<div class="card w-100">
					<div class="card-body">
						@include('frontend.partials.messages')
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="profile-tab profile-content">
									<h2>
										Profile 
										<span>
											<a href="{{route('business.partner.editprofile')}}" class="edit-p">
												<i class="fal fa-pencil"></i> Edit Profile
											</a>
										</span> 
									</h2>
									<table>
										<tbody>

											<tr>
												<td>Name</td>
												<td>{!!$businessPartnerProfileData->first_name !!} {!!$businessPartnerProfileData->last_name !!}</td>
											</tr>
											<tr>
												<td>Email</td>
												<td>{!!$businessPartnerProfileData->email!!}</td>
											</tr>
											<tr>
												<td>Business Telephone Number</td>
												<td>{!!$businessPartnerProfileData->mobile!!}</td>
											</tr>
											<tr>
												<td>Business Name</td>
												<td>{!!$businessPartnerProfileData->business_name!!}</td>
											</tr>
											<tr>
												<td>Address</td>
												<td>{!!$businessPartnerProfileData->location!!}</td>
											</tr>
											<tr>
												<td>Zipcode</td>
												<td>{!!$businessPartnerProfileData->zip_code!!}</td>
											</tr>										
										</tbody>
									</table>
								</div>				
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>				
	</div>
</main>		

@endsection 