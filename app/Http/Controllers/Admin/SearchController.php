<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use DB;

class SearchController extends Controller
{


	public function index(Request $request)
	{
		$validation = Validator::make($request->all(), [
			'search'    => 'required'
		]);

		if ($validation->fails()) {
			return redirect()->back()->withErrors($validation);
		} else {

			$searchRequest = $request->input('search');
			
			$users = User::query()
				->where('roles_id', '!=', 1)
				->where('first_name','like','%'.$searchRequest.'%')
				->orWhere('last_name','like','%'.$searchRequest.'%')
				->orWhere('email','like','%'.$searchRequest.'%')
				->get();

			/* return $users; */
            return view('admin.users.index')->withUsers($users);
		}
	}

	

}
