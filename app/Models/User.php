<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
// use Laravel\Cashier\Billable;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'password', 
        /* 'mobile', 
        'address',  */
        //'otp', 
        //'verified',  
        'status', 
        'device_type', 
        'device_token', 
        'profile_image',
        'is_trial',
        'free_trial_end',
        'is_subscribed'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','text_password'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /* public function types() {
        return $this->belongsTo('App\Models\User_Type', 'type', 'id');
    } */

    public function tokens(){
        return $this->hasMany('App\Models\PassportToken', 'user_id', 'id');
    }

    public function roles(){
        return $this->belongsTo('App\Models\Role','roles_id','id');     
    }
    
    public function reviews() {
        return $this->hasMany(Reviews::class, 'user_type', 'id');
    }

    public function avgreviews() {
        return $this->hasMany(Reviews::class, 'user_type', 'id')->orderBy('id','asc');
    }

    public function template(){
        return $this->belongsTo('App\Models\Template','template_id','id');     
    }   

    public function userdetail() {
        return $this->belongsTo('App\Models\BusinessInformation', 'id', 'professional_id');
    }

    public function subscriptions() {
        return $this->hasOne('App\Models\UserSubscription', 'cust_id', 'id')->latest(); 
    }

    /* public function scopeIsWithinMaxDistance($query, $coordinates, $radius = 50) {
        
        return $query
        ->select(\DB::raw("*,
            (6367 * ACOS(COS(RADIANS(".$coordinates['latitude']."))
            * COS(RADIANS(latitude))
            * COS(RADIANS(".$coordinates['longitude'].") - RADIANS(longitude))
            + SIN(RADIANS(".$coordinates['latitude']."))
            * SIN(RADIANS(latitude)))) AS distance"))->having('distance', '<', $radius)
        ->orderBy('distance', 'asc')     
        ->get();
    } */

    public function scopeIsWithinMaxDistance($query, $coordinates, $radius = 50, $userIds) {
        
        $userIds = (array) $userIds;
        
        return $query
        ->select(\DB::raw("*,
            (6367 * ACOS(COS(RADIANS(".$coordinates['latitude']."))
            * COS(RADIANS(latitude))
            * COS(RADIANS(".$coordinates['longitude'].") - RADIANS(longitude))
            + SIN(RADIANS(".$coordinates['latitude']."))
            * SIN(RADIANS(latitude)))) AS distance_km"))->having('distance_km', '<', $radius)
            
        ->orderBy('distance_km', 'asc')
        ->with('userdetail','reviews')
        ->whereIn('id', $userIds)        
        ->first();
    }
    
}
