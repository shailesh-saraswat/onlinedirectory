<section class="about-with-promo pb-100" id="aboutus">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-lg-6">
				<div class="about-content-wrap">
					<strong class="color-secondary">About Us</strong>
					<h2>{!! $about->title !!}</h2>
					<span class="animate-border mb-4"></span>
					<p>{!! $about->sort_description !!}</p>

					<ul class="check-list-info">
						{!! $about->description !!}
					</ul>

				</div>
			</div>
			{{-- <div class="col-lg-6">
				<div class="about-us-img">
					@if($about->featured_image)
					<img src="{{asset($about->featured_image)}}" alt="{{asset($about->featured_image)}}" class="img-fluid about-single-img">
					@else
					<img src="" alt="Not Uploaded" height="150" width="180" style="margin-top:25px"> 
					@endif
					
				</div>
			</div> --}}
		</div>
	</div>
</section>
