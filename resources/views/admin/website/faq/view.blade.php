@extends('admin.index')

@section('after-style')

    

@endsection

@section('content')

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">FAQ's</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">View FAQ's </h3>
                                <a href="{{url('admin/faq/edit',[ $faq->id])}}" style="float: right;">&nbsp;&nbsp;&nbsp;<i class="fas fa-edit" style="font-size:23px;"></i></a> 
                               
                                <a href="{{url('admin/faq/create')}}" style="float: right;"><i class="fas fa-plus-square" style="font-size:25px;"></i></a>
                            </div>
                            @if(session('message'))
                                <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
                            @endif
                            <div class="card-body">
                                <table id="page-list" class="table table-bordered table-striped">
                                
                                    <tr>
                                    <th colspan="2"></th>       
                                    </tr> 
                                        <tr>
                                        <th>Question</th>
                                        <td>{{ $faq->question}}</td>
                                        </tr>
                                        <tr>
                                        <th>Ansewer</th>
                                        <td>{!!$faq->answer!!} </td>
                                        </tr>		
                                        <tr>
                                        <th>Status</th>
                                        <td>@if( $faq->status==1) {{'Active'}} @else {{'In Active'}}@endif</td>
                                    </tr>
                                    <tr>
                                    <th>Created At</th>
                                    <td>@if(!empty( $faq->created_at)) {{date("d M, y",strtotime( $faq->created_at))}}@endif</td>
                                </tr>
                                <tr>
                                    <th>Updated At</th>
                                    <td>{{date("d M, y",strtotime( $faq->updated_at))}}</td>
                                </tr>
                                </table>
                                
                                <br>
                            <div class="box-footer" style="float:right">
                                <a href="{{ url('admin/faq')}}" class="btn btn-primary">Back</a>
                            </div>
                            </div> 
                        </div>
                    </div>  
                </div>
            </div>
        </section>
    </div>
@endsection

@section('after-scripts')


@endsection