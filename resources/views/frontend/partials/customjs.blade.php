<script src="https://maps.google.com/maps/api/js?key=AIzaSyCR-_1vWp4V7pifLhXGmq5AAGmDy4pB2Tg&libraries=places" type="text/javascript"></script>
<script>
$(document).ready(function(){
$(".main-search-input-item .chosen-select").addClass("removeNone");

/* map */
google.maps.event.addDomListener(window, 'load', initialize);
function initialize() {
	
    var locatorSection = document.getElementById("locator-input-section")
    var locatorButton = document.getElementById("locator-button");
        locatorButton.addEventListener("click", locatorButtonPressed)
       
    function locatorButtonPressed() {
        locatorSection.classList.add("loading")

        navigator.geolocation.getCurrentPosition(function (position) {
                getUserAddressBy(position.coords.latitude, position.coords.longitude)
            },
            function (error) {
                locatorSection.classList.remove("loading")
            })
    }

    function getUserAddressBy(lat, long) {
        $('#latitude').val(lat);
        $('#longitude').val(long);
        $('#latitude1').val(lat);
        $('#longitude1').val(long);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var address = JSON.parse(this.responseText)
                console.log(address.results[6].formatted_address);
                setAddressToInputField(address.results[6].formatted_address)
            }
        };
        xhttp.open("GET", "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + long + "&key=AIzaSyCR-_1vWp4V7pifLhXGmq5AAGmDy4pB2Tg", true);
        xhttp.send();
    }
    function setAddressToInputField(address) {

        input.value = address
        locatorSection.classList.remove("loading")
    }

	var input = document.getElementById('autocomplete');
	var autocomplete = new google.maps.places.Autocomplete(input);
	autocomplete.addListener('place_changed', function() {
		var place = autocomplete.getPlace();
        console.log(place.formatted_address);
		$('#latitude').val(place.geometry['location'].lat());
		$('#longitude').val(place.geometry['location'].lng());
		$('#longitude1').val(place.geometry['location'].lng());
		$('#latitude1').val(place.geometry['location'].lat());

	// --------- show lat and long ---------------
		$("#lat_area").removeClass("d-none");
		$("#long_area").removeClass("d-none");
	});
}

    
$("#lat_area").addClass("d-none");
$("#long_area").addClass("d-none");

var _token = $('input[name="_token"]').val();

//alert(_token);
load_data('', _token);

function load_data(id="", _token) {
    $.ajax({
        url:"{{ route('loadmore.load_data') }}",
        method:"POST",
        data:{id:id, _token:_token},
    success:function(response) {
        //console.log(response.data)
        $('#load_more_button').remove();
        $('#post_data').append(response.data.html);
        $('#loadMoreBTN').append(response.data.button);
        }
    }); 
}

$(document).on('click', '#load_more_button', function(){
        var id = $(this).data('id');
        $('#load_more_button').html('<b>Loading...</b>');
        load_data(id, _token);
});

/*  ====== for accordion   =====  */

const items = document.querySelectorAll(".accordion button");

function toggleAccordion() {
const itemToggle = this.getAttribute('aria-expanded');

for (i = 0; i < items.length; i++) {
    items[i].setAttribute('aria-expanded', 'false');
}

if (itemToggle == 'false') {
    this.setAttribute('aria-expanded', 'true');
}
}

items.forEach(item => item.addEventListener('click', toggleAccordion));


  

});

function get_data() {
   var APP_URL      = {!! json_encode(url('/')) !!}
   var sort         =  $('#sort').val();
   var token        = "{{request()->get('_token')}}";
   let locationTrim = "{{request()->get('location')}}";
   var location     = locationTrim.trim();
   var latitude     = "{{request()->get('latitude')}}";
   var longitude    = "{{request()->get('longitude')}}";
   var category     = "{{request()->get('category')}}";
   
   window.location.href=APP_URL+"/professional/filterData?latitude="+latitude+'&longitude='+longitude+'&location='+location+'&category='+category+'&sort='+sort;
}

</script>