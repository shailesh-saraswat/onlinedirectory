<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\PassportToken;
use Illuminate\Validation\Rule;
use App\Models\{User,Domains};
use Exception;

class DomainController extends Controller
{
    public $successStatus = 200;
    
    /*
    |--------------------------------------------------------------------------
    | ======== API Add Domain ========
    |--------------------------------------------------------------------------
    |
    | Local Url:http://localhost/onlinedirectory/api/v1/addDomain
    | Live Url: 
    | Key:type,name,email,password,device_type,device_token,confirm_password,mobile,zip_code,location;
    | Method:POST
    | Comments: Add Domain user with key values and Post method.
    |
    |--------------------------------------------------------------------------
    */ 

    public function AddUpdateDomain(Request $request) {

        try{

            $validation = Validator::make($request->all(), [

                'domain_name'       => 'required'
            ]);
        
            if ($validation->fails()) {

                $result['code']     = 400;
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);
            } 

        $user   = auth()->user();

        if(isset($user) && $user != null)
        { 
            $domain = Domains::get();
            $userid="";
            foreach ($domain as $key => $dovalue) {
                $userid = $dovalue->userid;
            }

            if($user->id == $userid){
            
                $updateDomain = Domains::query()->where('userid', $user->id)->first();
                $updateDomain->domain_name  = $request->domain_name;
                //Domains::where('userid',$user->id)->update(array('domain_name' => $request->domain_name));
                if($updateDomain->update()){
                    $result['code']     = 200;
                    $result['error']    = false;
                    $result['message']  = "Domain updated successfully.";

                return response()->json($result);

                } else {

                    $result['code']    = 500;
                    $result['error']   = true;
                    $result['message'] = "Something went wrong!";

                    return response()->json($result);
                } 

            } else{

                $domain                 = new Domains;
                $domain->userid         = $user->id;
                $domain->domain_name    = $request->domain_name;

                if($domain->save()){
                    $result['code']     = 200;
                    $result['error']    = false;
                    $result['message']  = "Domain created successfully.";

                return response()->json($result);

                } else {

                    $result['code']    = 500;
                    $result['error']   = true;
                    $result['message'] = "Something went wrong!";

                    return response()->json($result);
                } 
            }

        } else {

                $result['code']    = 401;
                $result['error']   = true;
                $result['message'] = "Unauthorized";
                return response()->json($result);
        }



        } catch(Exception $e){
            return response()->json($e->getMessage());
        }
    }
}
