<div class="modal-content">
<div class="modal-header">
    <h4 class="modal-title"><bold><span style="color:red;">{{ucfirst($professional->first_name) . $professional->last_name }}</span></bold></h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">

<div class="form-group row">
    <label for="title" class="col-sm-2 col-form-label">First Name</label>
    <div class="col-sm-4">
    <input type="text" class="form-control" id="first_name" name="first_name" value="{{$professional->first_name}}" placeholder="First Name" readonly/>
    </div>

    <label for="title" class="col-sm-2 col-form-label">Last Name</label>
    <div class="col-sm-4">
    <input type="text" class="form-control" id="last_name" name="last_name" value="{{$professional->last_name}}" placeholder="Last Name" readonly/>
    </div>
    
</div>  

<div class="form-group row">
    
    <label for="title" class="col-sm-2 col-form-label">Email</label>
    <div class="col-sm-4">
    <input type="text" class="form-control" id="email" name="email" value="{{$professional->email}}" placeholder="Email" readonly/>
    </div>

    <label for="title" class="col-sm-2 col-form-label">Category</label>
    <div class="col-sm-4">
        <select class="form-control select2" style="width: 100%;" name="category" readonly>
            @if(empty($professional->category_id))
            <option>No Category Selected</option>
            @else
            @foreach($categories as $cat)
            <option value="{{$cat->id}}" {{ $cat->id == $professional->category_id ? 'selected' : '' }} disabled>{{$cat->name}}</option>
            @endforeach
            @endif

        </select>
    </div>
    
</div>  
    
<div class="form-group row">

<label for="title" class="col-sm-2 col-form-label">Address</label>
<div class="col-sm-4">
<input type="text" class="form-control" id="location" name="location" value="{{$professional->location}}" placeholder="Location" readonly/>
</div>

<label for="title" class="col-sm-2 col-form-label">Zip Code</label>
<div class="col-sm-4">
<input type="text" class="form-control" id="zip_code" name="zip_code" value="{{$professional->zip_code}}" placeholder="Zip Code" readonly/>
</div>

</div>

<div class="form-group row">

<label for="Status" class="col-sm-2 col-form-label">Status</label>
<div class="col-sm-4">
    <select class="form-control select2" style="width: 100%;" name="status" readonly>
        <option value="1" {{ $professional->status == 1  ? 'selected' : ''}} disabled>Active</option>
        <option value="0" {{ $professional->status == 0  ? 'selected' : ''}} disabled>Inactive</option>
        <option value="2" {{ $professional->status == 2  ? 'selected' : ''}} disabled>Pending</option>
    </select> 
</div>
    
</div>  
    
</div>
<div class="modal-footer justify-content-between">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>