<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_subscriptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('pay_status', ['Complete','Incomplete','Incomplete Expired','Pending','Disputed','Refunded'])->default('Complete');
            $table->string('user_id')->nullable();
            $table->string('cust_id')->nullable();
            $table->string('template_id')->default('1');
            $table->string('customer_name')->nullable();
            $table->string('plan_name')->nullable();
            $table->enum('cycle_type', ['Subscription','Renewal']);
            $table->decimal('fee',10,2);
            $table->decimal('total_amount_received',10,2);
            $table->string('transaction_id')->nullable();
            $table->string('disputed_id')->nullable();
            $table->string('subscription_id')->nullable();
            $table->string('plan_id')->nullable();
            $table->string('payment_name')->nullable();
            $table->date('start_date');
            $table->date('renewal_date');
            $table->enum('status', ['0','1'])->default('1')->comment('0=>Inactive,1=>Active');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_subscriptions');
    }
}
