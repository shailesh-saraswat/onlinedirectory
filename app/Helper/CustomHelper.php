<?php
namespace App\Helper;

class CustomHelper
{
	public static function set_date($dt)
	{
		return date('d M Y, h:i:s A', strtotime($dt));
	}
	public static function show_date($dt, $name, $start_sep = '(', $end_sep = ')')
	{
		$output = static::set_date($dt);
		if(!empty($name)) {
			$output .= $start_sep . ' ' . $name . $end_sep;
		}
		return $output;
	}

	public static function set_active($path, $active = 'active') {
    	return call_user_func_array('Request::is', (array)$path) ? $active : '';
	}
}