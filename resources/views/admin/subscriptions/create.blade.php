@extends('admin.index')

@section('after-style')

<link rel="stylesheet" href="{{asset('public/plugins/summernote/summernote-bs4.css')}}">

@endsection    

@section('content')

<div class="content-wrapper">
        
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">Manage Subscription</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-outline card-info">
            <div class="card-header">
              <h3 class="card-title">
                Create 
              </h3>
              
              <div class="card-tools">
        
              </div>

            </div>
            @include('admin.partials.messages')
      
            <form class="form-horizontal" action="{{url('admin/subscriptions/store')}}" method="post">
              @csrf
              <div class="card-body text">
                <label for="Title" class="col-sm-2 col-form-label">Title</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="title" placeholder="Title" name="name" value="{{old('name')}}" required/>
                </div>
              </div>
              <div class="card-body text">
                <label for="Title" class="col-sm-2 col-form-label">Price</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="price" placeholder="Price" name="price" value="{{old('price')}}" required/>
                </div>
              </div>

               <div class="card-body text">
                <label for="Status" class="col-sm-2 col-form-label">Type</label>
                <div class="col-sm-10">
                  <select class="form-control select2" style="width: 100%;" name="time_interval" required>
                    @forelse($interval as $key=>$value)
                    <option value="{{$key}}">{{$value}}</option>
                    @empty
                    <option value=""></option>
                    @endforelse
                  </select>    
                </div>
              </div>

              <div class="card-body text">
                <label for="Description" class="col-sm-2 col-form-label">Description</label>
                <div class="col-sm-10">
                  <textarea class="textarea" name="description" id="description" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required>{{old('description')}}</textarea>
                </div>
                
              </div>
              <div class="card-body text">
                <label for="Status" class="col-sm-2 col-form-label">Status</label>
                <div class="col-sm-10">
                  <select class="form-control select2" style="width: 100%;" name="status" required>
                    <option value="0">Inactive</option>
                    <option value="1" selected>Active</option>
                  </select>    
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-info">Save</button>
                <a href="{{url('admin/subscriptions')}}" class="btn btn-default float-right">Cancel</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>

</div>

@endsection

@section('after-scripts')

<script src="{{asset('public/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script>
<script>
  setTimeout(function() {

    $('.alert-default-danger').fadeOut('fast');

  }, 3000);

  setTimeout(function() {

    $('.alert-default-success').fadeOut(1000);

  }, 3000);
</script>

@endsection