@extends('frontend.partials.index')
 
@section('content')

<section class="flat-row page-profile bg-theme">
<div class="container">
{{-- @include('frontend.partials.messages') --}}
<div class="text-center clearfix">
<form method="POST" action="{{url('reset-password')}}">
    @csrf
    <input type="hidden" name="token" value="{{ $token }}">
    <h3 class="title-formlogin">Reset Password</h3>
    @csrf
    <span class="input-login icon-form">
    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" autocomplete="email" placeholder="Email" autofocus>
    @error('email')
    <span class="invalid-feedback" role="alert" style="display:block;">
    <strong>{{ $message }}</strong>
    </span>
    @enderror

    <span class="input-login icon-form">
    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" name="password" autocomplete="new-password"></span>
    @error('password')
    <span class="invalid-feedback" role="alert" style="display:block;">
    <strong>{{ $message }}</strong>
    </span>
    @enderror

    <span class="input-login icon-form">
      <input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="new-password" placeholder="Confirm Password"></span>
    @error('password')
    <span class="invalid-feedback" role="alert" style="display:block;">
    <strong>{{ $message }}</strong>
    </span>
    @enderror

    <span class="wrap-button">
        <button type="submit" id="login-button" class=" login-btn effect-button" title="log in">  Reset Password</button>
    </span>

</form>
</div>
</div>
</section>

@endsection