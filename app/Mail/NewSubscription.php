<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewSubscription extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user_data;
    public $plan_name;
    public function __construct($user_data,$plan_name)
    {
        $this->user_data = $user_data;
        $this->plan_name = $plan_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email = $this->view('emails.user.new_subscription')
            ->subject('New Subscription')
            ->with([
            'user_data' => $this->user_data,
            'plan_name' => $this->plan_name,
        ]);
        return $email;
    }
}
