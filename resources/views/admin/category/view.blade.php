<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">{{$category->name}}</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
    
    <div class="form-group row">
        <label for="title" class="col-sm-2 col-form-label">Name</label>
        <div class="col-sm-4">
        <input type="text" class="form-control" id="name" name="name" value="{{$category->name}}" placeholder="Name" readonly/>
        </div>

        <label for="title" class="col-sm-2 col-form-label">Parent Name</label>
        <div class="col-sm-4">
        <input type="text" class="form-control" id="parent_name" name="parent_name" value="{{$category->parent_name}}" placeholder="Parent Name" readonly/>
        </div>
        
    </div>  

        
    <div class="form-group row">
        <label for="Status" class="col-sm-2 col-form-label">Status</label>
        <div class="col-sm-4">
            <select class="form-control select2" style="width: 100%;" name="status" readonly>
                <option value="1" {{ $category->status == 1  ? 'selected' : ''}} disabled>Active</option>
                <option value="0" {{ $category->status == 0  ? 'selected' : ''}} disabled>Inactive</option>
            </select> 
        </div>
        
    </div>  
        

    </div>
    <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</div>
       