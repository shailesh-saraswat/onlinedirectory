function fetch_data(device = '', verified = '', status = '') {

    $('#user-list').DataTable({
        "processing": true,
        "serverSide": true,
        "retrieve": true,
        "paging": false,
        "ajax": {
            "type": "POST",
            "url": "{{url('admin/filter')}}",
            "dataSrc": "",
            "data": {
                _token: "{{ csrf_token() }}",
                device: device,
                verified: verified,
                status: status
            },
            'draw': 'draw',
        },
        infoCallback: function(settings, start, end, max, total, pre) {
            return (!isNaN(total)) ?
                "Showing " + start + " to " + end + " of " + total + " entries" +
                ((total !== max) ? " (filtered from " + max + " total entries)" : "") :
                "Showing " + start + " to " + (start + this.api().data().length - 1) + " entries";
        },
        "columns": [{
                data: 'id',
                name: 'id'
            },
            {
                data: 'name',
                name: 'name'
            },
            {
                data: 'email',
                name: 'email'
            },
            {
                data: 'mobile',
                name: 'mobile'
            },
            {
                data: 'device_type',
                name: 'device_type'
            },
            {
                data: 'device_token',
                name: 'device_token'
            },
            {
                data: 'verified',
                name: 'verified'
            },
            {
                data: 'status',
                name: 'status'
            },
            {
                data: 'null',

                defaultContent:

                    "<a href='javascript:void();' data-toggle='modal' data-target='#user-info{{$user->id}}'><i class='fas fa-eye'></i>&nbsp;&nbsp;</a><a href='{{url('admin/edit-user/'.$user->id)}}'><i class='fas fa-edit'></i>&nbsp;&nbsp;</a><a href='{{url('admin/delete-user/'.$user->id)}}' onclick='return confirm('Are you sure?')'><i class='fas fa-trash'></i></a>"

            }
        ]
    });
}

$('#filter').click(function() {

    var device = $('#device_type').val();
    var status = $('#status').val();
    var verified = $('#verified').val();

    if (device != '' || status != '' || verified != '') {
        $('#user-list').DataTable().destroy();
        fetch_data(device, status, verified);
    }


});

$('#reset').click(function() {
    $('#device_type').val('');
    $('#status').val('');
    $('#verified').val('');

    fetch_data();
});