@extends('admin.index')

@section('content')

<!-- <div class="wrapper"> -->

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                     
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">Update Category</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10">

                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Update Category</strong></h3>
                            </div>
                            <form class="form-horizontal" method="post" action="{{url('admin/category-update')}}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{$category->id}}"/>
                                <div class="card-body">
                                <div class="form-group row">
                                    <label for="title" class="col-sm-2 col-form-label">Parent Category <span>(Don't select if there is no parent)</span></label>
                                    <div class="col-sm-10">
							        <select class="form-control" name="parent_id">
                                    <option value="">Select Category</option>
                                    @foreach($categories as $cat)
                                    <option {{ $cat->id == $category->parent_id ? 'selected':'' }} value="{{ $cat->id }}">{{ $cat->name }}</option>
                                    @endforeach
                                    </select>
                                    </select>         
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="title" class="col-sm-2 col-form-label">Category Name</label>
                                    <div class="col-sm-10">
                                    <input type="text" class="form-control" id="name" name="name" value="{{$category->name}}" placeholder="Category Name"/>
                                    @error('name')
                                    <span class="invalid-feedback" role="alert" style="display:block;">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="title" class="col-sm-2 col-form-label">Category Icon</label>
                                    <div class="col-sm-10">
                                    <input type="file" class="form-control" id="icon" name="icon" value="{{old('icon')}}" />
                                    @if($category->icon)
                                    <img src="{{asset($category->icon)}}" style="width:50px; height:50px; margin-top:10px;"> 
                                    @else 
                                    <img src="{{asset('public/uploads/categoryIcon/default.jpg')}}" style="width:50px; height:50px; margin-top:10px;"> 
                                    @endif
                                    @error('icon')
                                    <span class="invalid-feedback" role="alert" style="display:block;">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Status" class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10">
                                        <select class="form-control select2" style="width: 100%;" name="status" id="status">
                                            <option value="1" {{$category->status == 1  ? 'selected' : ''}}>Active</option>
                                            <option value="0" {{$category->status == 0  ? 'selected' : ''}}>Inactive</option>
                                        </select>
                                        @error('status')
                                        <span class="invalid-feedback" role="alert" style="display:block;">
                                        <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror   
                                    </div>
                                </div>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                <button type="submit" class="btn btn-info">Update</button>
                                <a href="{{url('admin/categories-list')}}" class="btn btn-default float-right">Cancel</a>
                                </div>
                                <!-- /.card-footer -->
                            </form>
                            </div>

                    </div>
                    <div class="col-1"></div>
                </div>
            </div>
        </section>

    </div>

<!-- </div> -->
@endsection