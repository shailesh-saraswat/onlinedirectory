@extends('admin.index')

@section('after-style')

<link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
@endsection

@section('content')

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Manage Users</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Users List</h3>

                            <a href="{{url('admin/add-user-form')}}" style="float: right;"><i class="fas fa-plus-square" style="font-size:25px;"></i></a>
                        </div>

                        @if(session('message'))
                            <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
                        @endif
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="user-list" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th>Type</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($users) && count($users) > 0)
                                        @php $i=1; @endphp
                                        @foreach($users as $user)
                                           
                                            <tr>
                                                <td>{{$i}}</td>
                                                <td>{{$user->first_name}}</td>
                                                <td>{{$user->last_name}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>{{$user->roles->name}}</td>
                                                <td style="text-align:center">
                                                    @if($user->status == 1)
                                                        <p style="color:green">Active</p>  
                                                    @elseif($user->status == 0)
                                                        <p style="color:red">In-active</p>   
                                                    @elseif($user->status == 2)
                                                        <p style="color:red">Pending</p>   
                                                    @endif
                                                </td>
                                                <td style="text-align: center;">
                                                <a data-toggle="modal" id="TargetID" data-target="#userInfo" data-url="{{ url('admin/show/user',['id'=>$user->id])}}" href="javascript(0)"><i class="fas fa-eye"></i>&nbsp;&nbsp;</a></a>
    
                                                        <a href="{{url('admin/edit-user/'.$user->id)}}"><i class="fas fa-edit"></i>&nbsp;&nbsp;</a>
                                                        <a href="{{url('admin/delete-user/'.$user->id)}}" onclick="return confirm('Are you sure you want to delete?')"><i class="fas fa-trash"></i></a>
                                                </td>
                                            </tr>
                                            @php $i++; @endphp
                                        @endforeach
                                    @endif
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    
    <div class="modal fade" id="userInfo" role="dialog">
        <div class="modal-dialog modal-lg">
        <!-- Modal content-->
            <div class="modal-content user-modal">
                
            </div>
        </div>
    </div>

</div>

@endsection
@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<script>
    $(function() {
        $("#user-list").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });
</script>

@include('admin.partials.customjsModal')

@endsection