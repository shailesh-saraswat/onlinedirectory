@extends('frontend.partials.index')
@section('after-style')

@endsection

@section('content')
<section class="bigHeader" style="background: url('{{asset('public/frontend/assets/image/banner2.jpg')}}') center center;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="tcent">
					Contact Us
					<span class="smallBredcm"> <a href="{{route('home')}}"> Home </a> >> Contact Us </span>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="contact-area area-padding contact-inside">
            <div class="container">
                <div class="row cnt-bg">
                    <!-- Start contact icon column -->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="office-city">
                            <h4>Head Office</h4>
                            <div class="contact-icon">
                                <div class="single-icon">
                                    <i class="fa fa-phone"></i>
                                    <p>
                                        Call : @if(!empty(Session::get('sitedata')->contact_mobile)) {{Session::get('sitedata')->contact_mobile}} @endif<br>
                                        <span>@if(!empty(Session::get('sitedata')->days_open)) {{Session::get('sitedata')->days_open}} @endif</span>
                                    </p>
                                </div>
                            </div>
                            <div class="contact-icon">
                                <div class="single-icon">
                                    <i class="fas fa-envelope"></i>
                                    <p>
                                        Email : @if(!empty(Session::get('sitedata')->contact_email)) {{Session::get('sitedata')->contact_email}} @endif<br>
                                        <span>Web: @if(!empty(Session::get('sitedata')->contact_website)) {{Session::get('sitedata')->contact_website}} @endif</span>
                                    </p>
                                </div>
                            </div>
                            <div class="contact-icon">
                                <div class="single-icon">
                                    <i class="fa fa-map-marker"></i>
                                    <p>
                                        Location :@if(!empty(Session::get('sitedata')->contact_location)) {{Session::get('sitedata')->contact_location}} @endif 
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End contact icon -->
                    
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="contact-form contact-form-bg">

                            @if (Session::has('log_message'))
                            <div class="alert alert-success alert-dismissible">
                                <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{ Session::get('log_message') }}
                                {{ Session::forget('log_message') }}
                               
                            </div>
                            @endif

                            @if (Session::has('message'))
                            <div class="alert alert-danger alert-dismissible">
                                <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{ Session::get('message') }}
                                {{ Session::forget('message') }}
                            </div>
                            @endif

                            <div class="row">
                                <form id="contactForm" method="POST" action="{{route('contactus')}}" class="contact-form" autocomplete="off">
                                    @csrf
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="name" class="form-control" name="name" autocomplete="off" value="{{old('name')}}" placeholder="Name" data-error="Please enter your name">
                                        @error('name')
                                        <span class="invalid-feedback" role="alert" style="display:block;">
                                        <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="email" class="email form-control" id="email" name="email" autocomplete="off" value="{{old('email')}}" placeholder="Email" data-error="Please enter your email">
                                        @error('email')
                                        <span class="invalid-feedback" role="alert" style="display:block;">
                                        <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="text" id="msg_subject" class="form-control" name="subject" value="{{old('subject')}}" placeholder="Subject" autocomplete="off" data-error="Please enter your message subject">
                                        @error('subject')
                                        <span class="invalid-feedback" role="alert" style="display:block;">
                                        <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <textarea id="message" rows="7" value="{{old('message')}}" placeholder="Message" name="message" class="form-control" data-error="Write your" autocomplete="off">{{old('message')}}</textarea>
                                        @error('message')
                                        <span class="invalid-feedback" role="alert" style="display:block;">
                                        <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                        <button type="submit" id="submit" name="submit" class="add-btn contact-btn">Send Message</button>
                                        <div id="msgSubmit" class="h3 text-center hidden"></div> 
                                        <div class="clearfix"></div>
                                    </div>   
                                </form>  
                            </div>
                        </div>
                    </div>
                    <!-- End contact Form -->
                </div>
                <div class="row contact-page">
                   
					<div class="col-md-12">
						<div class="row">
							<iframe src="@if(!empty(Session::get('sitedata')->map_link)) {{Session::get('sitedata')->map_link}} @endif" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
						</div>
					</div>
                </div>
                <!-- End row -->
            </div>
        </div>


@section('after-scripts')
<script type="text/javascript">
/* $('.alert-success').fadeIn("fast", function(){        
    $(".alert-success").fadeOut(10000);
}); */
</script>

@endsection


@endsection