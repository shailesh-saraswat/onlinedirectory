<script src="https://maps.google.com/maps/api/js?key=AIzaSyCR-_1vWp4V7pifLhXGmq5AAGmDy4pB2Tg&libraries=places" type="text/javascript"></script>
<script>
    google.maps.event.addDomListener(window, 'load', initialize);

    function initialize() {
        /* var options = {
            componentRestrictions: {country: "IN"}
        }; */

        var input = document.getElementById('location');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            console.log(place);
            //console.log(place.address_components.types);
            
            $('#latitude').val(place.geometry['location'].lat());
            $('#longitude').val(place.geometry['location'].lng());

        // --------- show lat and long ---------------
            $("#lat_area").removeClass("d-none");
            $("#long_area").removeClass("d-none");
        });
    }
</script>