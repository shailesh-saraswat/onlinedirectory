<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
//use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(AboutTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        //$this->call(UserTypeSeeder::class);
        //$this->call(CategoriesSeeder::class);
    }
}
