<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->roles_id == 1;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'category_title'       => 'required|unique:categories',
            //'category_description' => 'required',
            //'thumbnail'            => 'required|mimes:jpg,jpeg,gif,png',            
            'status'               => 'required',

        ];
    }
}
