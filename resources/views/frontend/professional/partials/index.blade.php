<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>{{env('APP_NAME')}}</title>
<link href="{{asset('public/frontend/assets/css/font-awesome.min.css')}}" rel="stylesheet">	
<link rel="stylesheet" href="{{asset('public/frontend/assets/newBp/css/swiper.min.css')}}">
<link rel="stylesheet" href="{{asset('public/frontend/assets/newBp/css/swiper-bundle.min.css')}}">	
<link  rel="stylesheet" href="{{asset('public/frontend/assets/newBp/css/app.css')}}">
<link  rel="stylesheet" href="{{asset('public/frontend/assets/newBp/css/bpcontentstyling.css')}}">
@yield('after-style')
</head>
<body>
<div class="wrapper">

    @include('frontend.professional.partials.navbar')
    
<div class="main">

    @include('frontend.professional.partials.header')

@yield('content')

@include('frontend.professional.partials.footer')

   
   