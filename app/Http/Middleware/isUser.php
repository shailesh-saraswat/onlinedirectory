<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Redirect;
use Closure;
use Session;

class isUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if ($request->user() && ($request->user()->roles_id == 3) )
        {	 
            return $next($request);

        }else if($request->user() !=null) {
            return Redirect::to('/login')
                ->withErrors("Either you are not authorise for here.or Use Your correct login details");
        }else{				
            return Redirect::to('/login');
        } 
    }

    
}
