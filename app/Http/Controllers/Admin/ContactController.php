<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Session;
use App\Models\ContactUs;

class ContactController extends Controller
{
     /**
     * Contact list page.
     *
     * @param   : none
     * @return  : none
     */
    public function index()
    {
         $contacts = ContactUs::with('usercontact')->orderBy('id', 'desc')->get();
        return view('admin/contact/listing',compact('contacts'));
    }
    
    public function ContactView($id)
    {
        $showcontact = ContactUs::with('usercontact')->where('id', $id)->first();
        return view('admin/contact/view',compact('showcontact'));
    }

}
