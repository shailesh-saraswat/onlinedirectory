<section class="tc_section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="privacy-policy-wrapper">
					<div class="content">
						
						{!! htmlspecialchars_decode($page->description) !!} 

						<p><strong>Effective Date: {!! $page->updated_at !!}</strong></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>