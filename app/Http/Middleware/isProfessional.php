<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Closure;
use Session;

class isProfessional
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        // if (Auth::check() && Auth::user()->roles_id == 2) {
        //     return $next($request);
        // }
        // abort(403);

        if ($request->user() && ($request->user()->roles_id == 2) )
        {	 
            return $next($request);

        }else if($request->user() !=null) {
            return Redirect::to('/business-login')
                ->withErrors("Either you are not authorise for here.or Use Your correct login details");
        }else{				
            return Redirect::to('/business-login');
        } 
    }
}
