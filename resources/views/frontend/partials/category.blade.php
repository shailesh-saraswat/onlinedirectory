
<form action="{{route('professional.search')}}" method="post" class="myclass">
@csrf
<div class="fromsearchsection">
<div class="main-search-input-item location">
    <div id="locator-input-section">
        <input id="autocomplete" name="location" type="text" placeholder="Location" value="{{ isset($location) ? $location : '' }}">
        <input type="hidden" name="latitude" id="latitude" value="{{ isset($latitude) ? $latitude : '' }}">
        <input type="hidden" name="longitude" id="longitude" value="{{ isset($longitude) ? $longitude : '' }}">
    </div>
    <a href="javascript:void(0)" id="locator-button"><i class="fal fa-map-marker-alt"></i></a>
</div>

<div class="main-search-input-item">
    <select data-placeholder="All Categories" name="category" class="chosen-select homeSrcCat" id="myselect">
        <option value="">All Categories</option>	
        @foreach($categories as $cat)
        @foreach($cat->children as $childrenCategory)
        <option value="{{$childrenCategory->id}}" @if($childrenCategory->id == $category) {{'selected="selected"'}} @endif>{{ ucfirst(trans($childrenCategory->name)) }}</option> 
        @endforeach
        @endforeach
    </select>
    
</div>
<button class="button">Search</button>
</div>
</form>
@section('after-scripts')
@include('frontend.partials.customjs')

@endsection
