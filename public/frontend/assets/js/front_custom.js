var base_app_url = window.location.origin+"/shortday_sinc/";
function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if(!regex.test(email)) {
    return false;
  }else{
    return true;
  }
}

function close_alert() {
	setTimeout(function(){ $('.custom_alert').hide(); }, 3000);
}

function check_decimal(el, event) {
    if(event.which < 46 || event.which > 59) {
        event.preventDefault();
    } // prevent if not number/dot

    if(event.which == 46 && $(el).val().indexOf('.') != -1) {
        event.preventDefault();
    } 
}

$(".number").keypress(function (e) {
//if the letter is not digit then display error and don't type anything
	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       return false;
	}	
});

$('.del_btn').on('click',function(){
	if (confirm("Are you sure to delete this item?")) {
        return true;
    }
    return false;
})