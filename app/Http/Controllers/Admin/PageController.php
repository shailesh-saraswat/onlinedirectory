<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\AddPageRequest;
use App\Http\Requests\UpdatePageRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Models\Page;
use Exception;

class PageController extends Controller
{
    public function index(){

        $page = Page::all();

        return view('admin.page.index')->withPage($page);

    }

    public function edit(Request $request, $id){

        try {
            
            $page = Page::findOrfail($id);

            return view('admin.page.edit')->withPage($page);

        }catch(Exception $e){

            return redirect()->back()
                    ->withErrors($e->getMessage());

        }  

    }

    public function create(){

        return view('admin.page.create');

    }

    public function store(AddPageRequest $request){

        if($request->isMethod('post')){
            
            try{
                    
                $requestData = $request->all();

                $page=new Page;
				$page->title=$request->title;
				$page->description=$request->description;
				$page->status=$request->status;
				$page->save();
				
				if ($request->hasFile('featured_image')) {

                    try {
						
						$uppage=Page::findOrfail($page->id);

                        $upload_document = $request->file('featured_image');

                        $name = $upload_document->getClientOriginalName();                       

                        $destinationFolder = public_path('/page/featured_image/'.$page->id);

                        // Delete folder
                        if(file_exists($destinationFolder.$name)){

                            unlink($destinationFolder.$name);
                        }

                        $upload_document->move($destinationFolder, $name);				
						$uppage->featured_image = "/public/page/featured_image/".$page->id.'/' . $name;
                        
						$uppage->save();
                    } catch (Exception $e) {
                            
                        return redirect()->back()
                                ->withErrors($e->getMessage());
                    }
                }		
				return Redirect::to('admin/pages')->withMessage("Page Added Successfully."); 
                
            }catch(Exception $e){

                return redirect()->back()
                    ->withErrors($e->getMessage());
            }
            
        }

    }

    public function update(UpdatePageRequest $request){

        if($request->isMethod('post')){

            try{

                $pageId = $request->input('id');

                $page = Page::query()->where('id', $pageId)->first();
                    
                $page->title=$request->title;
				$page->description=$request->description;
				$page->status=$request->status;
				if ($request->hasFile('featured_image')) {
                    try {						
						

                        $upload_document = $request->file('featured_image');

                        $name = $upload_document->getClientOriginalName();                       

                        $destinationFolder = public_path('/page/featured_image/'.$pageId);

                        // Delete folder
                        if(file_exists($destinationFolder.$name)){

                            unlink($destinationFolder.$name);
                        }

                        $upload_document->move($destinationFolder, $name);
						
						$page->featured_image = "/public/page/featured_image/".$pageId.'/' . $name;
                        
						
                    } catch (Exception $e) {
                            
                        return redirect()->back()
                                ->withErrors($e->getMessage());
                    }
                }		
				
				
				$page->save();
				

                return Redirect::to('admin/pages')->withMessage("Content Updated Successfully."); 
                
            }catch(Exception $e){

                return redirect()->back()
                    ->withErrors($e->getMessage());
            }
            
        }

    }

    public function delete($id){

        $page = Page::findOrfail($id);
        $page->delete();

        $message = "Page Deleted Successfully!";
        return redirect()->back()->withMessage($message);

    }
}
