<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('message')->nullable();
            $table->bigInteger('ratings')->unsigned();
            $table->bigInteger('user_type')->unsigned()->nullable();
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->tinyInteger('status')->default(1);
           /*  $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE'); */
            /* $table->foreign('user_type')
                ->references('type')
                ->on('users')
                ->onDelete('CASCADE'); */
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
