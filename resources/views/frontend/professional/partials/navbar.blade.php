ime <nav id="sidebar" class="sidebar">
<div class="sidebar-content js-simplebar">
    <a class="sidebar-brand" href="{{route('business.partner.home')}}">
        <span class="align-middle">{{env('APP_NAME')}}</span>
    </a>

    <ul class="sidebar-nav">
        <li class="sidebar-header">Pages</li>

        @php 
            $now         = time(); 
            $result_date = strtotime(Auth::user()->free_trial_end);
            $datediff    = ($result_date - $now); 
            //dd(auth()->user());
        @endphp

        @php
        $userid = Auth::user()->id;
        $userSubscription   = DB::table('user_subscriptions')->where('pay_status','Complete')->where('renewal_date','=',date('Y-m-d'))->first();
        //dd($userSubscription);
        @endphp
        
        @if($datediff >= null)
        
        <li class="sidebar-item {{ Route::is('business.partner.home') ? 'active' : '' }}">
            <a class="sidebar-link" href="{{route('business.partner.home')}}">
                <img src="{{asset('public/frontend/assets/newBp/images/dashboard.svg')}}" class="leftMenuIcon img-fluid rounded me-1" alt="Dashboard" />
                <span class="align-middle">Dashboard</span>
            </a>
        </li>
        <li class="sidebar-item {{ Route::is('business.partner.createdomain') ? 'active' : '' }}">
            <a class="sidebar-link" href="{{route('business.partner.createdomain')}}">
                <img src="{{asset('public/frontend/assets/newBp/images/domain.svg')}}" class="leftMenuIcon img-fluid rounded me-1" alt="Add/Update Domain" />
                <span class="align-middle">Add/Update Domain</span>
            </a>
        </li>

        <li class="sidebar-item {{ Route::is('business.partner.template') ? 'active' : '' }}">
            <a class="sidebar-link" href="{{route('business.partner.template')}}">
                <img src="{{asset('public/frontend/assets/newBp/images/templates.png')}}" class="leftMenuIcon img-fluid rounded me-1" alt="Charles Hall" />
                <span class="align-middle">Templates</span>
            </a>
        </li>
        <li class="sidebar-item {{ Route::is('business.partner.manageinformation') ? 'active' : '' }}">
            <a class="sidebar-link" href="{{route('business.partner.manageinformation')}}">
                <img src="{{asset('public/frontend/assets/newBp/images/manageinfo.png')}}" class="leftMenuIcon img-fluid rounded me-1" alt="Charles Hall" />
                <span class="align-middle">Manage Informations</span>
            </a>
        </li>
        @endif

        <li class="sidebar-item {{ Route::is('business.partner.subscription') ? 'active' : '' }}">
            <a class="sidebar-link" href="{{route('business.partner.subscription')}}">
                <img src="{{asset('public/frontend/assets/newBp/images/subscription.png')}}" class="leftMenuIcon img-fluid rounded me-1" alt="Charles Hall" />
                <span class="align-middle">Subscriptions</span>
            </a>
        </li>
        
        @if($datediff >= null)
        <li class="sidebar-item {{ Route::is('business.partner.providercontactus') ? 'active' : '' }}">
            <a class="sidebar-link" href="{{route('business.partner.providercontactus')}}">
                <img src="{{asset('public/frontend/assets/newBp/images/contactus.png')}}" class="leftMenuIcon img-fluid rounded me-1" alt="Charles Hall" />
                <span class="align-middle">Contact Us</span>
            </a>
        </li>
        @endif
    
    </ul>
</div>
</nav>