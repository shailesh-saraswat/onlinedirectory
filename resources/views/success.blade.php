@extends('frontend.professional.partials.index')
@section('content')
<main class="content">
<section id="home" class="iq-main-slider p-0 ">
   <div id="home-slider" class="slider m-0 p-0 margin-bottom-40">
      <div class="slide slick-bg s-bg-4 ">
      </div>
   </div>
</section>
<section class="mtb-100">
   <div class="container">
    <div class="col-lg-6 offset-lg-3">
        <div class="thankyou">
        <h1>THANK YOU!</h1>
        <i class="fas fa-check-circle"></i>
        <p>Your subscription has been completed.</p>
        <p>As a member of Online Directory you will be able to instantly features!<br> Enjoy your membership and keep making great browse!
        </p>
            <br>
            <a href="{{url('business-partner-dashboard')}}">Go Back</a>
        </div>
       </div>
   </div>    
</section>
</main>
@endsection
@section('afterjs')
<style type="text/css">
.thankyou {
text-align: center;
}

.thankyou h1{
color: black;
font-size: 50px;
font-weight: bold;
letter-spacing: 1px;
}

.thankyou i{
font-size: 50px;
margin: 30px 0px;
color: #24b663;
}

.thankyou p{
font-size: 16px;
color: #555;
letter-spacing: 1px;
line-height: 30px;
}
.thankyou a{
margin: 30px 0px;
background-color: #e50914;
padding: 10px 30px;
text-align: center;
color: white;
border-radius: 3px;
transition: 500ms;
}

.thankyou a:hover{

background-color: #bf000a;

color: white!important;

}
</style>
@endsection