<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Response;
use Stripe\Stripe;
use Carbon\Carbon;
use App\Models\UserSubscription;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewSubscription;


class WebhookController extends Controller
{
    
    public function webhookEvents(){

        Stripe::setApiKey("sk_test_51I7W6uA0Jyw2DHYsFM9pv9tih8z34m4SJwQLunheQrKlWT6UtQW2R4T3SlDS0PJMnrYSaIkpL2u5HMChiDq6QOGA00BbYbzVOR");
        $input = @file_get_contents("php://input");
        $event = json_decode($input);

        $response = $event->data->object;

        /*********Webhook for Events Hnadle ************/
        if($event->type == 'customer.subscription.updated')
        {
            if($event->data->object->cancel_at_period_end == true) { 
                $subscription = $event->data->object->id;
                UserSubscription::updateCustomerMemberShip($subscription, 'pause');
            }
        }
        else if($event->type == 'invoice.payment_succeeded')
        {   
            if($event->data->object->billing_reason=="subscription_cycle")
            {
                $customer_id = $event->data->object->customer;
                if($customer_id !='')
                {
                    $customer = \Stripe\Customer::retrieve($customer_id);
                    $subscription_id = $event->data->object->id;
                    $paymentmethod          = $event->data->object->default_payment_method;
                    $nameOnCardAllDetails   =  \Stripe\PaymentMethod::retrieve($paymentmethod);
                    $transaction_id = $nameOnCardAllDetails->id;
                    $nameOnCard     = str_replace("'","",$nameOnCardAllDetails['billing_details']['name']);
                    $amount = $event->data->object->plan->amount* 0.01;
                    $card = $nameOnCardAllDetails['card']['brand'];
                    $user_data  = User::where('email',$customer->email)->first();
                    
                    /*----------- Start other data for the order_subscription table ------------*/
                    $currentDate  = date("Y-m-d", $event->data->object->current_period_start);
                    $renewal_date = date("Y-m-d", $event->data->object->current_period_end);
                    
                    $date1              = date_create($currentDate);
                    $date2              = date_create($renewal_date);
                    $diff               = date_diff($date1,$date2);
                    $days_left          = $diff->format("%a");
                    $dateTime           = date("Y/m/d");
                    $cycle_type = 'Renewal';

                    $subscription = new UserSubscription();
                    $subscription->user_id = $customer_id;
                    $subscription->cust_id = $user_data->id;
                    $subscription->template_id = $user_data->template_id;
                    $subscription->pay_status = 'Complete';
                    $subscription->customer_name = $user_data->first_name." ".$user_data->last_name;
                    $subscription->plan_name = $event->data->object->plan->nickname;
                    $subscription->plan_id = $event->data->object->plan->id;
                    $subscription->cycle_type = 'Subscription';
                    $subscription->fee = $amount;
                    $subscription->total_amount_received = $amount;
                    $subscription->transaction_id = $transaction_id;
                    $subscription->subscription_id = $subscription_id;
                    $subscription->payment_name = $nameOnCard;
                    $subscription->start_date = date('Y-m-d',strtotime('now'));
                    $subscription->renewal_date = $renewal_date;
                    $subscription->status = '1';
                    $subscription->save();
                    //Update subsription mail
                    $subscription = $event->data->object->id;
                    $result =UserSubscription::updateCustomerMemberShip($subscription, 'resume');
                }
            }
        }
        else if($event->type == 'customer.subscription.deleted')
        {
            $customer_id = $event->data->object->customer;
            if($customer_id !='')
            {
                $subscription = $event->data->object->id;
                //Mail for delete subscription
                $result =UserSubscription::updateCustomerMemberShip($subscription, 'delete');
            }
        }
        else if($event->type == 'customer.subscription.created') {
            $customer_id = $event->data->object->customer;
            if($customer_id !='')
            {
                $customer = \Stripe\Customer::retrieve($customer_id);
                $subscription_id = $event->data->object->id;
                $paymentmethod          = $event->data->object->default_payment_method;
                $nameOnCardAllDetails   =  \Stripe\PaymentMethod::retrieve($paymentmethod);
                $transaction_id = $nameOnCardAllDetails->id;
                $nameOnCard     = str_replace("'","",$nameOnCardAllDetails['billing_details']['name']);
                $amount = $event->data->object->plan->amount* 0.01;
                $card = $nameOnCardAllDetails['card']['brand'];
                $user_data  = User::where('email',$customer->email)->first();
                
                /*----------- Start other data for the order_subscription table ------------*/
                $currentDate  = date("Y-m-d", $event->data->object->current_period_start);
                $renewal_date = date("Y-m-d", $event->data->object->current_period_end);
                
                $date1              = date_create($currentDate);
                $date2              = date_create($renewal_date);
                $diff               = date_diff($date1,$date2);
                $days_left          = $diff->format("%a");
                $dateTime           = date("Y/m/d");
                $cycle_type = 'Subscription';

                $subscription = new UserSubscription();
                $subscription->user_id = $customer_id;
                $subscription->cust_id = $user_data->id;
                $subscription->template_id = $user_data->template_id;
                $subscription->pay_status = 'Complete';
                $subscription->customer_name = $user_data->first_name." ".$user_data->last_name;
                $subscription->plan_name = $event->data->object->plan->nickname;
                $subscription->plan_id = $event->data->object->plan->id;
                $subscription->cycle_type = 'Subscription';
                $subscription->fee = $amount;
                $subscription->total_amount_received = $amount;
                $subscription->transaction_id = $transaction_id;
                $subscription->subscription_id = $subscription_id;
                $subscription->payment_name = $nameOnCard;
                $subscription->start_date = date('Y-m-d',strtotime('now'));
                $subscription->renewal_date = $renewal_date;
                $subscription->status = '1';
                $subscription->save();
                $subscription = $event->data->object->id;
                $result =UserSubscription::updateCustomerMemberShip($subscription, 'resume');
            }
        }
        else if($event->type == 'charge.refunded') {
            $subscription = $event->data->object->id;
            $result =UserSubscription::updateCustomerMemberShip($subscription, 'pause');
        }
        else if($event->type == 'charge.dispute.created') {
        }
        else if($event->type == 'charge.dispute.funds_withdrawn') {
        }
        else if($event->type == 'charge.dispute.closed') {
        }
        else if($event->type == 'invoice.payment_failed') {
            $customer_id = $event->data->object->customer;
            if($customer_id !='')
            {
                $customer = \Stripe\Customer::retrieve($customer_id);
                $user_data  = User::where('email',$customer->email)->first();
                $customerId = $user_data->id;
                $result =UserSubscription::updateCustomerStatus($customerId);
            }
        }
        else if($event->type == 'invoice.updated')
        {
            $customer_id = $event->data->object->customer;
            $subscription_id = $event->data->object->subscription;
            $amount = $event->data->object->amount_remaining;
            if($customer_id !='' && $subscription_id!='' && $amount>0)
            {
                //echo "yes1"; die;
                $customer = \Stripe\Customer::retrieve($customer_id);
                $user_data  = User::where('email',$customer->email)->first();
                $customerId = $user_data->id;
                $result =UserSubscription::updateCustomerFailedStatus($customerId,$subscription_id);
            }
            else if($customer_id !='' && $subscription_id!='' && $amount==0)
            {
                //echo "yes2"; die;
                $customer = \Stripe\Customer::retrieve($customer_id);
                $user_data  = User::where('email',$customer->email)->first();
                $customerId = $user_data->id;
                $result =UserSubscription::updateCustomerSuccessStatus($customerId,$subscription_id);
                $check_subsc = UserSubscription::where(['cust_id'=>$customerId,'subscription_id'=>$subscription_id,'pay_status'=>'Complete'])->count();
                if($check_subsc<=1)
                {
                    if($event->data->object->billing_reason=="subscription_create")
                    {
                        $plan_name = $event->data->object->lines->data[0]->plan->nickname;
                        Mail::to($customer->email)->send(new NewSubscription($user_data,$plan_name));
                    }
                }
            }
        }
        return json_encode(['status'=>'success','response'=>'success']);

    }

}
