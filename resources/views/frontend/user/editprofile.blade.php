@extends('frontend.partials.index')
 
@section('content')

<section class="bigHeader" style="background: url('{{asset('public/frontend/assets/image/banner2.jpg')}}') center center;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="tcent">
					Edit Profile
					<span class="smallBredcm"> <a href="{{route('home')}}"> Home </a> >> Edit Profile </span>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="login_section">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<form method="post" action="{{route('user.updateprofile')}}" class="" id="" enctype="multipart/form-data" novalidate>
				<input type="hidden" id="id" name="userId" value="{{auth()->id()}}" />
				@csrf
				<div class="loginBox">
					
					@if(session()->has('log_message'))
					<div class="alert alert-success alert-dismissible">
						<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{ session()->get('log_message') }}
					</div>
					@endif

					@if(session()->has('message'))
					<div class="alert alert-danger alert-dismissible">
						<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{ session()->get('message') }}
					</div>
					@endif

					<div class="">
						<h5 class="login_title" id="exampleModalLabel">Basic Infomation</h5>
					</div>

				   <div class="">
					<div class="form-group"> <label class="form-label">First Name</label> 
					<input type="text" name="first_name" class="form-control" value="{{$user->first_name}}" placeholder="Enter Your First Name" > 
					@error('first_name')
					<span class="invalid-feedback" role="alert" style="display:block;">
					<strong>{{ $message }}</strong>
					</span>
					@enderror
					</div>

					<div class="form-group"> <label class="form-label">Last Name</label> 
					<input type="text" name="last_name" class="form-control" value="{{$user->last_name}}" placeholder="Enter Your Last Name" > 
					@error('last_name')
					<span class="invalid-feedback" role="alert" style="display:block;">
					<strong>{{ $message }}</strong>
					</span>
					@enderror
					</div>

					<div class="form-group"> <label class="form-label">Email</label>
					<input type="email" name="email" class="form-control" value="{{$user->email}}" placeholder="Enter Your Email"> 
					@error('email')
					<span class="invalid-feedback" role="alert" style="display:block;">
					<strong>{{ $message }}</strong>
					</span>
					@enderror
					</div>
					
					<div class="form-group"> <label class="form-label">Phone</label>
					<input type="text" name="mobile" class="form-control" value="{{$user->mobile}}" placeholder="Enter Your Phone"> 
					@error('mobile')
					<span class="invalid-feedback" role="alert" style="display:block;">
					<strong>{{ $message }}</strong>
					</span>
					@enderror
					</div>	
				    <div class="customLogRegisWrapper">
					    <div class="submit"> 
							<button class="btn btn-primary btn-block">Submit</button>
						</div>
				    </div>
				
				</div>
			</div>
		</div>
	</div>
</section>


@endsection 