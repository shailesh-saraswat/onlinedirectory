<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\PassportToken;
use Illuminate\Validation\Rule;
use App\Models\{User,Domains};
use Exception;

class DomainController extends Controller
{   
    public function createDomain(Request $request){
        $domain = Domains::where('userid',auth()->user()->id)->first();
        return View('frontend.professional.businessPartnerDomain',compact('domain'));
    }

    public function AddUpdateDomain(Request $request) {

        try{

            $validation             = Validator::make($request->all(), [                 
                'domain_name'       => "required|url",
                //'domain_name'       => ['required','regex' => '/^((?:https?\:\/\/|www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)$/'],
            ]);
                 
        if ($validation->fails()) {    
			return  redirect()->back()->withErrors($validation)->withInput(Input::all());

		} else {

            $user   = auth()->user();
            $domain = Domains::get();
            $userid="";
            foreach ($domain as $key => $dovalue) {
                 $userid = $dovalue->userid;
            }

            if($user->id == $userid){

                $updateDomain = Domains::query()->where('userid', $user->id)->first();
                $updateDomain->domain_name  = $request->domain_name;

                if($updateDomain->update()){
                    return redirect()->back()->withMessage("Domain updated successfully.");
                } else {
                    return redirect()->back()->withMessage("Something went wrong!");
                }

            } else {

                $domain                 = new Domains;
                $domain->userid         = $user->id;
                $domain->domain_name    = $request->domain_name;

                if($domain->save()){
                    return redirect()->back()->withMessage("Domain created successfully.");
                } else {
                    return redirect()->back()->withMessage("Something went wrong!");
                }
            }
        }


        } catch(Exception $e){
            return response()->json($e->getMessage());
        }
    }
}
