@extends('frontend.partials.index')
@section('after-style')

@endsection

@section('content')
<section class="bigHeader" style="background: url('{{asset('public/frontend/assets/image/banner2.jpg')}}') center center;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="tcent">
					FAQ's
					<span class="smallBredcm"> <a href="{{route('home')}}"> Home </a> >> FAQ's </span>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="tc_section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="privacy-policy-wrapper">
					<div class="content">
						
						<!-- FAQ -->


<section class="sptb">
	<div class="container">
	   <h2>Frequently Asked Questions</h2>
	   <div class="accordion">
    
           @foreach ($faqs as $faq)
            <div class="accordion-item">
                <button id="accordion-button-1" aria-expanded="false"><span class="accordion-title">{{$faq->question}}</span><span class="icon" aria-hidden="true"></span></button>
                <div class="accordion-content">
                    {!! htmlspecialchars_decode($faq->answer) !!}
                </div>
            </div>
            @endforeach
		 
	   </div>
	</div>
</section>

<script>
const items = document.querySelectorAll(".accordion button");

function toggleAccordion() {
  const itemToggle = this.getAttribute('aria-expanded');
  
  for (i = 0; i < items.length; i++) {
    items[i].setAttribute('aria-expanded', 'false');
  }
  
  if (itemToggle == 'false') {
    this.setAttribute('aria-expanded', 'true');
  }
}

items.forEach(item => item.addEventListener('click', toggleAccordion));

</script>


</div>
</div>
</div>
</div>
</div>
</section>

@section('after-scripts')


@endsection

@endsection