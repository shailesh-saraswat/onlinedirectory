<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_menus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable()->comment('Menu Name');
            $table->text('url')->nullable();
            $table->string('controller')->nullable();
            $table->string('action')->nullable();
            $table->string('icon')->nullable()->comment('Fa Fa Icon');
            $table->tinyInteger('parent_menu_id')->nullable();
            $table->boolean('status')->nullable()->comment('1=Active,0=Inactive');
            $table->tinyInteger('priority')->nullable()->comment('Set Menu Possition');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_menus');
    }
}
