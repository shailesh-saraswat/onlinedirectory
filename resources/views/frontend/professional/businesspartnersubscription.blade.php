@extends('frontend.professional.partials.index')

@section('content')

<main class="content">
<div class="container-fluid p-0">
<div class="row mb-2 mb-xl-3">
	<div class="col-auto d-none d-sm-block">
		<h3><strong>{{auth()->user()->name ?? ""}}</strong> Subscription Plans</h3>
	</div>
	<div class="col-auto ms-auto text-end mt-n1">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb bg-transparent p-0 mt-1 mb-0">
				<li class="breadcrumb-item"><a href="{{route('business.partner.home')}}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Subscription Plans</li>
			</ol>
		</nav>
	</div>						
</div>
<div class="row">
	<div class="col-xl-12 col-xxl-12 d-flex">
		<div class="card w-100">
			@if($traildata >= 0)
			<h3>Stone: Free => {{$traildata}} Days Left</h3>
			@else
			<h3>Stone: Free => Sorry! Trail has expired.</h3>
			@endif

		@if($userSubscription)
			@if($plandetail != null)
			<h3>You have subscribed the Plan:</h3>
				<p>Plan Name: {{$plandetail->name}}</p>
				<p>Plan Price: {{$plandetail->amount}}</p>
				<p>Plan Description: {{$plandetail->description}}</p>
			@endif 
		@endif

			<div class="card-body">
				{{-- <h2> Manage Information </h2> --}}
				<div class="panel-body subscriptionBody">
				@php
					$counter = 1;
				@endphp
				@foreach($plans as $plan)
				<div class="planBoxWrapper @if($counter % 2 == 0) {{'greyBck'}} @else {{'default'}} @endif">
				<div class="planShapeDiv"> 
					<div class="planImageText">
						@if($counter % 2 == 0)
							<img src="{{asset('public/frontend/assets/image/planShapeGrey.png')}}">
						@else
							<img src="{{asset('public/frontend/assets/image/planShape.png')}}">
						@endif
						<div class="planTextAmount">
							<div class="planValue"> {{$plan->name}}</div>
							<div class="planAmount"> ${{$plan->amount}} </div>
						</div>
					</div>
					<div class="planBenifits">
						<div class="planDisp">
							{{$plan->description}}
						</div>
						{{-- <a href="{{ route('proceedForStripe', ['id' => encrypt($plan->id)]) }}" class="planBuyButton">Buy</a> --}}
						<a href="{{ route('planType', ['id' =>$plan->id]) }}" class="planBuyButton">Buy</a>
					</div>
				</div>
			</div>
			@php 
   				$counter++;
  			@endphp
			@endforeach				
				</div>									
			</div>
		</div>
	</div>
</div>				
	</div>
</main>

@endsection

@section('afterjs')

@endsection