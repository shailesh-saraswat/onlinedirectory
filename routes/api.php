<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'API\V1', 'prefix' => 'v1', 'as' => 'v1.'], function () {

    /* ===== Business Partner Login Section Start ==== */
    
        Route::post('reset-password', 'BusinessPartnerController@ResetPassword');
        Route::post('register', 'BusinessPartnerController@businessPartnerRegister');
        Route::post('login', 'BusinessPartnerController@Login');
        Route::post('forgotpassword', 'BusinessPartnerController@businessPartnerForgotpassword');
        Route::post('verifyotp', 'BusinessPartnerController@verifyOtp');
        Route::post('resend-otp', 'BusinessPartnerController@resendOtp');
        Route::get('formcategories', 'BusinessPartnerController@FormCategories');
        Route::post('list-categories', 'BusinessPartnerController@listCategories');
        Route::post('professionaldetail', 'BusinessPartnerController@ProfessionalDetail');
        Route::post('providerReviews', 'BusinessPartnerController@ProfessionalAllReview');
        Route::post('search-professional', 'BusinessPartnerController@searchProfessional');
        Route::post('searchProviderByAlpha', 'BusinessPartnerController@searchByAlphabet');
        Route::post('professionalsort', 'BusinessPartnerController@professionalSort');
    

    /* ===== Business Partner Login Section End ==== */

        
    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::post('change-password', 'BusinessPartnerController@changePassword');
        Route::post('changepassword', 'BusinessPartnerController@change_Password');
        Route::get('getprofile', 'BusinessPartnerController@getProfile');
        Route::post('updateprofile', 'BusinessPartnerController@updateProfile');
        Route::post('addreview', 'ReviewController@addReview');

        Route::get('getalltemplate', 'TemplateController@getAllTemplate');
        Route::get('faqs', 'FaqController@faqs');
        Route::post('manageinfo', 'TemplateController@ManageInformation'); 
        Route::get('checkmanageinfo', 'TemplateController@checkManageInfo');
        Route::get('viewtemplate', 'TemplateController@ViewTemplate');
        Route::post('templateUpdate', 'TemplateController@templateUpdate');
        Route::post('contact-us', 'ContactUsController@contact');
        Route::post('addDomain', 'DomainController@AddUpdateDomain');
        Route::get('plans', 'SubscriptionController@getPlans');
        Route::post('subscription', 'SubscriptionController@makeSubscription');
        Route::get('subscriptionStatus', 'SubscriptionController@SubscriptionLoginStatus');
        Route::get('currentPlan', 'SubscriptionController@CurrentPlan');
        Route::get('checkfreeTrial', 'SubscriptionController@checkfreeTrial');
        
        
        /* == Auth Token validation == */
        Route::get('validate-token', function () {
            return ['code' => 200, 'error' => false, 'message' => 'Authenticated'];
        }); 

    });

     Route::get('termsconditions/{pageid}', 'PageController@getTermPage');
     Route::get('privacy/{pageid}', 'PageController@getPrivacyPage');
     Route::get('aboutus', 'PageController@getAboutPage');
     Route::get('providertermsconditions/{pageid}', 'PageController@getTermPage');
     Route::get('providerprivacy/{pageid}', 'PageController@getPrivacyPage');
   
});