<?php

namespace App\Http\Controllers\Frontend;

use App\Models\User;
use App\Models\Role;
use App\Models\Category;
use App\Models\SiteSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Hash, Session,DB;
use Illuminate\Support\Str;

class FrontendLoginController extends Controller
{

    public function __construct() {
        $this->getSiteInfo(); 
    }

    public static function getSiteInfo()
    {
        $siteInfo=SiteSetting::first();  
        Session::put('sitedata',$siteInfo);       
    }

    public function loginPage(){
        $categories     = Category::where('parent_id','=',0)->where('status',1)->orderBy("id", "asc")->get();
        return view('frontend.auth.login',compact('categories'));
    }

    public function registerPage(){
        $categories     = Category::where('parent_id','=',0)->where('status',1)->orderBy("id", "asc")->get();
        return view('frontend.auth.register',compact('categories'));
    }


    public function registerUser(Request $request)
    {   
        //try{
        
            $rules = [
                'first_name' => ['required','regex:/^[a-zA-Z ]+$/u','max:255'],
                'last_name'  => ['required','regex:/^[a-zA-Z ]+$/u','max:255'],
                'email' => [
                    'required',
                    'string',
                    'email',
                    'max:255',
                    'unique:users',
                    'regex:/^\w+[-\.\w]*@(?!()\.com$)\w+[-\.\w]*?\.\w{2,4}$/'
                ],
                'password' => ['required', 'between:8,16','same:confirm_password'],
                'confirm_password' => ['required', 'between:8,16'],
                'mobile' => ['required','between:6,15', \Illuminate\Validation\Rule::unique('users')]
            ];

            $messages = [
                'email.regex' => 'We do not accept mails with this domain.',
                'mobile.between' => 'Mobile number must be between 6 to 15 digits.'
            ];

        $validation = Validator::make($request->all(), $rules, $messages);

        if ($validation->fails()) {
			return  redirect()->back()->withErrors($validation)->withInput(Input::all());
		} else {

                $user 			      = new User();
                $user->roles_id 	  =  3;
                $user->first_name 	  = $request->first_name;
                $user->last_name 	  = $request->last_name;
                $user->email 	      = $request->email;
                $user->password 	  = Hash::make($request->password);
                $user->mobile 	      = $request->mobile;
                $user->slug           = Str::slug($request->first_name , "-");
                $user->is_trial 	  = 1;
                $user->is_subscribed  = 0;
                $user->status 	      = 1;
                $user->free_trial_end = date('Y-m-d',strtotime('+15 days',strtotime('now')));

			if($user->save()) {
                return redirect()->route('user.login')->withMessage("User created successfully.");
			} 
        }

    }
    
    public function login(Request $request)
    {   
        
        $rules = array(
            'email'    => 'required|email|max:255|exists:users',
            'password' => 'required',
        );
        
        
        //$remember_me  = $request->has('remember') ? true : false;
        $validator    = Validator::make(Input::all(), $rules);
        
        if($validator->fails()) {
            return redirect()->route('user.login')
            ->withErrors("Email and Password is required to login.")
            ->withInput(Input::except('password'));
        }
        
        /* $userdata = array(
            'email'    => Input::get('email'),
            'password' => Input::get('password'),
        ); */

        /* if(Auth::attempt($userdata)) {
        $model  = User::with('roles')->where('email',$request->email)->first(); */
        if (Auth::attempt(['email' => request('email'), 'password' => request('password'), 'roles_id' => '3'])) {
        $model  = User::where('roles_id','3')->where('email',$request->email)->first();
        if($model != null){
    
            if($model->roles->role_text == "user"){
                return redirect()->route('home');
            } else if($model->roles->role_text == "admin" || $model->roles->role_text == "professional"){
                Auth::logout();
                Session::flush();
                $request->session()->flush();

                return redirect()->route('user.login')
                    ->withErrors("Forbidden! Invalid credentials.")
                    ->withInput(Input::except('password'));
            }  
            }   
        } else {
            return redirect()->route('user.login')
                ->withErrors("The credentials do not match with our records.")
                ->withInput(Input::except('password'));
        }  
    
	}

    public function profile(){

        $categories     = Category::where('parent_id','=',0)->where('status',1)->orderBy("id", "asc")->get();
        $user = auth()->user();
        return view('frontend.user.editprofile',compact('categories','user'));
    }

    public function updateprofile(Request $request){
    
        try {
        
        /* $validation = Validator::make($request->all(), [
                
                'first_name'    => "required|regex:/^[a-zA-Z]+$/u|max:255",
                'last_name'     => "required|regex:/^[a-zA-Z]+$/u|max:255",
                'email'         => "required|email|unique:users,email,".$request->input('userId'),
                'mobile'        =>  ['required','between:6,15',\Illuminate\Validation\Rule::unique('users')->ignore($request->input('userId'))]

		]); */
        
        $rules = [
                'first_name' => ['required','regex:/^[a-zA-Z ]+$/u','max:255'],
                'last_name'  => ['required','regex:/^[a-zA-Z ]+$/u','max:255'],
                /* 'email' => [
                    'required',
                    'string',
                    'email',
                    'max:255',
                    'unique:users',
                    'regex:/^\w+[-\.\w]*@(?!(?:outlook|myemail|yahoo|yopmail|red|mailinator|user)\.com$)\w+[-\.\w]*?\.\w{2,4}$/',\Illuminate\Validation\Rule::unique('users')->ignore($request->input('userId'))
                ], */
                'email' =>  ['required','max:255','regex:/^\w+[-\.\w]*@(?!(?:)\.com$)\w+[-\.\w]*?\.\w{2,4}$/', \Illuminate\Validation\Rule::unique('users')->ignore($request->input('userId'))],
                'mobile' => ['required','between:6,15', \Illuminate\Validation\Rule::unique('users')->ignore($request->input('userId'))]
            ];

            $messages = [
                'email.regex' => 'We do not accept mails with this domain.',
                'mobile.between' => 'Mobile number must be between 6 to 15 digits.'
            ];

        $validation = Validator::make($request->all(), $rules, $messages);

        if ($validation->fails()) {
			return  redirect()->back()->withErrors($validation)->withInput();
        } else{

            $userID                     = $request->input('userId');
            $userUpdate                 = User::query()->where('id', $userID)->first();
            $userUpdate->first_name 	= $request->first_name;
            $userUpdate->last_name 		= $request->last_name;
            $userUpdate->email 	        = $request->email;
            $userUpdate->mobile 	    = $request->mobile;
            $userUpdate->mobile 	    = $request->mobile;
            $userUpdate->slug           = Str::slug($request->first_name , "-");
            $userUpdate->update();

			if($userUpdate) {
                Session::flash('log_message', "Profile updated successfully");
                return Redirect::back();
			} else {
                Session::flash('message', "Profile not updated successfully.");
                return Redirect::back();
            } 
        }
        }catch(\Exception $e){

                return redirect()->back()
                    ->withErrors($e->getMessage());
        } 
    }

    public function changepassword(){
        $categories     = Category::where('parent_id','=',0)->where('status',1)->orderBy("id", "asc")->get();
        return view('frontend.user.changepassword',compact('categories'));
    }

    public function updatepassword(Request $request){
        try {
        
        $validation = Validator::make($request->all(), [

            'old_password'     => 'required|between:8,16',   
            'password'         => 'required|between:8,16',
            'confirm_password' => 'required|between:8,16|same:password'

		]);
        
        if ($validation->fails()) {
			return  redirect()->back()->withErrors($validation)->withInput();
        } else {
            
            $user = auth()->user();
            
            if (Hash::check($request->old_password, $user->password)) {

                $user->fill([
                    'password' => Hash::make($request->password),
                ]);
                $user->text_password=$request->password;
                if($user->update()) {
                    Auth::logout();
                    Session::flush();
                    $request->session()->flush();
                    Session::flash('message', "Password updated successfully.");
                    return redirect('/login');
                } else {
                    Session::flash('message', "Password not updated successfully.");
                    return Redirect::back();
                } 
            } else {
                Session::flash('message', "Current Password does not exists.");
                return Redirect::back();
            } 
           
        }
        }catch(\Exception $e){

                return redirect()->back()
                    ->withErrors($e->getMessage());
        } 
    }

    public function logout(Request $request) {
        Auth::logout();
        Session::flush();
        $request->session()->flush();

        return redirect()->route('user.login')->withMessage('Logged out successfully.');
    }

    
}
