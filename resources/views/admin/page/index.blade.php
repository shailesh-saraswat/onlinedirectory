@extends('admin.index')

@section('after-style')

    <link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endsection

@section('content')

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    {{--<h1>DataTables</h1>--}}
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">Manage CMS</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Pages</h3>
                                <a href="{{url('admin/add-page-form')}}" style="float: right;"><i class="fas fa-plus-square" style="font-size:25px;"></i></a>
                            </div>
                            @if(session('message'))
                                <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
                            @endif
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="page-list" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Title</th>
                                           <th>Description</th>
                                            <th>Status</th>
                                            <th>Action</th>                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($page) && $page != null)
                                            @foreach($page as $data)
                                                <tr>
                                                    <td>{{$data->id}}</td>
                                                    <td>{{$data->title}}</td>
                                                   <td>{!! str_limit(strip_tags($data->description), $limit = 50, $end = '...') !!}</td>
                                                    <td class="status">{{$data->status == 1 ? 'Active' : 'In-active'}}</td>
                                                    <td style="text-align: center;">
                                                        <a href="{{url('admin/page-edit/'.$data->id)}}"><i class="fas fa-edit"></i>&nbsp;&nbsp;</a>
                                                        <!-- <a href="{{url('admin/page-delete/'.$data->id)}}" onclick="return confirm('Are you sure?')" ><i class="fas fa-trash"></i></a> -->
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div> 
                        </div>
                    </div>  
                </div>
            </div>
        </section>
    </div>
@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<script>
    $(function () {
        $("#page-list").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });
</script>
<script>
$(document).ready(function(){

    $('td.status').each(function() {
        if ($(this).text() == 'Active') {
            $(this).css('color', 'green');
        }
        else{
            $(this).css('color', 'red');
        } 
    });
});
</script>
@endsection