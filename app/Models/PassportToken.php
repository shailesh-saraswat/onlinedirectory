<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PassportToken extends Model
{
    protected $table = "oauth_access_tokens";
}
