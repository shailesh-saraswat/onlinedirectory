<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;
use App\Models\{Category,Page,About,SiteSetting,User,Faq,Reviews,BusinessInformation,Visitor};
use Session;
use Illuminate\Support\Carbon;
use Exception, Hash, Validator,View, DB;

class HomeController extends Controller
{   
    
    public function __construct() {
        $this->getSiteInfo(); 
    }
    
    public static function getSiteInfo()
    {
        $siteInfo=SiteSetting::first(); 
        Session::put('sitedata',$siteInfo);       
    }

    public function home($category=""){
        $faqs           = Faq::orderBy('display_order', 'ASC')->get();
        $about          = About::where('id',1)->first();
        $categories     = Category::where('parent_id','=',0)->where('status',1)->orderBy('name','asc')->get();
        return view('frontend.home',compact('categories','category','faqs','about')); 
    }


    public function professionalSearch(Request $request) {
        
        $categories  = Category::where('parent_id',0)->where('status',1)->orderBy('name','asc')->get();
        //$ip          = '122.161.49.140';
        $ip          = $request->ip(); 
        $currentip   = \Location::get($ip);  
        $latitude    = $currentip->latitude;
        $longitude   = $currentip->longitude;
        $location    = $currentip->cityName.",".$currentip->regionName.",".$currentip->zipCode;

        if($request->has('location') && $request->location!='') {
            $location = $request->get('location');
        } else {
            $location = $location;
        }

        if($request->has('latitude') && $request->latitude!='') {
            $latitude = $request->latitude;
        } else {
            $latitude = $latitude;
        }

        if($request->has('longitude') && $request->longitude!='') {
            $longitude = $request->longitude;
        } else {
            $longitude = $longitude;
        }   
            $category               = $request->get('category');

            if($category){
                $professionals          = DB::table('users')->select('*');
                $haversine              = "(3959 * acos(cos(radians($latitude)) * cos(radians(latitude)) * cos(radians(longitude) - radians($longitude)) + sin(radians($latitude)) * sin(radians(latitude))))";
                $professionals          = $professionals->where('roles_id', '=', 2);
                $professionals          = $professionals->where('status','=',1);
                $professionals          = $professionals->where('category_id',$category);                    
                $professionals          = $professionals->selectRaw("{$haversine} AS distance");
                $professionals          = $professionals->whereRaw("{$haversine} < ?",10);
                $professionals          = $professionals->orderBy('distance', 'asc');
                $professionals          = $professionals->paginate(6);
            }else {
                $professionals          = DB::table('users')->select('*');
                $haversine              = "(3959 * acos(cos(radians($latitude)) * cos(radians(latitude)) * cos(radians(longitude) - radians($longitude)) + sin(radians($latitude)) * sin(radians(latitude))))";
                $professionals          = $professionals->where('roles_id', '=', 2);
                $professionals          = $professionals->where('status','=',1);
                $professionals          = $professionals->selectRaw("{$haversine} AS distance");
                $professionals          = $professionals->whereRaw("{$haversine} < ?",10);
                $professionals          = $professionals->orderBy('distance', 'asc');
                $professionals          = $professionals->paginate(6);
            }
            

            foreach($professionals as $mainKey => $getProfessional) {
            
                $binfo    = BusinessInformation::where('professional_id',$getProfessional->id)->first();
            
                if($binfo){
                    $professionals[$mainKey]->website_logo    = $binfo->website_logo;
                    $professionals[$mainKey]->average_charges = $binfo->average_charges;
                    $professionals[$mainKey]->opening_time    = $binfo->opening_time;
                    $professionals[$mainKey]->closing_time    = $binfo->closing_time;
                    $professionals[$mainKey]->details         = $binfo->details;
                }else{
                    $professionals[$mainKey]->website_logo    = "";
                    $professionals[$mainKey]->average_charges = "";
                    $professionals[$mainKey]->opening_time    = "";
                    $professionals[$mainKey]->closing_time    = "";
                    $professionals[$mainKey]->details         = "";
                }

                $reviews     = Reviews::where('user_type',$getProfessional->id)->get();
                $reviewsaVG  = Reviews::where('user_type',$getProfessional->id)->avg('ratings');
                $professionals[$mainKey]->reviews = $reviews;
                $professionals[$mainKey]->average_rating = isset($reviewsaVG) ? $reviewsaVG : "0.0";

            } 
            return View::make('frontend.dashboard.lists',compact(['professionals','categories', 'location','latitude', 'longitude','category'])); 
        
    }


    public function CategoryByProfessional(Request $request, $category) {
            
            $categories  = Category::where('parent_id','=',0)->where('status',1)->orderBy('name','asc')->get();
            
            //$ip          = '122.161.49.140'; 
            $ip          =  $request->ip();
            $currentip   = \Location::get($ip);  
            $latitude    = $currentip->latitude;
            $longitude   = $currentip->longitude;
            $location    = $currentip->cityName.",".$currentip->regionName.",".$currentip->zipCode;

            if($request->has('location') && $request->location!='') {
                $location = $request->location;
            } else {
                $location = $location;
            }

            if($request->has('latitude') && $request->latitude!='') {
                $latitude = $request->latitude;
            } else {
                $latitude = $latitude;
            }

            if($request->has('longitude') && $request->longitude!='') {
                $longitude = $request->longitude;
            } else {
                $longitude = $longitude;
            }

        $professionals          = DB::table('users')->select('*');
        $haversine              = "(3959 * acos(cos(radians($latitude)) * cos(radians(latitude)) * cos(radians(longitude) - radians($longitude)) + sin(radians($latitude)) * sin(radians(latitude))))";
        $professionals          = $professionals->where('roles_id', '=', 2);
        $professionals          = $professionals->where('status','=',1);
        $professionals          = $professionals->where('category_id',$category);                     
        $professionals          = $professionals->selectRaw("{$haversine} AS distance");
        $professionals          = $professionals->whereRaw("{$haversine} < ?",10);
        $professionals          = $professionals->orderBy('distance', 'asc');
        $professionals          = $professionals->paginate(6);

        foreach($professionals as $mainKey => $getProfessional) {
            
                $binfo    = BusinessInformation::where('professional_id',$getProfessional->id)->first();
            
                if($binfo){
                    $professionals[$mainKey]->website_logo    = $binfo->website_logo;
                    $professionals[$mainKey]->average_charges = $binfo->average_charges;
                    $professionals[$mainKey]->opening_time    = $binfo->opening_time;
                    $professionals[$mainKey]->closing_time    = $binfo->closing_time;
                    $professionals[$mainKey]->details         = $binfo->details;
                }else{
                    $professionals[$mainKey]->website_logo    = "";
                    $professionals[$mainKey]->average_charges = "";
                    $professionals[$mainKey]->opening_time    = "";
                    $professionals[$mainKey]->closing_time    = "";
                    $professionals[$mainKey]->details         = "";
                }

                $reviews     = Reviews::where('user_type',$getProfessional->id)->get();
                $reviewsaVG  = Reviews::where('user_type',$getProfessional->id)->avg('ratings');
                $professionals[$mainKey]->reviews = $reviews;
                $professionals[$mainKey]->average_rating = isset($reviewsaVG) ? $reviewsaVG : '0.0'; $reviewsaVG;
            
        } 
        return View::make('frontend.dashboard.lists',compact(['professionals','categories', 'location','latitude', 'longitude','category'])); 

    }


    public function professionaldetail($id){
        
        $categories   = Category::where('parent_id','=',0)->where('status',1)->orderBy('name','asc')->get();
        
        $getProfessionals = User::with(['userdetail' => function($q){
                $q->select('*');
                },	'reviews.user' => function($q){
	        		$q->select('*');
	        	}
	    ])->where('id',$id)->first(); 
        
        //return (isset($getProfessionals->userdetail) && $getProfessionals->userdetail->details == NULL);

        if(isset($getProfessionals)){
            $ip = \Request::getClientIp();
            $visited_date = Date("Y-m-d: H:i:s");
            $input = array('ip' => $ip,'userid' => $getProfessionals->id);
            Visitor::create($input);
            return view('frontend.dashboard.details')
                ->withCategories($categories)
            ->withGetProfessionals($getProfessionals);
        } else {
            abort(404);
        }
        
        
    }

    public function load_more_data(Request $request) {

        if($request->ajax()) {
    
            if($request->id > 0) {
                $data = DB::table('categories')
                ->where('id', '<', $request->id)
                ->where('parent_id', '!=', 0)
                ->where('status', 1)
                ->orderBy('id', 'DESC')
                ->limit(6)
                ->get();
            } else {
            
            $data = DB::table('categories')
                ->where('parent_id', '!=', 0)
                ->where('status', 1)
                ->orderBy('id', 'DESC')
                ->limit(6)
                ->get();
        }
            $output  = '';
            $last_id = '';
        
        foreach($data as $key => $row) {
            $catIcon = isset($row->icon) && $row->icon != NULL ? $row->icon : 'public/uploads/categoryIcon/default.jpg';
            $output .= '
            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
            <div class="item-all-card text-dark text-center">
                <a href="'.url("category/professional").'/'.$row->id.'"></a>';
            $output .='<div class="iteam-all-icon">
            <img src='.$catIcon.' class="imag-service" alt=""> </div>
                <div class="item-all-text mt-3">
                    <h5 class="mb-0 text-body">'.$row->name.'</h5>
                </div>
            </div>
            </div>';
            $last_id = $row->id;
        
        }

        if(count($data) == 6){
            $button = '
                <div class="mt-4"><a href="javascript:void(0)" class="btn btn-primary" data-id="'.$last_id.'" id="load_more_button" name="load_more_button" btn-primary">View More</a> </div>';
        }else{
            $button = '
                <div class="mt-4"> <a href="javascript:void(0)" class="btn btn-primary">No Data Found</a> </div>';
        }
            $data = ['html' => $output, 'button' => $button];
            return response()->json(['success' => true, 'data' => $data]);
            echo $output;
        }
    }

    public function servicesLists(Request $request, $category){

        $categories  = Category::where('parent_id','=',0)->where('status',1)->orderBy('name','asc')->get();
        
        //$ip          = '122.161.49.140'; 
        $ip        =  $request->ip();
        $currentip   = \Location::get($ip);  
        $latitude    = $currentip->latitude;
        $longitude   = $currentip->longitude;
        $location    = $currentip->cityName.",".$currentip->regionName.",".$currentip->zipCode;

        if($request->has('location') && $request->location!='') {
            $location = $request->get('location');
        } else {
            $location = $location;
        }

        if($request->has('latitude') && $request->latitude!='') {
            $latitude = $request->latitude;
        } else {
            $latitude = $latitude;
        }

        if($request->has('longitude') && $request->longitude!='') {
            $longitude = $request->longitude;
        } else {
            $longitude = $longitude;
        }

         $professionals          = DB::table('users')->select('*');
        $haversine              = "(3959 * acos(cos(radians($latitude)) * cos(radians(latitude)) * cos(radians(longitude) - radians($longitude)) + sin(radians($latitude)) * sin(radians(latitude))))";
        $professionals          = $professionals->where('roles_id', '=', 2);
        $professionals          = $professionals->where('status','=',1);
        $professionals          = $professionals->where('category_id',$category);                     
        $professionals          = $professionals->selectRaw("{$haversine} AS distance");
        $professionals          = $professionals->whereRaw("{$haversine} < ?",10);
        $professionals          = $professionals->orderBy('distance', 'asc');
        $professionals          = $professionals->paginate(6);

        foreach($professionals as $mainKey => $getProfessional) {
            
                $binfo    = BusinessInformation::where('professional_id',$getProfessional->id)->first();
            
                if($binfo){
                    $professionals[$mainKey]->website_logo    = $binfo->website_logo;
                    $professionals[$mainKey]->average_charges = $binfo->average_charges;
                    $professionals[$mainKey]->opening_time    = $binfo->opening_time;
                    $professionals[$mainKey]->closing_time    = $binfo->closing_time;
                    $professionals[$mainKey]->details         = $binfo->details;
                }else{
                    $professionals[$mainKey]->website_logo    = "";
                    $professionals[$mainKey]->average_charges = "";
                    $professionals[$mainKey]->opening_time    = "";
                    $professionals[$mainKey]->closing_time    = "";
                    $professionals[$mainKey]->details         = "";
                }

                $reviews     = Reviews::where('user_type',$getProfessional->id)->get();
                $reviewsaVG  = Reviews::where('user_type',$getProfessional->id)->avg('ratings');
                $professionals[$mainKey]->reviews = $reviews;
                $professionals[$mainKey]->average_rating = isset($reviewsaVG) ? $reviewsaVG : '0.0'; $reviewsaVG;

        } 
         return View::make('frontend.dashboard.lists',compact(['professionals','categories', 'location','latitude', 'longitude','category'])); 
    }

    public function termAndConditions(){
        $categories     = Category::where('parent_id','=',0)->where('status',1)->orderBy('name','asc')->get();
        $terms          = Page::where('id','=',1)->where('status',1)->first();
       
        return View::make('frontend.termandconditions',compact('categories','terms'));
    }

    public function privacyPolicy(){
        $categories     = Category::where('parent_id','=',0)->where('status',1)->orderBy('name','asc')->get();
        $privacy        = Page::where('id','=',2)->where('status',1)->first();
       
        return View::make('frontend.privacypolicy',compact('categories','privacy'));
    }

    public function faqs(){
        $categories     = Category::where('parent_id','=',0)->where('status',1)->orderBy('name','asc')->get();
        $faqs           = Faq::get();
      
        return View::make('frontend.faqs',compact('categories','faqs'));
    }

   

    public function filterData(Request $request){

        $categories  = Category::where('parent_id','=',0)->where('status',1)->orderBy('name','asc')->get();

        //$ip        = '122.161.49.140'; 
        $ip          = $request->ip();
        $currentip   = \Location::get($ip);  
        $latitude    = $currentip->latitude;
        $longitude   = $currentip->longitude;
        $location    = $currentip->cityName.",".$currentip->regionName.",".$currentip->zipCode;

        if($request->has('location') && $request->location!='') {
            $location = $request->location;
        } else {
            $location = $location;
        }

        if($request->has('latitude') && $request->latitude!='') {
            $latitude = $request->latitude;
        } else {
            $latitude = $latitude;
        }

        if($request->has('longitude') && $request->longitude!='') {
            $longitude = $request->longitude;
        } else {
            $longitude = $longitude;
        }
        $category = $request->category;

        if($request->sort=="relevance"){
        
        $professionals = User::where('roles_id','=',2)
            ->where('status','=',1)->with('reviews','userdetail')
            ->where('category_id', 'LIKE', "%$category%") 
            ->paginate(6);
        
            foreach($professionals as $mainKey => $getProfessional) {
            
                $binfo    = BusinessInformation::where('professional_id',$getProfessional->id)->first();
            
                if($binfo){
                    $professionals[$mainKey]->website_logo    = $binfo->website_logo;
                    $professionals[$mainKey]->average_charges = $binfo->average_charges;
                    $professionals[$mainKey]->opening_time    = $binfo->opening_time;
                    $professionals[$mainKey]->closing_time    = $binfo->closing_time;
                    $professionals[$mainKey]->details         = $binfo->details;
                }else{
                    $professionals[$mainKey]->website_logo    = "";
                    $professionals[$mainKey]->average_charges = "";
                    $professionals[$mainKey]->opening_time    = "";
                    $professionals[$mainKey]->closing_time    = "";
                    $professionals[$mainKey]->details         = "";
                }

                $reviews     = Reviews::where('user_type',$getProfessional->id)->get();
                $reviewsaVG  = Reviews::where('user_type',$getProfessional->id)->avg('ratings');
                $professionals[$mainKey]->reviews = $reviews;
                $professionals[$mainKey]->average_rating = isset($reviewsaVG) ? $reviewsaVG : '0.0'; $reviewsaVG;

            } 

        } elseif($request->sort=="nearby"){
            
            $professionals = User::with('userdetail','reviews')->where([ 'category_id' => $category, 'roles_id' =>2,'status'=>1])->select("*",DB::raw("6371 * acos(cos(radians(" . $latitude . ")) * cos(radians(latitude)) * cos(radians(longitude) - radians(" . $longitude . ")) + sin(radians(".$latitude.")) * sin(radians(latitude))) AS distance"))
                ->whereRaw('6371 * acos(cos(radians(' . $latitude . ')) * cos(radians(latitude)) * cos(radians(longitude) - radians(' . $longitude . ')) + sin(radians('.$latitude.')) * sin(radians(latitude))) < 10')
                ->orderBy('distance', 'asc')
                ->paginate(6); 
    
            foreach($professionals as $mainKey => $getProfessional) {
            
                $binfo    = BusinessInformation::where('professional_id',$getProfessional->id)->first();
            
                if($binfo){
                    $professionals[$mainKey]->website_logo    = $binfo->website_logo;
                    $professionals[$mainKey]->average_charges = $binfo->average_charges;
                    $professionals[$mainKey]->opening_time    = $binfo->opening_time;
                    $professionals[$mainKey]->closing_time    = $binfo->closing_time;
                    $professionals[$mainKey]->details         = $binfo->details;
                }else{
                    $professionals[$mainKey]->website_logo    = "";
                    $professionals[$mainKey]->average_charges = "";
                    $professionals[$mainKey]->opening_time    = "";
                    $professionals[$mainKey]->closing_time    = "";
                    $professionals[$mainKey]->details         = "";
                }

                $reviews     = Reviews::where('user_type',$getProfessional->id)->get();
                $reviewsaVG  = Reviews::where('user_type',$getProfessional->id)->avg('ratings');
                $professionals[$mainKey]->reviews = $reviews;
                $professionals[$mainKey]->average_rating = isset($reviewsaVG) ? $reviewsaVG : '0.0'; $reviewsaVG;

            }

        } elseif($request->sort=="rating"){

            /* $professionals = User::where('roles_id',2)->where('status','1')->withCount(['reviews as average_rating' => function($query) {
                $query->select(DB::raw('coalesce(avg(ratings),0)')); */
            
            $professionals = DB::table('reviews')->leftjoin('users','users.id','=','reviews.user_type')->where([ 'users.category_id' => $category, 'users.roles_id' =>2,'users.status'=>1])->groupBy('reviews.user_type')->paginate(6);

            //return $professionals;
            foreach($professionals as $mainKey => $getProfessional) {

                $binfo    = BusinessInformation::where('professional_id',$getProfessional->id)->first();
            
                if($binfo){
                    $professionals[$mainKey]->website_logo    = $binfo->website_logo;
                    $professionals[$mainKey]->average_charges = $binfo->average_charges;
                    $professionals[$mainKey]->opening_time    = $binfo->opening_time;
                    $professionals[$mainKey]->closing_time    = $binfo->closing_time;
                    $professionals[$mainKey]->details         = $binfo->details;
                }else{
                    $professionals[$mainKey]->website_logo    = "";
                    $professionals[$mainKey]->average_charges = "";
                    $professionals[$mainKey]->opening_time    = "";
                    $professionals[$mainKey]->closing_time    = "";
                    $professionals[$mainKey]->details         = "";
                }
                
                $reviews     = Reviews::where('user_type',$getProfessional->id)->get();
                $reviewsaVG  = Reviews::where('user_type',$getProfessional->id)->avg('ratings');
                $professionals[$mainKey]->reviews = $reviews;
                $professionals[$mainKey]->average_rating = isset($reviewsaVG) ? $reviewsaVG : '0.0'; $reviewsaVG;

            }
        }

       return View::make('frontend.dashboard.filter',compact('professionals','categories','category','location','latitude', 'longitude'));

    }
}
