<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3" method="post" action="{{url('admin/search')}}">
        @csrf
        <div class="input-group input-group-sm">
            <input class="form-control form-control-navbar" type="search" placeholder="Search Users" name="search" id="search" aria-label="Search" value="{{old('search')}}" autocomplete="off">
            <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                    <i class="fas fa-search"></i>
                </button>
            </div>
            @error('search')
            <span class="invalid-feedback" role="alert" style="display:block;">
            <strong>{{ $message }}</strong>
            </span>
            @enderror 
        </div>
    </form>

   </nav>
