<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class UserSubscription extends Model
{
    public $table="user_subscriptions";

    protected $guarded = [];

    public static function updateCustomerMemberShip($subscription_id, $membership)
    {
        if($membership == 'resume') {
            DB::statement("UPDATE users Left JOIN user_subscriptions ON users.id = user_subscriptions.cust_id SET member_status = 'Active' WHERE user_subscriptions.subscription_id = '".$subscription_id."' AND user_subscriptions.status = '1' AND pay_status='Complete'");
        }elseif($membership == 'failed') {
            DB::statement("UPDATE users Left JOIN user_subscriptions ON users.id = user_subscriptions.cust_id SET member_status = 'Inactive' WHERE user_subscriptions.subscription_id = '".$subscription_id);
        }elseif($membership == 'delete') {
            DB::statement("UPDATE users Left JOIN user_subscriptions ON users.id = user_subscriptions.cust_id SET member_status = 'Inactive' WHERE user_subscriptions.subscription_id = '".$subscription_id."' AND user_subscriptions.status = '1' AND pay_status='Complete'");
            DB::statement("UPDATE user_subscriptions SET status = '0' where subscription_id ='".$subscription_id."'");
            
        }
        return true;
    }

    public static function updateCustomerStatus($customer_id)
    {
        if($customer_id!='')
        {
            DB::statement("UPDATE users SET member_status = 'Inactive' WHERE users.id = '".$customer_id."'");
        }
        return true;
    }

    public static function updateCustomerFailedStatus($customer_id,$sub_id)
    {
        if($customer_id!='')
        {
            DB::statement("UPDATE users SET member_status = 'Inactive' WHERE users.id = '".$customer_id."'");
            $subscription_data = UserSubscription::where(['status'=>'1','cust_id'=>$customer_id,'subscription_id'=>$sub_id])->groupBy('subscription_id')->count();
            if($subscription_data>0)
            {
                $subscription_row_data = UserSubscription::where(['status'=>'1','cust_id'=>$customer_id,'subscription_id'=>$sub_id])->groupBy('subscription_id')->get()->toArray();
                foreach ($subscription_row_data as $key => $value) {
                    DB::statement("UPDATE user_subscriptions SET pay_status = 'Pending' WHERE status='1' AND user_subscriptions.subscription_id = '".$value['subscription_id']."'");
                }
            }
        }
        return true;
    }

    public static function updateCustomerSuccessStatus($customer_id,$sub_id)
    {
        if($customer_id!='')
        {
            DB::statement("UPDATE users SET member_status = 'Active' WHERE users.id = '".$customer_id."'");
            $subscription_data = UserSubscription::where(['status'=>'1','cust_id'=>$customer_id,'subscription_id'=>$sub_id])->groupBy('subscription_id')->count();
            if($subscription_data>0)
            {
                $subscription_row_data = UserSubscription::where(['status'=>'1','cust_id'=>$customer_id,'subscription_id'=>$sub_id])->groupBy('subscription_id')->get()->toArray();
                foreach ($subscription_row_data as $key => $value) {
                    DB::statement("UPDATE user_subscriptions SET pay_status = 'Complete' WHERE status='1' AND user_subscriptions.subscription_id = '".$value['subscription_id']."'");
                }
            }
        }
        return true;
    }

    /* current Plan */
    public function plan(){
        return $this->belongsTo('App\Models\Plans','id','plan_id');     
    }

}
