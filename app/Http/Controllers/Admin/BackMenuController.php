<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AdminMenu;
use App\Models\User;
use Validator,Session;

class BackMenuController extends Controller
{
    public static function index()
	{
		$menus=AdminMenu::orderBy('name')->get();
		return view('admin.menu.index',compact('menus')); 
	}
	
	public function create($id='')
    {
    	$pmenu = AdminMenu::where('parent_menu_id','0')->get();       
    	return view('admin.menu.create',compact('pmenu'));
    }
	public function edit($id='')
    {
    	if($id!=''){
		  $pmenus = AdminMenu::Where('parent_menu_id','0')->get();
          $data   = AdminMenu::find($id);
          return view('admin.menu.edit',compact('data','pmenus'));
		}else{
			return redirect()->back();
		}
    }

    public function store(Request $request,$id='')
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:admin_menus|max:255',                     
        ]);
		if($validator->fails())
        {
        	return redirect()->back()->withInput($request->input())->withErrors($validator->errors());
        }
        else
        {
			$menu                 = new AdminMenu;
            $menu->name           = $request['name'];
            $menu->url            = $request['url'];           
            $menu->parent_menu_id = $request['parent_menu_id'];
            $menu->icon           = $request['icon'];
            $menu->priority       = $request['priority'];                     
            $menu->save();
            //BackMenuController::userMenus(Session::get('backenduser')->roles_id);
            Session::flash('message', 'Menu Successfully created !');
            return redirect('admin/menu');
         }
    }

    public function update(Request $request,$id='')
    {	
        $validator = Validator::make($request->all(),['name' => 'required|max:255',]);
		if ($validator->fails())
        {
        return redirect()->back()->withInput($request->input())->withErrors($validator->errors());
        }
        else
        {                                   
        $menu                   = AdminMenu::where('id',$id)->first();;
        $menu->name             = $request['name'];
        $menu->url              = $request['url'];       
        $menu->parent_menu_id   = $request['parent_menu_id'];
        $menu->icon             = $request['icon'];
        $menu->priority         = $request['priority'];            
        $menu->save();
		//BackMenuController::userMenus(Session::get('backenduser')->roles_id);
        Session::flash('message', 'Menu Updated Successfully!');
        return redirect('admin/menu');
        }  
    }

    public static function status()
	{
        $id      = $_GET['id'];
        $status  = $_GET['value'];
        $model   = AdminMenu::find($id);
        if($model) 
        {
            $model->status = $status;
            $model->save();
        }
    }

    public function distroy(Request $request,$id='')
    {
        $ids      = $request->mul_del;
        AdminMenu::whereIn('id',$ids)->delete();
        Session::flash('message', 'Menu Deleted Successfully !');
        return redirect('admin/menu');
    }

}
