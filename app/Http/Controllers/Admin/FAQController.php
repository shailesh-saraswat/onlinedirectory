<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Session;
use App\Models\Faq;

class FAQController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function index()
	{
		$faqs =  Faq::orderBy('id','DESC')->get();
		return view('admin.website.faq.index',compact('faqs'));
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id='')
    {
        return view('admin.website.faq.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id='')
    {
        $validator  = Validator::make($request->all(), [
                'question' => 'required', 
                'answer'   => 'required'					 
        ]);
		if ($validator->fails())
        {
        	return redirect()->back()->withInput($request->input())->withErrors($validator->errors());
        }  else {
           
            $faq                = new Faq;
            $faq->question      = $request['question'];
            $faq->answer        = $request['answer'];       
			$faq->status        = $request['status'];
			$faq->display_order = $request['display_order'];            	
            $faq->save();
			Session::flash('message', 'FAQ Successfully created !');
            return redirect('/admin/faq/view/'.$faq->id); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id='')
    {
    	if($id!=''){
		 $faq = Faq::where('id',$id)->first();
          return view('admin.website.faq.view',compact('faq'));
		}else{
			return redirect()->back();
		}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if($id!=''){		
          $faq = Faq::where('id',$id)->first();
          return view('admin.website.faq.edit',compact('faq'));
		}
		return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator  = Validator::make($request->all(), [
                'question' => 'required', 
                'answer' => 'required'					 
        ]);
		if ($validator->fails())
        {
        	return redirect()->back()->withInput($request->input())->withErrors($validator->errors());
        }        
        $updateFaq = Faq::where('id',$id)->first();
        if(empty($updateFaq)){
                Session::flash('message', 'Page Not found !');
                return redirect()->back(); 
        }
        $updateFaq->question      = $request['question'];
        $updateFaq->answer        = $request['answer'];       
        $updateFaq->status        = $request['status'];
        $updateFaq->display_order = $request['display_order'];            	
        $updateFaq->save();
            Session::flash('message', 'FAQ Successfully updated !');
        return redirect('admin/faq/view/'.$updateFaq->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Faq::where('id',$id)->delete();
        Session::flash('message', 'FAQ Deleted Successfully !');
        return redirect('admin/faq');
    }

    public static function status(Request $request)
	{
		$id     = $request->id;
        $status = $request->value;
        $model  = Faq::find($id);
        if($model) 
        {   
            Session::flash('message', 'Status Updated Successfully !');
            $model->status = $status;
            $model->save();
        }
        
	}

}
