@extends('frontend.partials.index')
 
@section('content')

<section class="bigHeader" style="background: url('{{asset('public/frontend/assets/image/banner2.jpg')}}') center center;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="tcent">
					Forgot
					<span class="smallBredcm"> <a href="{{route('home')}}"> Home </a> >> Forgot </span>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="login_section">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="loginBox">
					<div class="">
						<h5 class="login_title" id="exampleModalLabel">Forgot Password</h5>
					</div>

					 <form action="{{route('forgotPass')}}" method="post" id="forgotPassForm" name="forgotPassForm" class="mt-4">
                     @csrf
                      @if(Session::has('message'))
                        <p class="alert alert-danger" id="alert_box">{{ Session::get('message') }}</p>
                      @endif
                      @if(Session::has('log_message'))
                        <p class="alert alert-success" id="alert_box">{{ Session::get('log_message') }}</p>
                      @endif
                      @if ($errors->any())
                         <div class="alert alert-danger alert-dismissible page_alert">
                           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                             <ul>
                                 @foreach ($errors->all() as $error)
                                     <li>{{ $error }}</li>
                                 @endforeach
                             </ul>
                         </div>
                       @endif
                       <div class="alert alert-danger alert-dismissible custom_alert" style="display: none;">
                           <span class="alert_message"></span>
                        </div> 
				   <div class="">
					  <div class="form-group"> <label class="form-label">Enter Your Registered Id</label> 
                 <input type="text" name="email" id="email" class="form-control" placeholder="Enter email"> </div>
				   </div>
				   <div class="customLogRegisWrapper">
					  <div class="submit"> <a class="btn btn-primary btn-block reset_btn">Email Me</a> </div>
				   </div>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
   $('.reset_btn').on('click',function(){
    var email         = $('#email').val();
    var error_message = "";
    if(email=="")
    {
      error_message = "Please Enter Email.";
    }
    else if(!IsEmail(email))
    {
      error_message = "Email must be a valid email address.";
    }
    if(error_message!='')
    {
      $('.custom_alert').css('display','block');
      $('.alert_message').text(error_message);
      close_alert();
      return false;
    }
    else
    {
      $('#forgotPassForm').submit();
    }
  });
</script>
@endsection