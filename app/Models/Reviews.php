<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reviews extends Model
{
     protected $fillable = ['user_id','user_type','message','ratings'];

    /* public function user(){

        return $this->hasMany('App\Models\User', 'type', 'id');
    } */

    public function user(){

        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function reviews(){
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
