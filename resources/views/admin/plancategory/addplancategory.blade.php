@extends('admin.index')

@section('after-style')

@endsection    

@section('content')

<div class="content-wrapper">
        
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">Manage Plan Category</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-outline card-info">
            <div class="card-header">
              <h3 class="card-title">
                Add Plan Category
              </h3>
              
              <div class="card-tools">
        
              </div>

            </div>
            @include('admin.partials.messages')
      
            <form class="form-horizontal" action="{{url('admin/plancategory/store')}}" method="post">
              @csrf
              <div class="card-body text">
                <label for="Title" class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="name" placeholder="Plan Category Name" name="name" value="{{old('name')}}" />
                </div>
              </div>
              
              <div class="card-body text">
                <label for="Status" class="col-sm-2 col-form-label">Status</label>
                <div class="col-sm-8">
                  <select class="form-control select2" style="width: 100%;" name="status" >
                    <option value="0">Inactive</option>
                    <option value="1" selected>Active</option>
                  </select>    
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-info">Save</button>
                <a href="{{url('admin/plancategory/list')}}" class="btn btn-default float-right">Cancel</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>

</div>

@endsection

@section('after-scripts')

@endsection