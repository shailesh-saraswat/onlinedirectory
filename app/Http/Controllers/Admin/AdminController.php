<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Session;

class AdminController extends Controller
{

    public function profile(){

        $registeredUsers = User::query()->where(['status' => 1])->get();
        $countRegisteredUsers = isset($registeredUsers) && $registeredUsers != null ? count($registeredUsers) : 0;

        return view('admin/profile')
                ->withCountRegisteredUsers($countRegisteredUsers);

    }

    public function changePasswordView(){

        return view('admin/change_password');

    }

    public function changePassword(Request $request){

        try {
        
        $validation = Validator::make($request->all(), [
              
            'old_password'     => 'required',
            'new_password'     => 'required|min:8',
            'confirm_password' => 'required|same:new_password'
		]);

        if ($validation->fails()) {
			return  redirect()->back()->withErrors($validation)->withInput();
        } else{

       $hashedPassword = auth()->user()->password;
 
        if (\Hash::check($request->old_password , $hashedPassword )) {
            } else{
               session()->flash('message','Old password doesnt matched ');
               return redirect()->back();
             }
         
            $user = auth()->user();
            if ($user) {
                $user->fill([
                    'password' => Hash::make($request->new_password),
                ]);
                $user->text_password=$request->new_password;
                //$user->update();
                if($user->update()) {
                    Session::flash('message', "Password updated successfully.");
                    return redirect('/admin');
                } else {
                    Session::flash('message', "Password not updated successfully.");
                    return Redirect::back();
                }
            }

        } 
        }catch(\Exception $e){
            return redirect()->back()
                    ->withErrors($e->getMessage());
        } 
        
        /* $rules = array(
            'password'         => 'required|min:8',
            'confirm_password' => 'required|same:password'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {

            try {

                $user = auth()->user();

                if ($user) {

                    $user->fill([
                        'password' => Hash::make($request->password),

                    ]);
					$user->text_password=$request->password;
                    if($user->save()) {
                        Session::flash('message', "Password updated successfully.");
                        return redirect('/admin');
                    } else {
                        Session::flash('message', "Password not updated successfully.");
                        return Redirect::back();
                    }

                }
            }catch(\Exception $e){

                return redirect()->back()->withErrors($e->getMessage());

            }

            return $user;
        } */


    }

    public function getProfile(){

        $user = auth()->user();

        return view('admin.update_profile')->withUser($user);

    }

    public function updateProfile(Request $request){

        $rules = array(
            'name'          => 'required',
            'profile_image' => 'sometimes|mimes:jpg,jpeg,png,gif'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        else{

            $user = auth()->user();

            $user->name = $request->input('name');

            if ($request->hasFile('profile_image')) {

                try {

                    $profile_image = $request->file('profile_image');

                    $name = $profile_image->getClientOriginalName();

                    $destinationFolder = public_path('/profile_pictures/admin-' . $user->id . '/');

                    // Delete folder
                    if(file_exists($destinationFolder.$name)){

                        unlink($destinationFolder.$name);
                    }

                    $profile_image->move($destinationFolder, $name);
                    $user->profile_image ="public/profile_pictures/admin-".$user->id.'/'.$name;

                } catch (\Exception $e) {
                    
                    return redirect()->back()->withErrors($e->getMessage());
                }
            }

            $user->update();

            return Redirect::to('admin/profile');
        }


    }

}
