<!--Header Main--> 
<div class="header-main">
   <div class="sticky" style="margin-bottom: 0px;">
      <div class="horizontal-header clearfix ">
         <div class="container">
            <a id="horizontal-navtoggle" class="animated-arrow"><span></span></a> 
            <span class="smllogo">
            <span class="font-weight-bold mb-0 fs-30 text-dark mt-0 leading-tight"><img class="customLogo" src="{{asset('public/frontend/assets/image/logo.png')}}"></span> 
            </span>
         </div>
      </div>
   </div>
   <div id="sticky-wrapper" class="sticky-wrapper" style="height: 90px;">
      <div class="horizontal-main clearfix" style="width: 1349px;">
         <div class="horizontal-mainwrapper container clearfix">
            <div class="desktoplogo">
               <a href="{{url('/')}}" class="d-flex">
                  <h2 class="font-weight-bold mb-0 fs-30 text-dark mt-1"><img class="customLogo" src="{{asset('public/frontend/assets/image/logo.png')}}"></h2>
               </a>
            </div>
            <!--Nav--> 
            <nav class="horizontalMenu clearfix d-md-flex">
               <div class="horizontal-overlapbg"></div>
               <ul class="horizontalMenu-list">
              
					<li><a href="{{url('/')}}">Home </a></li>
                  <li>
                     <span class="horizontalMenu-click"><i class="horizontalMenu-arrow fa fa-angle-down"></i></span><a href="javascript:void(0)">Services <span class="fa fa-caret-down m-0"></span></a> 
                     <div class="horizontal-megamenu clearfix">
                        <div class="container">
                           <div class="megamenu-content">
                           <div class="row">
                                 
                                 @if(count($categories) > 0)
                                 @foreach($categories as $category)    
                                 <ul class="col link-list">
                                    <li class="title">{{ ucwords($category['name']) }}</li>
                                    @foreach($category->children as $childrenCategory)
                                    <li><a href="{{url('services/lists',$childrenCategory->id)}}">{{ ucwords($childrenCategory['name']) }}</a></li>
                                    @endforeach
                                 </ul>
                                 @endforeach
                                 @endif
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
               <li><a href="{{URL::to('/')}}#aboutus">About Us </a></li>
               @if(Route::has('user.login'))
               @auth
					<li><a href="{{route('contact')}}"> Contact Us <span class="horizontal-arrow"></span></a></li>
               <li class="userli">
                     <span class="horizontalMenu-click"><i class="horizontalMenu-arrow fa fa-angle-down"></i></span><a href="javascript:void(0)">{{ Auth::user()->first_name ." ". Auth::user()->last_name }}<span class="fa fa-caret-down m-0"></span></a> 
                     <div class="horizontal-megamenu clearfix userdropmenuli">
                        <div class="container">
                           <div class="megamenu-content">
                              <div class="row">
                                 <ul class="col link-list">
                                    <li><a href="{{route('user.profile')}}" ><i class="fa fa-edit"></i> Edit Profile</a></li>
                                    <li><a href="{{route('user.changepassword')}}" ><i class="fa fa-lock"></i> Change Password</a></li>
                                 <li><a href="{{ route('user.logout') }}" onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                                    <i class="fa fa-sign-out"></i> Logout
                                 </a></li>
                                 <form id="logout-form" action="{{ route('user.logout') }}" method="POST" style="display: none;">
                                 @csrf
                                 </form>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                 @else 
					<li>
					   <ul class="mb-0 pr-2 startaspartner">
						  <li class="d-none d-lg-flex"> <a class="mt-1" href="{{route('business.register')}}"> Sign Up A Business </a></li>
					   </ul>					
					</li>					
					<li class="d-lg-none mt-5 pb-2 mt-lg-0"> <span><a class="btn btn-secondary ad-post mt-1" href="{{url('login')}}"><i class="fal fa-sign-in"></i> Login</a></span> </li>
					<li class="d-lg-none mt-0 pb-5 mt-lg-0"> <a class="btn btn-info ad-post mt-1" href="{{route('login.register')}}"> Register </a></span> </li>
					<li> 
					   <ul class="mb-0 pr-2">
						  <li class="d-none d-lg-flex"> <a class="btn btn-secondary ad-post mt-1" href="{{url('login')}}"> Login </a></li>
					   </ul>				  
					</li>
					<li>
					   <ul class="mb-0 pl-2 create-resume-btn">
						  <li class="d-none d-lg-flex"> <a class="btn btn-info ad-post mt-1" href="{{route('login.register')}}"><i class="far fa-user"></i> Register </a> </li>
					   </ul>				  
					</li>
                @endauth
               @endif
               {{-- <li><a href="{{url('/')}}">Home </a></li>
					<li><a href="#aboutus">About Us </a></li>
                  <li>
                     <span class="horizontalMenu-click"><i class="horizontalMenu-arrow fa fa-angle-down"></i></span><a href="javascript:void(0)">Services <span class="fa fa-caret-down m-0"></span></a> 
                     <div class="horizontal-megamenu clearfix">
                        <div class="container">
                           <div class="megamenu-content">
                              <div class="row">
                                 @if(count($categories) > 0)
                                 @foreach($categories as $category)    
                                 <ul class="col link-list">
                                    <li class="title">{{ ucwords($category['name']) }}</li>
                                    @foreach($category->children as $childrenCategory)
                                    <li><a href="{{url('services/lists',$childrenCategory->id)}}">{{ ucwords($childrenCategory['name']) }}</a></li>
                                    @endforeach
                                 </ul>
                                 @endforeach
                                 @endif
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
					<li><a href="{{route('contact')}}"> Contact Us <span class="horizontal-arrow"></span></a></li>
					<li>
					   <ul class="mb-0 pr-2 startaspartner">
						  <li class="d-none d-lg-flex"> <a class="mt-1" href="{{route('business.register')}}"> Sign Up A Business </a></li>
					   </ul>					
					</li>					
					<li class="d-lg-none mt-5 pb-2 mt-lg-0"> <span><a class="btn btn-secondary ad-post mt-1" href="{{url('login')}}"><i class="fal fa-sign-in"></i> Login</a></span> </li>
					<li class="d-lg-none mt-0 pb-5 mt-lg-0"> <a class="btn btn-info ad-post mt-1" href="{{route('login.register')}}"> Register </a></span> </li>
					<li> 
					   <ul class="mb-0 pr-2">
						  <li class="d-none d-lg-flex"> <a class="btn btn-secondary ad-post mt-1" href="{{url('login')}}"> Login </a></li>
					   </ul>				  
					</li>
					<li>
					   <ul class="mb-0 pl-2 create-resume-btn">
						  <li class="d-none d-lg-flex"> <a class="btn btn-info ad-post mt-1" href="{{route('login.register')}}"><i class="far fa-user"></i> Register </a> </li>
					   </ul>				  
					</li> --}}
               
               </ul>				   
            </nav>
         </div>
      </div>
   </div>
</div>