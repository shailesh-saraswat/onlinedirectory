<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\{Category,ContactUs,Plans};
use App\Models\SiteSetting;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Mail\Contact;
use Carbon\Carbon;
use Hash, Session,DB,View;

class ContactController extends Controller
{   
    public function __construct() {
        $this->getSiteInfo(); 
    }

    public static function getSiteInfo()
    {
        return $siteInfo=SiteSetting::first();  
        Session::put('sitedata',$siteInfo);       
    }

    public function contact(){
        $categories     = Category::where('parent_id','=',0)->where('status',1)->orderBy("id", "asc")->get();
        return View::make('frontend.contact',compact('categories'));
    }
    
    public function storeContact(Request $request){
    
        //try{

             $rules = [
                'name' => ['required','regex:/^[a-zA-Z ]+$/u','max:255'],
                'email' => [
                    'required',
                    'string',
                    'email',
                    'max:255',
                    //'unique:users',
                    'regex:/^\w+[-\.\w]*@(?!(?:)\.com$)\w+[-\.\w]*?\.\w{2,4}$/'
                ],
                'subject'  => ['required','regex:/^[a-zA-Z ]+$/u','max:255'],
                'message'  => ['required']
            ];

            $messages = [
                'email.regex' => 'We do not accept mails with this domain.'
            ];

        $validation = Validator::make($request->all(), $rules, $messages);
    
        if ($validation->fails()) {
			return  redirect()->back()->withErrors($validation)->withInput(Input::all());
		} else {
        
            $contact 		        = new ContactUs();
            $contact->name 		    = $request->name;
            $contact->email 	    = $request->email;
            $contact->subject 	    = $request->subject;
            $contact->user_id 		= Auth::user()->id;
            $contact->message       = $request->message;
                
			if($contact->save()) {
                
                $to = 'arpitwebmobril@gmail.com';
                Session::flash('log_message', "Your inquiry has been sent. We will contact you soon.");
                Mail::to($to)->send(new Contact($contact->name, $request->email, $request->subject, $request->message));
                return redirect('/');

			} else {
                Session::flash('message', "Your inquiry has been not sent.?");
                return redirect('/');
            } 
        }
       /*  }catch(\Exception $e){
                return redirect()->back()
                    ->withErrors($e->getMessage());
        } */ 
    }

    public function ProviderContact(){
        return View::make('frontend.professional.bussinessPartnerContact');
    }

    public function providerStoreContact(Request $request){
            
             $rules = [
                'name' => ['required','regex:/^[a-zA-Z ]+$/u','max:255'],
                'email' => [
                    'required',
                    'string',
                    'email',
                    'max:255',
                    //'unique:users',
                    'regex:/^\w+[-\.\w]*@(?!(?:)\.com$)\w+[-\.\w]*?\.\w{2,4}$/'
                ],
                'subject'  => ['required','regex:/^[a-zA-Z ]+$/u','max:255'],
                'message'  => ['required']
            ];

            $messages = [
                'email.regex' => 'We do not accept mails with this domain.'
            ];

        $validation = Validator::make($request->all(), $rules, $messages);
    
        if ($validation->fails()) {
			return  redirect()->back()->withErrors($validation)->withInput(Input::all());
		} else {
        
            $contact 		        = new ContactUs();
            $contact->name 		    = $request->name;
            $contact->email 	    = $request->email;
            $contact->subject 	    = $request->subject;
            $contact->user_id 		= Auth::user()->id;
            $contact->message       = $request->message;
                
			if($contact->save()) {
                
                $to = 'arpitwebmobril@gmail.com';
                Session::flash('message', "Your inquiry has been sent. We will contact you soon.");
                Mail::to($to)->send(new Contact($contact->name, $request->email, $request->subject, $request->message));
                return redirect()->back();

			} else {
                Session::flash('message', "Your inquiry has been not sent.?");
                return redirect()->back();
            } 
        }
       
    }
    
    public function providerSearch(Request $request)
	{
		$validation = Validator::make($request->all(), [
			'plansearch'    => 'required'
		]);

		if ($validation->fails()) {
			return  redirect()->back()->withErrors($validation)->withInput();
		} else{
			$searchRequest = $request->input('plansearch');
			$plans = Plans::query()
				->where('status', '=', 1)
				->where('name',  'LIKE', "%$searchRequest%")
				->orWhere('plan_id',  'LIKE', "%$searchRequest%")
				->orWhere('description',  'LIKE', "%$searchRequest%")
				->get();
            return view('frontend.professional.businessPartnerSearchPlan',compact('plans'));
		}
	}
}
