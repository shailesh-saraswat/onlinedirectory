<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForgotOtp extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $otp;
    public $user_name;
    public function __construct($user_name,$otp)
    {
        $this->user_name = $user_name;
        $this->otp = $otp;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email = $this->view('emails.forgot.otp_password')
            ->subject('Online Directory Reset Password')
            ->with([
            'otp' => $this->otp,
            'username' => $this->user_name,
        ]);
        return $email;
    }
}
