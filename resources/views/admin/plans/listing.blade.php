@extends('admin.index')

@section('after-style')

    <link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endsection

@section('content')

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                  
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">Plans</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Plans</h3>
                                <a href="{{route('addPlan')}}" style="float: right;"><i class="fas fa-plus-square" style="font-size:25px;"></i></a>
                            </div>
                           @if(Session::has('plan_message'))
                          <div class="alert alert-info alert-dismissible page_alert">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('plan_message') }}</strong>
                          </div>
                        @endif
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="page-list" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>Name</th>
                                            <th>Amount</th>
                                            <th>Interval</th>
                                            <th>Status</th>
                                            <th>Action</th>                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                      @php $i=1; @endphp
                                      @foreach($plans as $index => $plan_list)
                                        <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$plan_list->name}}</td>
                                        <td>{{$plan_list->amount}}</td>
                                        <td>
                                            @if($plan_list->interval=="month")  
                                            {{ "Monthly" }}
                                            @elseif($plan_list->interval=="year")
                                            {{ "Yearly" }}
                                            @endif
                                        </td>
                                        <td><b>{{($plan_list->status==1)?'Active':'Inactive'}}</b></td>
                                        <td style="text-align: center;">
                        
                                                <a href="{{route('updatePlan',$plan_list->id)}}" data-id="{{$plan_list->id}}" title="Edit Plan"><i class="fas fa-edit"></i>&nbsp;&nbsp;</a>
                                                <a href="{{route('deletePlan',$plan_list->id)}}" data-id="{{$plan_list->id}}" title="Delete Plan" onclick="return confirm('Are you sure you want to delete?')"><i class="fas fa-trash"></i></a>
                                          </td>
                                          
                                        </tr>
                                      @php $i++; @endphp
                                      @endforeach
                                    </tbody>
                                </table>
                            </div> 
                        </div>
                    </div>  
                </div>
            </div>
        </section>
    </div>
@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<script>
    $(function () {
        $("#page-list").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });
</script>
<script>
$(document).ready(function(){

    $('td.status').each(function() {
        if ($(this).text() == 'Active') {
            $(this).css('color', 'green');
        }
        else{
            $(this).css('color', 'red');
        } 
    });
});
</script>
@endsection