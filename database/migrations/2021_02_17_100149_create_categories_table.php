<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('parent_id')->nullable()->comment('Parent Category');
            $table->string('name')->nullable()->comment('Category Name');
            $table->string('icon')->nullable()->comment('Fa Fa Icon');
            $table->string('template')->nullable()->comment('Template');
            $table->string('slug')->nullable()->comment('Category Slug');
            $table->integer('user_id')->nullable()->comment('Created By');
            $table->boolean('status')->comment('1=>Active,0=>Inactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
