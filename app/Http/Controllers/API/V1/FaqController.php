<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\PassportToken;
use Illuminate\Validation\Rule;
use Twilio\Rest\Client;
use Twilio\Exceptions\TwilioException;
use App\Models\{User,Faq,Category,Page,Role};
use DB;
use Exception;


class FaqController extends Controller
{
    
    public $successStatus = 200;

/*
|--------------------------------------------------------------------------
| ========  API List Faqs ==========
|--------------------------------------------------------------------------
|
| Local Url:https://www.webmobril.org/dev/od/api/v1/faqs
| Live Url: 
| Key:token.
| Method:GET
| Comments: List faqs for user GET Method.
|
|--------------------------------------------------------------------------
*/ 

    public function faqs(Request $request){

            $faqs =  Faq::orderBy('id','DESC')->get();

            $faqdata  = [];
            foreach($faqs as $mkey=>$v){
                $faqdata[$mkey]['question'] = $v->question;
                $faqdata[$mkey]['answer'] = strip_tags($v->answer);
            }

            if(isset($faqs) && $faqs != null)
            {
                $result['code']    = 200;
                $result['error']   = false;
                $result['message'] = "Success";
                $result['result']  = $faqdata;
                return response()->json($result);

            } else {

                $result['code']    = 401;
                $result['error']   = true;
                $result['message'] = "Unauthorized";
                return response()->json($result);
            }
    }

    
}
