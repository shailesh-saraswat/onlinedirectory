<?php
namespace App\Http\Controllers\API\V1;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Twilio\Rest\Client;
use Twilio\Exceptions\TwilioException;
use App\Models\{Category,Reviews,BusinessInformation,User,PassportToken,UserSubscription,Domains,Visitor};
use App\Models\Role;
use DB;
use Exception;
use Illuminate\Support\Str;

class BusinessPartnerController extends Controller
{
    public $successStatus = 200;

    
    /*
    |--------------------------------------------------------------------------
    | ======== API Business Register ========
    |--------------------------------------------------------------------------
    |
    | Local Url: https://www.webmobril.org/dev/od/api/v1/register
    | Live Url: 
    | Key:type,name,email,password,device_type,device_token,confirm_password,mobile,zip_code,location;
    | Method:POST
    | Comments: Signup api for user with key values and Post method.
    |
    |--------------------------------------------------------------------------
    */  
    /*  ======= Twillio OTP SEND ======= */

     protected function sentOtp($mobile, $otp){

    $mobile = $mobile ? "+".$mobile : "";
    $otp    = $otp ? $otp : "";

    try{

        $account_sid   = 'ACfdeb0e4420d16bdefc6461ff6c781a9b';
        $auth_token    = '883847f233d5ba4a7535228c7d036169';
        $twilio_number = '+12029026863';
       
        $twilio        = new Client($account_sid, $auth_token);

         $twilio->messages->create($mobile, array(
            'from' => $twilio_number,
            'body' => "Dear Customer, your verification code is : ".$otp." Please do not share with anyone",
        ));
          
        return true;


    }catch(TwilioException $e){

        $jsonArray['error'] = true;
        $jsonArray['message'] = "Incorrect mobile number or country code please check.";
        return response()->json($jsonArray); 

        }catch(Exception $e){

            return response()->json($e->getMessage());
        } 

    
    }

    public function businessPartnerRegister(Request $request){
        
        try{

            /* $columns = [
				
                'role'               => ['required',Rule::in([2,3])],    
                'first_name'         => 'required',            
                'last_name'          => 'required',       
				'email'              => 'required|email|unique:users',
				'password'           => 'required|min:8|same:confirm_password',
                'confirm_password'   => 'required|min:8',
                //'mobile'             =>  ['required', \Illuminate\Validation\Rule::unique('users')],
                'mobile'             =>  ['required','between:6,15', \Illuminate\Validation\Rule::unique('users')],
                'business_name'      => 'required',
                'location'           => 'required',
                'latitude'           => 'required',
                'longitude'          => 'required',
                'zip_code'           => 'required'

			]; */

            if($request->role == 2){

				$columns=[

                    'role'               => ['required',Rule::in([2,3])],               
                    'first_name'         => 'required',            
                    'last_name'          => 'required',  
                    'email'              => 'required|email|unique:users',
                    'password'           => 'required|between:8,16|same:confirm_password',
                    'confirm_password'   => 'required|between:8,16',
                    'mobile'             => ['required','between:6,15', \Illuminate\Validation\Rule::unique('users')],
                    'business_name'      => 'required',
                    'location'           => 'required',
                    'latitude'           => 'required',
                    'longitude'          => 'required',
                    'zip_code'           => 'required|min:4'
				];
			}

            if($request->role == 3){
				
                $columns = [

                    'role'               => ['required',Rule::in([2,3])],
                    'first_name'         => 'required',            
                    'last_name'          => 'required',            
                    'email'              => 'required|email|unique:users',
                    'password'           => 'required|between:8,16|same:confirm_password',
                    'confirm_password'   => 'required|between:8,16',
                    'mobile'             =>  ['required','between:6,15', \Illuminate\Validation\Rule::unique('users')]
				];
			}

            $customMessages = [
                'role.in'            => 'Role Type should  be 2 for Professional, 3 for User',
                'mobile.between'     => 'Mobile number must be between 6 to 15 digits.',
                'password.between'   => 'Password must be between 8 and 16 characters.'
            ];

            $validation = Validator::make($request->all(),$columns,$customMessages);

            if($validation->fails()){
                $result['code']     = 400;
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);

            }
                $otp                  = $this->generate_OTP();
                $user                 = new User();
                $user->roles_id       = $request->role;
                $user->first_name 	  = $request->first_name;
                $user->last_name 	  = $request->last_name;
                $user->email 	      = $request->email;
                $user->password       = Hash::make($request->password);
                $user->mobile 	      = $request->mobile;
                $user->slug           = Str::slug($request->first_name , "-");
                $user->free_trial_end = date('Y-m-d',strtotime('+15 days',strtotime('now')));
                $user->location       = $request->location;
                $user->longitude      = $request->longitude;
                $user->latitude       = $request->latitude;
                $user->zip_code       = $request->zip_code;
                $user->is_trial       = 0;
                $user->is_subscribed  = 0;
                $user->otp            = $otp;
                $user->status 	      = 1;


            if($request->role == 2){

                $user->first_name 	  = $request->first_name;
                $user->last_name 	  = $request->last_name;
                $user->location       = $request->location;
                $user->longitude      = $request->longitude;
                $user->latitude       = $request->latitude;
                $user->business_name  = $request->business_name;
                $user->zip_code       = $request->zip_code;
                $user->slug           = Str::slug($request->first_name , "-");
                $user->free_trial_end = date('Y-m-d',strtotime('+15 days',strtotime('now')));
                $user->template_id    = 1;
                $user->category_id    = $request->category_id;
                $user->status 	      = 0;
            }
			    $user->otp=$otp ? $otp : "";	
                
                //send otp via email
                $to_name  = $request->name;
                $to_email = $request->email;
                $body     = 'Your OTP is : ' . $otp . ' Please do not share with anybody.';
                $data     = array('name' => "$to_name", 'body' => "$body");

                Mail::send('emails.mail', $data, function ($message) use ($to_name, $to_email) {
                    $message->to($to_email, $to_name)
                        ->subject('Registration Otp');
                    $message->from(config('MAIL_USERNAME'), env('APP_NAME'));
                });	

            //$sendOtpToMobile            = $this->sentOtp($request->mobile,$otp);
            // $deliverResponse     = json_encode($sendOtpToMobile, true);

            // if($deliverResponse == "true"){
                
            // if($user->save()){
                $user->save();
                    
                $result['code']     = 200;
                $result['error']    = false;
                $result['otp']      = $otp;
                $result['message']  = "User registered successfully & OTP sent on your given Mobile number";

                return response()->json($result);

            // } else {
            //     $result['code']    = 500;
            //     $result['error']   = true;
            //     $result['message'] = "Something went wrong!";
            //     return response()->json($result);
            // }

            // } else {
            //     $result['code']    = 400;
            //     $result['error']   = true;
            //     $result['message'] = "You have entered the invalid Mobile Number. Please try again!";
            //     return response()->json($result);
            // } 

        } catch(Exception $e){
            return response()->json($e->getMessage());
        }
    }


    /*
    |--------------------------------------------------------------------------
    | ========  API Provider & User Login ==========
    |--------------------------------------------------------------------------
    |
    | Local Url:https://www.webmobril.org/dev/od/api/v1/login
    | Live Url: 
    | Key:email,password;
    | Method:POST
    | Comments: Login api for user with key values and Post method.
    |
    |--------------------------------------------------------------------------
    */

    public function Login(Request $request){
        try {

            $validation = Validator::make($request->all(), [
                'email'        => 'required',
                'password'     => 'required',
            ]);

            if($validation->fails()) {

                $result['code']    = 400;
                $result['error']   = true;
                $result['message'] = $validation->mesfirst_namesages()->first();
                return response()->json($result);

            } else {
            
            $credentials = $request->only(['email', 'password']);
            $checkUser   = User::query()->where(['email'=> $request->email])->first();
            
            if(!$checkUser){

                $result['code']    = 404;
                $result['error']   = true;
                $result['message'] = "This email id does not exist in our records!";
                return response()->json($result);

            } 
            if (Auth::attempt($credentials)) {
                $model  = User::where('email',$request->email)->first();
                $domain      = Domains::where('userid',auth()->user()->id)->first();
                if($model->roles_id == $request->role) {

                if(null != $model){
                    if($model->status == 0){
                        
                        $result['code']    = 401;
                        $result['error']   = true;
                        $result['message'] = "Your account is under review";
                        return response()->json($result);
                        
                    } else if($model->status == 2){
                    
                        $result['code']    = 401;
                        $result['error']   = true;
                        $result['message'] = "Forbidden! Your Status is Pending."; 
                        return response()->json($result);

                    } 

                    if($model->verified == 0) {
                        $otp      = $this->generate_OTP();
                        $model1   = User::where('id', auth()->user()->id)->update(['otp' => $otp]);
                        $to_name  = auth()->user()->name;
                        $to_email = auth()->user()->email;
                        $body     = 'Your OTP is : ' . $otp . ' Please do not share with anybody.';
                        $data     = array('name' => $to_name, 'body' => $body);

                        Mail::send('emails.mail', $data, function ($message) use ($to_name, $to_email) {
                            $message->to($to_email, $to_name)
                                ->subject('Verification Otp');
                            $message->from(config('mail.username'), env('APP_NAME', 'OnlineDirectory'));
                        });

                        $sendOtpToMobile     = $this->sentOtp($model->mobile,$otp);
                        $deliverResponse     = json_encode($sendOtpToMobile, true);

                        if($deliverResponse == "true"){
                        $model->tokens()->delete();
                        $token                     = $model->createToken('OnlineDirectory')->accessToken;
                        /* $error['code']           = 202;
                        $error['error']          = true;
                        $error['verified']       = 0;
                        $error['message']        = "Please verify your account. We have sent an otp to your Mobile.";
                        $error['otp']            = $otp;
                        $error['mobile']         = auth()->user()->mobile; */
                        $error['code']            = 200;
                        $error['error']           = false;
                        $error['message']         = "Please verify your account. We have sent an otp to your Mobile.";
                        $error['domain']          = $domain['domain_name'] ?: "";
                        $error['result']          = $model;
                        $error['is_trial']        = $model->is_trial;
                        $error['is_subscribed']   = $model->is_subscribed;
                        $error['result']['token'] = $token;
                        return response()->json($error);

                        } else {
                        
                        $result['code']    = 400;
                        $result['error']   = true;
                        $result['message'] = "You have entered the invalid Mobile Number. Please try again!";
                        return response()->json($result);   

                        } 

                    }
                        $model->tokens()->delete();
                        $token                     = $model->createToken('OnlineDirectory')->accessToken;
                        $updateDevice              = User::where('id', auth()->user()->id)->update(['device_type' => $request->device_type, 'device_token' => $request->device_token]);
                        
                        /* $is_subscribed = UserSubscription::where(['cust_id' => auth()->id(), 'status'=>'1'])->where('renewal_date','>',date('Y-m-d'))->get();
                        if($is_subscribed!='[]'){
                            $is_subscribed =1;
                        }else{
                            $is_subscribed =0;
                        } */
                        // return $is_subscribed;
                        
                        if($model->verified == 1 && $model->is_trial == 1){
                            $afterExpireTrail = User::where('id', auth()->user()->id)->where('status','1')->where(function ($query){
                                $query->where('free_trial_end', '>=', \Carbon\Carbon::now()->addDays(15));
                            })->first(); 
                            //return $afterExpireTrail; 
                            if($afterExpireTrail) {
                                $expireTrail = User::where('id', auth()->user()->id)->update(['is_trial' => 2]);
                            }   
                        }

                        $result['code']            = 200;
                        $result['error']           = false;
                        $result['message']         = "Logged in successfully.";
                        $result['domain']          = $domain['domain_name'] ?: "";
                        $result['result']          = $model;
                        $result['is_trial']        = $model->is_trial;
                        $result['is_subscribed']   = $model->is_subscribed;
                        $result['result']['token'] = $token;
                        return response()->json($result); 
                }
                } else {

                        $result['code']    = 401;
                        $result['error']   = true;
                        $result['message'] = "Invalid credentials detail"; 
                        return response()->json($result);
                }
                } else {

                        $result['code']    = 401;
                        $result['error']   = true;
                        $result['message'] = "Credentials do not match!";
                        return response()->json($result);
                }
            }

        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }
    }

     /*
    |--------------------------------------------------------------------------
    | ========  API User Forgot Password ==========
    |--------------------------------------------------------------------------
    |
    | Local Url:http://localhost/onlinedirectory/api/v1/business-partner-forgotpassword
    | Live Url: 
    | Key:email;
    | Method:POST
    | Comments: Forgot api for user with key values and Post method.
    |
    |--------------------------------------------------------------------------
    */ 

    public function businessPartnerForgotpassword(Request $request){
        
        try {

            $validation = Validator::make($request->all(), [
                'email' => 'required|email',
                'role'  => 'required'
            ]);

            if($validation->fails()) {

                $result['code']    = 400;
                $result['error']   = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);
            }

            $email                 = $request->input('email');
            $checkUser             = User::query()->where('email', $email)->first();
    
            if(isset($checkUser) && $checkUser != null){
                if($checkUser->roles_id == $request->role) {
	
                $generateOtp       = $this->generate_OTP();

                $updateOtp         = User::where('id', $checkUser->id)->update(array('otp'=> $generateOtp ,'request_otp' => 0));
                $to_name           = $checkUser->name;
                $to_email          = $email;
                $body              = 'Your OTP is : ' . $generateOtp . ' Please do not share with anybody.';
                $data              = array('name' => $to_name, 'body' => $body);

                Mail::send('emails.mail', $data, function ($message) use ($to_name, $to_email) {
                    $message->to($to_email, $to_name)
                        ->subject('Forgot Password OTP');
                    $message->from(config('mail.username'), env('APP_NAME', 'OnlineDirectory'));
                });

                $result['code']          = 200;
                $result['error']         = false;
                $result['message']       = "Please Verify your account we sent otp on your registered email.";
                $result['result']['otp'] = $generateOtp;
                return response()->json($result);
            
            } else {

                $result['code']    = 401;
                $result['error']   = true;
                $result['message'] = "Invalid credentials detail"; 
                return response()->json($result);
            }

            } else {

                $result['code']          = 404;
                $result['error']         = true;
                $result['message']       = "Email Id does not exist in our records!";
                return response()->json($result);
            }

        } catch(Exception $e){
            return response()->json($e->getMessage());
        }    
    }



/*
|--------------------------------------------------------------------------
| ========  API User Verify OTP ==========
|--------------------------------------------------------------------------
|
| Local Url:https://www.webmobril.org/dev/od/api/v1/verifyotp
| Live Url: 
| Key:email,otp;
| Method:POST
| Comments: verify OTP api for user with key values and Post method.
|
|--------------------------------------------------------------------------
*/ 

    public function verifyOtp(Request $request){
        
        try {

            $type = $request->type;
        
            if($type == 1){

                $validation      = Validator::make($request->all(), [
                    'email'      => 'required|email',
                    'otp'        => 'required|min:4',
                ],
            
                [
                    'otp.required'  => 'Please enter otp',
                    'otp.min'       => 'The otp must be of 4 digits.'
                ]);

                if ($validation->fails()) {

                    $result['code']    = 401;
                    $result['error']   = true;
                    $result['message'] = $validation->messages()->first();
                    return response()->json($result);
                }

                $useremail  = User::query()->where('email', $request->email)->first();
               
                if (isset($useremail) && $useremail != null) {
                  
                if ($useremail->otp == $request->otp) {
                    
                    $useremail->verified = 1;
                    $useremail->request_otp = 1;

                    $token = $useremail->createToken('OnlineDirectoryVerifyOtp')->accessToken;

                    if($useremail->save()){

                        $result['code']    = 200;
                        $result['error']   = false;
                        $result['message'] = "Otp Verified Successfully.";
                        $result['result']  = $useremail->only(['id','name','email','mobile','verified','status']);
                        $result['result']['token'] = $token;

                        return response()->json($result);
                    }

                    else{

                        $result['code']    = 500;
                        $result['error']   = true;
                        $result['message'] = "Something went wrong!";

                        return response()->json($result);
                    }

                } else {

                    $result['code']    = 400;
                    $result['error']   = true;
                    $result['message'] = "Invalid otp";
                    return response()->json($result);
                }

                } else {

                    $result['code']        = 404;
                    $result['error']       = true;
                    $result['message']     = "Email Id does not found!";
                    return response()->json($result);
                }
            }
            if($type == 2 ){
                
                $validation      = Validator::make($request->all(), [
                    'mobile'     => 'required',
                    'otp'        => 'required|min:4',
                ],
            
                [
                    'otp.required'  => 'Please enter otp',
                    'otp.min'       => 'The otp must be of 4 digits.'
                ]);

                if ($validation->fails()) {

                    $result['code']    = 401;
                    $result['error']   = true;
                    $result['message'] = $validation->messages()->first();
                    return response()->json($result);
                }
                
                $usermobile  = User::query()->where('mobile', $request->mobile)->first();

                if (isset($usermobile) && $usermobile != null) {
                
                    if ($usermobile->otp == $request->otp) {
                    $usermobile->verified = 1;
                    $usermobile->request_otp = 1;

                    $token = $usermobile->createToken('OnlineDirectoryVerifyOtp')->accessToken;

                    if($usermobile->save()){

                        $result['code']    = 200;
                        $result['error']   = false;
                        $result['message'] = "Otp Verified Successfully.";
                        $result['result']  = $usermobile->only(['id','name','email','mobile','verified','status']);
                        $result['result']['token'] = $token;

                        return response()->json($result);
                    }

                    else{

                        $result['code']    = 500;
                        $result['error']   = true;
                        $result['message'] = "Something went wrong!";

                        return response()->json($result);
                    }

                    } else {

                        $result['code']    = 400;
                        $result['error']   = true;
                        $result['message'] = "Invalid otp.";
                        return response()->json($result);
                    }

                } else {

                    $result['code']        = 404;
                    $result['error']       = true;
                    $result['message']     = "Mobile no does not found!";
                    return response()->json($result);
                }
        }

        } catch (Exception $e) {

            return response()->json($e->getMessage());
        }

    }

/*
|--------------------------------------------------------------------------
| ========  API User Resend OTP ==========
|--------------------------------------------------------------------------
|
| Local Url:https://www.webmobril.org/dev/od/api/v1/business-partner-resend-otp
| Live Url: 
| Key:email,otp;
| Method:POST
| Comments: verify OTP api for user with key values and Post method.
|
|--------------------------------------------------------------------------
*/ 

    public function resendOtp(Request $request)
    {
        try{

            $type = $request->type;

            if($type == 1){

            $validation = Validator::make($request->all(), [
                'email'     => 'required|email',
            ]);

            if ($validation->fails()) {
                $result['code'] = 400;
                $result['error'] = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);
            }

            $email = $request->input('email');
            $user  = User::where(['email'=> $email])->first();

            if(isset($user) && $user != null){

                $otp = $this->generate_OTP();
                
                $updateOtp  = User::where('id',$user->id)->update(array('otp' => $otp,'request_otp' => 0));
                $to_name    = $user->name;
                $to_email   = $email;
                $body       = 'Your OTP is : '.$otp.' Please do not share.';
                $data       = array('name'=>$to_name,'body' => $body);

                Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email) {
                    $message->to($to_email, $to_name)
                        ->subject('Resent OTP');
                    $message->from(config('mail.username'),'OnlineDirectory');
                });

                /* $input['otp']    = $otp ? $otp : "";
                $sendOtpToMobile = $this->sentOtp($user->email,$otp);
                $deliverResponse = json_encode($sendOtpToMobile, true);

                if($deliverResponse == "true"){
                    $updateOtp  = User::where('id',$user->id)->update(array('otp' => $otp,'request_otp' => 0));
                } else {
                    $result['code']    = 400;
                    $result['error']   = true;
                    $result['message'] = "You have entered the invalid Mobile Number. Please try again!";
                    return response()->json($result);
                }  */

                $result['code']          = 200;
                $result['error']         = false;
                $result['message']       = 'We have resent an OTP on your email.';
                $result['result']['otp'] = $otp;
                return response()->json($result);

            } else {

                $result['code'] = 404;
                $result['error'] = true;
                $result['message'] = 'Email does not exist in our records!';
                return response()->json($result);
            }
        }
        if($type == 2){

            $validation = Validator::make($request->all(), [
                'mobile'     => 'required|numeric',
            ]);

            if ($validation->fails()) {
                $result['code'] = 400;
                $result['error'] = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);
            }

            
            $mobile = $request->input('mobile');
            $user   = User::where(['mobile'=> $mobile])->first();

            if(isset($user) && $user != null){

                $otp = $this->generate_OTP();

                /* $updateOtp  = User::where('id',$user->id)->update(array('otp' => $otp,'request_otp' => 0));
                $to_name    = $user->name;
                $to_email   = $user->email;
                
                $body       = 'Your OTP is : '.$otp.' Please do not share.';
                $data       = array('name'=>$to_name,'body' => $body);

                Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email) {
                    $message->to($to_email, $to_name)
                        ->subject('Resent OTP');
                    $message->from(config('mail.username'),'OnlineDirectory');
                }); */

                $input['otp']    = $otp ? $otp : "";
                $sendOtpToMobile = $this->sentOtp($request->mobile,$otp);
                $deliverResponse = json_encode($sendOtpToMobile, true);

                if($deliverResponse == "true"){
                    $updateOtp  = User::where('id',$user->id)->update(array('otp' => $otp,'request_otp' => 0));
                } else {
                    $result['code']    = 400;
                    $result['error']   = true;
                    $result['message'] = "You have entered the invalid Mobile Number. Please try again!";
                    return response()->json($result);
                } 

                $result['code']          = 200;
                $result['error']         = false;
                $result['message']       = 'We have resent an OTP on your mobile.';
                $result['result']['otp'] = $otp;
                return response()->json($result);

            } else {

                $result['code'] = 404;
                $result['error'] = true;
                $result['message'] = 'Mobile does not exist in our records!';
                return response()->json($result);
            }
        }

        }catch(Exception $e){

            return response()->json($e->getMessage());
        }

    }
/*
|--------------------------------------------------------------------------
| ==========  Random Number ==========
|--------------------------------------------------------------------------
*/ 

    private function generate_OTP()
    {
        return $randomOtp = mt_rand(1000, 9999);
    }

    /*
|--------------------------------------------------------------------------
| ========  API Change Password ==========
|--------------------------------------------------------------------------
|
| Local Url:https://www.webmobril.org/dev/od/api/v1/changepassword
| Live Url: 
| Key:email,new password,confirm password;
| Method:POST
| Comments: verify OTP api for user with key values and Post method.
|
|--------------------------------------------------------------------------
*/ 

    public function change_Password(Request $request)
    {
       try{

            $validation = Validator::make($request->all(),
                [
                    'password'          => 'required',
                    'confirm_password'  => 'required|same:password',
                ]

            );

            if($validation->fails()) {

                $result['code']    = 400;
                $result['error']   = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);

            }

            $user      = auth()->user();
            $checkUser = User::query()->where('email', $user->email)->first();
            
            if(isset($checkUser) && $checkUser != null) {
				
                if($user->email != $user->email){
					
                    $result['code']    = 404;
                    $result['error']   = true;
                    $result['message'] = "Not Authenticated!";

                    return response()->json($result);
				}

                    $user->fill([
                        'password' => Hash::make($request->password)
                    ])->save();

                    $result['code']    = 200;
                    $result['error']   = false;
                    $result['message'] = "Password changed successfully.";
                    $result['result']  = $user->only(['id', 'name','email','status']);

                    return response()->json($result);

            } else {

                $result['code']    = 404;
                $result['error']   = true;
                $result['message'] = "User does not found!";
                return response()->json($result);
            }
            
            } catch (Exception $e) {
                return response()->json($e->getMessage());
        }

    }


    /*
|--------------------------------------------------------------------------
| ========  API Change Password ==========
|--------------------------------------------------------------------------
|
| Local Url:https://www.webmobril.org/dev/od/api/v1/change-password
| Live Url: 
| Key:email,new password,confirm password;
| Method:POST
| Comments: verify OTP api for user with key values and Post method.
|
|--------------------------------------------------------------------------
*/ 

    public function changePassword(Request $request)
    {   
    
       try{

            $validation = Validator::make($request->all(),
                [   
                    'current_password'  => 'required',
                    'new_password'      => 'required',
                    'confirm_password'  => 'required|same:new_password',
                ]
            );
            
            if($validation->fails()) {

                $result['code']    = 400;
                $result['error']   = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);

            }

            $user      = auth()->user();
    
            $checkUser = User::query()->where('email', $user->email)->first();
            
            if(isset($checkUser) && $checkUser != null) {
				
                if($user->email != $user->email){
					
                    $result['code']    = 404;
                    $result['error']   = true;
                    $result['message'] = "Not Authenticated!";

                    return response()->json($result);
				} 
                if (Hash::check($request->current_password, $user->password)) {

                    $user->fill([
                        'password' => Hash::make($request->new_password)
                    ])->save();

                    $result['code']    = 200;
                    $result['error']   = false;
                    $result['message'] = "Password changed successfully.";
                    $result['result']  = $user->only(['id', 'name','email','status']);

                    return response()->json($result);
                }else {

                    $result['code'] = 400;
                    $result['error'] = true;
                    $result['message'] = "Old Password does not exists.";
                    return response()->json($result);
                }

            } else {

                $result['code']    = 404;
                $result['error']   = true;
                $result['message'] = "User does not found!";
                return response()->json($result);
            }
            
            } catch (Exception $e) {
                return response()->json($e->getMessage());
        }

    }

   /*
|--------------------------------------------------------------------------
| ========  API Reset Password ==========
|--------------------------------------------------------------------------
|
| Local Url:https://www.webmobril.org/dev/od/api/v1/business-partner-changepassword
| Live Url: 
| Key:email,new password,confirm password;
| Method:POST
| Comments: verify OTP api for user with key values and Post method.
|
|--------------------------------------------------------------------------
*/ 

    public function ResetPassword(Request $request)
    {
       try{

            $validation = Validator::make($request->all(),
                [
                    'email'             => 'required|email',
                    'new_password'      => 'required',
                    'confirm_password'  => 'required|same:new_password',
                ]

            );

            if ($validation->fails()) {

                $result['code']    = 400;
                $result['error']   = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);

            }
            
            //$user = auth()->user();
    
            $checkUser = User::query()->where('email', $request->email)->first();
            
            if (isset($checkUser) && $checkUser != null) {

                $checkUser->fill([
                    'password' => Hash::make($request->new_password)
                ])->save();

                $result['code'] = 200;
                $result['error'] = false;
                $result['message'] = "Password reset successfully.";
                $result['result'] = $checkUser;

                return response()->json($result);

            } else {

                $result['code'] = 404;
                $result['error'] = true;
                $result['message'] = "User does not found!";
                return response()->json($result);
            }

            
        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }

    }

/*
|--------------------------------------------------------------------------
| ========  API GET Profile ==========
|--------------------------------------------------------------------------
|
| Local Url:https://www.webmobril.org/dev/od/api/v1/getprofile
| Live Url: 
| Key:
| Method:GET
| Comments: getProfile for user GET method.
|
|--------------------------------------------------------------------------
*/ 


    public function getProfile(Request $request){

            $user = auth()->user();

            if(isset($user) && $user != null)
            {
                $result['code']    = 200;
                $result['error']   = false;
                $result['message'] = "Success";
                $result['result']  = $user->only('id','first_name','last_name','email','mobile','location','zip_code','latitude','longitude');
                return response()->json($result);

            } else {

                $result['code']    = 401;
                $result['error']   = true;
                $result['message'] = "Unauthorized";
                return response()->json($result);
            }

    }

 
/*
|--------------------------------------------------------------------------
| ========  API Update Profile ==========
|--------------------------------------------------------------------------
|
| Local Url:https://www.webmobril.org/dev/od/api/v1/updateprofile
| Live Url: 
| Key:email,etc.
| Method:POST
| Comments: updateprofile for user POST method with key value.
|
|--------------------------------------------------------------------------
*/ 

    public function updateProfile(Request $request)
    {
        try{

            $auth  = auth()->user();
            if($request->role == 2){
              
				/* $columns=[

                    'role'                  => ['required',Rule::in([2,3])],        
                    'first_name'            => "required|string|regex:/^[a-zA-Z ]+$/u|max:255",
                    'last_name'             => "required|string|regex:/^[a-zA-Z ]+$/u|max:255",
                    'email'                 =>  ['required','max:255','regex:/^\w+[-\.\w]*@(?!(?:)\.com$)\w+[-\.\w]*?\.\w{2,4}$/', \Illuminate\Validation\Rule::unique('users')->ignore($auth->id)],
                    'mobile'                => ['required','between:6,15',\Illuminate\Validation\Rule::unique('users')->ignore($auth->id)],
                    'location'              => 'required',
                    'latitude'              => 'required',
                    'longitude'             => 'required',
                    'zip_code'              => 'required|numeric|min:6'
				]; */
                $columns = [
                    'role'       => ['required',Rule::in([2,3])],        
                    'first_name' => ['required','regex:/^[a-zA-Z ]+$/u','max:255'],
                    'last_name'  => ['required','regex:/^[a-zA-Z ]+$/u','max:255'],
                    'location'   => ['required'],
                    'latitude'   => 'required',
                    'longitude'  => 'required',
                    'zip_code'   => ['required','min:4'],
                    'email'      =>  ['required','max:255','regex:/^\w+[-\.\w]*@(?!(?:)\.com$)\w+[-\.\w]*?\.\w{2,4}$/', \Illuminate\Validation\Rule::unique('users')->ignore($auth->id)],
                    'mobile' => ['required','between:6,15', \Illuminate\Validation\Rule::unique('users')->ignore($auth->id)]
                ];

                $customMessages = [
                    'role.in'            => 'Role Type should be 2 for Professional, 3 for User',
                    'mobile.between'     => 'Mobile number must be between 6 to 15 digits.'
                ];

			}

            if($request->role == 3){
				
                /* $columns = [

                    'role'                  => ['required',Rule::in([2,3])],
                    'first_name'            => 'required',
                    'last_name'             => 'required',
                    //'email'               => 'required',
                    'email'                 =>  ['required','max:255','regex:/^\w+[-\.\w]*@(?!(?:)\.com$)\w+[-\.\w]*?\.\w{2,4}$/', \Illuminate\Validation\Rule::unique('users')->ignore($auth->id)],
                    'mobile'                => ['required','between:6,15',\Illuminate\Validation\Rule::unique('users')->ignore($auth->id)],
                    'mobile'                => ['required',\Illuminate\Validation\Rule::unique('users')->ignore($auth->id)],
				]; */
                
                $columns = [
                    'role'       => ['required',Rule::in([2,3])],        
                    'first_name' => ['required','regex:/^[a-zA-Z ]+$/u','max:255'],
                    'last_name'  => ['required','regex:/^[a-zA-Z ]+$/u','max:255'],
                    'email'      =>  ['required','max:255','regex:/^\w+[-\.\w]*@(?!(?:)\.com$)\w+[-\.\w]*?\.\w{2,4}$/', \Illuminate\Validation\Rule::unique('users')->ignore($auth->id)],
                    'location'   => ['required'],
                    'latitude'   => 'required',
                    'longitude'  => 'required',
                    'zip_code'   => ['required','min:4'],
                    'mobile'     => ['required','between:6,15', \Illuminate\Validation\Rule::unique('users')->ignore($auth->id)]
                ];

                $customMessages = [
                    'role.in'            => 'Role Type should be 2 for Professional, 3 for User',
                    'mobile.between'     => 'Mobile number must be between 6 to 15 digits.'
                ];

			}

            $validation = Validator::make($request->all(),$columns,$customMessages);

            if($validation->fails()){
                $result['code']     = 400;
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);

            }

            $user = auth()->user();

            if($request->role == 3){
                $user->first_name    = $request->input('first_name');
                $user->last_name     = $request->input('last_name');
                $user->email         = $request->input('email');
                $user->mobile        = $request->input('mobile');
                $user->slug          = Str::slug($request->first_name , "-");
                $user->location      = $request->input('location');
                $user->latitude      = $request->input('latitude');
                $user->longitude     = $request->input('longitude');
                $user->zip_code      = $request->input('zip_code');
                $user->update();

            }

            if($request->role == 2){
        
                $user->first_name    = $request->input('first_name');
                $user->last_name     = $request->input('last_name');
                $user->email         = $request->input('email');
                $user->mobile        = $request->input('mobile');
                $user->slug          = Str::slug($request->first_name , "-");
                $user->location      = $request->input('location');
                $user->latitude      = $request->input('latitude');
                $user->longitude     = $request->input('longitude');
                $user->zip_code      = $request->input('zip_code');
                $user->update();
            }

            $result['code']      = 200;
            $result['error']     = false;
            $result['message']   = "Profile Updated Successfully.";
            $result['result']    = $user->only(['id', 'first_name','last_name','email','mobile','location','latitude','longitude','zip_code']);
            return response()->json($result); 

        }catch(Exception $e) {
            return response()->json($e->getMessage());
        }
    }

/*
|--------------------------------------------------------------------------
| ========  API List Of Professional detail ==========
|--------------------------------------------------------------------------
|
| Local Url:https://www.webmobril.org/dev/od/api/v1/professional/professionaldetail
| Live Url: 
| Key:email,etc.
| Method:POST
| Comments: List Professional for user POST Method.
|
|--------------------------------------------------------------------------
*/ 

    public function ProfessionalDetail(Request $request){
    
        try{

            $validation = Validator::make($request->all(), [                              
                'providerId'    => 'required'                     
            ]);

            if ($validation->fails()) {
                $result['code']    = 400;
                $result['error']   = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);
            }

        /* $professional = User::where('status',1)->with(['reviews','userdetail' => function($q){
            $q->select('*');
            },	'reviews.user' => function($q){
                $q->select('*');
            }
        ])->where('slug',$request->slug)->first();  */

        /*$professional = User::where('status',1)->with('userdetail')->where('id',$request->providerId)->first(); 
        
        if(!empty($professional)){
    
            $result['code']    = 200;
            $result['error']   = false;
            $result['result']  = $professional;
            return response()->json($result);

        } else {

            $result['code']   = 404;
            $result['error']  = true;
            $result['result'] = "We're sorry. We were not able to find a match";
            return response()->json($result);
        }  */

        $professional = User::where('status',1)->where('id',$request->providerId)->with('userdetail')->first();
        if(isset($professional)){
            $ip = \Request::getClientIp();
            $visited_date = Date("Y-m-d: H:i:s");
            $input = array('ip' => $ip,'userid' => $professional->id);
            Visitor::create($input);
        }

        if(isset($professional)){

        if($professional->userdetail !=""){

            $sliderpart = explode(',', $professional->userdetail->sliders);
            foreach($sliderpart as $index=>$imgpath){ 
                $img[$index]['slider_img'] = $imgpath;
                $img[$index]['counter']    = $index+1;
            }   
            
            $result['code']      = 200;
            $result['error']     = false;
            $result['result']    = $professional;
            if($professional->userdetail->sliders == null){
                $result['sliders']   = [];
            }else {
                $result['sliders']   = $img;
            } 
            return response()->json($result);

        } else {
            $result['code']      = 200;
            $result['error']     = false;
            $result['result']    = $professional;
            $result['sliders']   = [];
            return response()->json($result);
        } 
        }else {
            $result['code']      = 404;
            $result['error']     = true;
            $result['messgae']   = "Provider ID Not Found.";
            return response()->json($result);
        }

        }catch(Exception $e) {
            return response()->json($e->getMessage());
        }
    }

    /*
|--------------------------------------------------------------------------
| ========  API Provider ALl Review ==========
|--------------------------------------------------------------------------
|
| Local Url:https://www.webmobril.org/dev/od/api/v1/professional/providerReviews
| Live Url: 
| Key:email,etc.
| Method:POST
| Comments: List Professional for user POST Method.
|
|--------------------------------------------------------------------------
*/ 

    public function ProfessionalAllReview(Request $request){
    
        try{

            $validation = Validator::make($request->all(), [                              
                'providerId'    => 'required'                     
            ]);

            if ($validation->fails()) {
                $result['code'] = 400;
                $result['error'] = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);
            } 
            
            $reviews = Reviews::with('reviews')->where('user_type',$request->providerId)->get();
    
        if(!empty($reviews)){
    
            $result['code']    = 200;
            $result['error']   = false;
            $result['result']  = $reviews;
            return response()->json($result);

        } else {

            $result['code']   = 404;
            $result['error']  = true;
            $result['result'] = "No Data Found";
            return response()->json($result);
        } 

        }catch(Exception $e) {

            return response()->json($e->getMessage());

        }
    }

    public function FormCategories(){
        
        try{

        $categories = Category::select('id','name')->where('status','=',1)->where('parent_id','!=',0)->orderby('name', 'asc')->get();
       
        if(!empty($categories)){
    
            $result['code']    = 200;
            $result['error']   = false;
            $result['result']  = $categories;
            return response()->json($result);

        } else {

            $result['code']   = 404;
            $result['error']  = true;
            $result['result'] = "No result found!";
            return response()->json($result);
        }

        }catch(Exception $e) {
            return response()->json($e->getMessage());
        }
    }
    
    public function listCategories(){
        
        try{

        $categories = Category::where('status','=',1)->where('parent_id','!=',0)->orderby('name', 'asc')->get();
       
        if(!empty($categories)){
    
            $result['code']    = 200;
            $result['error']   = false;
            $result['result']  = $categories;
            return response()->json($result);

        } else {

            $result['code']   = 404;
            $result['error']  = true;
            $result['result'] = "No result found!";
            return response()->json($result);
        }

        }catch(Exception $e) {
            return response()->json($e->getMessage());
        }
    }

    public function searchProfessional(Request $request){
           
        try{
            
            $validation = Validator::make($request->all(), [                              
                'latitude'    => 'required|numeric',                
                'longitude'   => 'required|numeric',                
                'category_id'   => 'required|numeric',                            
            ]);

            if ($validation->fails()) {
                $result['code']    = 400;
                $result['error']   = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);
            }

            //$ip          = '122.161.49.140'; 
            $ip          = $request->ip();
            $currentip   = \Location::get($ip);  
            $latitude    = $currentip->latitude;
            $longitude   = $currentip->longitude;
            $location    = $currentip->cityName;

            if($request->has('latitude') && $request->latitude!='') {
                $latitude = $request->latitude;
            } else {
                $latitude = $latitude;
            }
            if($request->has('longitude') && $request->longitude!='') {
                $longitude = $request->longitude;
            } else {
                $longitude = $longitude;
            }

            if($request->has('location') && $request->location!='') {
                $location = $request->location;
            } else {
                $location = $location;
            } 

            /* $location       = $request->get('location');
            $latitude       = $request->get('latitude');
            $longitude      = $request->get('longitude');  */
            $category_id    = $request->get('category_id');
            
            $professionals          = DB::table('users');
            $professionals          = $professionals->select("*", DB::raw("6371 * acos(cos(radians(" . $latitude . "))
                                    * cos(radians(latitude)) * cos(radians(longitude) - radians(" . $longitude . "))
                                    + sin(radians(" .$latitude. ")) * sin(radians(latitude))) AS distance"));
            $professionals          = $professionals->where('roles_id', '=', 2);
            $professionals          = $professionals->where('status','=',1);
            $professionals          = $professionals->where('category_id',$category_id);
            $professionals          = $professionals->having('distance', '<', 50);
            $professionals          = $professionals->orderBy('distance', 'asc');
            $professionals          = $professionals->get();

            $results = [];

            foreach ($professionals as $mainKey => $getProfessional) {
            
            $results[$mainKey]['id']             = $getProfessional->id;
            $results[$mainKey]['roles_id']       = $getProfessional->roles_id;
            $results[$mainKey]['slug']           = $getProfessional->slug;
            $results[$mainKey]['first_name']     = $getProfessional->first_name;
            $results[$mainKey]['last_name']      = $getProfessional->last_name;
            $results[$mainKey]['email']          = $getProfessional->email;
            $results[$mainKey]['mobile']         = $getProfessional->mobile;
            $results[$mainKey]['profile_image']  = $getProfessional->profile_image;
            $results[$mainKey]['verified']       = $getProfessional->verified;
            $results[$mainKey]['business_name']  = $getProfessional->business_name;
            $results[$mainKey]['location']       = $getProfessional->location;
            $results[$mainKey]['latitude']       = $getProfessional->latitude;
            $results[$mainKey]['longitude']      = $getProfessional->longitude;
            $results[$mainKey]['category_id']    = $getProfessional->category_id;
            $results[$mainKey]['template_id']    = $getProfessional->template_id;
            $results[$mainKey]['status']         = $getProfessional->status;
            
            $binfo    = BusinessInformation::where('professional_id',$getProfessional->id)->first();
        
            if($binfo){
                $results[$mainKey]['website_logo']    = $binfo->website_logo;
                $results[$mainKey]['average_charges'] = $binfo->average_charges;
                $results[$mainKey]['opening_time']    = $binfo->opening_time;
                $results[$mainKey]['closing_time']    = $binfo->closing_time;
                $results[$mainKey]['details']         = $binfo->details;
            }else{
                $results[$mainKey]['website_logo']    = "";
                $results[$mainKey]['average_charges'] = "";
                $results[$mainKey]['opening_time']    = "";
                $results[$mainKey]['closing_time']    = "";
                $results[$mainKey]['details']         = "";
            }

            $reviews = Reviews::where('user_type',$getProfessional->id)->get();

            $results[$mainKey]['reviews'] = [];
            $reviewAndRating = [];

            foreach ($reviews as $key => $review) {
                
                $results[$mainKey]['reviews'][$key]['message'] = $review->message;
                $results[$mainKey]['reviews'][$key]['ratings'] = $review->ratings;
                $reviewAndRating[]        = $review->ratings;

            }
            
                $count = count($reviewAndRating);
                $sum   = array_sum($reviewAndRating);

                if($count > 0){

                    $results[$mainKey]['average_ratings'] = $sum/$count;
                    $results[$mainKey]['total_ratings'] = $count;

                } else {

                    $results[$mainKey]['average_ratings'] = 0;
                    $results[$mainKey]['total_ratings']   = 0;
                }
            }

            if(!empty($results)){
    
            $result['code']    = 200;
            $result['error']   = false;
            $result['result']  = $results;
            return response()->json($result);

            } else {

                $result['code']   = 404;
                $result['error']  = true;
                /* $result['message'] = "We're sorry. We were not able to find a match"; */
                $result['message'] = "No Data Found";
                return response()->json($result);
            }

        }catch(Exception $e) {
            return response()->json($e->getMessage());
        }
    }

    /* =======  Search by Alphabets ======== */

    public function searchByAlphabet(Request $request){
            
        try{
            
            $validation = Validator::make($request->all(), [                              
                'alpha'    => 'required|string'                           
            ]);

            if ($validation->fails()) {
                $result['code'] = 400;
                $result['error'] = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);
            }

            $alpha                  = $request->get('alpha');
            $professionals          = User::where('roles_id',2)->where('business_name', 'LIKE', "$alpha%")->orderby('business_name', 'ASC')->get();

            $results = [];

            foreach ($professionals as $mainKey => $getProfessional) {
            
            $results[$mainKey]['id']             = $getProfessional->id;
            $results[$mainKey]['roles_id']       = $getProfessional->roles_id;
            $results[$mainKey]['slug']           = $getProfessional->slug;
            $results[$mainKey]['first_name']     = $getProfessional->first_name;
            $results[$mainKey]['last_name']      = $getProfessional->last_name;
            $results[$mainKey]['email']          = $getProfessional->email;
            $results[$mainKey]['mobile']         = $getProfessional->mobile;
            $results[$mainKey]['profile_image']  = $getProfessional->profile_image;
            $results[$mainKey]['verified']       = $getProfessional->verified;
            $results[$mainKey]['business_name']  = $getProfessional->business_name;
            $results[$mainKey]['location']       = $getProfessional->location;
            $results[$mainKey]['latitude']       = $getProfessional->latitude;
            $results[$mainKey]['longitude']      = $getProfessional->longitude;
            $results[$mainKey]['category_id']    = $getProfessional->category_id;
            $results[$mainKey]['template_id']    = $getProfessional->template_id;
            $results[$mainKey]['status']         = $getProfessional->status;

            $binfo    = BusinessInformation::where('professional_id',$getProfessional->id)->get();
            $results[$mainKey]['businessinfo'] = $binfo;
            
            $reviews = Reviews::where('user_type',$getProfessional->id)->get();

            $results[$mainKey]['reviews'] = [];
            $reviewAndRating = [];

            foreach ($reviews as $key => $review) {
                $results[$mainKey]['reviews'][$key]['message'] = $review->message;
                $results[$mainKey]['reviews'][$key]['ratings'] = $review->ratings;
                $reviewAndRating[]        = $review->ratings;
            }
            
                $count = count($reviewAndRating);
                $sum   = array_sum($reviewAndRating);

                if($count > 0){

                    $results[$mainKey]['average_ratings'] = $sum/$count;
                    $results[$mainKey]['total_ratings'] = $count;

                } else {

                    $results[$mainKey]['average_ratings'] = 0;
                    $results[$mainKey]['total_ratings']   = 0;
                }
            }

            if(!empty($results)){
    
            $result['code']    = 200;
            $result['error']   = false;
            $result['result']  = $results;
            return response()->json($result);

            } else {

                $result['code']   = 404;
                $result['error']  = true;
                $result['result'] = "No Data Found";
                return response()->json($result);
            }

        }catch(Exception $e) {
            return response()->json($e->getMessage());
        }
    }

    

    public function professionalSort(Request $request){

        try{
            
            $validation = Validator::make($request->all(), [                              
                'latitude'    => 'required|numeric',                
                'longitude'   => 'required|numeric',                
                'category_id'   => 'required|numeric',                            
            ]);

            if ($validation->fails()) {
                $result['code'] = 400;
                $result['error'] = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);
            }

             /* $ip          = '122.161.49.140'; 
            $currentip   = \Location::get($ip);  
            $latitude    = $currentip->latitude;
            $longitude   = $currentip->longitude;
            $location    = $currentip->cityName;

            if($request->has('latitude') && $request->latitude!='') {
                $latitude = $request->latitude;
            } else {
                $latitude = $latitude;
            }
            if($request->has('longitude') && $request->longitude!='') {
                $longitude = $request->longitude;
            } else {
                $longitude = $longitude;
            }

            if($request->has('location') && $request->location!='') {
                $location = $request->location;
            } else {
                $location = $location;
            } */

            $location       = $request->get('location');
            $latitude       = $request->get('latitude');
            $longitude      = $request->get('longitude');
            $category_id    = $request->get('category_id');

            $professionals          = DB::table('users');
            $professionals          = $professionals->select("*", DB::raw("6371 * acos(cos(radians(" . $latitude . "))
                                    * cos(radians(latitude)) * cos(radians(longitude) - radians(" . $longitude . "))
                                    + sin(radians(" .$latitude. ")) * sin(radians(latitude))) AS distance"));
            $professionals          = $professionals->where('roles_id', '=', 2);
            $professionals          = $professionals->where('status','=', 1);
            $professionals          = $professionals->where('category_id', 'LIKE', '%' . $category_id . '%');
            $professionals          = $professionals->having('distance', '<', 50);
            $professionals          = $professionals->orderBy('business_name','asc');
            $professionals          = $professionals->orderBy('distance', 'asc');
            $professionals          = $professionals->get();

            $results = [];

            foreach ($professionals as $mainKey => $getProfessional) {
            
            $results[$mainKey]['id']             = $getProfessional->id;
            $results[$mainKey]['roles_id']       = $getProfessional->roles_id;
            $results[$mainKey]['slug']           = $getProfessional->slug;
            $results[$mainKey]['first_name']     = $getProfessional->first_name;
            $results[$mainKey]['last_name']      = $getProfessional->last_name;
            $results[$mainKey]['email']          = $getProfessional->email;
            $results[$mainKey]['mobile']         = $getProfessional->mobile;
            $results[$mainKey]['profile_image']  = $getProfessional->profile_image;
            $results[$mainKey]['verified']       = $getProfessional->verified;
            $results[$mainKey]['business_name']  = $getProfessional->business_name;
            $results[$mainKey]['location']       = $getProfessional->location;
            $results[$mainKey]['latitude']       = $getProfessional->latitude;
            $results[$mainKey]['longitude']      = $getProfessional->longitude;
            $results[$mainKey]['category_id']    = $getProfessional->category_id;
            $results[$mainKey]['template_id']    = $getProfessional->template_id;
            $results[$mainKey]['status']         = $getProfessional->status;

            $binfo    = BusinessInformation::where('professional_id',$getProfessional->id)->first();
            
             if($binfo){
                $results[$mainKey]['website_logo']    = $binfo->website_logo;
                $results[$mainKey]['average_charges'] = $binfo->average_charges;
                $results[$mainKey]['opening_time']    = $binfo->opening_time;
                $results[$mainKey]['closing_time']    = $binfo->closing_time;
                $results[$mainKey]['details']         = $binfo->details;
            }else{
                $results[$mainKey]['website_logo']    = "";
                $results[$mainKey]['average_charges'] = "";
                $results[$mainKey]['opening_time']    = "";
                $results[$mainKey]['closing_time']    = "";
                $results[$mainKey]['details']         = "";
            }

            
            $reviews = Reviews::where('user_type',$getProfessional->id)->get();

            $results[$mainKey]['reviews'] = [];
            $reviewAndRating = [];

            foreach ($reviews as $key => $review) {
                
                $results[$mainKey]['reviews'][$key]['message'] = $review->message;
                $results[$mainKey]['reviews'][$key]['ratings'] = $review->ratings;
                $reviewAndRating[]        = $review->ratings;

            }
            
                $count = count($reviewAndRating);
                $sum   = array_sum($reviewAndRating);

                if($count > 0){

                    $results[$mainKey]['average_ratings'] = $sum/$count;
                    $results[$mainKey]['total_ratings'] = $count;

                } else {

                    $results[$mainKey]['average_ratings'] = 0;
                    $results[$mainKey]['total_ratings'] = 0;
                }
            }

            if(!empty($results)){
    
            $result['code']    = 200;
            $result['error']   = false;
            $result['result']  = $results;
            return response()->json($result);

            } else {

                $result['code']   = 404;
                $result['error']  = true;
                /* $result['message'] = "We're sorry. We were not able to find a match"; */
                return response()->json($result);
            }

        }catch(Exception $e) {
            return response()->json($e->getMessage());
        }
    }

}
