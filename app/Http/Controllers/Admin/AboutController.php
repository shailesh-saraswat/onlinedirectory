<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Session;
use App\Models\About;

class AboutController extends Controller
{
    public function index($id='')
    {   
        $about = About::first(); 
        //return $about;
    	return view('admin.about.index',compact('about'));
    }

    public function update(Request $request,$id=''){

        $validator  = Validator::make($request->all(), [
                'title'            => 'required', 
                'sort_description' => 'required', 
                'description'      => 'required'				 
        ]);

		if ($validator->fails()) {
        	return redirect()->back()->withInput($request->input())->withErrors($validator->errors());
        } else {
        
        $updateAbout = About::where('id',$request->aboutid)->first();
        if(empty($updateAbout)){
            Session::flash('message', 'Page Not found !');
            return redirect()->back(); 
        }

        if ($request->hasFile('featured_image')) {

            try {

                $profile_image = $request->file('featured_image');

                $name = $profile_image->getClientOriginalName();

                $destinationFolder = public_path('/uploads/about-' . $updateAbout->id . '/');

                // Delete folder
                if(file_exists($destinationFolder.$name)){

                    unlink($destinationFolder.$name);
                }

                $profile_image->move($destinationFolder, $name);
                $updateAbout->featured_image ="public/uploads/about-".$updateAbout->id.'/'.$name;

            } catch (\Exception $e) {
                
                return redirect()->back()->withErrors($e->getMessage());
            }
        }

        $updateAbout->title              = $request['title'];
        $updateAbout->sort_description   = $request['sort_description'];       
        $updateAbout->description        = $request['description'];       	
        $updateAbout->save();
            Session::flash('message', 'About Successfully updated !');
        return redirect('admin/about');

        }
    }

}
