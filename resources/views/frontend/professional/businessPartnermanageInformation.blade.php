@extends('frontend.professional.partials.index')
@section('after-style')

@include('frontend/partials/timercss')

@endsection
@section('content')
<main class="content">
				<div class="container-fluid p-0">

					<div class="row mb-2 mb-xl-3">
						<div class="col-auto d-none d-sm-block">
							<h3><strong>{{auth()->user()->name ?? ""}}</strong> Manage Informations</h3>
						</div>
						<div class="col-auto ms-auto text-end mt-n1">
							<nav aria-label="breadcrumb">
								<ol class="breadcrumb bg-transparent p-0 mt-1 mb-0">
									<li class="breadcrumb-item"><a href="{{route('business.partner.profile')}}">Home</a></li>
									<li class="breadcrumb-item active" aria-current="page">Manage Informations</li>
								</ol>
							</nav>
						</div>						
					</div>
					<div class="row">
						<div class="col-xl-7 col-xxl-5 d-flex">
							<div class="card w-100">
								@include('frontend.partials.messages')
								<div class="card-body">
									<h2> Manage Information </h2>
									<div class="panel-body">
										<form action="{{route('business.partner.updateinformation')}}" method="post" enctype="multipart/form-data">
										@csrf
											<div class="form-group customBannerStyling">
												<label for="exampleInputFile">Website Logo</label>
												<input type="file" id="website_logo" name="website_logo">
												
											</div>	

											@if(!empty($businessInfo->website_logo))
											<img src="{{$businessInfo->website_logo ?: '' }}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
											@else 
											<img src="{{asset('uploads/default.jpeg')}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
											@endif

											<div class="form-group">
												<label for="">Website</label>
												<input type="text" class="form-control" name="website_url" id="website_url" placeholder="Enter Your Website Url" value="{{$businessInfo->website_url ?? '' }}">
											</div>

											<div class="form-group">
												<label for="">Details</label>
												<div>
												<textarea class="form-control" rows="3" name="details" placeholder="Details">{{$businessInfo->details ?? ''}}</textarea>
												</div>
											</div>

											<div class="form-group">
                                            <label for="">Opening Time</label>
                                            <div class="input-group date" id="timepicker_openingTime" data-target-input="nearest">
                                            <input type="text" value="{{$businessInfo->userdetail->opening_time ?? ''}}" name="opening_time" class="form-control datetimepicker-input" data-target="#timepicker_openingTime"/>
                                            <div class="input-group-append" data-target="#timepicker_openingTime" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="far fa-clock"></i></div>
                                            </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="">Closing Time</label>
                                           <div class="input-group date" id="timepicker_closingTime" data-target-input="nearest">
                                            <input type="text" value="{{$businessInfo->userdetail->closing_time ?? ''}}" name="closing_time" class="form-control datetimepicker-input" data-target="#timepicker_closingTime"/>
                                            <div class="input-group-append" data-target="#timepicker_closingTime" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="far fa-clock"></i></div>
                                            </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                        <label for="">Average Charges</label>
                                        <div class="input-icon">
                                            <input type="text" class="form-control"  name="average_charges" id="average_charges" value="{{$businessInfo->userdetail->average_charges ?? ''}}" placeholder="0.00">
                                        <i>$</i>
                                        </div>
                                        </div>	

											<div class="form-group customBannerStyling">
												<label for="exampleInputFile">Main Banner</label>
												 <input type="file" id="banner1" name="banner1">
											</div>

											@if(!empty($businessInfo->banner1))
											<img src="{{$businessInfo->banner1 ?: '' }}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
											@else 
											<img src="{{asset('uploads/default.jpeg')}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
											@endif

											<div class="form-group customBannerStyling">
												<label for="exampleInputFile">Banner Second</label>
												 <input type="file" id="banner2" name="banner2">
											</div>

											@if(!empty($businessInfo->banner2))
											<img src="{{$businessInfo->banner2 ?: '' }}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
											@else 
											<img src="{{asset('uploads/default.jpeg')}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
											@endif

											<div class="form-group customBannerStyling">
												<label for="exampleInputFile">Banner Third</label>
												 <input type="file" id="banner3" name="banner3">
											</div>	
											@if(!empty($businessInfo->banner3))
											<img src="{{$businessInfo->banner3 ?: '' }}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
											@else 
											<img src="{{asset('uploads/default.jpeg')}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
											@endif
											<br>
											<button type="submit" class="btn btn-default btnCustom">Submit</button>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>				
				</div>
			</main>
				
@section('after-scripts')

@include('frontend/partials/timerjs')

@endsection					
@endsection