@extends('admin.index')

@section('content')

<!-- <div class="wrapper"> -->

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                     
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">Edit User</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10">

                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Edit User</strong></h3>
                            </div>
                            {{-- @include('admin.partials.messages') --}}
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form class="form-horizontal" method="post" action="{{url('admin/update-user')}}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{$users->id}}"/>
                                <div class="card-body">

                               <input type="hidden" class="form-control" name="type" value="3"/>

                                <div class="form-group row">
                                    <label for="title" class="col-sm-2 col-form-label">First Name</label>
                                    <div class="col-sm-10">
                                    <input type="text" class="form-control" id="first_name" name="first_name" value="{{$users->first_name}}" placeholder="First Name"/>
                                    @error('first_name')
                                    <span class="invalid-feedback" role="alert" style="display:block;">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="title" class="col-sm-2 col-form-label">Last Name</label>
                                    <div class="col-sm-10">
                                    <input type="text" class="form-control" id="last_name" name="last_name" value="{{$users->last_name}}" placeholder="Last Name"/>
                                    @error('last_name')
                                    <span class="invalid-feedback" role="alert" style="display:block;">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label for="title" class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                    <input type="text" class="form-control" id="email" name="email" value="{{$users->email}}" placeholder="Email"/>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert" style="display:block;">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    </div>
                                </div>

            
                                <div class="form-group row">
                                    <label for="title" class="col-sm-2 col-form-label">Phone</label>
                                    <div class="col-sm-10">
                                    <input type="text" class="form-control" id="phone" name="mobile" value="{{$users->mobile}}" placeholder="Phone"/>
                                    @error('mobile')
                                    <span class="invalid-feedback" role="alert" style="display:block;">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    </div>
                                </div>

                                
                                {{--  <div class="form-group row">
                                    <label for="title" class="col-sm-2 col-form-label">Address</label>
                                    <div class="col-sm-10">
                                    <input type="text" name="location" class="form-control" id="location" placeholder="Select Location" value="{{$users->location}}">
                                    <input type="hidden" name="latitude" class="form-control" id="latitude" value="{{ old('latitude') }}">
                                    <input type="hidden" name="longitude" class="form-control" id="longitude" value="{{ old('longitude') }}">
                                    @error('location')
                                    <span class="invalid-feedback" role="alert" style="display:block;">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    </div>
                                </div> --}}

                                 {{-- <div class="form-group row">
                                    <label for="title" class="col-sm-2 col-form-label">Zip Code</label>
                                    <div class="col-sm-10">
                                    <input type="text" class="form-control" id="zip_code" name="zip_code" value="{{$users->zip_code}}" placeholder="Zip Code"/>
                                    @error('zip_code')
                                    <span class="invalid-feedback" role="alert" style="display:block;">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    </div>
                                </div> --}}

                                 {{-- <div class="form-group row">
                                    <label for="title" class="col-sm-2 col-form-label">Category</label>
                                    <div class="col-sm-10">
                                     <select class="form-control select2" style="width: 100%;" name="category">
                                        <option value="">Select Category</option>
                                            @foreach($categories as $cat)
                                            <option value="{{$cat->id}}" {{ $cat->id == $users->category_id ? 'selected' : '' }}>{{$cat->name}}</option>
                                            @endforeach
                                        </select> 
                                    @error('category')
                                    <span class="invalid-feedback" role="alert" style="display:block;">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    </div>
                                </div> --}}

                                

                                <div class="form-group row">
                                    <label for="Status" class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10">
                                        <select class="form-control select2" style="width: 100%;" name="status">
                                        <option value="" disabled>Select Status</option>
                                           <option value="1" {{ $users->status == 1  ? 'selected' : ''}}>Active</option>
                                            <option value="0" {{ $users->status == 0  ? 'selected' : ''}}>Inactive</option>
                                            <option value="2" {{ $users->status == 2  ? 'selected' : ''}}>Pending</option>
                                        </select> 
                                        @error('status')
                                        <span class="invalid-feedback" role="alert" style="display:block;">
                                        <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror   
                                </div>


                                </div>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                <button type="submit" class="btn btn-info">Update</button>
                                <a href="{{url('admin/users-list')}}" class="btn btn-default float-right">Cancel</a>
                                </div>
                                <!-- /.card-footer -->
                            </form>
                            </div>

                    </div>
                    <div class="col-1"></div>
                </div>
            </div>
        </section>

    </div>

<!-- </div> -->
@section('after-scripts')
{{-- @include('admin.partials.locationscript'); --}}
@endsection
@endsection