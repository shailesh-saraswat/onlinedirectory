<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Twilio\Rest\Client;
use Twilio\Exceptions\TwilioException;
use App\Models\{User,Template,BusinessInformation,PassportToken,Category,Role,Visitor,Domains,UserSubscription};
use DB;
use Illuminate\Support\Carbon;
use Exception;

class TemplateController extends Controller
{
    public $successStatus = 200;

    /*
    |--------------------------------------------------------------------------
    | ======== API All Template ========
    |--------------------------------------------------------------------------
    |
    | Local Url: http://127.0.0.1:8000/api/v1/getalltemplate
    | Live Url: 
    | Key:token;
    | Method:GET
    | Comments: get all template data by get method.
    |
    |--------------------------------------------------------------------------
    */  

    public function getAllTemplate(Request $request){

        $auth = auth()->user();
        
        if(isset($auth) && $auth != null){

        $template = Template::get();

            $result['code']    = 200;
            $result['error']   = false;
            $result['result']  = $template;
            return response()->json($result);

        } else {

            $result['code']    = 401;
            $result['error']   = true;
            $result['message'] = "Unauthorized";
            return response()->json($result);
        } 

    }


    public function ManageInformation(Request $request)
    {   
        
        try{

            $validation = Validator::make($request->all(),
                [
                    'website_url'     => 'required',
                    'details'         => 'required',
                    'opening_time'    => 'required',
                    'closing_time'    => 'required',
                    'average_charges' => 'required'
                ]
            );

            if ($validation->fails()) {

                $result['code']    = 400;
                $result['error']   = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);

            }

            $auth = auth()->user();
            if(isset($auth) && $auth != null) {
                
            $updateInfo = BusinessInformation::select('id','website_logo','website_url','details','opening_time','closing_time','average_charges','banner1','banner2','banner3','banner4')->where('professional_id',auth()->user()->id)->first();

            if(is_null($updateInfo)) {

                    $bInformation                    = new BusinessInformation;
                    $bInformation->website_url       = $request->website_url;
                    $bInformation->details           = $request->details;
                    $bInformation->opening_time      = $request->opening_time;
                    $bInformation->closing_time      = $request->closing_time;
                    $bInformation->average_charges   = $request->average_charges;
                    $bInformation->professional_id   = auth()->user()->id;
                    $bInformation->save();

                    //return $bInformation; 
                /*  =========    WebSite Logo ====== */

            if ($request->hasFile('website_logo')) {

                try {
                    
                    $bInformation  = BusinessInformation::findOrfail($bInformation->id);
                    $upload_document = $request->file('website_logo');
                    $name = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/websiteLogo/'.$bInformation->id);
                    // Delete folder
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }
                    $upload_document->move($destinationFolder, $name);              
                    $bInformation->website_logo = "public/uploads/businesspartner/websiteLogo/".$bInformation->id.'/' . $name;
                    $bInformation->save();
                
                } catch (Exception $e) {
                    return response()->json($e->getMessage());
                } 
            }

             if ($request->hasFile('banner1')) {

                try {
                    
                    $bInformation=BusinessInformation::findOrfail($bInformation->id);
                    $upload_document = $request->file('banner1');
                    $name = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/Banners/'.$bInformation->id);
                    // Delete folder
                    if(file_exists($destinationFolder.$name)){

                        unlink($destinationFolder.$name);
                    }

                    $upload_document->move($destinationFolder, $name);				
                    $bInformation->banner1 = "public/uploads/businesspartner/Banners/".$bInformation->id.'/' . $name;
                    $bInformation->save();

               } catch (Exception $e) {
                    return response()->json($e->getMessage());
                } 
            }

            if ($request->hasFile('banner2')) {

                try {
                    
                    $bInformation=BusinessInformation::findOrfail($bInformation->id);
                    $upload_document = $request->file('banner2');
                    $name = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/Banners/'.$bInformation->id);

                    // Delete folder
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }

                    $upload_document->move($destinationFolder, $name);				
                    $bInformation->banner2 = "public/uploads/businesspartner/Banners/".$bInformation->id.'/' . $name;
                    $bInformation->save();
                } catch (Exception $e) {
                    return response()->json($e->getMessage());
                } 
            }
            
            if ($request->hasFile('banner3')) {

                try {
                    
                    $upbInformation=BusinessInformation::findOrfail($bInformation->id);
                    $upload_document = $request->file('banner3');
                    $name = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/Banners/'.$bInformation->id);

                    // Delete folder
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }

                    $upload_document->move($destinationFolder, $name);				
                    $bInformation->banner3 = "public/uploads/businesspartner/Banners/".$bInformation->id.'/' . $name;
                    $bInformation->save();
                } catch (Exception $e) {
                    return response()->json($e->getMessage());
                } 
            }

            if ($request->hasFile('banner4')) {

                try {
                    
                    $bInformation=BusinessInformation::findOrfail($bInformation->id);
                    $upload_document = $request->file('banner4');
                    $name = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/Banners/'.$bInformation->id);

                    // Delete folder
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }

                    $upload_document->move($destinationFolder, $name);				
                    $bInformation->banner4 = "public/uploads/businesspartner/Banners/".$bInformation->id.'/' . $name;
                    $bInformation->save();
                } catch (Exception $e) {
                    return response()->json($e->getMessage());
                } 
            }

            $allImages = null;
            if($request->hasFile('sliders')){

            try {

                $r=-1;
                foreach($request->file('sliders') as $index=>$image)
                {  
                    $r++; 
                    $destinationFolder = public_path('uploads/businesspartner/sliders/'.$bInformation->id);
                    $filename = time().$image->getClientOriginalName();
                    // Delete folder
                    
                    if(file_exists($destinationFolder.$filename)){
                        unlink($destinationFolder.$filename);
                    }
                    $image->move($destinationFolder, $filename);
                    $fullPath = $destinationFolder . $filename;
                    $imgpath = "public/uploads/businesspartner/sliders/".$bInformation->id.'/' . $filename;
                    $allImages .= $allImages == null ? $imgpath : ',' . $imgpath;
                    $img[$r]['slider_img'] = $imgpath;
                    $img[$r]['counter'] = $r+1;
                }

                $bInformation->sliders = $allImages;
                $bInformation->save();
            } catch (Exception $e) {
                    return response()->json($e->getMessage());
                } 
            }

                $insertedData = BusinessInformation::select('id','website_logo','website_url','details','opening_time','closing_time','average_charges','banner1','banner2','banner3','banner4')->where('professional_id',auth()->user()->id)->first();
               
                $result['code']      = 200;
                $result['error']     = false;
                $result['message']   = "Information Saved Successfully.";
                /* $result['result']    = $insertedData;
                if($img){
                    $result['sliders']   = $img;
                } else {
                    $result['sliders']   = "null";
                } */
                return response()->json($result);

            /*********=========   
             * 
             * Update tempate
             * 
             * ========= ********/
            } else {

                $validation = Validator::make($request->all(),
                    [
                        'website_url'     => 'required',
                        'details'         => 'required',
                        'opening_time'    => 'required',
                        'closing_time'    => 'required',
                        'average_charges' => 'required'
                    ]
                );

                if ($validation->fails()) {

                    $result['code']    = 400;
                    $result['error']   = true;
                    $result['message'] = $validation->messages()->first();
                    return response()->json($result);

                }
 
                $bInformationUpdate    = BusinessInformation::select('id','website_logo','website_url','details','opening_time','closing_time','average_charges','banner1','banner2','banner3','banner4')->where('professional_id',auth()->user()->id)->first();
 
                $bInformationUpdate->website_url       = $request->website_url;
                $bInformationUpdate->details           = $request->details;
                $bInformationUpdate->opening_time      = $request->opening_time;
                $bInformationUpdate->closing_time      = $request->closing_time;
                $bInformationUpdate->average_charges   = $request->average_charges;
                $bInformationUpdate->update();

            if ($request->hasFile('website_logo')) {

                try {
                        
                    $upload_document = $request->file('website_logo');
                    $name = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/websiteLogo/'.$bInformationUpdate->id);

                    // Delete folder
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }

                    $upload_document->move($destinationFolder, $name);              
                    $bInformationUpdate->website_logo = "public/uploads/businesspartner/websiteLogo/".$bInformationUpdate->id.'/' . $name;
                    $bInformationUpdate->update();

                } catch (Exception $e) {
                    return response()->json($e->getMessage());
                } 
            }
                
            $allImages = null;
            if($request->hasFile('sliders')){
                    
            try {

                $r=-1;
                $img = [];
                foreach($request->file('sliders') as $index=>$image)
                {  
                    $r++; 
                    $destinationFolder = public_path('uploads/businesspartner/sliders/'.$bInformationUpdate->id);
                    $filename = time().$image->getClientOriginalName();
                    // Delete folder
                    
                    if(file_exists($destinationFolder.$filename)){
                        unlink($destinationFolder.$filename);
                    }
                    $image->move($destinationFolder, $filename);
                    $fullPath = $destinationFolder . $filename;
                    $imgpath = "public/uploads/businesspartner/sliders/".$bInformationUpdate->id.'/' . $filename;
                    $allImages .= $allImages == null ? $imgpath : ',' . $imgpath;

                    $img[$r]['slider_img'] = $imgpath;
                    $img[$r]['counter'] = $r+1;
                }

                $bInformationUpdate->sliders = $allImages;
                $bInformationUpdate->update();
            } catch (Exception $e) {
                    return response()->json($e->getMessage());
                } 
            } 
            
            if ($request->hasFile('banner1')) {
               
                try {
                
                    $upload_document = $request->file('banner1');
                    $name = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/Banners/'.$bInformationUpdate->id);

                    // Delete folder
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }

                    $upload_document->move($destinationFolder, $name);				
                    $bInformationUpdate->banner1 = "public/uploads/businesspartner/Banners/".$bInformationUpdate->id.'/' . $name;
                    $bInformationUpdate->update();
                } catch (Exception $e) {
                    return response()->json($e->getMessage());
                } 
            }

            if ($request->hasFile('banner2')) {
               
                try {
                    
                    $upload_document = $request->file('banner2');
                    $name = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/Banners/'.$bInformationUpdate->id);

                    // Delete folder
                    if(file_exists($destinationFolder.$name)){

                        unlink($destinationFolder.$name);
                    }

                    $upload_document->move($destinationFolder, $name);				
                    $bInformationUpdate->banner2 = "public/uploads/businesspartner/Banners/".$bInformationUpdate->id.'/' . $name;
                    $bInformationUpdate->update();
                } catch (Exception $e) {
                    return response()->json($e->getMessage());
                } 
            }
            
            if ($request->hasFile('banner3')) {
                
                try {
                    
                    $upload_document = $request->file('banner3');
                    $name            = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/Banners/'.$bInformationUpdate->id);

                    // Delete folder
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }

                    $upload_document->move($destinationFolder, $name);				
                    $bInformationUpdate->banner3 = "public/uploads/businesspartner/Banners/".$bInformationUpdate->id.'/' . $name;
                    $bInformationUpdate->update();
               } catch (Exception $e) {
                    return response()->json($e->getMessage());
                } 
            }

            if ($request->hasFile('banner4')) {
                
                try {
                    
                    $upbInformation=BusinessInformation::findOrfail($bInformationUpdate->id);
                    $upload_document = $request->file('banner4');
                    $name = time().$upload_document->getClientOriginalName();                       
                    
                    $destinationFolder = public_path('uploads/businesspartner/Banners/'.$bInformationUpdate->id); 
                    // Delete folder
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }

                    $upload_document->move($destinationFolder, $name);				
                    $bInformationUpdate->banner4 = "public/uploads/businesspartner/Banners/".$bInformationUpdate->id.'/' . $name;
                    $bInformationUpdate->update();
                } catch (Exception $e) {
                    return response()->json($e->getMessage());
                }  
            }
                
                $result['code']      = 200;
                $result['error']     = false;
                $result['message']   = "Information Updated Successfully.";
                /*$result['result']    = $updateInfo;
                 if($img){
                    $result['sliders']   = $img;
                } else {
                    $result['sliders']   = "[]";
                } */
                return response()->json($result);
        }
        
        } else {

                $result['code']    = 401;
                $result['error']   = true;
                $result['message'] = "Unauthorized";
                return response()->json($result);
        }

        } catch (Exception $e) {
        return response()->json($e->getMessage());
        }  
    }  


    public function ViewTemplate(){
        try {

            $auth = auth()->user();
            if(isset($auth) && $auth != null) {
                
                $providerDetail = User::where('id',auth()->user()->id)->with('userdetail')->first();
                $subscription = UserSubscription::select('plan_id')->where('user_id',$providerDetail->id)->orderBy('id','desc')->first();
                $domain         = Domains::where('userid',auth()->user()->id)->first();
                $today          = Visitor::where('userid',auth()->user()->id)->whereDate('created_at', Carbon::today())->count();
                $week           = Visitor::where('userid',auth()->user()->id)->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
                $month          = Visitor::where('userid',auth()->user()->id)->whereMonth('created_at', date('m'))
                    ->whereYear('created_at', date('Y'))
                    ->count();

                if($providerDetail->userdetail !=""){

                    $sliderpart = explode(',', $providerDetail->userdetail->sliders);
                    foreach($sliderpart as $index=>$imgpath){ 
                        $img[$index]['slider_img'] = $imgpath;
                        $img[$index]['counter'] = $index+1;
                    }   
                    $result['code']      = 200;
                    $result['error']     = false;
                    $result['today']     = $today;
                    $result['week']      = $week;
                    $result['month']     = $month;
                    $result['domain']    = $domain['domain_name'] ?: [];
                    $result['result']    = $providerDetail;
                    $result['subscription']    = $subscription;
                    $result['sliders']   = $img;
                    return response()->json($result);
                }else{

                    $result['code']      = 200;
                    $result['error']     = false;
                    $result['today']     = $today;
                    $result['week']      = $week;
                    $result['month']     = $month;
                    $result['result']    = $providerDetail;
                    $result['subscription']    = $subscription;
                    $result['sliders']   = [];
                    return response()->json($result);
                }

            } else {

                $result['code']    = 401;
                $result['error']   = true;
                $result['message'] = "Unauthorized";
                return response()->json($result);
            }

        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }  
    }

    public function templateUpdate(Request $request)
    {   
        try {

            $validation = Validator::make($request->all(), [ 
                    'template_id'          => "required|numeric"
                ],
                ['template_id.required' => "Please choose a template"]
            ); 

            if($validation->fails()) {

                $result['code']    = 400;
                $result['error']   = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);
            }

            $auth = auth()->user();

            if(isset($auth) && $auth != null) {

                User::where('id', '=',auth()->user()->id)->update(['template_id' => $request->template_id]); 
        
                $result['code']      = 200;
                $result['error']     = false;
                $result['message']   = 'Template updated successfully.';
                return response()->json($result);

            } else {

                $result['code']      = 401;
                $result['error']     = true;
                $result['message']   = "Unauthorized";
                return response()->json($result);
            }

        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }  
    }

    /* check data manage information */

    public function checkManageInfo(){

        try {

            $user = auth()->user();

            if(isset($user) && $user != null) {

                $infoDetail = User::where('id',$user->id)->with('userdetail')->first();
                
                if($infoDetail->userdetail != null){

                    $result['code']      = 200;
                    $result['error']     = false;
                    $result['result']    = $infoDetail->userdetail;
                    return response()->json($result);

                }  else {

                    $result['code']      = 404;
                    $result['error']     = true;
                    $result['result']    = $infoDetail->userdetail;
                    return response()->json($result);
                } 

            } else {

                $result['code']      = 401;
                $result['error']     = true;
                $result['message']   = "Unauthorized";
                return response()->json($result);
            }

        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }
    }

}
