<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Exception;
use Illuminate\Support\Facades\Validator;
use App\Models\Page;
use App\Models\About;

class PageController extends Controller
{
    
    public function getTermPage(Request $request,$page_id){

        try{
           
            $page = Page::query()->where('id', $page_id)->where('status', 1)->first();
            if(isset($page) && $page != null){
                return view('api.terms',compact('page'));
            } else{

                $result['code'] = 404;
                $result['error'] = true;
                $result['message'] = "Page not found!";

                return response()->json($result);

            }

        }catch(Exception $e){

            return response()->json($e->getMessage());

        }

    }

    public function getPrivacyPage(Request $request,$page_id){

        try{
           
            $page = Page::query()->where('id', $page_id)->where('status', 1)->first();
            if(isset($page) && $page != null){
                return view('api.privacy',compact('page'));
            } else{

                $result['code'] = 404;
                $result['error'] = true;
                $result['message'] = "Page not found!";

                return response()->json($result);

            }

        }catch(Exception $e){

            return response()->json($e->getMessage());

        }

    }

    public function getAboutPage(Request $request){

        try{
           
            $about = About::where('id',1)->first();
            if(isset($about) && $about != null){
                return view('api.about',compact('about'));
            } else{

                $result['code'] = 404;
                $result['error'] = true;
                $result['message'] = "Page not found!";

                return response()->json($result);

            }

        }catch(Exception $e){

            return response()->json($e->getMessage());

        }

    }

}
