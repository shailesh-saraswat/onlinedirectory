@extends('frontend.partials.index')

@section('after-style')

@endsection    

@section('content')

<section class="bigHeader" style="background: url('{{asset('public/frontend/assets/image/banner2.jpg')}}') center center;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="tcent">
					Register
					<span class="smallBredcm"> <a href="{{url('/')}}"> Home </a> >> Register </span>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="login_section">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
			<form action="{{route('user.registeruser')}}" method="post">
            @csrf
				<div class="loginBox">
					
					<div class="">
						<h5 class="login_title" id="exampleModalLabel">Register</h5>
					</div>
					  
				   <div class="">
					<div class="form-group"> <label class="form-label">First Name</label> 
					<input type="text" name="first_name" value="{{old('first_name')}}" class="form-control" placeholder="Enter Your First Name">
					@error('first_name')
					<span class="invalid-feedback" role="alert" style="display:block;">
					<strong>{{ $message }}</strong>
					</span>
					@enderror 
					</div>				   
					
					<div class="form-group"> <label class="form-label">Last Name</label> 
					<input type="text" name="last_name" value="{{old('last_name')}}" class="form-control" placeholder="Enter Your Last Name">
					@error('last_name')
					<span class="invalid-feedback" role="alert" style="display:block;">
					<strong>{{ $message }}</strong>
					</span>
					@enderror 
					</div>	

					<div class="form-group"> <label class="form-label">E-mail</label> 
					<input type="email" name="email" value="{{old('email')}}" class="form-control" placeholder="Enter Your Email"> 
					@error('email')
					<span class="invalid-feedback" role="alert" style="display:block;">
					<strong>{{ $message }}</strong>
					</span>
					@enderror
					</div>

					<div class="form-group"> <label class="form-label">Mobile</label> 
					<input type="text" name="mobile" value="{{old('mobile')}}" class="form-control" placeholder="Enter Your Mobile Number"> 
					@error('mobile')
					<span class="invalid-feedback" role="alert" style="display:block;">
					<strong>{{ $message }}</strong>
					</span>
					@enderror
					</div>

					<div class="form-group"> <label class="form-label">Password</label> 
					<input type="password" name="password" value="{{old('password')}}" class="form-control" placeholder="Enter Your Password">
					@error('password')
					<span class="invalid-feedback" role="alert" style="display:block;">
					<strong>{{ $message }}</strong>
					</span>
					@enderror 
					</div>

					<div class="form-group"> <label class="form-label">Confirm Password</label> 
					<input type="password" name="confirm_password" value="{{old('confirm_password')}}" class="form-control" placeholder="Enter Your Confirm Password">
					@error('confirm_password')
					<span class="invalid-feedback" role="alert" style="display:block;">
					<strong>{{ $message }}</strong>
					</span>
					@enderror 
					</div>					  

				   </div>
				   <div class="customLogRegisWrapper">
					<div class="submit"> 
					{{-- <a class="btn btn-primary btn-block" href="#" data-dismiss="modal">Register</a> --}} 
					<button type="submit" class="btn btn-primary btn-block" data-dismiss="modal">Register</button>
					</div>
				   </div>
				</div>
			</form>
			</div>
		</div>
	</div>
</section>


@endsection 