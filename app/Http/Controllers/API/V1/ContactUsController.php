<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\ContactUs;
use Exception;
use App\Mail\Contact;
use Mail;
class ContactUsController extends Controller
{
    
    public function contact(Request $request){
        
        try{
            
            $validation      = Validator::make($request->all(), [
                'name'       => 'required|max:255',
                'email'      => 'required',
                'subject'    => 'required',
                'message'    => 'required',
            ]);

            if($validation->fails()) {

                $result['code']     = 400;
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);
            }

         $user   = auth()->user();
        if(isset($user) && $user != null) {  
            
            $contact 		        = new ContactUs();
            $contact->name 		    = $request->name;
            $contact->email 	    = $request->email;
            $contact->subject 	    = $request->subject;
            $contact->message       = $request->message;
            $contact->user_id       = auth()->user()->id ? : "";
            if($contact->save()) {
                
                $to = 'atahar.webmobril@gmail.com';
                Mail::to($to)->send(new Contact($contact->name, $request->email, $request->subject, $request->message));

                $result['code']    = 200;
                $result['error']   = false;
                $result['message'] = "Your inquiry has been sent. We will contact you soon.";
                return response()->json($result);

            } else {
                $result['code']    = 500;
                $result['error']   = true;
                $result['message'] = "Your inquiry has been not sent.?";
                return response()->json($result);
            } 
        
        } else {

            $result['code']    = 401;
            $result['error']   = true;
            $result['message'] = "Unauthorized";
            return response()->json($result);
        }

        }catch(Exception $e){

            return response()->json();
        }
        
    }

}
