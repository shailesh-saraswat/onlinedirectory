<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use View;

class ReportController extends Controller
{
    public function userView(Request $request){
        
        $filtertype = $request->type;
        
        if (isset($filtertype) && $filtertype != null) {
            $data = array(
                'filtertype' => 'result_by_date',
                'type' => $filtertype
            );
             $result = $this->userData($data);

        }else{
            
            $result = User::where('roles_id',3)->get();

        }
        return View::make('admin.reports.users', compact('result','filtertype')); 
       

    }

    public function userData($search)
    {
        $type       = isset($search['type']) ? $search['type']:'';
        $filtertype = isset($search['filtertype']) ? $search['filtertype']:'';

        $result = User::where('roles_id',3);

        if ($filtertype === 'result_by_date' || $filtertype === 'export_by_date') {

            if($type == 'today'){
            
                $result->whereDate('created_at', Carbon::today());
                $lists = $result->get();

            }else if($type == 'week'){
                $result->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
                $lists = $result->get();

            }else if($type == 'month'){
            
                $result->whereDate('created_at', '>', Carbon::today()->subDays(30));
                $lists = $result->get();

            }else{
                 $lists = $result->get();
            }
        }else{
            $lists = $result->get();
        }
        return $lists;

       
    }

    /* export User */
    public function exportUser(Request $request){
        
       
        $filtertype = $request->type;

        if (isset($filtertype) && $filtertype != null) {
            $data = array(
                'filtertype' => 'export_by_date',
                'type' => $filtertype
            );
            $userList = $this->userData($data);

        }else {
            $data = array(
                'type' => 'export_all',
            );
            $userList = $this->userData($data);
        }

        $customerscoloumn = array(
           
            "First Name",            
            "Last Name",          
            "Email",            
            "Date",            
            "Status"           
            
        );

        $filename = "user_view_report.csv";
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="' . $filename . '";');
        
        $f = fopen('php://output', 'w');
        fputcsv($f, $customerscoloumn);

        foreach ($userList as $key=>$user) {
            
            $line = array(

                $user['first_name'],
                $user['last_name'],
                $user['email'],
                $user['created_at'],
                $user['status']
               
            );
            fputcsv($f, $line);
        }

    }
    
    /* Professional Export Section */

    public function professionalView(Request $request){
        
        $filtertype = $request->type;
        
        if (isset($filtertype) && $filtertype != null) {
            $data = array(
                'filtertype' => 'result_by_date',
                'type' => $filtertype
            );
             $result = $this->professionalData($data);

        }else{
            
            $result = User::where('roles_id',2)->get();

        }
        return View::make('admin.reports.professional', compact('result','filtertype')); 
    }

    public function professionalData($search) {

        $type       = isset($search['type']) ? $search['type']:'';
        $filtertype = isset($search['filtertype']) ? $search['filtertype']:'';

        $result = User::where('roles_id',2);

        if ($filtertype === 'result_by_date' || $filtertype === 'export_by_date') {

            if($type == 'today'){
            
                $result->whereDate('created_at', Carbon::today());
                $lists = $result->get();

            }else if($type == 'week'){
                $result->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
                $lists = $result->get();

            }else if($type == 'month'){
            
                $result->whereDate('created_at', '>', Carbon::today()->subDays(30));
                $lists = $result->get();

            }else{
                 $lists = $result->get();
            }
        }else{
            $lists = $result->get();
        }
        return $lists;
    }

    /* export Professional */

    public function exportProfessional(Request $request){
        
       
        $filtertype = $request->type;

        if (isset($filtertype) && $filtertype != null) {
            $data = array(
                'filtertype' => 'export_by_date',
                'type' => $filtertype
            );
            $userList = $this->professionalData($data);

        }else {
            $data = array(
                'type' => 'export_all',
            );
            $userList = $this->professionalData($data);
        }

        $customerscoloumn = array(
           
            "First Name",            
            "Last Name",          
            "Email",            
            "Date",            
            "Status"           
            
        );

        $filename = "professional_view_report.csv";
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="' . $filename . '";');
        
        $f = fopen('php://output', 'w');
        fputcsv($f, $customerscoloumn);

        foreach ($userList as $key=>$user) {
            
            $line = array(

                $user['first_name'],
                $user['last_name'],
                $user['email'],
                $user['created_at'],
                $user['status']
               
            );
            fputcsv($f, $line);
        }

    }

}
