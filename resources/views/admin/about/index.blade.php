@extends('admin.index')

@section('content')

<!-- <div class="wrapper"> -->

<div class="content-wrapper">

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active">About</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">About</h3>
            </div>
           
            @if(Session::has('message')) 
            <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div
            >@endif
            
            <div class="col-md-12">
           
            <!-- /.card-header -->
            <div class="card-body">
                <form role="form" action="{{ url('/admin/about/update')}}" method="post" enctype="multipart/form-data"> 
				@csrf

                <input type="hidden" name="aboutid" value="{{$about->id}}">
                <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>{{'Title'}}</label>
                        <input type="text" class="form-control" id="title" name="title" value="{{ !empty($about->title) ? $about->title:'' }}">
                        @error('title')
                        <span class="invalid-feedback" role="alert" style="display:block;">
                        <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                      </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>{{'Short Description'}}</label>
                            <textarea class="form-control" name="sort_description">{{!empty($about->sort_description) ? $about->sort_description:''}}</textarea>
                            @error('sort_description')
                            <span class="invalid-feedback" role="alert" style="display:block;">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>{{'Description'}}</label>
                            <textarea class="form-control" name="description">{{!empty($about->description) ? $about->description:''}}</textarea>
                            @error('description')
                            <span class="invalid-feedback" role="alert" style="display:block;">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                    <div class="form-group">
                        <label>{{'Featured Image'}}</label>
                        <input type="file"  class="form-control" name="featured_image"/>
                        <input type="hidden" name="header" value="{{ !empty($about->featured_image) ? $about->featured_image:'' }}">
                    </div>
                    @if(empty($about->featured_image))
                        <img class="img-circle elevation-2" src="{{ asset('public/siteimages/site/defaultLogo.png') }}" alt="Default Logo" height="80px" width="80px">
                    @else
                    <img class="img-circle elevation-2" src="{!! asset($about->featured_image)!!}" alt="Logo" height="80px" width="80px" id="site_logo"/>
                    
                    @endif
                       
                    </div>
                </div>

                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-info">Save</button>
                    <a href="{{url('admin/dashboard')}}" class="btn btn-default float-right">Cancel</a>
                </div>
                 
                </form>
            
            <!-- /.card -->
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
</div>
</section>

</div>

@section('after-scripts')

@endsection

@endsection