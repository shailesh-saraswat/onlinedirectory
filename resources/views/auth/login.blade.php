@extends('frontend.partials.index')
 
@section('content')

      
<section class="bigHeader" style="background: url('{{asset('public/frontend/assets/image/banner2.jpg')}}') center center;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="tcent">
					Login
					<span class="smallBredcm"> <a href="{{url('/')}}"> Home </a> / Login </span>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="login_section">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<form action="{{route('user.login')}}" method="post" novalidate>
				@csrf
				<div class="loginBox">
					@include('frontend.partials.messages')
					<div class="">
						<h5 class="login_title" id="exampleModalLabel">Login</h5>
					</div>

				   <div class="">
					<div class="form-group"> <label class="form-label">E-mail</label> 
					<input type="email" name="email" class="form-control" value="{{old('email')}}" placeholder="Enter Your Email" > </div>
					<div class="form-group"> <label class="form-label">Password</label>
					<input type="password" name="password" class="form-control" placeholder="Enter Your Password"> </div>
				   </div>
				    <div class="customLogRegisWrapper">
						<div class="submit"> 
							<button class="btn btn-primary btn-block">Login</button>
							{{-- <a class="btn btn-primary btn-block" href="#" data-dismiss="modal">Login</a>  --}}
						</div>
						<p class="text-muted mb-0 mt-1 custom_forgotPwd"><a href="#" class="text-primary ml-1">Forgot Password?</a></p>
				    </div>
				</div>
				</form>
			</div>
		</div>
	</div>
</section>


@endsection 