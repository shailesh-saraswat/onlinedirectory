<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserSubscription;
use App\UserCards;
use Validator;
use Response;
use Session;
use Redirect;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;


class CustomerController extends Controller
{
    public function subscriber_customers(){
        /* $subscription_row_data = UserSubscription::select('user_subscriptions.*',DB::raw("(SELECT email FROM users WHERE users.id = user_subscriptions.cust_id) as customer_email"))->where(['user_subscriptions.status'=>'1'])->get(); */
        $subscription_row_data = DB::table('user_subscriptions')
        ->select('user_subscriptions.*', 'users.email as customer_email','plans.name as plan_name')
        ->leftjoin('users', 'users.id', '=', 'user_subscriptions.cust_id')
        ->leftjoin('plans', 'plans.plan_id', '=', 'plans.plan_id')
        ->where(['user_subscriptions.status'=>'1'])
        ->groupBy('id')
        ->get();

        /* return $subscription_row_data; */
        return view('admin/customer/sub_listing',compact('subscription_row_data'));

    }

}
