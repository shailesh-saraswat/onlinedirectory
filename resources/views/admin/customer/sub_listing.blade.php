@extends('admin.index')

@section('after-style')

    <link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endsection

@section('content')

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                  
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">Subscribed Customers</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Subscribed Customers List</h3>
                            </div>
                           @if(Session::has('customer_message'))
                            <div class="alert alert-info alert-dismissible page_alert">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>{{ Session::get('customer_message') }}</strong>
                            </div>
                            @endif
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="page-list" class="table table-bordered table-striped">
                                    <thead>
                                       <tr>
                                        <th>S.No</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Plan Name</th>
                                        <th>Amount</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        </tr>
                                    </thead>
                                   <tbody>
                                    @php $i=1; @endphp
                                    @foreach($subscription_row_data as $index => $subscription_row_list)
                                        <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$subscription_row_list->customer_name}}</td>
                                        <td>{{$subscription_row_list->customer_email}}</td>
                                        <td>{{$subscription_row_list->plan_name}}</td>
                                        <td>{{$subscription_row_list->total_amount_received}}</td>
                                        <td>{{date('d M Y',strtotime($subscription_row_list->start_date))}}</td>
                                        <td>{{date('d M Y',strtotime($subscription_row_list->renewal_date))}}</td>
                                        </tr>
                                    @php $i++; @endphp
                                    @endforeach
                                    </tbody>
                                </table>
                            </div> 
                        </div>
                    </div>  
                </div>
            </div>
        </section>
    </div>
@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<script>
    $(function () {
        $("#page-list").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });
</script>
<script>
$(document).ready(function(){

    $('td.status').each(function() {
        if ($(this).text() == 'Active') {
            $(this).css('color', 'green');
        }
        else{
            $(this).css('color', 'red');
        } 
    });
});
</script>
@endsection