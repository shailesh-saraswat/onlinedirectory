<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
<title>{{env('APP_NAME', 'Online Directory')}}</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="{{asset('public/plugins/jquery/jquery.min.js')}}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

<link href="{{asset('public/frontend/assets/css/font-awesome.min.css')}}" rel="stylesheet">		
<link href="{{asset('public/frontend/assets/css/bootstrap.min.css')}}" rel="stylesheet" />
<link href="{{asset('public/frontend/assets/css/style.css')}}" rel="stylesheet" />
<link rel="stylesheet" href="{{asset('public/frontend/assets/css/search_styling.css')}}" >
<link rel="stylesheet" href="{{asset('public/frontend/assets/css/accordion.css')}}">
<link rel="stylesheet" href="{{asset('public/frontend/assets/css/custom.css')}}">
<style>
.removeNone { display:unset !important; }
</style>


@yield('after-style')
</head>
<body>
       
@include('frontend.partials.header')
<!-- /.header -->

@yield('content')


@include('frontend.partials.footer')
</body>
</html>
   
   