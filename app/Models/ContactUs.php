<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    protected $table = "contactus";

    protected $fillable = ['user_id', 'name','email','subject', 'message'];

    public function usercontact(){
        return $this->hasMany('App\Models\User', 'id', 'user_id');
    } 
}
