@extends('frontend.professional.partials.index')

@section('content')

<main class="content">
<div class="container-fluid p-0">
<div class="row mb-2 mb-xl-3">
	<div class="col-auto d-none d-sm-block">
		<h3><strong>{{auth()->user()->name ?? ""}}</strong> Subscription Plans</h3>
	</div>
	<div class="col-auto ms-auto text-end mt-n1">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb bg-transparent p-0 mt-1 mb-0">
				<li class="breadcrumb-item"><a href="{{route('business.partner.home')}}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Subscription Plans</li>
			</ol>
		</nav>
	</div>						
</div>
<div class="row">
	<div class="col-xl-12 col-xxl-12 d-flex">
		<div class="card w-100">
			<div class="card-body">
            @if(count($plans) >0)
            <div class="panel-body subscriptionBody">
                @foreach($plans as $plan)
			<div class="planBoxWrapper 
				@php
				if($plan->name == "Option One" || $plan->name == "Option Three" || $plan->name == "Option Five" || $plan->name == "Option Seven"){
					echo "greyBck";
				}else{
					echo " ";
				}
				@endphp 
				">
				
				<div class="planShapeDiv"> 
					<div class="planImageText">
						@if($plan->name == "Option One" || $plan->name == "Option Three" || $plan->name == "Option Five" || $plan->name == "Option Seven")
						<img src="{{asset('public/frontend/assets/image/planShapeGrey.png')}}">
						@elseif($plan->name =="Gold")
						@else
						<img src="{{asset('public/frontend/assets/image/planShape.png')}}">
						@endif
						<div class="planTextAmount">
							<div class="planValue"> {{$plan->name}}</div>
							<div class="planAmount"> ${{$plan->amount}} </div>
						</div>
					</div>
					<div class="planBenifits">
						<div class="planDisp">
							{{$plan->description}}
						</div>
						<a href="{{ route('proceedForStripe', ['id' => encrypt($plan->id)]) }}" class="planBuyButton">Buy</a>
					</div>
				</div>
			</div>
			@endforeach
            </div>									
			@else
            <p>No Search Results Found</p>
            @endif		
			</div>
		</div>
	</div>
</div>				
	</div>
</main>

@endsection

@section('afterjs')

@endsection