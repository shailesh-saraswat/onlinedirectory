@extends('frontend.professional.partials.index')

@section('content')

<main class="content">
<div class="container-fluid p-0">

	<div class="row mb-2 mb-xl-3">
		<div class="col-auto d-none d-sm-block">
			<h3><strong> {{-- {{auth()->user()->name ?? ""}} --}}</strong>Contact Us</h3>
		</div>
		
		<div class="col-auto ms-auto text-end mt-n1">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb bg-transparent p-0 mt-1 mb-0">
					<li class="breadcrumb-item"><a href="{{route('business.partner.profile')}}">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Contact Us</li>
				</ol>
			</nav>
		</div>						
	</div>
	<div class="row">
		<div class="col-xl-7 col-xxl-5 d-flex">
			<div class="card w-100">
				<div class="card-body">
				{{-- <h2> Contact Us </h2> --}}
				@include('frontend.partials.messages')
				<form action="{{route('business.provider.contactus')}}" method="post">
				@csrf
				<div class="panel-body">
					<div class="profile-tab profile-content changepwdPT">										
						<div class="form-group">
							<label for="">Name</label>
							<input type="text" name="name" class="form-control" value="{{old('name')}}" placeholder="Please Enter Name">
						</div>
						<div class="form-group">
							<label for="">Email</label>
							<input type="email" name="email" class="form-control"  value="{{old('email')}}" placeholder="Please Enter Email">
							
						</div>
						<div class="form-group">
							<label for="">Subject</label>
							<input type="text" name="subject" class="form-control"  value="{{old('subject')}}" placeholder="Please Enter Subject">
						</div>

						<div class="form-group">
							<label for="">Message</label>
							<textarea class="form-control" name="message" rows="3" cols="20" class="form-control" placeholder="Please Enter Message">{{old('message')}}</textarea>
						</div>										
					</div>
					<button type="submit" class="btn btn-default btnCustom">Submit</button>									
				</div>
				</form>
			</div>
		</div>
	</div>
</div>				
</div>
</main>
@endsection