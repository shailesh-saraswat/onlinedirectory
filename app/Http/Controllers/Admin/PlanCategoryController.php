<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\{Plans,PlanCategory};
use Validator;
use Response;
use Session;
use Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;

class PlanCategoryController extends Controller
{
    /**
     * Plans Category list page.
     *
     * @param   : none
     * @return  : none
     */
    public function index()
    {
        $plancategory = PlanCategory::orderBy('id', 'desc')->get();
        return view('admin/plancategory/listplancategory',compact('plancategory'));
    }


    /**
    *   ===== View Add Plans Category =====
    **/
    public function addPlanCategory()
    {
        return view('admin/plancategory/addplancategory');
    }


    /**
    *   ===== Update Plans Category =====
    **/

	public function updatePlanCategory($id,Request $request)
	{
		$plancategory_data = PlanCategory::where('id',$id)->first();

		if($plancategory_data)
		{	
			return view('admin/plancategory/editplancategory',compact('plancategory_data'));
		}
		else
		{
			Session::flash ('plan_message', "Plan category not exist.");
			return redirect('/admin/plancategory/list');
		}
	}

    /**
    *   ===== Store and Edit Plans Category =====
    **/

	public function StorePlanCategory(Request $request)
	{
		try {

            $rules  = array(
                'name' => 'required'
            );
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            
            if ($request->has('plan_category_id') && $request->plan_category_id != "") {
                $plancategory = PlanCategory::find($request->plan_category_id);
            } else {
                $plancategory = new PlanCategory();
            }

            if(!empty($plancategory)){
                
                $plancategory->name    = $request->name;
                $plancategory->status  = $request->status;
                $plancategory->save();

                if($request->has('plan_category_id'))
                {
                    Session::flash ('plan_message', "Plan Category Created Successfully.");
                }
                else{
                    Session::flash ('plan_message', "Plan Category Updated successfully.");
                }				
			    return redirect('/admin/plancategory/list');
		    }
			
        } catch(QueryException $ex) { 
            Session::flash ('plan_message', $ex->getMessage());
            return redirect('/admin/plancategory/list');
        }

	}


    /**
    *   ===== Delete Plans Category =====
    **/

    public function deletePlanCategory($id,Request $request)
    {
        $plan_category_data = PlanCategory::where('id',$id)->first();

        if($plan_category_data)
        {
            PlanCategory::where('id',$id)->delete();
            Session::flash ('plan_message', "Plan category deleted successfully.");
            return redirect('/admin/plancategory/list');

        } else {
            Session::flash ('plan_message', "Server error please try again.");
            return redirect('/admin/plancategory/list');
        }
    }
}
