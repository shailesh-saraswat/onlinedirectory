<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;
use App\Models\Reviews;
use App\Models\User;
use App\Models\SiteSetting;
use Exception;
use Validator;
use Session;

class ReviewController extends Controller
{
    public function __construct() {
        $this->getSiteInfo(); 
    }

    public static function getSiteInfo()
    {
        $siteInfo=SiteSetting::first();  
        Session::put('sitedata',$siteInfo);       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function reviewStore(Request $request)
    {   
        
        ///try{
        
        $validation = Validator::make($request->all(), [ 
                'rating'    => "required",
                'message'   => "required"
		]);

        if ($validation->fails()) {
			return  redirect()->back()->withErrors($validation)->withInput();
		} else {
            
            $user   = User::find($request->userID);
            $rating = $user->reviews()->where('user_id', auth()->user()->id)->where('user_type',$request->userID)->first();

        if(isset($rating)){
            Session::flash('message', "You have already made the review");
            return back();
        } else{
            $review 		    = new Reviews();
            $review->message    = $request->message;
            $review->ratings    = $request->rating;
            $review->user_id    = auth()->user()->id;
            $review->user_type  = $request->userID;
            $review->status     = 1;
            
            if($review->save()) {
                Session::flash('log_message', "You have made the review successfully.");
                return back();
            }
        }
        
        }
        /* }catch(\Exception $e){

                return redirect()->back()
                    ->withErrors($e->getMessage());
        }  */

    }

   
}
