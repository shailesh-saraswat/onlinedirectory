<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Plans;
use Validator;
use Response;
use Session;
use Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;
use Cartalyst\Stripe\Stripe;


class PlanController extends Controller
{
    /**
     * Plans list page.
     *
     * @param   : none
     * @return  : none
     */
  public function index()
  {
    $plans = Plans::where('deleted','0')->orderBy('id', 'desc')->get();
    return view('admin/plans/listing',compact('plans'));
  }


  //View Add Plan
  public function addPlan()
  {
    return view('admin/plans/add');
  }


  //Update Plan Data
  public function updatePlan($id,Request $request)
  {
    $plan_data = Plans::where('id',$id)->where('deleted','0')->first();

    if($plan_data)
    {
      return view('admin/plans/edit',compact('plan_data'));
    }
    else
    {
      Session::flash ('plan_message', "Plan not exist.");
      return redirect('/admin/plans');
    }
  }

  //Add Plan Data

  public function store(Request $request)
  {
    try
    {
      if ($request->filled('plan_id')) {
      //   $rules = array(
      //     'name' => 'required|unique:plans,name,'.$request->plan_id,
      //   );
      //   $validator = Validator::make($request->all(), $rules);
      // }
      // else
      // {
        $rules = array(
          'name' => 'required',
          'amount' => 'required|numeric'
        );
        $validator = Validator::make($request->all(), $rules);
      }
    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else
    {
      $plan = new Plans();
    if ($request->has('plan_id') && $request->plan_id!='') 
    {
      $plan = Plans::find($request->plan_id);
    }
      $plan->name=$request->name;
      $plan->amount=$request->amount;
      $plan->interval=$request->interval;
      $plan->description=$request->description;
      $plan->status=$request->status;
      $plan->save();
      $stripe = Stripe::make(env('STRIPE_SECRET'));
      if ($request->has('plan_id') && $request->plan_id!='') {
      $plans = $stripe->plans()->update($plan->plan_id, [
      'nickname' => $request->name,
    ]);
      Session::flash ('plan_message', "Plan updated successfully.");
      return redirect('/admin/plans');
    }
      else
      {
        $plans = $stripe->plans()->create([
        'amount'    => $request->amount,
        'nickname'  => $request->name,
        'currency'  => 'usd',
        'interval'  => $request->interval,
        'product'   => env('STRIPE_PRODUCT_ID'),
        ]);
        if(!empty($plans))
        {
          Plans::where('id',$plan->id)->update(['plan_id'=>$plans['id']]);
        }
          Session::flash ('plan_message', "Plan added successfully.");
          return redirect('/admin/plans');
      }
    }
  }
  catch(QueryException $ex)
  { 
    Session::flash ('plan_message', $ex->getMessage());
    return redirect('/admin/plans');
  }
  }



//Delete Plan Data
  public function delete($id,Request $request)
  {
    $plan_data=Plans::where('id',$id)->first();

  if($plan_data)
  {
    $stripe = Stripe::make(env('STRIPE_SECRET'));
    $stripe->plans()->delete($plan_data->plan_id);
    Plans::where('id',$id)->delete();
    Session::flash ('plan_message', "Plan deleted successfully.");
    return redirect('/admin/plans');
  }
    else
    {
      Session::flash ('plan_message', "Server error please try again.");
      return redirect('/admin/plans');
    }
  }
}
