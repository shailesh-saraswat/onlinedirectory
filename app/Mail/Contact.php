<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Contact extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $name;
    public $fromAddress;
    public $sub;
    public $msg;

    public function __construct($name, $fromAddress, $sub, $msg)
    {
        $this->name         = $name;
        $this->fromAddress  = $fromAddress;
        $this->sub          = $sub;
        $this->msg          = $msg;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        $email = $this->view('emails.contactus')
            ->subject($this->sub)
            ->with([
            'msg'  => $this->msg,
            'name' => $this->name
        ]);
        return $email;
    }
}
