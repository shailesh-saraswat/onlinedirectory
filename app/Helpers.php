<?php
//use DB;
if (!function_exists('get_ratingAvg')) {

    /**
     * @param $provider_id
     * @return mixed
     */
    function get_ratingAvg($provider_id)
    {
        $avg = \App\Models\Reviews::where('user_type',$provider_id)->avg('ratings');
        return isset($avg) ? $avg : 0;
    }

    function count_rating_users($provider_id)
    {
         $avg = \App\Models\Reviews::where('user_type',$provider_id)->count();
        return $avg ? $avg : 0;
    }

     function getDistance($latitude, $longitude, $provider_id) {


        $provider          =       \App\Models\User::where('id',$provider_id);

        $provider          =       $provider->select( DB::raw("6371 * acos(cos(radians(" . $latitude . "))
                                * cos(radians(latitude)) * cos(radians(longitude) - radians(" . $longitude . "))
                                + sin(radians(" .$latitude. ")) * sin(radians(latitude))) AS distance"));
       // $shops          =       $shops->having('distance', '<', 20);
       // $shops          =       $shops->orderBy('distance', 'asc');

        return $provider          =       $provider->first();

       
    }
}