<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $user = new \App\Models\User();
        $user->name = "Admin";
        $user->email = "admin@admin.com";
        $user->mobile = "919999999998";
        $user->password = bcrypt('hello123');
        $user->roles_id = 1;
        $user->status = 1;
        $user->save();
              
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
