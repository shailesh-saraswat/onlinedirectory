<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Response;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
    
       if ($request->is('api/*')) {
            
            abort(response()->json([
                
                'code' => 401,
                'error' => true,
                'message' => 'Unauthenticated'
                
            ]));

        } else {
           return url('/');
        }

        /* if ($request->is('api/*')) {
           
            $array['code'] = 401;
            $array['error'] = true;
            $array['message'] = "Unauthenticated";

            print_r(json_encode($array));

        } else {

        return url('/login');
        } */
    }
}
