@extends('admin.index')

@section('after-style')

    <link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endsection

@section('content')

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
							
                            <li class="breadcrumb-item active">Manage Subscription</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Subscriptions</h3>
                            </div>
                            @if(session('message'))
                                <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
                            @endif
                    
                            <div class="card-body">
                                <table id="accent-list" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Plan Name</th>
                                            <th>Price</th>
                                             <th>Interval</th>
                                             <th>Currency</th>
                                            <th>Status</th>
                                            <th>Action</th>                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                       
                                            @forelse($plans as $data)
                                                <tr>
                                                   <td>{{$data->id}}</td>
                                                   <td>{{$data->subscription}}</td>
                                                   <td>{{$data->price}}</td>
                                                   <td>{{$data->interval}}</td>
                                                   <td>{{$data->currency}}</td>
                                                    <td class="status">{{$data->status == 1 ? 'Active' : 'In-active'}}</td>
                                                    
                                                    <td style="text-align: center;">
                                                        <a href="{{url('admin/subscriptions/edit/'.$data->stripe_price_id)}}"><i class="fas fa-edit"></i>&nbsp;&nbsp;</a>
                                                        <a href="{{url('admin/subscriptions/delete/'.$data->stripe_price_id)}}" onclick="return confirm('Are you sure?')" ><i class="fas fa-trash"></i></a> 
                                                    </td>
                                                </tr>
                                                @empty
                                                <tr><td colspan="7" style="text-align: center;">No Records Found</td></tr>
                                            @endforelse
                                        
                                    </tbody>
                                </table>
                            </div> 
                        </div>
                    </div>  
                </div>
            </div>
        </section>
    </div>
@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<script>
    $(function () {
        $("#specalization-list").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });
</script>
<script>
$(document).ready(function(){

    $('td.status').each(function() {
        if ($(this).text() == 'Active') {
            $(this).css('color', 'green');
        }
        else{
            $(this).css('color', 'red');
        } 
    });
});
</script>
@endsection