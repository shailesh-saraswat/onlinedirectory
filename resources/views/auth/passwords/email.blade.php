@extends('frontend.partials.index')
 
@section('content')

<section class="flat-row page-profile bg-theme">
<div class="container">
{{-- @include('frontend.partials.messages') --}}
<div class="text-center clearfix">
<form method="POST" action="{{url('forget-password')}}">
    <h3 class="title-formlogin">Reset Password</h3>
    @csrf
    <span class="input-login icon-form">
    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Your Email*" name="email" value="{{ old('email') }}" autocomplete="email" autofocus>
    @error('email')
    <span class="invalid-feedback" role="alert" style="display:block;">
    <strong>{{ $message }}</strong>
    </span>
    @enderror

    <span class="wrap-button">
        <button type="submit" id="login-button" class=" login-btn effect-button" title="log in"> Send Password Reset Link</button>
    </span>

</form>
</div>
</div>
</section>


@endsection