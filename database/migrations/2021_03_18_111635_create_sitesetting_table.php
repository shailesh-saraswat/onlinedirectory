<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitesettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sitesetting', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('site_name')->nullable();
            $table->string('site_mobile')->nullable();
            $table->string('header_logo')->nullable();
            $table->string('inner_logo')->nullable();
            $table->string('site_address')->nullable();
            $table->string('site_email1')->nullable();
            $table->string('site_email2')->nullable();
            $table->string('fb_link')->nullable();
            $table->string('twitter_link')->nullable();
            $table->string('youtube_link')->nullable();
            $table->string('pin_link')->nullable();
            $table->string('instagram_link')->nullable();
            $table->string('linkedin_link')->nullable();
            $table->string('site_country')->nullable();
            $table->string('address_city')->nullable();
            $table->string('copy_right')->nullable();
            $table->string('apple_link')->nullable();
            $table->string('playstore_link')->nullable();
            $table->string('aboutus')->nullable();
            $table->string('map_link')->nullable();
            $table->string('account_deactive_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sitesetting');
    }
}
