@extends('frontend.professional.partials.index')

@section('content')

<main class="content">
<div class="container-fluid p-0">

    <div class="row mb-2 mb-xl-3">
        <div class="col-auto d-none d-sm-block">
            <h3><strong>{{auth()->user()->name ?? ""}}</strong> Edit Profile</h3>
        </div>
        <div class="col-auto ms-auto text-end mt-n1">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-transparent p-0 mt-1 mb-0">
                    <li class="breadcrumb-item"><a href="{{route('business.partner.profile')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit Profile</li>
                </ol>
            </nav>
        </div>						
    </div>
    <div class="row">
        <div class="col-xl-7 col-xxl-5 d-flex">
            <div class="card w-100">
                <div class="card-body">
                
                    <div class="panel-body">
                        {{-- @include('frontend.partials.messages') --}}
                        {{-- <h2>
                            Edit Profile 
                        </h2> --}}
                        <form action="{{route('business.partner.updateprofile')}}" method="post" enctype="multipart/form-data">
                        @csrf

                         <input type="hidden" name="profileID" value="{!!$businessPartnerEditProfileData->id!!}">
                            <div class="form-group">
                                <label for="">First Name</label>
                                <input type="text" name="first_name" class="form-control" id="first_name" value="{!!$businessPartnerEditProfileData->first_name!!}" placeholder="Enter Your First Name">
                                @error('first_name')
                                <span class="invalid-feedback" role="alert" style="display:block;">
                                <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="">Last Name</label>
                                <input type="text" name="last_name" class="form-control" id="last_name" value="{!!$businessPartnerEditProfileData->last_name!!}" placeholder="Enter Your Last Name">
                                @error('last_name')
                                <span class="invalid-feedback" role="alert" style="display:block;">
                                <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            
                            <div class="form-group">
                                <label for="">Email</label>
                               <input type="text" name="email" class="form-control" id="email" value="{!!$businessPartnerEditProfileData->email!!}" placeholder="Enter Your Email" readonly>
                                
                            </div>

                            <div class="form-group">
                                <label for="">Business Telephone Number</label>
                                <input type="text" name="mobile" class="form-control" id="mobile" value="{!!$businessPartnerEditProfileData->mobile!!}" placeholder="Enter Your Business Telephone Number">
                                @error('mobile')
                                <span class="invalid-feedback" role="alert" style="display:block;">
                                <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="">Business Name</label>
                                <input type="text" name="business_name" class="form-control" id="business_name" value="{!!$businessPartnerEditProfileData->business_name!!}" placeholder="Enter Your Business Name">
                                @error('business_name')
                                <span class="invalid-feedback" role="alert" style="display:block;">
                                <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            {{-- <div class="form-group">
                                <label for="">Avatar</label>
                                <input type="file" name="avatar" class="form-control" id="avatar">
                                @if($businessPartnerEditProfileData->profile_image)
                                <img src="{!!$businessPartnerEditProfileData->profile_image!!}" class="img-responsive img-rounded" style="width:50px; height:50px; margin-top:10px;"> 
                                @else 
                                <img src="{{asset('public/images/user-logo.svg')}}" style="width:50px; height:50px; margin-top:10px;"> 
                                @endif
                                @error('avatar')
                                <span class="invalid-feedback" role="alert" style="display:block;">
                                <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div> --}}
                            <div class="form-group">
                                <label for="">Address</label>
                                <input type="text" name="location" class="form-control" id="location" value="{!!$businessPartnerEditProfileData->location!!}" placeholder="Enter Your Address">
                                <input type="hidden" name="latitude" class="form-control" id="latitude" value="{{ old('latitude',$businessPartnerEditProfileData->latitude) }}">
                                <input type="hidden" name="longitude" class="form-control" id="longitude" value="{{ old('longitude',$businessPartnerEditProfileData->longitude) }}">
                                @error('location')
                                <span class="invalid-feedback" role="alert" style="display:block;">
                                <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="">Zipcode</label>
                                <input type="number" name="zip_code" class="form-control" id="zip_code" value="{!!$businessPartnerEditProfileData->zip_code!!}" placeholder="Enter Your Zipcode">
                            </div>	
                            <button type="submit" class="btn btn-default btnCustom">Submit</button>
                        </form>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>				
</div>
</main>
@section('after-scripts')
@include('admin.partials.locationscript');

@endsection

@endsection 