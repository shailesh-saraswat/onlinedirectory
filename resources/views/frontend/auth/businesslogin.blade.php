@extends('frontend.partials.index')
 
@section('content')

<section class="bigHeader" style="background: url('{{asset('public/frontend/assets/image/banner2.jpg')}}') center center;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="tcent">
					Business Login
					<span class="smallBredcm"> <a href="{{route('home')}}"> Home </a> / Business Login </span>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="login_section">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<form action="{{route('business.loginstore')}}" method="post" novalidate>
				@csrf
				<div class="loginBox">
					@include('frontend.partials.messages')
					<div class="">
						<h5 class="login_title" id="exampleModalLabel">Business Login</h5>
					</div>

				   <div class="">
					<div class="form-group"> <label class="form-label">E-mail</label> 
					<input type="email" name="email" class="form-control" value="{{old('email')}}" placeholder="Enter Your Email" > </div>
					<div class="form-group"> <label class="form-label">Password</label>
					<input type="password" name="password" class="form-control" placeholder="Enter Your Password"> </div>

				   </div>
				    <div class="customLogRegisWrapper">
					  <div class="submit"> 
							<button class="btn btn-primary btn-block">Login</button>
						</div>
						<p class="text-muted mb-0 mt-1 custom_forgotPwd">
							{{-- @if (Route::has('password.request')) --}}
								<a href="{{ route('business.forgotPassword') }}" class="text-primary ml-1">{{ __('Forgot Your Password?') }}</a>
                            {{-- @endif --}}
						</p>
				    </div>
					<div class="alreadyHaveAccountBP">
						Dont’t have a account?  <a href="{{route('business.register')}}"> Sign Up Business </a>
				   </div>
				</div>
			</div>
		</div>
	</div>
</section>


@endsection 