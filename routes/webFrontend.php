<?php


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;



/*  ====== Front End  =======  */

Auth::routes();

Route::get('/forgot_password', 'CommonController@forgot_password')->name('forgotPassword');
Route::post('/forgot_password', 'CommonController@forgotPass')->name('forgotPass');
Route::post('/reset/password', 'CommonController@resetPassword')->name('resetPassword');
Route::get('/reset/password/{token}', 'CommonController@reset_password_page')->name('reset_password_page');

Route::get('/business/forgot_password', 'CommonController@businessForgotPassword')->name('business.forgotPassword');
Route::post('/business/forgot_password', 'CommonController@businessForgotPass')->name('business.forgotPass');
Route::post('/business/reset/password', 'CommonController@businessresetPassword')->name('business.resetPassword');
Route::get('/business/reset/password/{token}', 'CommonController@businessreset_password_page')->name('business.reset_password_page');


Route::namespace('Frontend')->group(function () {

    /* ====  User Login Routes Start =====  */
    Route::get('/',                                     ['uses'  => 'HomeController@home'])->name('home');
    Route::get('/login',                                ['uses'  => 'FrontendLoginController@loginPage'])->name('login');
    Route::get('/register',                             ['uses'  => 'FrontendLoginController@registerPage'])->name('login.register');
    Route::post('/login',                               ['uses'  => 'FrontendLoginController@login'])->name("user.login");
    Route::post('/registeruser',                        ['uses'  => 'FrontendLoginController@registerUser'])->name("user.registeruser");
    /* ====  User Login Routes End =====  */

    /* ====  Business Login Routes Start =====  */
    Route::get('business-login',                        ['uses'  => 'BusinessPartnerController@businessloginPage'])->name('business.login');
    Route::post('business-login-store',                 ['uses'  => 'BusinessPartnerController@businessloginStore'])->name('business.loginstore');
    Route::get('/business-register',                    ['uses'  => 'BusinessPartnerController@businessRegisterPage'])->name('business.register');
    Route::post('/business-register-store',             ['uses'  => 'BusinessPartnerController@businessRegisterStore'])->name("business.businessregisterstore");
    Route::post('/get-states-by-country',               ['uses'  => 'BusinessPartnerController@getState'])->name("getstatebycountry");
    Route::post('/get-cities-by-state',                 ['uses'  => 'BusinessPartnerController@getCity'])->name("getcitybystate");
    /* ====  Business Login Routes End =====  */
    Route::any('/loadmore/load_data',                   ['uses'  => 'HomeController@load_more_data'])->name('loadmore.load_data');
    Route::any('/category/professional/{id}',           ['uses'  => 'HomeController@CategoryByProfessional'])->name('professional.categorybyprofessional');

    Route::any('/professional/search', ['uses'  => 'HomeController@professionalSearch'])->name('professional.search');

    Route::any('/professional/filterData/', ['uses'  => 'HomeController@filterData'])->name('professional.filterData');

    Route::any('/services/lists/{category}',            ['uses'  => 'HomeController@servicesLists'])->name('services.lists');
    Route::match(['get','post'],'/professional/filter', ['uses'  => 'HomeController@professionalFilter'])->name('professional.filter');
    Route::get('/professional/detail/{id}',             ['uses'  => 'HomeController@professionaldetail'])->name('professional.detail');
    Route::get('/terms-and-conditions',                 ['uses'  => 'HomeController@termAndConditions'])->name('termsAndConditions');
    Route::get('/privacy-policy',                       ['uses'  => 'HomeController@privacyPolicy'])->name('privacyPolicy');
    Route::get('/faqs',                                 ['uses'  => 'HomeController@faqs'])->name('faqs');

    Route::get('/stripe/webhook', 'BusinessPartnerController@captureWebhook')->name('captureWebhook'); 
    Route::post('/webhookevents', 'WebhookController@webhookEvents');

    Route::group(['middleware' => ['auth','isProfessional']], function(){

        Route::get('/business-partner-dashboard',  ['uses'  => 'BusinessPartnerController@index'])->name("business.partner.home");
        Route::get('/bussiness-partner-profile',                ['uses'  => 'BusinessPartnerController@BusinessPartnerProfile'])->name("business.partner.profile");
        Route::get('/business-partner-edit-profile',                ['uses'  => 'BusinessPartnerController@BusinessPartnerEditProfile'])->name("business.partner.editprofile");
        Route::post('/business-partner-update-profile',                ['uses'  => 'BusinessPartnerController@BusinessPartnerUpdateProfile'])->name("business.partner.updateprofile");
        Route::get('/business-partner-change-password',                ['uses'  => 'BusinessPartnerController@BusinessPartnerChangePassword'])->name("business.partner.changepassword");
        Route::post('/business-partner-update-password',                ['uses'  => 'BusinessPartnerController@BusinessPartnerUpdatePassword'])->name("business.partner.updatepassword");
        Route::get('/business-partner-manage-information',                ['uses'  => 'BusinessPartnerController@BusinessPartnerManageInformation'])->name("business.partner.manageinformation");
        Route::post('/business-partner-update-information',                ['uses'  => 'BusinessPartnerController@BusinessPartnerUpdateInformation'])->name("business.partner.updateinformation");
        Route::get('/business-partner-template',                ['uses'  => 'BusinessPartnerController@BusinessPartnerTemplate'])->name("business.partner.template");
        Route::get('/business-partner-subscription',                ['uses'  => 'BusinessPartnerController@BusinessPartnerSubscription'])->name("business.partner.subscription");
        Route::get('/business-template-update',                ['uses'  => 'BusinessPartnerController@templateUpdate'])->name("business.partner.templateupdate");
        Route::get('/business-partner-create-domain',                ['uses'  => 'DomainController@createDomain'])->name("business.partner.createdomain");
        Route::post('/business-add-domain',                ['uses'  => 'DomainController@AddUpdateDomain'])->name("business.partner.addupdatedomain");
        Route::post('/business-logout',                                              ['uses'  => 'BusinessPartnerController@logout'])->name("business.logout");

        Route::get('provider/stripe_payment/{plan_id}', 'BusinessPartnerController@proceedForStripe')->name('proceedForStripe');
        Route::post('provider/getplanbyinterval', ['uses'  => 'BusinessPartnerController@getplanByInterval'])->name("getplanByinterval");
        Route::get('provider/plantype/{id}', 'BusinessPartnerController@SubscriptionPlanType')->name('planType');
        Route::get('success_payment/{session_id}', 'BusinessPartnerController@success_payment')->name('successPayment');
        Route::get('cancel_payment', 'BusinessPartnerController@cancel_payment')->name('cancelPayment');

        Route::get('business-partner-providercontact',    ['uses'  => 'ContactController@ProviderContact'])->name("business.partner.providercontactus");
        Route::post('providercontactus',                  ['uses'  => 'ContactController@providerStoreContact'])->name('business.provider.contactus');
        Route::post('provider/searchprovider', 'ContactController@providerSearch')->name('business.provider.search');
    });


    Route::group(['middleware' => ['auth','isUser']], function(){

        /*  ========   User Routes ==========  */
        Route::get('/contact-us',                  ['uses'  => 'ContactController@contact'])->name('contact');
        Route::post('/contactus',                  ['uses'  => 'ContactController@storeContact'])->name('contactus');
        Route::get('/user/profile',                ['uses'  => 'FrontendLoginController@profile'])->name("user.profile");
        Route::post('/updateprofile',              ['uses'  => 'FrontendLoginController@updateprofile'])->name("user.updateprofile");
        Route::get('/changepassword',              ['uses'  => 'FrontendLoginController@changepassword'])->name("user.changepassword");
        Route::post('/updatepassword',             ['uses'  => 'FrontendLoginController@updatepassword'])->name("user.updatepassword");
        Route::post('/logout',                     ['uses'  => 'FrontendLoginController@logout'])->name("user.logout");
        Route::post('/professional/review',        ['uses'  => 'ReviewController@reviewStore'])->name('professionals.review'); 

    });
});
