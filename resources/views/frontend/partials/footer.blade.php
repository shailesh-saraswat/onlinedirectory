<style>
.row.mt { padding-top: 20px; }
</style>

<div id="footer" class="sticky-footer">
	<!-- Main -->
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-sm-6">
				<img class="footer-logo customLogo" src="{{asset('public/frontend/assets/image/logo.png')}}">
				<br><br>
				<p>@if(!empty(Session::get('sitedata')->aboutus)) {{Session::get('sitedata')->aboutus}} @endif</p>
			</div>

			<div class="col-md-4 col-sm-6 ">
				<h4>Helpful Links</h4>
					@if(Route::has('user.login'))
               		@auth
					@else
				<ul class="footer-links">
					   
					<li><a href="{{url('login')}}">Login</a></li>
					<li><a href="{{route('login.register')}}">Register</a></li>
					<li><a href="{{route('business.register')}}">Sign Up Business</a></li>
				</ul>
					@endauth
               		@endif

				<ul class="footer-links">
					@if(Route::has('user.login'))
               		@auth
					   <li><a href="{{route('business.register')}}">Sign Up Business</a></li>
					@endauth
					@endif
					<li><a href="{{route('faqs')}}">FAQ</a></li>
					<li><a href="{{route('termsAndConditions')}}">Terms and Conditions</a></li>
					<li><a href="{{route('privacyPolicy')}}">Privacy Policy</a></li>
					<li><a href="{{route('contact')}}">Contact</a></li>
				</ul>
				<div class="clearfix"></div>
			</div>		

			<div class="col-md-3  col-sm-12">
				<h4>Contact Us</h4>
				<div class="text-widget">
					<span>@if(!empty(Session::get('sitedata')->site_address)) {{Session::get('sitedata')->site_address}} @endif</span> <br>
					<span class="customFooterPhone"> Phone: </span> <span>@if(!empty(Session::get('sitedata')->site_mobile)) {{Session::get('sitedata')->site_mobile}} @endif</span><br>
					<span class="customFooterEmail"> E-Mail: </span> <span> <a href="#">@if(!empty(Session::get('sitedata')->site_email1)) {{Session::get('sitedata')->site_email1}}@endif</a> </span><br>
				</div>

				<ul class="social-icons margin-top-20">
					<li>
						@if(!empty(Session::get('sitedata')->fb_link))
							<a class="facebook" target="_blank" href="{{Session::get('sitedata')->fb_link}}"><i class="fab fa-facebook-f"></i></a>
						@endif
					</li>
					<li>
						@if(!empty(Session::get('sitedata')->twitter_link))
						<a class="twitter" target="_blank" href="{{Session::get('sitedata')->twitter_link}}"><i class="fab fa-twitter"></i></a>
						@endif
					</li>
					<li>
						@if(!empty(Session::get('sitedata')->instagram_link))
						<a class="instagram" target="_blank" href="{{Session::get('sitedata')->instagram_link}}"><i class="fab fa-instagram"></i></a>
						@endif
					</li>
					<li>
					@if(!empty(Session::get('sitedata')->linkedin_link))  
						<a class="linkedin" target="_blank" href="{{Session::get('sitedata')->linkedin_link}}"><i class="fab fa-linkedin"></i></a>
					@endif
					</li>
				</ul>

			</div>

		</div>
		
		<!-- Copyright -->
		<div class="row mt">
			<div class="col-md-12">
				<div class="copyrights">© {{date('Y')}} @if(!empty(Session::get('sitedata')->copy_right))  
						{{Session::get('sitedata')->copy_right}}
					@endif
				</div>
			</div>
		</div>

	</div>
</div>

<!-- JQuery js--> 
<script src="{{asset('public/frontend/assets/js/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('public/frontend/assets/js/horizontal.js')}}"></script>
<script src="{{asset('public/frontend/assets/js/chosen.min.js')}}"></script>
<script src="{{asset('public/frontend/assets/js/custom.js')}}"></script>
<script src="{{asset('public/frontend/assets/js/front_custom.js')}}"></script>
@yield('after-scripts')
