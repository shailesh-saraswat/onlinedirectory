@extends('frontend.partials.index')
@section('after-style')
<link rel="stylesheet" href="{{asset('public/frontend/assets/css/search_styling.css')}}">
@endsection

@section('content')
<section class="bigHeader" style="background: url('{{asset('public/frontend/assets/image/banner2.jpg')}}') center center;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="tcent">
					Listing
					<span class="smallBredcm"> <a href="{{route('home')}}"> Home </a> >> Listing </span>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="list-search-section">
   <div class="banner-1 cover-image sptb-tab">
      <div class="header-text mb-0">
         <div class="container">
			 <div class="listing-search-input">
				@include('frontend.partials.category')
			</div>
         </div>
      </div>
   </div>
</section>
 
<section class="filter_section">
	<div class="container">
		<div class="row">
			@if($professionals->count() == 0)
				<h4>No Result found.</h4>
			@else 
			<div class="col-md-12">
				<div class="filterWrapper">
					<h6 class="">Showing <b> {{$professionals->count()}} to {{ $professionals->total() }}</b>Entries</h6>	
					<div class="grid_wrapper">
						<input type="hidden" name="latitude1" id="latitude1" value="">
        				<input type="hidden" name="longitude1" id="longitude1" value="">
						<div class="sort_Wrapper">
							<label class="">Sort By:</label> 
						<select class="form-control" name="sort" id="sort" onchange="get_data()">
							<option value="relevance" @if(!empty($_GET['sort']) && $_GET['sort']=='relevance') selected @endif>Relevance</option>
							<option value="nearby"  @if(!empty($_GET['sort']) && $_GET['sort']=='nearby') selected @endif>Nearby</option>
							<option value="rating"  @if(!empty($_GET['sort']) && $_GET['sort']=='rating') selected @endif>Rating</option>
						</select>
					</div>

					</div>		
				</div>
			</div>
			@endif
		</div>
	</div>
</section>
<section class="pagination_section">
	<div class="container">
		<div class="row">
			@if(isset($professionals) && $professionals != null)
            @foreach($professionals as $professional)
			<div class="col-md-6">
				<div class="card overflow-hidden br-0 overflow-hidden">
					<div class="d-sm-flex card-body p-3">
						<div class="p-0 m-0 mr-3">
							<div class=""> 
								<a href="#"></a> 
								@if($professional->website_logo)
									<img class="w-9 h-9" src="{{ asset($professional->website_logo) }}" />
								@else
									<img class="w-9 h-9" src="{{ asset('public/images/user-logo.svg') }}" />
								@endif
							</div>
						</div>
						<div class="item-card9 mt-3 mt-md-5">
							<a href="{{route('professional.detail',$professional->id)}}" class="text-dark">
								<h4 class="font-weight-semibold mt-1">{{ucwords($professional->business_name)}}</h4>
							</a>
							<div class="rating-stars d-inline-flex">
								<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="{{count($professional->reviews)}}"> 
								<div class="rating-stars-container mr-2">
									@for($i = 0; $i < 5; $i++)
									<div class="rating-star sm {{ $professional->average_rating <= $i ? '' : 'is--active' }}"> <i class="fa fa-star"></i> </div>
									@endfor
								</div>
								({{count($professional->reviews)}} Ratings) 
							</div>
						</div>
					</div>
					<div class="card overflow-hidden border-0 box-shadow-0 br-0 mb-0">
						<div class="card-body table-responsive border-top">
							<table class="table table-borderless w-100 m-0 text-nowrap ">
								<tbody class="p-0">
									{{-- <tr>
										<td class="px-0 py-1"><span class="font-weight-semibold">Address</span></td>
										<td class="p-1"><span>:</span></td>
										<td class="p-1"><span>{{$professional->location}}</span></td>
									</tr>
									<tr>
										<td class="px-0 py-1"><span class="font-weight-semibold">Phone No</span></td>
										<td class="p-1"><span>:</span></td>
										<td class="p-1"><span>{{$professional->mobile}}</span></td>
									</tr>
									<tr>
										<td class="px-0 py-1"><span class="font-weight-semibold">Charges</span></td>
										<td class="p-1"><span>:</span></td>
										<td class="p-1"><span>{{$professional->average_charges ? $professional->average_charges : "NA"}}</span></td>
									</tr> --}}
								</tbody>
							</table>
							<div class="mt-3"> <a class="btn btn-light" href="{{route('professional.detail',$professional->id)}}" data-toggle="modal">See Details</a> </div>
						</div>
					</div>
				</div>
			</div>
			@endforeach
            @endif  
			
		
		</div>
	
		<div class="row">
			<div class="col-md-12">
				<div class="pagination_wrapper">
					<ul class="pagination mb-5 mb-lg-0">
						{!! $professionals->appends(Input::except('page','_token'))->render() !!} 
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
