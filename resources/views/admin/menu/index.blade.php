@extends('admin.index')

@section('after-style')

    <link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endsection

@section('content')

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    <h1>{{"Menus"}} ({{ count($menus)}})</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">Menus</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        
                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Create Menu</h3>
                                <a href="javascript:muldelete()" style="float: right;"><i class="fas fa-trash" style="font-size:25px;"></i></a> 
                                <a href="{{url('admin/menu/create')}}" style="float: right;"><i class="fas fa-plus-square" style="font-size:25px;"></i></a>
                            </div>
                            @if(session('message'))
                                <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
                            @endif
                            <!-- /.card-header -->
                            <form id="menu" name="menu" action="" method="post">
                            <div class="card-body">
                                <table id="page-list" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                    <th><input type="checkbox" name="cb1" value="1" onClick="isValid(this.form)">&nbsp;&nbsp;All</th>
                                    <th>Name</th>
                                    <th>Parent</th>
                                    <th>Status</th>
                                    <th>Action</th>                                        
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($menus as $menu)
                                    <tr>
                                    <th scope="col"><input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    <input type="checkbox" name="mul_del[]" id="mul_del[]" value="{{$menu->id}}" /></th>
                                    <td>{{$menu->name}}</td>							            
                                    <td>@if(!empty($menu->parent_menu)) {{$menu->parent_menu['name']}} @endif</td>
                                    <td>
                                    <a href="javascript:" onclick="update_status('{{ $menu->id}}',{{abs($menu->status-1)}})">
                                    <span class="label  @if($menu->status!='0') {{'label-success'}} @else {{'label-warning'}} @endif">
                                        @if($menu->status=='1') {{'Active'}} @else {{'Inactive'}} @endif 
                                    </span>
                                    </a>
                                    </td>
                                    <td><a title="Edit" href="{{url('admin/menu/edit',[$menu->id])}}"><i class="fa fa-pencil"></i></a> </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div> 
                            </form>
                        </div>
                    </div>  
                </div>
            </div>
        </section>
    </div>
@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<script>
    $(function () {
        $("#page-list").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });
</script>
<script>
$(document).ready(function(){

    $('td.status').each(function() {
        if ($(this).text() == 'Active') {
            $(this).css('color', 'green');
        }
        else{
            $(this).css('color', 'red');
        } 
    });
});
</script>

<script type="text/javascript">
/*******Search *********/

/* $("#search").keyup(function(){

var search = $(this).val();              
    $.ajax({
    url: 'menu/search',
    type: 'get',
    data: {search:search},                       
    success:function(response){                          
        $('#rowbody').html(response);                            
        }
});
}); */

/*******Search *********/
  
function isValid(formRef)
{
    for(var i=0;i<formRef.elements.length;i++)
    {
        if(formRef.elements[i].type == "checkbox")
        {
            formRef.elements[i].checked = formRef.cb1.checked
        }
    }
}
function update_status(id,value)
 {     
    $.ajax({
    type: 'GET',
    data: {'id':id,'value':value},
    url: "../admin/menu/status",
    success: function(result){
    alert( 'Update Action Completed.');
   location.reload();
    
    }});
}

function muldelete()
    {
        element_lenght= menu.elements.length;
        alert(element_lenght);
        for(i=0;i<element_lenght;i++)
        {
            if(menu.elements[i].name=="mul_del[]")
            {
                if(menu.elements[i].checked==true)
                {
                    if(confirm("Are you sure delete record(s)?"))
                    {
                        $("#menu").attr("action", "{{url('admin/menu/delete')}}");
                        this.menu.submit();
                        break;
                    }
                }   
            }
        }
    }
</script>
@endsection