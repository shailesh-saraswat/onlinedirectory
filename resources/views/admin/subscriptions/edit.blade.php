@extends('admin.index')

@section('after-style')

<link rel="stylesheet" href="{{asset('public/plugins/summernote/summernote-bs4.css')}}">

@endsection

@section('content')

<div class="content-wrapper">

  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">Master</li>
          </ol>
        </div>
      </div>
    </div>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-outline card-info">
          <div class="card-header">
            <h3 class="card-title">
              Customize Content
            </h3>
    
            <div class="card-tools">
    
            </div>

          </div>
          @include('admin.partials.messages')
    
          <form class="form-horizontal" action="{{url('admin/subscriptions/update',$subscription->stripe_price_id)}}" method="post">
            @csrf
            <input type="hidden" id="id" name="id" value="{{$subscription->stripe_price_id}}" />

            <div class="card-body text">
              <label for="Title" class="col-sm-2 col-form-label">Subscription</label>
              <div class="mb-3">
                <input type="text" class="form-control" id="subscription" name="subscription" placeholder="Title" value="{{$subscription->subscription}}" required  />
              </div>
            </div>
            <div class="card-body text">
                <label for="Title" class="col-sm-2 col-form-label">Price</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="amount" placeholder="Price" name="amount" value="{{$subscription->price}}" required />
                </div>
              </div>
              <div class="card-body text">
                <label for="Status" class="col-sm-2 col-form-label">Interval</label>
                <div class="col-sm-10">
                  <select class="form-control select2" style="width: 100%;" name="interval" required>
                     <option value="day" selected="">Daily</option>
                      <option value="week">Weekly</option>
                      <option value="month">Month</option>
                      <option value="quarter">Every 3 Months</option>
                      <option value="semiannual">Every 6 Months</option>
                      <option value="year">Year</option>
                  </select>    
                </div>
              </div>
           <div class="card-body text">
                <label for="Description" class="col-sm-2 col-form-label">Description</label>
                <div class="col-sm-10">
                  <textarea class="textarea" name="description" id="description" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required>{{$subscription->description}}</textarea>
                </div>
                
              </div>
            <div class="card-body text">
              <label for="Status" class="col-sm-2 col-form-label">Status</label>
              <div class="mb-3">
                <select class="form-control select2" style="width: 100%;" name="status" required>
                  <option value="0" {{$subscription->status == 0  ? 'selected' : ''}}>Inactive</option>
                  <option value="1" {{$subscription->status == 1  ? 'selected' : ''}}>Active</option>
                </select>
              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-info">Update</button>
              <a href="{{url('admin/subscriptions')}}" class="btn btn-default float-right">Cancel</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>

</div>

@endsection

@section('after-scripts')

<script src="{{asset('public/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script>
  $(function() {
    // Summernote
    $('.textarea').summernote()
  })
</script>
<script>
  setTimeout(function() {

    $('.alert-default-danger').fadeOut('fast');

  }, 3000);

  setTimeout(function() {

    $('.alert-default-success').fadeOut(1000);

  }, 3000);
</script>

@endsection