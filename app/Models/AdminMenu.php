<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminMenu extends Model
{
    ///protected $table = "admin_menus";

    public function parent_menu() {
        return $this->belongsTo('App\Models\AdminMenu','parent_menu_id' ,'id');
    }
}
