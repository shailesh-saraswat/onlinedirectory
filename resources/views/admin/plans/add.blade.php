@extends('admin.index')

@section('content')

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                     
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">Add Plan</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10">

                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Add Plan</strong></h3>
                            </div>
                            
                            {{-- <div class="alert alert-danger custom_alert" style="display:none;">
                              <span class="alert_message"></span>
                            </div> --}}
                             <form name="storeForm" id="storeForms" role="form" class="form-horizontal" action="{{route('storePlan')}}" method="post">
  
                              @if ($errors->any())
                                <div class="alert alert-danger alert-dismissible page_alert">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                              @endif
                              @csrf
                                <div class="card-body">

                                <div class="form-group row">
                                    <label for="title" class="col-sm-2 col-form-label">Name</label>
                                    <div class="col-sm-10">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" maxlength="100">
                                    
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="title" class="col-sm-2 col-form-label">Price</label>
                                    <div class="col-sm-10">
                                   <input type="text" class="form-control" id="amount" name="amount" placeholder="Enter Price" onkeypress="check_decimal(this,event);">
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label for="title" class="col-sm-2 col-form-label">Interval</label>
                                    <div class="col-sm-10">
                                    <select class="form-control" id="interval" name="interval">
                                      <option value="month" selected>Monthly</option>
                                      <option value="year">Yearly</option>
                                    </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="title" class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10">
                                    <label>Status<font> *</font></label>
                                    <select class="form-control" id="status" name="status">
                                      <option value="1" selected>Active</option>
                                      <option value="2">Inactive</option>
                                    </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="title" class="col-sm-2 col-form-label">Description</label>
                                    <div class="col-sm-10">
                                   <textarea rows="4" cols="50" class="form-control" name="description" id="description" placeholder="Enter Description"></textarea>
                                    </div>
                                </div>
                                
                                 <!-- /.card-body -->
                                <div class="card-footer">
                                <button type="submit" class="btn btn-info add_btn">Save</button>
                                <a href="{{route('planList')}}" class="btn btn-default float-right">Cancel</a>
                                </div>
                                <!-- /.card-footer -->

                                </div>
                                </div>
                               
                            </form>
                            </div>

                    </div>
                    <div class="col-1"></div>
                </div>
            </div>
        </section>

    </div>

@section('after-scripts')
<script type="text/javascript">
  $('#amount').on("cut copy paste",function(e) {
      e.preventDefault();
  });
  $('.add_btn').on('click',function(){
    var name          = $('#name').val().trim();
    var amount         = $('#amount').val().trim();
    var description         = $('#description').val().trim();
    var error_message = "";
    if(name=='')
    {
      error_message = "Please Enter Name.";
    }
    else if(amount=="")
    {
      error_message = "Please Enter Price.";
    }
    else if(description=="")
    {
      error_message = "Please Enter Description.";
    }
    if(amount!="" && amount<=0)
    {
      error_message = "Please Enter Price Greater Than 0.";
    }
    //alert(error_message);
    if(error_message!='')
    {
      $('.custom_alert').css('display','block');
      $('.alert_message').text(error_message);
      close_alert();
      return false;
    }
    else
    {
      $('#storeForm').submit();
    }
  });
</script>

<script>
function check_decimal(el, event) {
    if(event.which < 46 || event.which > 59) {
        event.preventDefault();
    } 

    if(event.which == 46 && $(el).val().indexOf('.') != -1) {
        event.preventDefault();
    } 
}
</script>
@endsection
@endsection