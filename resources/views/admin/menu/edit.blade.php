@extends('admin.index')

@section('after-style')

    <link rel="stylesheet" href="{{asset('public/plugins/summernote/summernote-bs4.css')}}">

@endsection    

@section('content')

<div class="content-wrapper">
        
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                {{-- <h1>DataTables</h1>--}}
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">Add Menu</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">
                Add Menu
              </h3>
            
            </div>
            {{-- @include('admin.partials.messages') --}}
            <!-- /.card-header -->
            <div class="col-md-12">
                <div class="card-body">
                 <form role="form" action="{!! url('admin/menu/update',[$data->id]) !!}" method="post">
                     <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div class="row">
                    <div class="col-sm-6">
                    <div class="form-group">
                    <label>{{'Name'}}</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ $data->name }}"  placeholder="Name"> 
                    </div>
                    @error('name')
                        <span class="invalid-feedback" role="alert" style="display:block;">
                        <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>

                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>{{'URL'}}</label>
                         <input type="text" class="form-control"id="url" name="url" placeholder="Action" value="{{ $data->url }}"/>
                      </div>
                      @error('url')
                        <span class="invalid-feedback" role="alert" style="display:block;">
                        <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                    <div class="form-group">
                    <label>{{'Icon Class'}}</label>
                    <input type="text" class="form-control" id="icon" name="icon" placeholder="Icon" 
                   value="{{ $data->icon }}"/>
                    </div>
                    @error('icon')
                        <span class="invalid-feedback" role="alert" style="display:block;">
                        <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>

                    <div class="col-sm-6">
                    <div class="form-group">
                    <label>{{'Parent Menu'}}</label>
                    <select class="form-control" name="parent_menu_id" required>
                    <option value="0">Select</option>>
                    @foreach($pmenus as $pmenu)                         
                    <option value="{{$pmenu->id}}" @if($data->parent_menu_id == $pmenu->id) {{ 'selected="true"'}} @endif >{{$pmenu->name}}</option>
                    @endforeach 
                    </select>
                    </div>
                    @error('parent_menu_id')
                        <span class="invalid-feedback" role="alert" style="display:block;">
                        <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                    <div class="form-group">
                    <label>{{'Status'}}</label>
                     <select class="form-control" name="status">
                        <option value="1" @if($data->status=='1') {{ 'selected="true"'}} @endif>Active</option>
                        <option value="0" @if($data->status=='0') {{ 'selected="true"'}} @endif>Inactive</option>
                    </select>
                    </div>
                    @error('priority')
                        <span class="invalid-feedback" role="alert" style="display:block;">
                        <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>

                    <div class="col-sm-6">
                    <div class="form-group">
                    <label>{{'Priority'}}</label>
                    <input type="text" class="form-control" id="priority" name="priority" placeholder="Priority" value="{{ $data->priority}}"/>
                    </div>
                    @error('priority')
                        <span class="invalid-feedback" role="alert" style="display:block;">
                        <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>
                </div>

                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-info">Save</button>
                    <a href="{{url('admin/menu')}}" class="btn btn-default float-right">Cancel</a>
                </div>
                 
                </form>
            
            <!-- /.card -->
            </div>
            </div>

          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->

</div>

@endsection

@section('after-scripts')

<script>
  setTimeout(function() {

    $('.alert-default-danger').fadeOut('fast');

  }, 3000);

  setTimeout(function() {

    $('.alert-default-success').fadeOut(1000);

  }, 3000);
</script>

@endsection