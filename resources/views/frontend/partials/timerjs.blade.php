<script src="{{asset('public/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('public/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('public/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>

<script>
$(function () {
//Timepicker
    $('#timepicker_openingTime').datetimepicker({
        format: 'hh:mm:ss A'
        //format: 'LT'
    })
    $('#timepicker_closingTime').datetimepicker({
        format: 'hh:mm:ss A'
        //format: 'LT'
    })
})
</script>