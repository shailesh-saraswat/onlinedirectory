<nav class="navbar navbar-expand navbar-light navbar-bg">
				
<!-- Responsive Left Nav Hamburger -->

<a class="sidebar-toggle d-flex">
    <i class="hamburger align-self-center"></i>
</a>


<!-- Header Search Bar -->

<form name="searchForm" class="d-none d-sm-inline-block" method="post" action="{{url('provider/searchprovider')}}">
    @csrf
    <div class="input-group input-group-navbar">
        <input type="text" class="form-control" name="plansearch" placeholder="Search Plans" value="{{old('plansearch')}}" aria-label="Search">
        <button class="btn" type="button">
            <i class="align-middle" data-feather="search"></i>
        </button>
    </div>
    @error('plansearch')
    <span class="invalid-feedback" role="alert" style="display:block;">
    <strong>{{ $message }}</strong>
    </span>
    @enderror 
</form>

<div class="navbar-collapse collapse">
    <ul class="navbar-nav navbar-align">
        
        <!-- Header Profile Logout -->
        
        <li class="nav-item dropdown">
            <a class="nav-icon dropdown-toggle d-inline-block d-sm-none" href="#" data-bs-toggle="dropdown">
                <i class="align-middle" data-feather="settings"></i>
            </a>

            <a class="nav-link dropdown-toggle d-none d-sm-inline-block" href="#" data-bs-toggle="dropdown">
                @if (Auth::user()->profile_image)
                <img src="{{ asset(auth()->user()->profile_image) }}" class="avatar img-fluid rounded me-1" alt="Charles Hall" /> 
                 @else
                <img src="{{ asset('public/images/user-logo.svg') }}" class="avatar img-fluid rounded me-1" alt="Charles Hall" /> 
                @endif

                <span class="text-dark">{{auth()->user()->name ?? ""}}</span>
            </a>
            
            <div class="dropdown-menu dropdown-menu-end profileSettingPDiv">
                <a class="dropdown-item" href="{{route('business.partner.profile')}}">
                    <i class="fal fa-user"></i> Profile
                </a>
                <a class="dropdown-item" href="{{route('business.partner.changepassword')}}">
                    <i class="fal fa-key"></i> Change Password
                </a>
                <div class="dropdown-divider"></div>

                <a class="dropdown-item" href="{{ url('business-logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                    <i class="fal fa-sign-out-alt"></i>Log out
                    <form id="logout-form" action="{{ url('business-logout') }}" method="POST" style="display: none;">
                    @csrf
                    </form>
                </a>
            </div>
        </li>
    </ul>
</div>
</nav>