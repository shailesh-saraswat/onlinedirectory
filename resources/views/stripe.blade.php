<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="https://js.stripe.com/v3"></script>

<!-- Create a button that your customers click to complete their purchase. Customize the styling to suit your branding. -->
<button
  style="background-color:#6772E5;color:#FFF;padding:8px 12px;border:0;border-radius:4px;font-size:1em;display:none;"
  id="checkout-button-001"
  role="link"
>
  Checkout
</button>
<input type="hidden" name="plan_id" value="{{$plan_data->plan_id}}">
<div id="error-message"></div>

<script>
var stripe = Stripe("pk_test_51I7W6uA0Jyw2DHYs2rhLJKp8XmVBHUkLhlA3hFyF6Tcd6U85zgUdKe4gzxahaUdsGjeVVYJ95C4M8JUC4GHX2nAw00jshnXgKr");
/* var stripe = Stripe(env('STRIPE_KEY')); */
var checkoutButton = document.getElementById('checkout-button-001');
  checkoutButton.addEventListener('click', function () {
      stripe.redirectToCheckout({
      items: [{plan: "{{$plan_data->plan_id}}", quantity: 1}],
          successUrl: "{{$success_url}}/{{$session_data['id']}}",
          cancelUrl: "{{$cancel_url}}",
          customerEmail: '{{$user->email}}',
      })
    .then(function (result) {
      if (result.error) {
        var displayError = document.getElementById('error-message');
        displayError.textContent = result.error.message;
      }
    });
});
  $('#checkout-button-001').click();
</script>


