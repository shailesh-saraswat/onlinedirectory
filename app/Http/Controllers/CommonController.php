<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Response;
use Session;
use Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
use App\Mail\ForgotPassword;
use App\Mail\UserReg;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Mail;
use DB;
use App\Models\Category;

class CommonController extends Controller
{

    /**
     * forgot password page.
     *
     * @param   : none
     * @return  : none
 	*/
    public function forgot_password(Request $request)
    {   
        $categories     = Category::where('parent_id','=',0)->where('status',1)->orderBy("id", "asc")->get();
        return view('forgot_password',compact('categories'));
    }

    public function forgotPass(Request $request)
    {
        try {
            $rules = array(
                'email' => 'required|email',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            } else {
                $user = User::where('email', $request->email)->first();
                 
                if ($user) {
                    $token = Str::random(60);
                    $link = url('/') . '/reset/password/' . $token . '?email=' . base64_encode($user->email);
                    \DB::table('password_resets')->insert([
                        'email' => $request->email,
                        'token' => $token,
                        'created_at' => Carbon::now()
                    ]);
                    Mail::to($user->email)->send(new ForgotPassword($user->name, $link));
                    Session::flash('message', "Password reset link sent to your email id successfully.");
                    return redirect('/login');
                } else {
                    Session::flash('message', "Email id not exist please try with registered mail id.");
                    return redirect('/forgot_password');
                }
            }
        } catch (QueryException $ex) {
            Session::flash('message', $ex->getMessage());
            return redirect('/forgot_password');
        }
    }

    public function reset_password_page($token, Request $request)
    {   
        $categories     = Category::where('parent_id','=',0)->where('status',1)->orderBy("id", "asc")->get();
        $tokenData = \DB::table('password_resets')
            ->where('token', $token)->first();
        if ($tokenData) {
            $email = $request->email;
            return view('reset_password', compact('token', 'email','categories'));
        } else {
            Session::flash('message', "Token not valid or may be expired please try again.");
            return redirect('/login');
        }
    }

    public function resetPassword(Request $request)
    {

        $validator = Validator::make($request->all(), [   
            'password'         => 'required|between:8,16',
            'confirm_password' => 'required|between:8,16|same:password'
        ]);

        //check if payload is valid before moving on
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $password = $request->password;
        $email = base64_decode($request->reset_email);
        $user = User::where('email', $email)->first();
        if ($user) {
            $user->password = bcrypt($password);
            $user->update(); //or $user->save();
            \DB::table('password_resets')->where('email', $user->email)
                ->delete();
            Session::flash('message', 'Password reset successfully');
            return redirect('/login');
        } else {
            Session::flash('message', 'Network error please try again.');
            return redirect('/login');
        }
    }

    /* ==== business forgot password ==== */

    /**
     * forgot password page.
     *
     * @param   : none
     * @return  : none
 	*/
    public function businessForgotPassword(Request $request)
    {   
        $categories     = Category::where('parent_id','=',0)->where('status',1)->orderBy("id", "asc")->get();
        return view('business_forgot_password',compact('categories'));
    }

    public function businessForgotPass(Request $request)
    {
        try {
            $rules = array(
                'email' => 'required|email',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            } else {
                $user = User::where('email', $request->email)->first();
                 
                if ($user) {
                    $token = Str::random(60);
                    $link = url('/') . '/business/reset/password/' . $token . '?email=' . base64_encode($user->email);
                    \DB::table('password_resets')->insert([
                        'email' => $request->email,
                        'token' => $token,
                        'created_at' => Carbon::now()
                    ]);
                    Mail::to($user->email)->send(new ForgotPassword($user->name, $link));
                    Session::flash('message', "Password reset link sent to your email id successfully.");
                    return redirect('/business-login');
                } else {
                    Session::flash('message', "Email id not exist please try with registered mail id.");
                    return redirect('/business/forgot_password');
                }
            }
        } catch (QueryException $ex) {
            Session::flash('message', $ex->getMessage());
            return redirect('/business/forgot_password');
        }
    }

    public function businessreset_password_page($token, Request $request)
    {   
        $categories     = Category::where('parent_id','=',0)->where('status',1)->orderBy("id", "asc")->get();
        $tokenData = \DB::table('password_resets')
            ->where('token', $token)->first();
        if ($tokenData) {
            $email = $request->email;
            return view('business_reset_password', compact('token', 'email','categories'));
        } else {
            Session::flash('message', "Token not valid or may be expired please try again.");
            return redirect('/business-login');
        }
    }

    public function businessresetPassword(Request $request)
    {
        //Validate input
        $validator = Validator::make($request->all(), [
            'password'         => 'required|between:8,16',
            'confirm_password' => 'required|between:8,16|same:password'
        ]);

        //check if payload is valid before moving on
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $password = $request->password;
        $email = base64_decode($request->reset_email);
        $user = User::where('email', $email)->first();
        if ($user) {
            $user->password = bcrypt($password);
            $user->update(); //or $user->save();
            \DB::table('password_resets')->where('email', $user->email)
                ->delete();
            Session::flash('message', 'Password reset successfully');
            return redirect('/business-login');
        } else {
            Session::flash('message', 'Network error please try again.');
            return redirect('/business-login');
        }
    }

}
