@extends('frontend.partials.index')
@section('after-style')

@endsection

@section('content')
<section class="bigHeader" style="background: url('{{asset('public/frontend/assets/image/banner2.jpg')}}') center center;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="tcent">
					{!! $terms->title !!}
					<span class="smallBredcm"> <a href="{{route('home')}}"> Home </a> >> {!! $terms->title !!} </span>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="tc_section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="privacy-policy-wrapper">
					<div class="content">
						
						{!! htmlspecialchars_decode($terms->description) !!} 

						<p><strong>Effective Date: {!! $terms->updated_at !!}</strong></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


@section('after-scripts')


@endsection

@endsection