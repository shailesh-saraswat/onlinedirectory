@extends('frontend.professional.partials.index')

@section('content')
 
<main class="content">
  @php 
	$provider = auth()->user();
	$now = time(); 
	$your_date = strtotime($provider->free_trial_end);
	$datediff = ($your_date - $now); 
	$finalDays = round($datediff / (60 * 60 * 24));
  @endphp
  {{-- @if($finalDays <=0)
  <p><bold>Access Denied!</bold> Your subscription has been ended !</p>
  @else  --}}
		<div class="container-fluid p-0">
			<div class="row mb-2 mb-xl-3">
				<div class="col-auto d-none d-sm-block">
					<h3><strong>{{auth()->user()->name ?? ""}}</strong> Dashboard</h3>
				</div>
				<div class="col-auto ms-auto text-end mt-n1">
					<nav aria-label="breadcrumb">
						<ol class="breadcrumb bg-transparent p-0 mt-1 mb-0">
							<li class="breadcrumb-item"><a href="{{route('business.partner.home')}}">Home</a></li>
							<li class="breadcrumb-item active" aria-current="page">Dashboard</li>
						</ol>
					</nav>
				</div>	
			</div>
		</div>
		<div class="dropdown-divider"></div>
		
		<div class="row">
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <div class="info-box-content">
                <div class="info-box-text">People Visited Today</div>
                <div class="dynNum"> <div class="info-box-number gradientBox ycolor"><span>{{$today}}</span></div> </div>
              </div>
            </div>
          </div>

          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <div class="info-box-content">
                <div class="info-box-text">People Visited Weekly</div>
                <div class="dynNum"> <div class="info-box-number gradientBox bcolor"><span>{{$week}}</span></div> </div>
              </div>
            </div>
          </div>

          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <div class="info-box-content">
                <div class="info-box-text">People Visited Monthly</div>
                <div class="dynNum"><div class="info-box-number gradientBox gcolor"><span>{{$month}}</span></div></div>
              </div>
            </div>
          </div>

        </div>
    {{-- @endif --}}
</main>
@endsection 