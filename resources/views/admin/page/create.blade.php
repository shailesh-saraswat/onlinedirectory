@extends('admin.index')

@section('after-style')

    <link rel="stylesheet" href="{{asset('public/plugins/summernote/summernote-bs4.css')}}">

@endsection    

@section('content')

<div class="content-wrapper">
        
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
{{--                        <h1>DataTables</h1>--}}
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">CMS</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-outline card-info">
            <div class="card-header">
              <h3 class="card-title">
                Add Page
              </h3>
              
              <!-- tools box -->
              <div class="card-tools">
                <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool btn-sm" data-card-widget="remove" data-toggle="tooltip"
                        title="Remove">
                  <i class="fas fa-times"></i></button>
              </div>
              <!-- /. tools -->
            </div>
            {{-- @include('admin.partials.messages') --}}
            <!-- /.card-header -->
            <form class="form-horizontal" action="{{url('admin/page-add')}}" method="post" enctype="multipart/form-data" novalidate>
              @csrf
              <div class="card-body text">
                <label for="Title" class="col-sm-2 col-form-label">Title</label>
                <div class="mb-3">
                  <input type="text" class="form-control" id="title" placeholder="Title" name="title" value="{{old('title')}}"/>
                    @error('title')
                    <span class="invalid-feedback" role="alert" style="display:block;">
                    <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
              </div>

              <div class="card-body pad">
                <label for="Description" class="col-sm-2 col-form-label">Description</label>
                <div class="mb-3">
                  <textarea class="textarea" name="description" id="description" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required>{{old('description')}}</textarea>
                    @error('description')
                    <span class="invalid-feedback" role="alert" style="display:block;">
                    <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                
              </div>
              
              <div class="card-body text">
                <label for="Document" class="col-sm-2 col-form-label">Feature Image</label>
                <div class="mb-3">
                  <input type="file" class="form-control" id="featured_image" name="featured_image" value="{{old('featured_image')}}"/>
                    @error('featured_image')
                    <span class="invalid-feedback" role="alert" style="display:block;">
                    <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
              </div>
			       
			  
              <div class="card-body text">
                <label for="Status" class="col-sm-2 col-form-label">Status</label>
                <div class="col-sm-10">
                  <select class="form-control select2" style="width: 100%;" name="status" required>
                    <option value="0">Inactive</option>
                    <option value="1">Active</option>
                  </select>    
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-info">Add Page</button>
                <a href="{{url('admin/pages')}}" class="btn btn-default float-right">Cancel</a>
              </div>
            </form>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->

</div>

@endsection

@section('after-scripts')

<script src="{{asset('public/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script>
<script>
  setTimeout(function() {

    $('.alert-default-danger').fadeOut('fast');

  }, 3000);

  setTimeout(function() {

    $('.alert-default-success').fadeOut(1000);

  }, 3000);
</script>

@endsection