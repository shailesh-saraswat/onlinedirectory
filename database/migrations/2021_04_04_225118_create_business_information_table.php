<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_information', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('website_logo')->nullable()->comment('WebSite Logo');
            $table->string('website_url')->nullable()->comment('Website URL');
            $table->text('details')->nullable()->comment('Details');
            $table->string('opening_time')->nullable()->comment('Opening Time');
            $table->string('closing_time')->nullable()->comment('Closing Time');
            $table->string('average_charges')->nullable()->comment('Average Charge');
            $table->string('banner1')->nullable()->comment('Main Banner');
            $table->string('banner2')->nullable()->comment('Second Banner');
            $table->string('banner3')->nullable()->comment('Third Banner');
            $table->integer('professional_id')->nullable()->comment('Professional ID');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_information');
    }
}
