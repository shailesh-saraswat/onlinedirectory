<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('amount');
            $table->string('plan_id')->nullable();
            $table->string('plan_type')->default('1');
            $table->text('description')->nullable();
            $table->enum('interval', ['day','week','month','year'])->default('month');
            $table->integer('update_count')->default('0');
            $table->enum('status', ['1','2'])->default('1')->comment('1=>Active,2=>Inactive');
            $table->enum('deleted', ['1','0'])->default('0')->comment('1=>Yes,0=>No');
            $table->dateTime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->dateTime('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
