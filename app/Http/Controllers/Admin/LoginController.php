<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Role;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Session;

class LoginController extends Controller
{

    public function index(){
        
        $users                 = User::where('roles_id',3)->get();
        $professionals         = User::where('roles_id',2)->get();
        $categories            = Category::all();
        $countUsers            = isset($users) ? count($users) : 0;
        $countProfessionals    = isset($professionals) ? count($professionals) : 0;
        $countCategories       = isset($categories) ? count($categories) : 0;
        return view('admin.dashboard.AdminDashboard')
                ->withCountUsers($countUsers)
                ->withCountProfessionals($countProfessionals)
                ->withCountCategories($countCategories);
    }

    public function loginPage(){
        return view('admin.auth.login');

    }

    public function login(Request $request)
    {

            $rules = array(
                'email'    => 'required|email|max:255',
                'password' => 'required|'
            );

            $remember_me  = $request->has('remember') ? true : false;
            $validator    = Validator::make(Input::all(), $rules);

            if($validator->fails()) {
                return Redirect::to('/admin')
                    ->withErrors("Email and Password is required to login.")
                    ->withInput(Input::except('password'));

            } else {

			$email = $request->email;
            $userdata = array(
                'email'    => Input::get('email'),
                'password' => Input::get('password'),
            );

            if(Auth::attempt($userdata, $remember_me)) {
				$model = User::with('roles')->where('email',$email)->where('status','1')->first();
				//dd($model);
            if(!$model == null){
                //$model->last_login=time();
                $model->save(); 
                    session(['role' => $model->roles->role_text]);
                    Session::put('backenduser',$model);
                if($model->roles->role_text =='admin')
                {					
                    return redirect('admin/dashboard');
                
                }else{
                    Session::forget('role');						
                    Session::forget('backenduser');		
                    return Redirect::to('/admin')
                    ->withErrors("Forbidden! You do not have permission to access this route.");	
                }
            } else{	
                    Auth::logout();
                    Session::flush();
                    $request->session()->flush();
                    return Redirect::to('/admin')
                        ->withErrors("You are not authorize for this section.");				
            }
            
            }else{
                    Auth::logout();
                    Session::flush();
                    $request->session()->flush();
                    return Redirect::to('/admin')
                    ->withErrors("The credentials do not match with our records.");
            }
            
        }
	}

    public function logout(Request $request) {
                Auth::logout();
                Session::flush();
                $request->session()->flush();

                return Redirect::to('/admin')->withMessage('Logged out successfully.');
    }
}
