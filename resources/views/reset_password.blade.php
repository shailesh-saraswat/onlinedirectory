@extends('frontend.partials.index')
 
@section('content')

<section class="bigHeader" style="background: url('{{asset('public/frontend/assets/image/banner2.jpg')}}') center center;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="tcent">
					Reset Password
					<span class="smallBredcm"> <a href="{{route('home')}}"> Home </a> >> Reset Password </span>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="login_section">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="loginBox">
					<div class="">
						<h5 class="login_title" id="exampleModalLabel">Reset Password</h5>
					</div>

					 <form action="{{route('resetPassword')}}" method="post" id="resetPasswordForm" name="resetPasswordForm" class="mt-4">
                      @csrf
                        @if(Session::has('message'))
                          <p class="alert alert-danger" id="alert_box">{{ Session::get('message') }}</p>
                        @endif
                        @if(Session::has('log_message'))
                          <p class="alert alert-success" id="alert_box">{{ Session::get('log_message') }}</p>
                        @endif
                        @if ($errors->any())
                          <div class="alert alert-danger alert-dismissible page_alert">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <ul>
                                  @foreach ($errors->all() as $error)
                                      <li>{{ $error }}</li>
                                  @endforeach
                              </ul>
                          </div>
                        @endif
                        <input type="hidden" name="reset_token" value="{{$token}}">
                        <input type="hidden" name="reset_email" value="{{$email}}">

                        <div class="alert alert-danger alert-dismissible custom_alert" style="display: none;">
                                <span class="alert_message"></span>
                            </div>  
				   <div class="">
					  <div class="form-group"> <label class="form-label">Password</label> 
                 <input type="password" name="password" id="password" class="form-control" placeholder="Password"> </div>
				   </div>

                <div class="">
					  <div class="form-group"> <label class="form-label">Confirm Password</label> 
                 <input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Confirm Password"> </div>
				   </div>

				   <div class="customLogRegisWrapper">
					  <div class="submit"> <a class="btn btn-primary btn-block reset_btn">Reset</a> </div>
				   </div>
				</div>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
  $('.reset_btn').on('click',function(){
    var password         = $('#password').val();
    var confirm_password         = $('#confirm_password').val();
    var error_message = "";
    if(password=="")
    {
      error_message = "Please Enter Password.";
    }
    else if(confirm_password=="")
    {
      error_message = "Please Enter Confirm Password.";
    }
    else if(confirm_password!=password)
    {
      error_message = "Password not matched.";
    }
    if(error_message!='')
    {
      $('.custom_alert').css('display','block');
      $('.alert_message').text(error_message);
      close_alert();
      return false;
    }
    else
    {
      $('#resetPasswordForm').submit();
    }
  });
</script>
@endsection