@extends('frontend.partials.index')

@section('content')
 
<section class="bannersec">
   <div class="banner-1 cover-image sptb-3 pb-14 sptb-tab bg-background2" data-image-src="{{asset('public/frontend/assets/image/banner1.jpg')}}" style="background:url({{asset('public/frontend/assets/image/banner1.jpg')}}); ">
      <div class="header-text mb-0">
         <div class="container">
            <div class="text-center text-white mb-7">
               <h1 class="mb-1">Find The Best ipsum standard Future</h1>			
               <p>The point of using lorem ipsum is that normal distracted by the readable</p>
            </div>
            <div class="main-search-input">
               @include('frontend.partials.category')
            </div>

         </div>
      </div>
   </div>
</section>


<section class="sptb">
    <div class="container">
     	<div class="section-title center-block text-center">
			<h1>All Services</h1>
			<p>Mauris ut cursus nunc. Morbi eleifend, ligula at consectetur vehicula</p>
      	</div>
			<div class="item-all-cat center-block text-center education-categories">
				<div class="row" id="post_data">
				{{ csrf_field() }}
				</div>
				<div class="mt-4" id="loadMoreBTN"> </div>
			</div>
   	</div>
</section>

<!-- Recommended Service -->
 
<section class="about-with-promo pb-100" id="aboutus">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-lg-6">
				<div class="about-content-wrap">
					<strong class="color-secondary">About Us</strong>
					<h2>{!! $about->title !!}</h2>
					<span class="animate-border mb-4"></span>
					<p>{!! $about->sort_description !!}</p>

					<ul class="check-list-info">
						{!! $about->description !!}
					</ul>

				</div>
			</div>
			<div class="col-lg-6">
				<div class="about-us-img">
					@if($about->featured_image)
					<img src="{{asset($about->featured_image)}}" alt="{{asset($about->featured_image)}}" class="img-fluid about-single-img">
					@else
					<img src="" alt="Not Uploaded" height="150" width="180" style="margin-top:25px"> 
					@endif
					
				</div>
			</div>
		</div>
	</div>
</section>


<!-- FAQ -->


<section class="sptb">
	<div class="container">
	   	<h2>Frequently Asked Questions</h2>
	   	<div class="accordion">
			@if(count($faqs) > 0)
			@php $i=1; @endphp
			@foreach($faqs as $key=>$faq)
			<div class="accordion-item">
				<button id="accordion-button-{{$key}}" aria-expanded="false"><span class="accordion-title">{{$faq->question}}</span><span class="icon" aria-hidden="true"></span></button>
				<div class="accordion-content">
					<p>{!!$faq->answer!!}</p>
				</div>
			</div>
			@endforeach
			@endif
		  
	   </div>
	</div>
</section>


<div class="frequently">
 <div class="container">
	<h2>Get connected with online directory</h2>
	<a href="{{route('contact')}}">Contact Us</a>
	<p>For more information about Online Directory for your business or Category needs, go to Category Business.</p>
 </div>
</div> 


@endsection 
