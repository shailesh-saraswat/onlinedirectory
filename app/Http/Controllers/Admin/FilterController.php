<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;

class FilterController extends Controller
{
    public function index(Request $request){
        
        if ($request->ajax()) {

            $status   = $request->input('status');
            $verified = $request->input('verified');
            $device   = $request->input('device_type');

            $query = User::query()->where('type', '=', 1);
            
            if($request->filled('device_type')) {
                $query->where('device_type', $request->get('device_type'));
            }
            if ($request->filled('verified')) {
                $query->where('verified', $request->get('verified'));
            }
            if ($request->filled('status')) {
                $query->where('status', $request->get('status'));
            }

            $users = $query->get();
            return response()->json($users);
           
        }
    }
}
