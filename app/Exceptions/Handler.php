<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception) { 

        if ($exception instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
            
            if ($request->is('api/*')) {
                
                return response()->json([
                        'code'    => 404,
                        'error'   => true,
                        'message' => 'URL Not Found'
                    ]
                );
            }

            return response()->view('errors.404', [], 404);
        }

        if ($exception instanceof \Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException) {
            
            if ($request->is('api/*')) {
                
                return response()->json([
                        'code'    => 405,
                        'error'   => true,
                        'message' => 'Method not allowed.'
                    ]
                );
            }

            return response()->view('errors.405', [], 405);
        }
        
        if ($exception instanceof \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException) {
            
            if ($request->is('api/*')) {
                
                return response()->json([
                        'code'    => 401,
                        'error'   => true,
                        'message' => 'Unauthorized.'
                    ]
                );
            }

            return response()->view('errors.401', [], 401);
        }

        if ($exception instanceof ModelNotFoundException) {
            
            if ($request->is('api/*')) {
                
                return response()->json([
                        'code'    => 500,
                        'error'   => true,
                        'message' => 'Server error.'
                    ]
                );
            }

            return response()->view('errors.500', [], 500);
        }

        if ($exception instanceof \ErrorException) {
            
            if ($request->is('api/*')) {
                
                return response()->json([
                        'code'    => 500,
                        'error'   => true,
                        'message' => 'Server error.'
                    ]
                );
            }

            return response()->view('errors.500', [], 500);
        }
        return parent::render($request, $exception);
    }
}
