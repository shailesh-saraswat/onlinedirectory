@extends('frontend.professional.partials.index')

@section('content')

<main class="content">
<div class="container-fluid p-0">
<div class="row mb-2 mb-xl-3">

	<div class="col-auto d-none d-sm-block">
		<h3><strong></strong> Subscription Plan Type</h3>
	</div>

	<div class="col-auto ms-auto text-end mt-n1">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb bg-transparent p-0 mt-1 mb-0">
				<li class="breadcrumb-item"><a href="{{route('business.partner.home')}}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Subscription Plan Type</li>
			</ol>
		</nav>
	</div>
    
    <div class="row">
		<div class="col-xl-7 col-xxl-5 d-flex">
			<div class="card w-100">
				<div class="card-body">
                {{-- <form action="{{ route('proceedForStripe', encrypt($plantype)) }}" method="post">
				@csrf --}}
				<div class="panel-body">
					<div class="profile-tab profile-content changepwdPT">										
						<div class="form-group">
							<label for="">Choose Plan</label>
                            <select class="form-control" name="plan_name" id="plans">
                                @foreach($plans as $plan)
                                <option value="{{$plan->id}}">{{ucwords($plan->interval)}}</option>
                                @endforeach
                            </select>
						</div>
						   {{--  <button type="submit" class="btn btn-default btnCustom">Submit</button>	 --}}						
					</div>
					{{-- </form>	 --}}							
				</div>
			</div>
		</div>
	</div>

		<div class="card w-100">
			<div class="card-body">
				<div class="panel-body subscriptionBody">
				<div class="planBoxWrapper {{-- greyBck --}}">
				<div class="planShapeDiv"> 
					<div class="planImageText">
							<img src="{{-- {{asset('public/frontend/assets/image/planShape.png')}} --}}">
						<div class="planTextAmount">
							<div class="planValue" id="interval_plan_name"></div>
							<div class="planAmount" id="interval_plan_price"></div>
						</div>
					</div>
					<div class="planBenifits">
						<div class="planDisp" id="interval_plan_description"></div>
						<a href="{{ route('proceedForStripe', ['id' => encrypt($plantype)]) }}" class="planBuyButton">Buy</a> 
					</div>
				</div>
			</div>
						
			</div>									
			</div>
		</div>
    
</div>				
</div>
</main>

@endsection
@section('after-scripts')
<script>
$(document).ready(function() {

	$('#plans').on('change', function() {
		var plan_id = this.value;
		$("#plan_data").html('');
		var APP_URL = "{{url('/')}}";
		$.ajax({
			url:"{{url('provider/getplanbyinterval')}}",
			type: "POST",
			data: {
			plan_id: plan_id,
			_token: '{{csrf_token()}}'
		},
		dataType : 'json',
		success: function(result){
			$.each(result.plans,function(key,value){
				if(value.interval == "year"){
					 $('.planBoxWrapper').addClass('greyBck');
					 $('.planBoxWrapper').removeClass('default');
					 $(".planImageText img").attr("src",APP_URL+'/public/frontend/assets/image/planShapeGrey.png');
					}else if(value.interval == "month"){
					$(".planImageText img").attr("src",APP_URL+'/public/frontend/assets/image/planShape.png');
					 $('.planBoxWrapper').removeClass('greyBck');
					 $('.planBoxWrapper').addClass('default');
				}
				$('#interval_plan_id').html(value.plan_id);
				$('#interval_plan_name').html(value.name);
				$('#interval_plan_price').html("$"+value.amount);
				$('#interval_plan_description').html(value.description);
			});  
		}
		});
	});  
	$('#plans').trigger("change");
});
</script>
@endsection