<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes(['register' => false]);

Route::namespace('Admin')->group(function () {
    Route::group(['prefix' => 'admin'], function(){

        Route::get('/', ['as' => 'login', 'uses' => 'LoginController@loginPage']);

        Route::post('/login', ['uses' => 'LoginController@login'])->name("Admin.Login");
        
        Route::group(['middleware' => ['admin']], function(){
            
            Route::post('/logout', ['uses' => 'LoginController@logout'])->name("Admin.Logout");
            /* ===== Admin Route  ====  */

            Route::get('/dashboard', 'LoginController@index')->name('admin.Dashboard');
            Route::get('/profile', 'AdminController@profile')->name('admin.Profile');
            Route::get('/change-password-view', 'AdminController@changePasswordView')->name('admin.ChangePassword.View');
            Route::get('/get-profile', 'AdminController@getProfile')->name('admin.Get.Profile');
            Route::post('/change-password', 'AdminController@changePassword')->name('admin.ChangePassword');
            Route::post('/update-profile', 'AdminController@updateProfile')->name('admin.Update.Profile'); 

            /* ===== Users Route  ====  */

            Route::get('/users-list', 'UsersController@index')->name('admin.Users.List');
            Route::get('/add-user-form', 'UsersController@create')->name('admin.Add.User.Form');
            Route::post('/add-user', 'UsersController@store')->name('admin.Add.User');  
            Route::get('/edit-user/{id}', 'UsersController@edit')->name('admin.Get.User');
            Route::post('/update-user', 'UsersController@update')->name('admin.Update.User');
            Route::get('/delete-user/{id}', 'UsersController@destroy')->name('admin.Delete.User');
            Route::get('/show/user/{id}','UsersController@show')->name('admin.Show.User');
            
            /* ===== Categories Route  ====  */
            
            Route::get('/categories-list', 'CategoryController@index')->name('admin.Categories.List');
            Route::get('/category-add', 'CategoryController@create')->name('admin.Category.Add.View');
            Route::get('/category-edit/{id}', 'CategoryController@edit')->name('admin.Category.Edit');
            Route::get('/category-delete/{id}', 'CategoryController@destroy')->name('admin.Category.Delete');
            Route::post('/category-add', 'CategoryController@store')->name('admin.Category.Add');
            Route::post('/category-update', 'CategoryController@update')->name('admin.Category.Update');
            Route::get('/show/category/{id}','CategoryController@show')->name('admin.Show.Category');
            
            /* ===== Professional List  ====  */
            
            Route::get('/professional-list', 'UsersController@ProfessionalList')->name('Professional.List');
            Route::get('/add-professional-form', 'UsersController@professionalcreate')->name('Add.Professional.Form');
            Route::post('/add-professional', 'UsersController@professionalstore')->name('Add.Professional');  
            Route::get('/edit-professional/{id}', 'UsersController@professionaledit')->name('Get.Professional');
            Route::post('/update-professional', 'UsersController@professionalupdate')->name('Update.Professional');
            Route::get('/delete-professional/{id}', 'UsersController@professionaldestroy')->name('Delete.Professional');
            Route::get('/show/professional/{id}','UsersController@ProfessionalShow')->name('admin.Show.Professional');
            Route::get('/show/visitor/{id}','UsersController@VisitorsShow')->name('admin.Show.Visitor');
            
            /* ===== Pages Route  ====  */ 

            Route::get('/pages', 'PageController@index')->name('admin.Pages.List');
            Route::get('/page-edit/{id}', 'PageController@edit')->name('admin.Page.Edit');
            Route::get('/page-delete/{id}', 'PageController@delete')->name('admin.Page.Delete');
            Route::get('/add-page-form', 'PageController@create')->name('admin.Page.Get');
            Route::post('/page-update', 'PageController@update')->name('admin.Page.Update');
            Route::post('/page-add', 'PageController@store')->name('admin.Page.Add');

            /**********************Backend Sitesetting Management ***************/

            Route::get('/sitesetting','SiteSettingController@index')->name('admin.siteSetting.list');
            Route::get('/sitesetting/remove_headerimage','SiteSettingController@remove_headerimage')->name('admin.siteSetting.headerimage');
            Route::get('/sitesetting/remove_footerimage','SiteSettingController@remove_footerimage')->name('admin.siteSetting.footerimage');
            Route::post('/sitesetting/update','SiteSettingController@update')->name('admin.siteSetting.update');

            /**********************Backend About Home Page ***************/

            Route::get('/about','AboutController@index')->name('admin.about.list');
            Route::get('/about/remove_footerimage','AboutController@remove_featuredimg')->name('admin.about.featuredimage');
            Route::post('/about/update','AboutController@update')->name('admin.about.update');

            /***********Backend Menu Management *********/
           
            Route::get('/menu','BackMenuController@index')->name('admin.menu.list');
            Route::get('/menu/create','BackMenuController@create')->name('admin.menu.create');
            Route::post('/menu/store','BackMenuController@store')->name('admin.menu.store');
            Route::get('/menu/edit/{slug?}','BackMenuController@edit')->name('admin.menu.edit');
            Route::post('/menu/update/{slug?}','BackMenuController@update')->name('admin.menu.update');
            Route::post('/menu/delete','BackMenuController@distroy')->name('admin.menu.delete');
            Route::get('/menu/status','BackMenuController@status')->name('admin.menu.status');

            /**********************Backend FAQ Management ********/	
            Route::get('/faq','FAQController@index')->name('admin.faq.list');
            Route::get('/faq/create','FAQController@create')->name('admin.faq.create');
            Route::post('/faq/store','FAQController@store')->name('admin.faq.store');
            Route::get('/faq/view/{id?}','FAQController@view')->name('admin.faq.view');
            Route::get('/faq/edit/{id?}','FAQController@edit')->name('admin.faq.edit');
            Route::post('/faq/update/{id?}','FAQController@update')->name('admin.faq.update');
            Route::get('/faq/delete/{id}','FAQController@destroy')->name('admin.faq.destroy');
            Route::get('/faq/status','FAQController@status')->name('admin.faq.status');

            /********************** Search on dashboard Route ********/	
            Route::post('/search', 'SearchController@index')->name('admin.search');

            /**********************Backend Report Management ********/
            Route::get('/userview','ReportController@userView')->name('admin.reports.userview');
            Route::get('/professionalview','ReportController@professionalView')->name('admin.reports.professionalview');
            Route::get('user-export','ReportController@exportUser')->name('exportUser');
            Route::get('professional-export','ReportController@exportProfessional')->name('exportProfessional');
            

            /********************** Plans Routes *************************/

            Route::get('plans', 'PlanController@index')->name('planList');
            Route::get('plans/add', 'PlanController@addPlan')->name('addPlan');
            Route::post('plans/store', 'PlanController@store')->name('storePlan');
            Route::get('plans/edit/{id}', 'PlanController@updatePlan')->name('updatePlan');
            Route::get('plans/delete/{id}', 'PlanController@delete')->name('deletePlan');
            Route::get('subscriber_customers', 'CustomerController@subscriber_customers')->name('subscriberCustomersList');

            /********************** Plans Category Routes *************************/

            Route::get('plancategory/list', 'PlanCategoryController@index')->name('PlanCategoryList');
            Route::get('plancategory/add', 'PlanCategoryController@addPlanCategory')->name('addPlanCategory');
            Route::post('plancategory/store', 'PlanCategoryController@StorePlanCategory')->name('StorePlanCategory');
            Route::get('plancategory/edit/{id}', 'PlanCategoryController@updatePlanCategory')->name('updatePlanCategory');
            Route::get('plancategory/delete/{id}', 'PlanCategoryController@deletePlanCategory')->name('deletePlanCategory');


            /********************** Contact Us Routes *************************/
            Route::get('contact/list', 'ContactController@index')->name('contactList');
            Route::get('contact/view/{id}', 'ContactController@ContactView')->name('contactView');

        });
    });
});