@extends('frontend.professional.partials.index')
@section('after-style')

@include('frontend/partials/timercss')

@endsection
@section('content')
<main class="content">
				<div class="container-fluid p-0">

					<div class="row mb-2 mb-xl-3">
						<div class="col-auto d-none d-sm-block">
							<h3><strong>{{auth()->user()->name ?? ""}}</strong> Manage Informations</h3>
						</div>
						<div class="col-auto ms-auto text-end mt-n1">
							<nav aria-label="breadcrumb">
								<ol class="breadcrumb bg-transparent p-0 mt-1 mb-0">
									<li class="breadcrumb-item"><a href="{{route('business.partner.profile')}}">Home</a></li>
									<li class="breadcrumb-item active" aria-current="page">Manage Informations</li>
								</ol>
							</nav>
						</div>						
					</div>
					<div class="row">
						<div class="col-xl-7 col-xxl-5 d-flex">
							<div class="card w-100">
								@include('frontend.partials.messages')
                                {{-- @if(Session::has('provider_message'))
                                    <div class="alert alert-info alert-dismissible page_alert">
                                        <a href="#" class="close" data-dismiss="alert"></a>
                                        <strong>{{ Session::get('provider_message') }}</strong>
                                    </div>
                                @endif --}}
								<div class="card-body">
									<h2> Manage Information </h2>
									<div class="panel-body">
										<form action="{{route('business.partner.updateinformation')}}" method="post" enctype="multipart/form-data">
										@csrf 

                                        <div class="form-group customBannerStyling">
                                            <label for="exampleInputFile">Website Logo</label>
                                            <input type="file" id="website_logo" name="website_logo">	
										</div>	

                                        @if(!empty($businessInfo->userdetail->website_logo))
                                        <img src="{{$businessInfo->userdetail->website_logo ?: '' }}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
                                        @else 
                                        <img src="{{asset('public/uploads/default.jpeg')}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
                                        @endif

                                        <div class="form-group">
                                            <label for="">Website</label>
                                            <input type="text" class="form-control" name="website_url" id="website_url" placeholder="Enter Your Website Url" value="{{old('website_url') ?? $businessInfo->userdetail->website_url ?? ''}}">
                                        </div>

                                        <div class="form-group">
                                            <label for="">Details</label>
                                            <div>
                                            <textarea class="form-control" rows="3" name="details" placeholder="Details">{{old('details') ?? $businessInfo->userdetail->details ?? ''}}</textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="">Opening Time</label>
                                            <div class="input-group date" id="timepicker_openingTime" data-target-input="nearest">
                                            <input type="text" value="{{old('opening_time') ??    $businessInfo->userdetail->opening_time ?? ''}}" name="opening_time" class="form-control datetimepicker-input" data-target="#timepicker_openingTime"/>
                                            <div class="input-group-append" data-target="#timepicker_openingTime" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="far fa-clock"></i></div>
                                            </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="">Closing Time</label>
                                           <div class="input-group date" id="timepicker_closingTime" data-target-input="nearest">
                                            <input type="text" value="{{old('closing_time') ?? $businessInfo->userdetail->closing_time ?? ''}}" name="closing_time" class="form-control datetimepicker-input" data-target="#timepicker_closingTime"/>
                                            <div class="input-group-append" data-target="#timepicker_closingTime" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="far fa-clock"></i></div>
                                            </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                        <label for="">Average Charges</label>
                                        <div class="input-icon">
                                            <input type="text" class="form-control" name="average_charges" id="average_charges" value="{{old('average_charges') ?? $businessInfo->userdetail->average_charges ?? ''}}" placeholder="0.00">
                                        <i>$</i>
                                        </div>
                                        </div>

                                        {{-- Template 2 start --}}

                                        @if($businessInfo->template_id == 2)
                                        <div class="form-group customBannerStyling">
                                            <label for="exampleInputFile">Banner 1</label>
                                            <input type="file" id="banner1" name="banner1">
                                        </div>

                                        @if(!empty(auth()->user()->userdetail->banner1))
                                        <img src="{{auth()->user()->userdetail->banner1}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
                                        @else 
                                        <img src="{{asset('public/uploads/default.jpeg')}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
                                        @endif

                                        <div class="form-group customBannerStyling">
                                            <label for="exampleInputFile">Banner 2</label>
                                                <input type="file" id="banner2" name="banner2">
                                        </div>

                                        @if(!empty(auth()->user()->userdetail->banner2))
                                        <img src="{{auth()->user()->userdetail->banner2}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
                                        @else 
                                        <img src="{{asset('public/uploads/default.jpeg')}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
                                        @endif

                                        <div class="form-group customBannerStyling">
                                            <label for="exampleInputFile">Banner 3</label>
                                                <input type="file" id="banner3" name="banner3">
                                        </div>	
                                        @if(!empty(auth()->user()->userdetail->banner3))
                                        <img src="{{auth()->user()->userdetail->banner3}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
                                        @else 
                                        <img src="{{asset('public/uploads/default.jpeg')}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
                                        @endif

                                        <div class="form-group customBannerStyling">
                                            <label for="exampleInputFile">Banner 4</label>
                                                <input type="file" id="banner4" name="banner4">
                                        </div>	
                                        @if(!empty(auth()->user()->userdetail->banner4))
                                        <img src="{{auth()->user()->userdetail->banner4}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
                                        @else 
                                        <img src="{{asset('public/uploads/default.jpeg')}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
                                        @endif

                                        @endif
                                        {{-- Template 2 end --}}
                                        
                                        {{-- Template 3 start --}}
                                        @if($businessInfo->template_id == 3)
                                        <div class="form-group customBannerStyling">
											<label for="exampleInputFile">Banner 1</label>
											<input type="file" id="banner1" name="banner1">
										</div>

                                        @if(!empty(auth()->user()->userdetail->banner1))
                                        <img src="{{auth()->user()->userdetail->banner1 ?: '' }}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
                                        @else 
                                        <img src="{{asset('public/uploads/default.jpeg')}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
                                        @endif

                                        @endif
                                        {{-- Template 3 end --}}

                                        {{-- Template 4 start --}}
                                        @if($businessInfo->template_id == 4)
                                        <div class="form-group customBannerStyling">
                                        <label for="exampleInputFile">Slider</label>
                                            <input type="file" name="slider[]" multiple="multiple">
                                        </div>

                                        @if(!empty(auth()->user()->userdetail->sliders))
                                        @php
                                        $parts = explode(',', auth()->user()->userdetail->sliders);
                                        @endphp
                                        @foreach ($parts as $key => $value) 
                                        <img src="{{asset($value)}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;">

                                        @endforeach
                                        
                                        @else 
                                        <img src="{{asset('public/uploads/default.jpeg')}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
                                        @endif

                                        @endif
                                        {{-- Template 4 end --}}

                                        {{-- Template 5 start --}}
                                        @if($businessInfo->template_id == 5)
                                        <div class="form-group customBannerStyling">
                                        <label for="exampleInputFile">Slider</label>
                                            <input type="file" name="slider[]" multiple="multiple">
                                        </div>

                                        @if(!empty(auth()->user()->userdetail->sliders))
                                        @php
                                        $parts = explode(',', auth()->user()->userdetail->sliders);
                                        @endphp
                                        @foreach ($parts as $key => $value) 
                                        <img src="{{asset($value)}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;">

                                        @endforeach
                                        
                                        @else 
                                        <img src="{{asset('public/uploads/default.jpeg')}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
                                        @endif

                                        @endif
                                        {{-- Template 5 end --}}

                                        {{-- Template 6 start --}}
                                        @if($businessInfo->template_id == 6)
                                        <div class="form-group customBannerStyling">
											<label for="exampleInputFile">Banner 1</label>
											<input type="file" id="banner1" name="banner1">
										</div>

                                        @if(!empty(auth()->user()->userdetail->banner1))
                                        <img src="{{auth()->user()->userdetail->banner1 ?: '' }}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
                                        @else 
                                        <img src="{{asset('public/uploads/default.jpeg')}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
                                        @endif

                                        @endif
                                        {{-- Template 6 end --}}

                                            {{-- Template 7 start --}}
                                            @if($businessInfo->template_id == 7)
                                            <div class="form-group customBannerStyling">
												<label for="exampleInputFile">Banner 1</label>
												 <input type="file" id="banner1" name="banner1">
											</div>

											@if(!empty(auth()->user()->userdetail->banner1))
											<img src="{{auth()->user()->userdetail->banner1}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
											@else 
											<img src="{{asset('public/uploads/default.jpeg')}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
											@endif

											<div class="form-group customBannerStyling">
												<label for="exampleInputFile">Banner 2</label>
												 <input type="file" id="banner2" name="banner2">
											</div>

											@if(!empty(auth()->user()->userdetail->banner2))
											<img src="{{auth()->user()->userdetail->banner2}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
											@else 
											<img src="{{asset('public/uploads/default.jpeg')}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
											@endif

											<div class="form-group customBannerStyling">
												<label for="exampleInputFile">Banner 3</label>
												 <input type="file" id="banner3" name="banner3">
											</div>	
											@if(!empty(auth()->user()->userdetail->banner3))
											<img src="{{auth()->user()->userdetail->banner3}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
											@else 
											<img src="{{asset('public/uploads/default.jpeg')}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
											@endif 

											<div class="form-group customBannerStyling">
												<label for="exampleInputFile">Banner 4</label>
												 <input type="file" id="banner4" name="banner4">
											</div>	
											@if(!empty(auth()->user()->userdetail->banner4))
											<img src="{{auth()->user()->userdetail->banner4}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
											@else 
											<img src="{{asset('public/uploads/default.jpeg')}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
											@endif

                                            @endif
                                            {{-- Template 7 end --}}
                                            
                                            {{-- Template 8 start --}}
                                            @if($businessInfo->template_id == 8)
                                            <div class="form-group customBannerStyling">
												<label for="exampleInputFile">Banner 1</label>
												 <input type="file" id="banner1" name="banner1">
											</div>

											@if(!empty(auth()->user()->userdetail->banner1))
											<img src="{{auth()->user()->userdetail->banner1 ?: '' }}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
											@else 
											<img src="{{asset('public/uploads/default.jpeg')}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
											@endif
                                            @endif
                                            {{-- Template 8 end --}}

                                            {{-- Template 9 start --}}
                                            @if($businessInfo->template_id == 9)
                                            <div class="form-group customBannerStyling">
												<label for="exampleInputFile">Banner 1</label>
												 <input type="file" id="banner1" name="banner1">
											</div>

											@if(!empty(auth()->user()->userdetail->banner1))
											<img src="{{auth()->user()->userdetail->banner1 ?: '' }}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
											@else 
											<img src="{{asset('public/uploads/default.jpeg')}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
											@endif
                                            @endif
                                            {{-- Template 9 end --}}

                                            {{-- Template 10 start --}}
                                            @if($businessInfo->template_id == 10)
                                            <div class="form-group customBannerStyling">
												<label for="exampleInputFile">Banner 1</label>
												 <input type="file" id="banner1" name="banner1">
											</div>

											@if(!empty(auth()->user()->userdetail->banner1))
											<img src="{{auth()->user()->userdetail->banner1 ?: '' }}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
											@else 
											<img src="{{asset('public/uploads/default.jpeg')}}" class="img-responsive img-rounded" style="width:80px; height:80px; margin-bottom:10px;"> 
											@endif
                                            @endif
                                            {{-- Template 10 end --}}

                                        <br>
                                        <input type="hidden" name="templateid" id="templateid" value="{{$businessInfo->template_id}}">
                                        <input type="hidden" name="providerid" id="providerid" value="{{$businessInfo->id}}">
										<button type="submit" class="btn btn-default btnCustom">Submit</button>
										</form>  
									</div>
								</div>
							</div>
						</div>
					</div>				
				</div>
			</main>
				
@section('after-scripts')

@include('frontend/partials/timerjs')

@endsection					
@endsection