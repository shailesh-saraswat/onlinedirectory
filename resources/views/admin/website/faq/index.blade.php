@extends('admin.index')

@section('after-style')

    <link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endsection

@section('content')

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                   
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">FAQ's</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">FAQ's </h3>
                                <a href="{{url('admin/faq/create')}}" style="float: right;"><i class="fas fa-plus-square" style="font-size:25px;"></i></a>
                            </div>
                            @if(session('message'))
                                <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
                            @endif
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="page-list" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Question</th>
                                            <th>Display Order</th>
                                            <th>Status</th>
                                            <th>Action</th>                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($faqs) && $faqs != null)
                                            @foreach($faqs as $faq)
                                                <tr>
                                                   
                                                    <td>{{$faq->question}}</td>
                                                    <td>{{$faq->display_order}}</td>
                                                    <td>
                                                        <a href="javascript:" onclick="update_status('{{ $faq->id}}',{{abs($faq->status-1)}})">
                                                        <span class="badge  @if($faq->status!='0') {{'badge-success'}} @else {{'badge-warning'}} @endif">
                                                            @if($faq->status=='1') {{'Active'}} @else {{'Inactive'}} @endif 
                                                        </span>
                                                        </a>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <a href="{{url('admin/faq/edit/'.$faq->id)}}"><i class="fas fa-edit"></i> | &nbsp;&nbsp;</a>
                                                        <a href="{{url('admin/faq/delete/'.$faq->id)}}" onclick="return confirm('Are you sure you want to delete?')" ><i class="fas fa-trash"></i></a> | &nbsp;&nbsp;
                                                        <a title="View" href="{!! url('admin/faq/view',[$faq->id]) !!}">
                                                        <i class="fa fa-desktop"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div> 
                        </div>
                    </div>  
                </div>
            </div>
        </section>
    </div>
@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<script>
    $(function () {
        $("#page-list").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });
</script>
<script>
function update_status(id,value){   
    var CURRENT_URL = location.href;  
    $.ajax({
        type: 'GET',
        data: {'id':id,'value':value},
        url: CURRENT_URL+"/status",
        success: function(result){
        /* alert( 'Update Action Completed.'); */
        location.reload();
        
    }});
}

</script>
@endsection