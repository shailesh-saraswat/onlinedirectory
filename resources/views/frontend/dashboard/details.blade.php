@extends('frontend.partials.index')
@section('after-style')
<link rel="stylesheet" href="{{asset('public/frontend/assets/css/search_styling.css')}}">
<link rel="stylesheet" href="{{asset('public/frontend/assets/newBp/css/swiper.min.css')}}">
<link rel="stylesheet" href="{{asset('public/frontend/assets/newBp/css/swiper-bundle.min.css')}}">
<link rel="stylesheet" href="{{asset('public/frontend/assets/newBp/css/bpcontentstyling.css')}}">
<style>
.input_hide{ display:none; }

#star-1:checked ~ .starWrap [for=star-1] svg path, #star-2:checked ~ .starWrap [for=star-1] svg path, #star-2:checked ~ .starWrap [for=star-2] svg path, #star-3:checked ~ .starWrap [for=star-1] svg path, #star-3:checked ~ .starWrap [for=star-2] svg path, #star-3:checked ~ .starWrap [for=star-3] svg path, #star-4:checked ~ .starWrap [for=star-1] svg path, #star-4:checked ~ .starWrap [for=star-2] svg path, #star-4:checked ~ .starWrap [for=star-3] svg path, #star-4:checked ~ .starWrap [for=star-4] svg path, #star-5:checked ~ .starWrap [for=star-1] svg path, #star-5:checked ~ .starWrap [for=star-2] svg path, #star-5:checked ~ .starWrap [for=star-3] svg path, #star-5:checked ~ .starWrap [for=star-4] svg path, #star-5:checked ~ .starWrap [for=star-5] svg path {
  fill: #FFBB00;
  stroke: #cc9600;
}

.starWrap {
  width: 300px;
  text-align: left;
}

label {
  display: inline-block;
  width: 30px;
  text-align: center;
  cursor: pointer;
}
label svg {
  width: 100%;
  height: auto;
  fill: white;
  stroke: #0b0b0b;
  transform: scale(0.8);
  transition: transform 200ms ease-in-out;
}
label svg path {
  transition: fill 200ms ease-in-out, stroke 100ms ease-in-out;
}

</style>
@endsection

@section('content')

<section class="bigHeader" style="background: url('{{asset('public/frontend/assets/image/banner2.jpg')}}') center center;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="tcent">
					Details
					<span class="smallBredcm"> <a href="{{route('home')}}"> Home </a> >> Details </span>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="pagination_section_details">
	<div class="container">
		
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				@if(session()->has('log_message'))
				<div class="alert alert-success alert-dismissible">
					<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					{{ session()->get('log_message') }}
				</div>
				@endif
				@if(session()->has('message'))
				<div class="alert alert-danger alert-dismissible">
					<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					{{ session()->get('message') }}
				</div>
				@endif
					
				<div class="card overflow-hidden br-0 overflow-hidden customCardStyling">
					<div class="d-sm-flex card-body p-3">
						<div class="p-0 m-0 mr-3">
							<div class=""> 
								<a href="#"></a> 
								@if(isset($getProfessionals->userdetail) && $getProfessionals->userdetail->website_logo == NULL)
									<img src="{{ asset('public/images/user-logo.svg') }}" alt="img" class="w-9 h-9"> 
								@elseif($getProfessionals->userdetail)
									<img class="w-9 h-9" src="{{ asset($getProfessionals->userdetail->website_logo) }}" />
								@else
									<img src="{{ asset('public/images/user-logo.svg') }}" alt="img" class="w-9 h-9"> 
								@endif 
							</div>
						</div>
						<div class="item-card9 mt-3 mt-md-5">
							<a href="javascript:void(0)" class="text-dark">
								<h4 class="font-weight-semibold mt-1">{{ucwords($getProfessionals->business_name)}}</h4>
							</a>
							
							<div class="rating-stars d-inline-flex">
								<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="{{count($getProfessionals['reviews'])}}"> 
								<div class="rating-stars-container mr-2">
									
									@php
									$avgRating = 0;
									$reviewAndRating = [];
									foreach($getProfessionals['reviews'] as $key => $rw) {
										$reviewAndRating[] = $rw->ratings;
									}

									$count = count($reviewAndRating);
									$sum   = array_sum($reviewAndRating);

									if($count > 0){

										$avgRating = $sum/$count;

									} else {

										$avgRating = 0;
									}
									@endphp
									@for($i = 0; $i < 5; $i++)
									<div class="rating-star sm {{ $avgRating <= $i ? '' : 'is--active' }}"> <i class="fa fa-star"></i> </div>
									@endfor
						
								</div>
								({{count($getProfessionals['reviews'])}} Reviews)
							</div>
						</div>
					</div>

					{{-- ==== Banner 1 ===== --}}

					<div class="card overflow-hidden border-0 box-shadow-0 br-0 mb-0">
						@if($getProfessionals->template_id == 1)
						
						<div class="panel panel-default tempt1_pd">
							<div class="panel-body">

								<div id="tempt1" class="template">
									<div class="template-tab template-content">
										<table>
											<tbody>
												<tr>
												<td class="titleHead">Email Id</td>
												<td>{{$getProfessionals->email}}</td>
											</tr>
											<tr>
												<td class="titleHead">Mobile Number</td>
												<td>{{$getProfessionals->mobile}}</td>
											</tr>
											<tr>
												<td class="titleHead">Business Name</td>
												<td>{{$getProfessionals->business_name}}</td>
											</tr>
											<tr>
												<td class="titleHead">Address</td>
												<td>{{$getProfessionals->location}}</td>
											</tr>
											<tr>
												<td class="titleHead">Zip Code</td>
												<td>{{$getProfessionals->zip_code}}</td>
											</tr>
											<tr>
												<td class="titleHead">Opening Time</td>
												@if(isset($getProfessionals->userdetail) && $getProfessionals->userdetail)
												<td>{{$getProfessionals->userdetail->opening_time}}</td>
												@else
												<td>{{"NA"}}</td>
												@endif
											</tr>
											<tr>
												<td class="titleHead">Closing Time</td>
												@if(isset($getProfessionals->userdetail) && $getProfessionals->userdetail)
												<td>{{$getProfessionals->userdetail->closing_time}}</td>
												@else
												<td>{{"NA"}}</td>
												@endif
											</tr>
											<tr>
												<td class="titleHead">Website</td>
												@if(isset($getProfessionals->userdetail) && $getProfessionals->userdetail)
												<td><a href="{{$getProfessionals->userdetail->website_url}}" target="_blank">{{$getProfessionals->userdetail->website_url}}</a></td>
												@else
												<td><a href="http://www.dummy.com" target="_blank">www.dummy.com</a></td>
												@endif
											</tr>								
											</tbody>
										</table>
									</div>
								</div>
																						
							</div>		
						</div>
						@endif
						@if($getProfessionals->template_id == 2)
						{{-- ==== banner 2 ===== --}}
						<div class="panel panel-default tempt2_pd">
														
							<div class="panel-body">
								
								<div id="tempt2" class="template">
									<div class="template_content">
										<div class="leftBanner">
											<img class="" src="{{asset($getProfessionals->userdetail->banner1 ?? "public/uploads/default340.png")}}">
										</div>
										<div class="template-tab template-content">
											<div class="topBannerWrapper">
												<img class="topBanner1" src="{{asset($getProfessionals->userdetail->banner2 ?? "public/uploads/default80.png")}}">
												<img class="topBanner2" src="{{asset($getProfessionals->userdetail->banner3 ?? "public/uploads/default80.png")}}">
												<img class="topBanner3" src="{{asset($getProfessionals->userdetail->banner4 ?? "public/uploads/default80.png")}}">
											</div>
											<table>
											<tbody>
											<tr>
												<td class="titleHead">Email Id</td>
												<td>{{$getProfessionals->email}}</td>
											</tr>
											<tr>
												<td class="titleHead">Mobile Number</td>
												<td>{{$getProfessionals->mobile}}</td>
											</tr>
											<tr>
												<td class="titleHead">Business Name</td>
												<td>{{$getProfessionals->business_name}}</td>
											</tr>
											<tr>
												<td class="titleHead">Address</td>
												<td>{{$getProfessionals->location}}</td>
											</tr>
											<tr>
												<td class="titleHead">Zip Code</td>
												<td>{{$getProfessionals->zip_code}}</td>
											</tr>
											<tr>
												<td class="titleHead">Opening Time</td>
												@if(isset($getProfessionals->userdetail) && $getProfessionals->userdetail)
												<td>{{$getProfessionals->userdetail->opening_time}}</td>
												@else
												<td>{{"NA"}}</td>
												@endif
											</tr>
											<tr>
												<td class="titleHead">Closing Time</td>
												@if(isset($getProfessionals->userdetail) && $getProfessionals->userdetail)
												<td>{{$getProfessionals->userdetail->closing_time}}</td>
												@else
												<td>{{"NA"}}</td>
												@endif
											</tr>
											<tr>
												<td class="titleHead">Website</td>
												@if(isset($getProfessionals->userdetail->website_url))
												<td><a href="{{$getProfessionals->userdetail->website_url}}" target="_blank">{{$getProfessionals->userdetail->website_url}}</a></td>
												@else
												<td><a href="http://www.dummy.com" target="_blank">www.dummy.com</a></td>
												@endif
											</tr>									
												</tbody>
											</table>
										</div>
									</div>

							</div>								   

							
						</div>
						@endif

						@if($getProfessionals->template_id == 3)
						{{-- ==== banner 3 ===== --}}
						<div class="panel panel-default tempt1_pd">
													
							<div class="panel-body">

								<div id="tempt3" class="template">
									<div class="template_content">
										<div class="template-tab template-content">
											<table>
												<tbody>
													<tr>
														<td class="titleHead">Email Id</td>
													</tr>
													<tr>
														<td class="titleHead">Mobile Number</td>
													</tr>
													<tr>
														<td class="titleHead">Business Name</td>
													</tr>
													<tr>
														<td class="titleHead">Address</td>
													</tr>
													<tr>
														<td class="titleHead">Zip Code</td>
													</tr>
													<tr>
														<td class="titleHead">Opening Time</td>
													</tr>
													<tr>
														<td class="titleHead">Close Time</td>
													</tr>
													<tr>
														<td class="titleHead">Website</td>
													</tr>									
												</tbody>
											</table>										
										</div>
										<div class="template-tab template-content">
											<table>
												<tbody>
													<tr>
														<td>{{$getProfessionals->email}}</td>
													</tr>
													<tr>
														<td>{{$getProfessionals->mobile}}</td>
													</tr>
													<tr>
														<td>{{$getProfessionals->business_name}}</td>
													</tr>
													<tr>
														<td>{{$getProfessionals->location}}</td>
													</tr>
													<tr>
														<td>{{$getProfessionals->zip_code}}</td>
													</tr>
													<tr>
														@if(isset($getProfessionals->userdetail) && $getProfessionals->userdetail)
														<td>{{$getProfessionals->userdetail->opening_time}}</td>
														@else
														<td>{{"NA"}}</td>
														@endif
													</tr>
													<tr>
														@if(isset($getProfessionals->userdetail) && $getProfessionals->userdetail)
														<td>{{$getProfessionals->userdetail->closing_time}}</td>
														@else
														<td>{{"NA"}}</td>
														@endif
													</tr>
													<tr>
														@if(isset($getProfessionals->userdetail->website_url))
														<td><a href="{{$getProfessionals->userdetail->website_url}}" target="_blank">{{$getProfessionals->userdetail->website_url}}</a></td>
														@else
														<td><a href="http://www.dummy.com" target="_blank">www.dummy.com</a></td>
														@endif
												
													</tr>									
												</tbody>
											</table>											
										</div>
									</div>
									<div class="bottomBanner">
										<img src="{{asset($getProfessionals->userdetail->banner1 ?? "public/uploads/defaultBanner.png")}}">		
									</div>
								</div>	
																						
							</div>	
														
						</div>	
						@endif

						@if($getProfessionals->template_id == 4)
						{{-- ===== banner 4 ==== --}}
						<div class="panel panel-default tempt2_pd">								
							<div class="panel-body">														  
								<div id="tempt4" class="template">
									<div class="template_content">
										<div class="leftBanner">
											<div class="swiper-container">
												<div class="swiper-wrapper">
												@if(isset($getProfessionals->userdetail) && $getProfessionals->userdetail)
												@php
												$parts = explode(',', $getProfessionals->userdetail->sliders);	
												@endphp
												
												@foreach($parts as $key => $value) 
												<div class="swiper-slide"><img src="{{asset($value ?? 'public/uploads/defaultBanner.png')}}"></div>
												@endforeach
												@endif
												</div>
												<!-- Add Pagination -->
												<div class="swiper-pagination"></div>
												<!-- Add Arrows -->
												<div class="swiper-button-next"></div>
												<div class="swiper-button-prev"></div>
											</div>
										</div>
										<div class="template-tab template-content">
											<table>
												<tbody>
													<tr>
														<td class="titleHead">Email Id</td>
														<td>{{$getProfessionals->email}}</td>
													</tr>
													<tr>
														<td class="titleHead">Mobile Number</td>
														<td>{{$getProfessionals->mobile}}</td>
													</tr>
													<tr>
														<td class="titleHead">Business Name</td>
														<td>{{$getProfessionals->business_name}}</td>
													</tr>
													<tr>
														<td class="titleHead">Address</td>
														<td>{{$getProfessionals->location}}</td>
													</tr>
													<tr>
														<td class="titleHead">Zip Code</td>
														<td>{{$getProfessionals->zip_code}}</td>
													</tr>
													<tr>
														<td class="titleHead">Opening Time</td>
														@if(isset($getProfessionals->userdetail) && $getProfessionals->userdetail)
														<td>{{$getProfessionals->userdetail->opening_time}}</td>
														@else
														<td>{{"NA"}}</td>
														@endif
													</tr>
													<tr>
														<td class="titleHead">Closing Time</td>
														@if(isset($getProfessionals->userdetail) && $getProfessionals->userdetail)
														<td>{{$getProfessionals->userdetail->closing_time}}</td>
														@else
														<td>{{"NA"}}</td>
														@endif
													</tr>
													<tr>
														<td class="titleHead">Website</td>
														@if(isset($getProfessionals->userdetail->website_url))
														<td><a href="{{$getProfessionals->userdetail->website_url}}" target="_blank">{{$getProfessionals->userdetail->website_url}}</a></td>
														@else
														<td><a href="http://www.dummy.com" target="_blank">www.dummy.com</a></td>
														@endif
													</tr>									
												</tbody>
											</table>
										</div>
									</div>
								</div>																							   
							</div>
						</div>
						@endif

						@if($getProfessionals->template_id == 5)
						{{-- ===== banner 5 ==== --}}
						<div class="panel panel-default tempt1_pd">						
							<div class="panel-body">
								<div id="tempt5" class="template">
									<div class="template_content">
										<div class="template-tab template-content">
											<table>
												<tbody>
													<tr>
														<td class="titleHead">Email Id</td>
														<td>{{$getProfessionals->email}}</td>
													</tr>
													<tr>
														<td class="titleHead">Mobile Number</td>
														<td>{{$getProfessionals->mobile}}</td>
													</tr>
													<tr>
														<td class="titleHead">Business Name</td>
														<td>{{$getProfessionals->business_name}}</td>
													</tr>
													<tr>
														<td class="titleHead">Address</td>
														<td>{{$getProfessionals->location}}</td>
													</tr>
													<tr>
														<td class="titleHead">Zip Code</td>
														<td>{{$getProfessionals->zip_code}}</td>
													</tr>
													<tr>
														<td class="titleHead">Opening Time</td>
														@if(isset($getProfessionals->userdetail) && $getProfessionals->userdetail)
														<td>{{$getProfessionals->userdetail->opening_time}}</td>
														@else
														<td>{{"NA"}}</td>
														@endif
													</tr>
													<tr>
														<td class="titleHead">Closing Time</td>
														@if(isset($getProfessionals->userdetail) && $getProfessionals->userdetail)
														<td>{{$getProfessionals->userdetail->closing_time}}</td>
														@else
														<td>{{"NA"}}</td>
														@endif
													</tr>
													<tr>
														<td class="titleHead">Website</td>
														@if(isset($getProfessionals->userdetail->website_url))
														<td><a href="{{$getProfessionals->userdetail->website_url}}" target="_blank">{{$getProfessionals->userdetail->website_url}}</a></td>
														@else
														<td><a href="http://www.dummy.com" target="_blank">www.dummy.com</a></td>
														@endif
													</tr>									
												</tbody>
											</table>
										</div>
									</div>

									<div class="leftBanner">
										<div class="swiper-container">
											<div class="swiper-wrapper">
											@if(isset($getProfessionals->userdetail) && $getProfessionals->userdetail)
											@php
											$parts = explode(',', $getProfessionals->userdetail->sliders);	
											@endphp
											@foreach($parts as $key => $value) 
											<div class="swiper-slide"><img src="{{asset($value ?? "public/uploads/defaultBanner.png")}}"></div>
											@endforeach
											@endif
											</div>
											<!-- Add Pagination -->
											<div class="swiper-pagination"></div>
											<!-- Add Arrows -->
											<div class="swiper-button-next"></div>
											<div class="swiper-button-prev"></div>
										</div>
									</div>
								</div>	
									
																						
							</div>	
						</div>
						@endif

						@if($getProfessionals->template_id == 6)
						
						{{-- ===== banner 6 ==== --}}
						<div class="panel panel-default tempt2_pd">							
						<div class="panel-body">
							<div id="tempt6" class="template">
								<div class="template_content">
									<div class="template-tab template-content">
										<table>
											<tbody>
												<tr>
													<td class="titleHead">Email Id</td>
												</tr>
												<tr>
													<td class="titleHead">Mobile Number</td>
												</tr>
												<tr>
													<td class="titleHead">Business Name</td>
												</tr>
												<tr>
													<td class="titleHead">Address</td>
												</tr>
												<tr>
													<td class="titleHead">Zip Code</td>
												</tr>
												<tr>
													<td class="titleHead">Opening Time</td>
												</tr>
												<tr>
													<td class="titleHead">Close Time</td>
												</tr>
												<tr>
													<td class="titleHead">Website</td>
												</tr>									
											</tbody>
										</table>										
									</div>
									<div class="template-tab template-content">
										<table>
											<tbody>
												<tr>
													<td>{{$getProfessionals->mobile}}</td>
												</tr>
												<tr>
													<td>{{$getProfessionals->mobile}}</td>
												</tr>
												<tr>
													<td>{{$getProfessionals->business_name}}</td>
												</tr>
												<tr>
													<td>{{$getProfessionals->location}}</td>
												</tr>
												<tr>
													<td>{{$getProfessionals->zip_code}}</td>
												</tr>
												<tr>
													@if(isset($getProfessionals->userdetail) && $getProfessionals->userdetail)
													<td>{{$getProfessionals->userdetail->opening_time}}</td>
													@else
													<td>{{"NA"}}</td>
													@endif
												</tr>
												<tr>
													@if(isset($getProfessionals->userdetail) && $getProfessionals->userdetail)
													<td>{{$getProfessionals->userdetail->closing_time}}</td>
													@else
													<td>{{"NA"}}</td>
													@endif
												</tr>
												<tr>
													@if(isset($getProfessionals->userdetail->website_url))
													<td><a href="{{$getProfessionals->userdetail->website_url}}" target="_blank">{{$getProfessionals->userdetail->website_url}}</a></td>
													@else
													<td><a href="http://www.dummy.com" target="_blank">www.dummy.com</a></td>
													@endif
													
												</tr>									
											</tbody>
										</table>											
									</div>
								</div>
								<div class="bottomBanner">
									<img src="{{asset($getProfessionals->userdetail->banner1 ?? "public/uploads/defaultBanner.png")}}">	
									{{-- <img src="{{asset($getProfessionals->banner1 ?: "public/uploads/defaultBanner.png")}}"> --}}									
								</div>
								</div>
						</div>
						</div>
						@endif

						@if($getProfessionals->template_id == 7)
						{{-- ===== banner 7 ==== --}}
						<div class="panel panel-default tempt1_pd">						
							<div class="panel-body">
								<div id="tempt7" class="template">
									<div class="template_content">
										<div class="template-tab template-content">
											<div class="topBannerWrapper">
												<img class="topBanner1" src="{{asset($getProfessionals->userdetail->banner2 ?: "public/uploads/default80.png")}}">
												<img class="topBanner2" src="{{asset($getProfessionals->userdetail->banner3 ?: "public/uploads/default80.png")}}">
												<img class="topBanner3" src="{{asset($getProfessionals->userdetail->banner4 ?: "public/uploads/default80.png")}}">
											</div>
											<table>
												<tbody>
													<tr>
														<td class="titleHead">Email Id</td>
														<td>{{$getProfessionals->email}}</td>
													</tr>
													<tr>
														<td class="titleHead">Mobile Number</td>
														<td>{{$getProfessionals->mobile}}</td>
													</tr>
													<tr>
														<td class="titleHead">Business Name</td>
														<td>{{$getProfessionals->business_name}}</td>
													</tr>
													<tr>
														<td class="titleHead">Address</td>
														<td>{{$getProfessionals->location}}</td>
													</tr>
													<tr>
														<td class="titleHead">Zip Code</td>
														<td>{{$getProfessionals->zip_code}}</td>
													</tr>
													<tr>
														<td class="titleHead">Opening Time</td>
														@if(isset($getProfessionals->userdetail) && $getProfessionals->userdetail)
														<td>{{$getProfessionals->userdetail->opening_time}}</td>
														@else
														<td>{{"NA"}}</td>
														@endif
													</tr>
													<tr>
														<td class="titleHead">Closing Time</td>
														@if(isset($getProfessionals->userdetail) && $getProfessionals->userdetail)
														<td>{{$getProfessionals->userdetail->closing_time}}</td>
														@else
														<td>{{"NA"}}</td>
														@endif
													</tr>
													<tr>
														<td class="titleHead">Website</td>
														@if(isset($getProfessionals->userdetail->website_url))
														<td><a href="{{$getProfessionals->userdetail->website_url}}" target="_blank">{{$getProfessionals->userdetail->website_url}}</a></td>
														@else
														<td><a href="http://www.dummy.com" target="_blank">www.dummy.com</a></td>
														@endif
														
													</tr>									
												</tbody>
											</table>
										</div>
										<div class="leftBanner">
											<img class="" src="{{asset($getProfessionals->userdetail->banner1 ?: "public/uploads/default600.png")}}">
										</div>		
									</div>

								</div>	
																							
							</div>	
						</div>
						@endif

						@if($getProfessionals->template_id == 8)
						{{-- ===== banner 8 ==== --}}
						<div class="panel panel-default tempt2_pd">						
							<div class="panel-body">														  
																																										<div id="tempt8" class="template">
									<div class="template_content">
										<div class="template-tab template-content">
											<div class="topBannerWrapper">
												<img src="{{asset($getProfessionals->userdetail->banner1 ?: "public/uploads/defaultBanner.png")}}">
											</div>
											<table>
												<tbody>
													<tr>
														<td class="titleHead">Email Id</td>
														<td>{{$getProfessionals->email}}</td>
													</tr>
													<tr>
														<td class="titleHead">Mobile Number</td>
														<td>{{$getProfessionals->mobile}}</td>
													</tr>
													<tr>
														<td class="titleHead">Business Name</td>
														<td>{{$getProfessionals->business_name}}</td>
													</tr>
													<tr>
														<td class="titleHead">Address</td>
														<td>{{$getProfessionals->location}}</td>
													</tr>
													<tr>
														<td class="titleHead">Zip Code</td>
														<td>{{$getProfessionals->zip_code}}</td>
													</tr>
													<tr>
														<td class="titleHead">Opening Time</td>
														@if(isset($getProfessionals->userdetail) && $getProfessionals->userdetail)
														<td>{{$getProfessionals->userdetail->opening_time}}</td>
														@else
														<td>{{"NA"}}</td>
														@endif
													</tr>
													<tr>
														<td class="titleHead">Closing Time</td>
														@if(isset($getProfessionals->userdetail) && $getProfessionals->userdetail)
														<td>{{$getProfessionals->userdetail->closing_time}}</td>
														@else
														<td>{{"NA"}}</td>
														@endif
													</tr>
													<tr>
														<td class="titleHead">Website</td>
														@if(isset($getProfessionals->userdetail->website_url))
														<td><a href="{{$getProfessionals->userdetail->website_url}}" target="_blank">{{$getProfessionals->userdetail->website_url}}</a></td>
														@else
														<td><a href="http://www.dummy.com" target="_blank">www.dummy.com</a></td>
														@endif
													
													</tr>									
												</tbody>
											</table>
										</div>	
									</div>

								</div>															
								
							</div>	
						</div>
						@endif

						@if($getProfessionals->template_id == 9)
						{{-- ===== banner 9 ==== --}}
						<div class="panel panel-default tempt1_pd">							
							<div class="panel-body">
								<div id="tempt9" class="template">
									<div class="template_content">
										<div class="template-tab template-content">
											<div class="topBannerWrapper">
												<img src="{{asset($getProfessionals->userdetail->banner1 ?: "public/uploads/defaultBanner.png")}}">
											</div>
											<table>
												<tbody>
													<tr>
														<td class="titleHead"><div class="theme9HeadField">Email Id</div></td>
														<td>{{$getProfessionals->email}}</td>
													</tr>
													<tr>
														<td class="titleHead"><div class="theme9HeadField">Mobile Number</div></td>
														<td>{{$getProfessionals->mobile}}</td>
													</tr>
													<tr>
														<td class="titleHead"><div class="theme9HeadField">Business Name</div></td>
														<td>{{$getProfessionals->business_name}}</td>
													</tr>
													<tr>
														<td class="titleHead"><div class="theme9HeadField">Address</div></td>
														<td>{{$getProfessionals->location}}</td>
													</tr>
													<tr>
														<td class="titleHead"><div class="theme9HeadField">Zip Code</div></td>
														<td>{{$getProfessionals->zip_code}}</td>
													</tr>
													<tr>
														<td class="titleHead">Opening Time</td>
														@if(isset($getProfessionals->userdetail) && $getProfessionals->userdetail)
														<td>{{$getProfessionals->userdetail->opening_time}}</td>
														@else
														<td>{{"NA"}}</td>
														@endif
													</tr>
													<tr>
														<td class="titleHead">Closing Time</td>
														@if(isset($getProfessionals->userdetail) && $getProfessionals->userdetail)
														<td>{{$getProfessionals->userdetail->closing_time}}</td>
														@else
														<td>{{"NA"}}</td>
														@endif
													</tr>
													<tr>
														<td class="titleHead"><div class="theme9HeadField">Website</div></td>
														@if(isset($getProfessionals->userdetail->website_url))
														<td><a href="{{$getProfessionals->userdetail->website_url}}" target="_blank">{{$getProfessionals->userdetail->website_url}}</a></td>
														@else
														<td><a href="http://www.dummy.com" target="_blank">www.dummy.com</a></td>
														@endif
														
													</tr>									
												</tbody>
											</table>
										</div>	
									</div>

								</div>	
																							
							</div>	
						</div>
						@endif

						@if($getProfessionals->template_id == 10)
						{{-- ===== banner 10 ==== --}}
						<div class="panel panel-default tempt2_pd">					
							<div class="panel-body"> 
								<div id="tempt10" class="template">
									<div class="template_content">
										<div class="template-tab template-content">
											<table>
												<tbody>
													<tr>
														<td class="titleHead"><div class="theme9HeadField">Email Id</div></td>
														<td>{{$getProfessionals->email}}</td>
													</tr>
													<tr>
														<td class="titleHead"><div class="theme9HeadField">Mobile Number</div></td>
														<td>{{$getProfessionals->mobile}}</td>
													</tr>
													<tr>
														<td class="titleHead"><div class="theme9HeadField">Business Name</div></td>
														<td>{{$getProfessionals->business_name}}</td>
													</tr>
													<tr>
														<td class="titleHead"><div class="theme9HeadField">Address</div></td>
														<td>{{$getProfessionals->location}}</td>
													</tr>
													<tr>
														<td class="titleHead"><div class="theme9HeadField">Zip Code</div></td>
														<td>{{$getProfessionals->zip_code}}</td>
													</tr>
													<tr>
														<td class="titleHead">Opening Time</td>
														@if(isset($getProfessionals->userdetail) && $getProfessionals->userdetail)
														<td>{{$getProfessionals->userdetail->opening_time}}</td>
														@else
														<td>{{"NA"}}</td>
														@endif
													</tr>
													<tr>
														<td class="titleHead">Closing Time</td>
														@if(isset($getProfessionals->userdetail) && $getProfessionals->userdetail)
														<td>{{$getProfessionals->userdetail->closing_time}}</td>
														@else
														<td>{{"NA"}}</td>
														@endif
													</tr>
													<tr>
														<td class="titleHead"><div class="theme9HeadField">Website</div></td>
														@if(isset($getProfessionals->userdetail->website_url))
														<td><a href="{{$getProfessionals->userdetail->website_url}}" target="_blank">{{$getProfessionals->userdetail->website_url}}</a></td>
														@else
														<td><a href="http://www.dummy.com" target="_blank">www.dummy.com</a></td>
														@endif
													</tr>									
												</tbody>
											</table>
											<div class="topBannerWrapper">
												<img src="{{asset($getProfessionals->userdetail->banner1 ?: "public/uploads/defaultBanner.png")}}">
											</div>												
										</div>	
									</div>
								</div>																
							</div>
						</div>
						@endif
						{{-- ======  End Templates ==== --}}
					</div>
					
				</div>
				<div class="reviewHead">
					Details
				</div>
				<div class="reviewWritebox">
				<textarea class="form-control" name="desription" rows="3" value="" placeholder="Description.." disabled>@if(isset($getProfessionals->userdetail) && $getProfessionals->userdetail->details != NULL){{ $getProfessionals->userdetail->details }}@endif</textarea>
				</div>
				<div class="reviewWrapper">

					@if(Auth::user()) 
					{{-- @if ($errors->any())
						@foreach ($errors->all() as $error)
							<div>{{$error}}</div>
						@endforeach
					@endif --}}
					<div class="reviewHead">
					Reviews
					</div>
					<form action="{{route('professionals.review')}}" method="post" class="comment-form clearfix">
					@csrf
					<input type="hidden" name="userID" value="{{$getProfessionals->id}}">

					<div class="reviewPDiv">
					<input type="radio" name="rating" value="1" class="input_hide" id="star-1" />
					<input type="radio" name="rating" value="2" class="input_hide" id="star-2" />
					<input type="radio" name="rating" value="3" class="input_hide" id="star-3" />
					<input type="radio" name="rating" value="4" class="input_hide" id="star-4" />
					<input type="radio" name="rating" value="5" class="input_hide" id="star-5" />

						<div class="starWrap">
							<label for="star-1">
								<svg viewBox="0 0 51 48">
								<path d="m25,1 6,17h18l-14,11 5,17-15-10-15,10 5-17-14-11h18z"/>
								</svg>
							</label>
							<label for="star-2">
								<svg viewBox="0 0 51 48">
								<path d="m25,1 6,17h18l-14,11 5,17-15-10-15,10 5-17-14-11h18z"/>
								</svg>
							</label>
							<label for="star-3">
								<svg viewBox="0 0 51 48">
								<path d="m25,1 6,17h18l-14,11 5,17-15-10-15,10 5-17-14-11h18z"/>
								</svg>
							</label>
							<label for="star-4">
								<svg viewBox="0 0 51 48">
								<path d="m25,1 6,17h18l-14,11 5,17-15-10-15,10 5-17-14-11h18z"/>
								</svg>
							</label>
							<label for="star-5">
								<svg viewBox="0 0 51 48">
								<path d="m25,1 6,17h18l-14,11 5,17-15-10-15,10 5-17-14-11h18z"/>
								</svg>
							</label>
						</div>
						@error('rating')
						<span class="invalid-feedback" role="alert" style="display:block;">
						<strong>{{ $message }}</strong>
						</span>
						@enderror
					</div>
					<div class="reviewWritebox">
						<textarea class="form-control" name="message" rows="3" value="{{old('message')}}" placeholder="Write a review"></textarea>
						@error('message')
						<span class="invalid-feedback" role="alert" style="display:block;">
						<strong>{{ $message }}</strong>
						</span>
						@enderror
					</div>

					<div class="reviewButtonWrapper">
						<button type="submit" id="submit" class="add-btn contact-btn">Add Review</button>
						<div id="msgSubmit" class="h3 text-center hidden"></div> 
						<div class="clearfix"></div>
                    </div>

					</form>
					@endif
					@foreach($getProfessionals->reviews as $mk=>$row)
					
					<div class="reviewCard">
						<div class="customStarName">
							<div class="customerName">
								{{ ucwords($row['user']['first_name'])." ".ucwords($row['user']['last_name']) }}
							</div>						
							<div class="customstars">
								@for($i = 0; $i < 5; $i++)
								<i class="far {{ $row->ratings <= $i ? '' : 'fas' }} fa-star"></i>
								@endfor
							</div>
						</div>
						<div>
							{{$row->message}}
						</div>
					</div>				
					@endforeach			
		
				</div>
				
			</div>
			
		</div>

	</div>
</section>

@section('after-scripts')
<script src="{{asset('public/frontend/assets/newBp/js/swiper.min.js')}}"></script>
<script src="{{asset('public/frontend/assets/newBp/js/swiper-bundle.min.js')}}"></script>
<script>
    var swiper = new Swiper('.swiper-container', {
        pagination: {
		autoHeight: true,
        el: '.swiper-pagination',
        type: 'fraction',
        },
        autoplay: {
        delay: 5000,
        },
        navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
        },
    });
</script>
@endsection
@endsection