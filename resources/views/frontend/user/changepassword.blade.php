@extends('frontend.partials.index')
 
@section('content')

<section class="bigHeader" style="background: url('{{asset('public/frontend/assets/image/banner2.jpg')}}') center center;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="tcent">
					Change Password
					<span class="smallBredcm"> <a href="{{route('home')}}"> Home </a> >> Change Password</span>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="login_section">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<form method="post" action="{{route('user.updatepassword')}}" novalidate>
				@csrf
				<div class="loginBox">
					@if(session()->has('log_message'))
					<div class="alert alert-success alert-dismissible">
						<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{ session()->get('log_message') }}
					</div>
					@endif

					@if(session()->has('message'))
					<div class="alert alert-danger alert-dismissible">
						<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{ session()->get('message') }}
					</div>
					@endif
                                    
					<div class="">
						<h5 class="login_title" id="exampleModalLabel">Change Password</h5>
					</div>

				   <div class="">
					<div class="form-group"> <label class="form-label">Old Password</label> 
					<input type="password" name="old_password" class="form-control" value="{{old('old_password')}}" placeholder="Enter Your Old Password" > 
					@error('old_password')
					<span class="invalid-feedback" role="alert" style="display:block;">
					<strong>{{ $message }}</strong>
					</span>
					@enderror
					</div>

					<div class="form-group"> <label class="form-label">Password</label> 
					<input type="password" name="password" class="form-control" value="{{old('password')}}" placeholder="Enter Your Password" > 
					@error('password')
					<span class="invalid-feedback" role="alert" style="display:block;">
					<strong>{{ $message }}</strong>
					</span>
					@enderror
					</div>

					<div class="form-group"> <label class="form-label">Confirm Password</label> 
					<input type="password" name="confirm_password" class="form-control" value="{{old('confirm_password')}}" placeholder="Enter Your Confirm Password" > 
					@error('confirm_password')
					<span class="invalid-feedback" role="alert" style="display:block;">
					<strong>{{ $message }}</strong>
					</span>
					@enderror
					</div>

				    <div class="customLogRegisWrapper">
					    <div class="submit"> 
							<button class="btn btn-primary btn-block">Submit</button>
						</div>
				    </div>
				
				</div>
			</div>
		</div>
	</div>
</section>


@endsection 