<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Models\Reviews;
use App\Models\User;
use Exception;
use Validator;

class ReviewController extends Controller
{
    public $successStatus = 200;

    public function addreview(Request $request){
        
        //try{
        
        $validation = Validator::make($request->all(), [ 
            'star'          => "required|numeric|min:1",
            'message'       => "required",
            'pid'    => "required",
            ],
            ['message.required' => 'Please write review']
        ); 
        

        if($validation->fails()) {
            $result['code']    = 400;
            $result['error']   = true;
            $result['message'] = $validation->messages()->first();
            return response()->json($result);
        }
             $review     = Reviews::where('user_id', auth()->user()->id)->where('user_type',$request->pid)->first();

            if(isset($review)){
                
                $result['code']    = 401;
                $result['error']   = true;
                $result['message'] = "You have already made the review";
                return response()->json($result);

            } else {

                $review 		    = new Reviews();
                $review->message    = $request->message;
                $review->ratings    = $request->star;
                $review->user_id    = auth()->user()->id;
                $review->user_type  = $request->pid;
                $review->status     = 1;
            
            if($review->save()){
                
                $result['code']            = 200;
                $result['error']           = false;
                $result['message']         = "You have made the review successfully.";
                $result['result']          = $review;
                return response()->json($result);
            }
                
            }

        /* } catch (Exception $e) {
            return response()->json($e->getMessage());
        }  */
    }

    
}
