<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Controllers\StripePaymentController;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Twilio\Rest\Client;
use Twilio\Exceptions\TwilioException;
use Illuminate\Support\Facades\Hash;
use App\Models\PassportToken;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

class UsersController extends Controller
{

public $successStatus = 200;


/*
|--------------------------------------------------------------------------
| ======== API User Register ========
|--------------------------------------------------------------------------
|
| Local Url:http://localhost/onlinedirectory/api/v1/register-user
| Live Url: 
| Key:type,name,email,password,device_type,device_token,confirm_password,mobile,zip_code,location;
| Method:POST
| Comments: Signup api for user with key values and Post method.
|
|--------------------------------------------------------------------------
*/  


    public function registerUser(Request $request) {

        try{

           $validation = Validator::make($request->all(), [

                'first_name'       => 'required|string|max:255',
                'last_name'        => 'required|string|max:255',
                'email'            => 'required|email|unique:users,email',
                'password'         => 'required|min:6|same:confirm_password',
                'confirm_password' => 'required|min:6',
                'mobile'           => 'required|min:11|numeric',
                'address'          => 'required',
                'zip_code'         => 'required',
                'location'         => 'required'
            ]);
        
            if ($validation->fails()) {

                $result['code']     = 400;
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);
            }

                $user               = new User;
                $slugStr            = strtolower($request->name);
                $types              = User_Type::where('id','=',$request->type)->get();
                $user->type 		= $request->type;
                $user->name 		= $request->first_name." ".$request->last_name;
                $user->first_name   = $request->first_name;
                $user->last_name 	= $request->last_name;
                $user->email 	    = $request->email;
                $user->password 	= Hash::make($request->password);
                $user->mobile 	    = $request->mobile;
                $user->slug         = Str::slug($request->first_name , "-");
                $user->address 	    = $request->address;
                $user->location 	= $request->location;
                $user->latitude 	= $request->latitude;
                $user->longitude 	= $request->longitude;
                $user->zip_code 	= $request->zip_code;
                $user->status 	    = 0;
    
            if($user->save()){
                $result['code']     = 200;
                $result['error']    = false;
                $result['message']  = "User register successfully.";

                return response()->json($result);

            } else {

                $result['code']    = 500;
                $result['error']   = true;
                $result['message'] = "Something went wrong!";

                return response()->json($result);
            }


        } catch(Exception $e){
            return response()->json($e->getMessage());
        }

    }

/*
|--------------------------------------------------------------------------
| ========  API User Login ==========
|--------------------------------------------------------------------------
|
| Local Url:http://localhost/onlinedirectory/api/v1/login-user
| Live Url: 
| Key:email,password;
| Method:POST
| Comments: Login api for user with key values and Post method.
|
|--------------------------------------------------------------------------
*/ 

    public function loginUser(Request $request){

        try {
            $validation = Validator::make($request->all(), [
                'email'        => 'required',
                'password'     => 'required',
                /* 'device_type'  => 'required',
                'device_token' => 'required', */
            ]);

            if ($validation->fails()) {

                $result['code']    = 400;
                $result['error']   = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);

            }

            $credentials = $request->only(['email', 'password']);

            $checkUser = User::query()->where(['email'=> $request->email])->first();
			
			if(!$checkUser){

				$result['code']    = 404;
                $result['error']   = true;
                $result['message'] = "This email id does not exist in our records!";
                return response()->json($result);

			} 
            
            if (Auth::attempt($credentials)) {

                $user = auth()->user();

            if($user->status == 0){

                $result['code']    = 401;
                $result['error']   = true;
                $result['message'] = "Your account is currently deactivated from Admin. Please contact for support.";
                return response()->json($result);

            } else if (auth()->user()->status > 2){

                $result['code']    = 201;
                $result['error']   = true;
                $result['message'] = "Your account is currently Pending from Admin. Please contact for support";
                return response()->json($result);

            } else if(auth()->user()->type == 1){

                $result['code']    = 202;
                $result['error']   = true;
                $result['message'] = "Forbidden! You do not have permission to access this route.";
                return response()->json($result);
            } 
        
            $user->tokens()->delete();
            $token         = $user->createToken('OnlineDirectory')->accessToken;
            ///$updateDevice  = User::where('id', auth()->user()->id)->update(['device_type' => 'android', 'device_token' => 'andr123']);
        
            $result['code']            = 200;
            $result['error']           = false;
            $result['message']         = "Logged in successfully.";
            $result['result']          = $user;
            $result['result']['token'] = $token;
            return response()->json($result);                    

        } else {
            $result['code'] = 401;
            $result['error'] = true;
            $result['message'] = "Credentials do not match!";
            return response()->json($result);
        }

        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }
    }


/*
|--------------------------------------------------------------------------
| ========  API User Forgot Password ==========
|--------------------------------------------------------------------------
|
| Local Url:http://localhost/onlinedirectory/api/v1/forgot-password
| Live Url: 
| Key:email;
| Method:POST
| Comments: Forgot api for user with key values and Post method.
|
|--------------------------------------------------------------------------
*/ 

    public function forgotPassword(Request $request)
    {
         try {

            $validation = Validator::make($request->all(), [
                'email' => 'required|email'
            ]);

            if ($validation->fails()) {

                $result['code']    = 400;
                $result['error']   = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);
            }

            $email                 = $request->input('email');
            $checkUser             = User::query()->where('email', $email)->first();

            if(isset($checkUser) && $checkUser != null){
		
                $generateOtp       = $this->generate_OTP();
                $updateOtp         = User::where('id', $checkUser->id)->update(array('otp' => $generateOtp ,'request_otp' => 0));
                $to_name           = $checkUser->name;
                $to_email          = $email;
                $body              = 'Your OTP is : ' . $generateOtp . ' Please do not share with anybody.';
                $data              = array('name' => $to_name, 'body' => $body);

                Mail::send('emails.mail', $data, function ($message) use ($to_name, $to_email) {
                    $message->to($to_email, $to_name)
                        ->subject('Forgot Password OTP');
                    $message->from(config('mail.username'), env('APP_NAME', 'OnlineDirectory'));
                });

                $result['code']          = 201;
                $result['error']         = true;
                $result['message']       = "Please Verify your account we send otp on your registerd email.";
                $result['result']['otp'] = $generateOtp;
                return response()->json($result);

            } else {

                $result['code']          = 404;
                $result['error']         = true;
                $result['message']       = "Email Id does not exist in our records!";
                return response()->json($result);
            }

        } catch(Exception $e){
            return response()->json($e->getMessage());
        }

    }

/*
|--------------------------------------------------------------------------
| ==========  Reset Password ==========
|--------------------------------------------------------------------------
*/

    public function resetPassword(Request $request)
    {
        try {

            $validation = Validator::make($request->all(), [
                'email'                 => 'required|email',
                'password'              => 'required|min:6',
                'confirm_password'      => 'required|same:password',
            ]);

            if ($validation->fails()) {

                $result['code']    = 400;
                $result['error']   = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);
            }

            $user = User::query()->where(['email' => $request->email])->first();

            if (isset($user) && $user != null) {

                if ($user->verified == 1 && $user->request_otp==1) {
                    
                    $requestData           = $request->all();
					$user->text_password   = $requestData['password'];
					$user->password        = bcrypt($requestData['password']);

                    if($user->save()){

                        $result['code']    = 200;
                        $result['error']   = false;
                        $result['message'] = "Password reset successfully.";
                        $result['result']  = $user;

                        return response()->json($result);

                    } else {

                        $result['code']    = 500;
                        $result['error']   = true;
                        $result['message'] = "Something went wrong while saving data!";
                        return response()->json($result);
                    }

                } else {

                    $result['code']        = 202;
                    $result['error']       = true;
                    $result['message']     = "Please verify your account.";
                    return response()->json($result);
                }

            } else {

                $result['code'] = 404;
                $result['error'] = true;
                $result['message'] = "Email Id does not exist in our records!";
                return response()->json($result);
            }

        } catch (Exception $e) {

            return response()->json($e->getMessage());
        }
    }

/*
|--------------------------------------------------------------------------
| ========  API User Verify OTP ==========
|--------------------------------------------------------------------------
|
| Local Url:http://localhost/onlinedirectory/api/v1/verify-otp
| Live Url: 
| Key:email,otp;
| Method:POST
| Comments: verify OTP api for user with key values and Post method.
|
|--------------------------------------------------------------------------
*/ 

    public function verifyOtp(Request $request){

        try {

            $validation      = Validator::make($request->all(), [
                'email'      => 'required|email',
                'otp'        => 'required|numeric|min:4',
            ]);

            if ($validation->fails()) {

                $result['code']    = 400;
                $result['error']   = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);
            }

            $user = User::query()->where('email', $request->email)->first();

            if (isset($user) && $user != null) {

                if ($user->otp == $request->otp) {
                    $user->verified = 1;
                    $user->request_otp = 1;

                    $token = $user->createToken('OnlineDirectoryVerifyOtp')->accessToken;

                    if($user->save()){

                        $result['code']    = 200;
                        $result['error']   = false;
                        $result['message'] = "Otp Verified Successfully.";
                        $result['result']  = $user;
                        $result['result']['token'] = $token;

                        return response()->json($result);
                    }

                    else{

                        $result['code'] = 500;
                        $result['error'] = true;
                        $result['message'] = "Something went wrong!";

                        return response()->json($result);
                    }

                } else {

                    $result['code']    = 400;
                    $result['error']   = true;
                    $result['message'] = "Invalid otp.";
                    return response()->json($result);
                }

            } else {

                $result['code']        = 404;
                $result['error']       = true;
                $result['message']     = "Email Id does not found!";
                return response()->json($result);
            }

        } catch (Exception $e) {

            return response()->json($e->getMessage());
        }

    }

/*
|--------------------------------------------------------------------------
| ========== Resend OTP ==========
|--------------------------------------------------------------------------
*/ 

    public function resendOtp(Request $request)
    {
        try{

            $validation = Validator::make($request->all(), [
                'email'     => 'required|email',
            ]);

            if ($validation->fails()) {
                $result['code'] = 400;
                $result['error'] = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);
            }

            $email = $request->input('email');
            $user  = User::where(['email'=> $email])->first();

            if(isset($user) && $user != null){

                $otp = $this->generate_OTP();

                $updateOtp  = User::where('id',$user->id)->update(array('otp' => $otp,'request_otp' => 0));
                $to_name    = $user->name;
                $to_email   = $email;
                $body       = 'Your OTP is : '.$otp.' Please do not share.';
                $data       = array('name'=>$to_name,'body' => $body);

                Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email) {
                    $message->to($to_email, $to_name)
                        ->subject('Resend Otp');
                    $message->from(config('mail.username'),'OnlineDirectory');
                });

                $result['code']       = 200;
                $result['error']      = false;
                $result['message']    = 'We have resend an OTP on your email.';
                $result['result']['otp'] = $otp;
                return response()->json($result);

            } else {

                $result['code'] = 404;
                $result['error'] = true;
                $result['message'] = 'Email does not exist in our records!';
                return response()->json($result);
            }

        }catch(Exception $e){

            return response()->json($e->getMessage());
        }

    }
/*
|--------------------------------------------------------------------------
| ==========  Random Number ==========
|--------------------------------------------------------------------------
*/ 

    public function getProfile(Request $request){

            $user = auth()->user();

            if(isset($user) && $user != null)
            {
                $data['code']      = 200;
                $data['error']     = false;
                $data['result']    = $user;
                return response()->json($data)->withHeaders([
                    'Content-Type' => 'application/json',
                    'Accept'       => 'application/json',
                ]);

            } else {

                return response()->json([
                    'error'        => true,
                    'code'         => 401,
                    'message'      => "Unauthorized"])->withHeaders([
                    'Content-Type' => 'application/json',
                    'Accept'       => 'application/json',
                ]);
            }

    }

/*
|--------------------------------------------------------------------------
| ==========  Update Profile ==========
|--------------------------------------------------------------------------
*/ 

    public function updateProfile(Request $request)
    {
        try{

            $validation = Validator::make($request->all(),
                [
                    'name'             => 'required',
                    'profile_image'    => 'required|mimes:jpg,jpeg,png,gif'
                ]);

            if($validation->fails()) {

                $result['code'] = 400;
                $result['error'] = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);

            }
            $user = auth()->user();
        
            $user->name      = $request->input('name');
            if ($request->hasFile('profile_image')) {

                try {

                    $profile_image = $request->file('profile_image');

                    $name = $profile_image->getClientOriginalName();

                    $destinationFolder = public_path('/profile_pictures/user-' . $user->id . '/');

                    // Delete folder
                    if(file_exists($destinationFolder.$name)){

                        unlink($destinationFolder.$name);
                    }

                    $profile_image->move($destinationFolder, $name);
                    $user->profile_image ="public/profile_pictures/user-".$user->id.'/'.$name;

                } catch (Exception $e) {

                    $result['code'] = 500;
                    $result['error'] = true;
                    $result['message'] = $e->getMessage();
                    return response()->json($result);
                }
            }

            $user->save();

            $result['code'] = 200;
            $result['error'] = false;
            $result['message'] = "Profile Updated Successfully.";
            $result['result'] = $user;
            return response()->json($result);

        }catch(Exception $e) {

            return response()->json($e->getMessage());

        }
    }

/*
|--------------------------------------------------------------------------
| ==========  Random Number ==========
|--------------------------------------------------------------------------
*/ 

    private function generate_OTP()
    {
        return $randomOtp = mt_rand(10000, 99999);
    }


/*
|--------------------------------------------------------------------------
| ==========  List Professionals ==========
|--------------------------------------------------------------------------
*/

    public function getProfessionalList(){    
		
        $professionals = User::where('type',2)->where('status',1)->get()->toArray();
        
        if(count($professionals) > 0){
			
                $result['code'] = 200;
                $result['error']  = false;
                $result['result'] = $professionals;                
                return response()->json($result);
		    }
                $result['code']    = 404;
                $result['error']   = true;
                $result['message'] = "Professional list is empty!";
                return response()->json($result);
	}

    public function logout(Request $request){
            
            $accessToken = Auth::user()->token();
            $token       = $request->user()->tokens->find($accessToken);
            $token->revoke();
            $result['code']    = 200;
            $result['error']   = true;
            $result['message'] = "Successfully logout";
            return response()->json($result);
    }

}
