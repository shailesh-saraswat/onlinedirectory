<script>
$(document).on('click', '#TargetID', function(e){
    e.preventDefault();
    var url = $(this).data('url');
    $('.user-modal').html('');  
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'html'
    })
    .done(function(data){
        $('.user-modal').html('');    
        $('.user-modal').html(data); // load response 
    })
    .fail(function(){
        $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
    });
    
});
</script>