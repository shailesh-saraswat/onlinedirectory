@extends('admin.index')

@section('content')

<!-- <div class="wrapper"> -->

<div class="content-wrapper">

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active">Site Setting</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Website Settings</h3>
            </div>
           
            @if(Session::has('message')) 
            <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div
            >@endif
            
            <div class="col-md-12">
           
            <!-- /.card-header -->
            <div class="card-body">
                 <form role="form" action="{{ url('/admin/sitesetting/update')}}" method="post" enctype="multipart/form-data"> 
				 @csrf
                <div class="row">

                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>{{'Site Title'}}</label>
                        <input type="text" class="form-control" id="site_name" name="site_name" value="{{ !empty($sitedata->site_name) ? $sitedata->site_name:'' }}">
                      </div>
                    </div>

                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>{{'Contact no.'}}</label>
                        <input type="text" class="form-control" id="site_contact_no" name="site_contact_no"  value="{{ !empty($sitedata->site_mobile) ? $sitedata->site_mobile:'' }}">
                      </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>{{'Logo'}}</label>
                            <input type="file"  class="form-control" name="header_logo"/>
                            <input type="hidden" name="header" value="{{ !empty($sitedata->header_logo) ? $sitedata->header_logo:'' }}">
                        </div>
                        @if(empty($sitedata->header_logo))
                         <img class="img-circle elevation-2" src="{{ asset('public/siteimages/site/defaultLogo.png') }}" alt="Default Logo" height="80px" width="80px">
                        @else
                        <img class="img-circle elevation-2" src="{!! asset($sitedata->header_logo)!!}" alt="Logo" height="80px" width="80px" id="site_logo"/>
                        @if(!empty($sitedata->header_logo))
                        <a href="javascript:void(0)"><i class="fa fa-pencil" title="Remove" onclick="remove_headerimage({{$sitedata->id}});"></i></a>
                        @endif
                        @endif
                       
                    </div>
                   
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>{{'Address Line1'}}</label>
                         <input type="text" class="form-control" id="site_address" name="site_address" value="{{ !empty($sitedata->site_address) ? $sitedata->site_address:'' }}">
                      </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>{{'Site  Email'}}</label>
                            <input type="text" class="form-control" id="email1" name="site_email1"  value="{{!empty($sitedata->site_email1) ? $sitedata->site_email1:''}}">
                        </div>
                    </div>
                   
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>{{'Address Line2'}}</label>
                          <input type="text" class="form-control" id="site_address" name="address_city" value="{{!empty($sitedata->address_city) ? $sitedata->address_city:''}}">
                      </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>{{'Footer About us'}}</label>
                            <textarea class="form-control" name="aboutus">{{!empty($sitedata->aboutus) ? $sitedata->aboutus:''}}</textarea>
                        </div>
                    </div>
                   
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>{{'Address Line3'}}</label>
                           <input type="text" class="form-control" id="site_country" name="site_country" value="{{!empty($sitedata->site_country) ? $sitedata->site_country:''}}">
                      </div>
                    </div>
                </div>
                
               <div class="row">

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>{{'Contact Mobile'}}</label>
                            <input type="text" class="form-control" name="contact_mobile" value="{{!empty($sitedata->contact_mobile) ? $sitedata->contact_mobile:''}}"></textarea>
                        </div>
                    </div>
                   
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>{{'Open Days'}}</label>
                           <input type="text" class="form-control" id="days_open" name="days_open" value="{{!empty($sitedata->days_open) ? $sitedata->days_open:''}}">
                      </div>
                    </div>
                </div>


                <div class="row">

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>{{'Contact Email'}}</label>
                            <input type="text" class="form-control" name="contact_email" value="{{!empty($sitedata->contact_email) ? $sitedata->contact_email:''}}"></textarea>
                        </div>
                    </div>
                   
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>{{'Contact Website'}}</label>
                           <input type="text" class="form-control" id="contact_website" name="contact_website" value="{{!empty($sitedata->contact_website) ? $sitedata->contact_website:''}}">
                      </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>{{'Contact Location'}}</label>
                            <input type="text" class="form-control" name="contact_location" value="{{!empty($sitedata->contact_location) ? $sitedata->contact_location:''}}"></textarea>
                        </div>
                    </div>

                     <div class="col-sm-6">
                        <div class="form-group">
                            <label>{{'Contact Map'}}</label>
                            <input type="text" class="form-control" name="map_link" value="{{!empty($sitedata->map_link) ? $sitedata->map_link:''}}"></textarea>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>{{'Facebook Link'}}</label>
                            <input type="text" class="form-control" name="fb_link" value="{{!empty($sitedata->fb_link) ? $sitedata->fb_link:''}}"></textarea>
                        </div>
                    </div>

                     <div class="col-sm-6">
                        <div class="form-group">
                            <label>{{'Twitter Link'}}</label>
                            <input type="text" class="form-control" name="twitter_link" value="{{!empty($sitedata->twitter_link) ? $sitedata->twitter_link:''}}"></textarea>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>{{'Instagram Link'}}</label>
                            <input type="text" class="form-control" name="instagram_link" value="{{!empty($sitedata->instagram_link) ? $sitedata->instagram_link:''}}"></textarea>
                        </div>
                    </div>

                     <div class="col-sm-6">
                        <div class="form-group">
                            <label>{{'Linked in Link'}}</label>
                            <input type="text" class="form-control" name="linkedin_link" value="{{!empty($sitedata->linkedin_link) ? $sitedata->linkedin_link:''}}"></textarea>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>{{'Copyright Text'}}</label>
                            <input type="text" class="form-control" name="copy_right" value="{{!empty($sitedata->copy_right) ? $sitedata->copy_right:''}}"></textarea>
                        </div>
                    </div>
                </div>

            </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-info">Save</button>
                    <a href="{{url('admin/dashboard')}}" class="btn btn-default float-right">Cancel</a>
                </div>
                 
                </form>
            
            <!-- /.card -->
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
</div>
</section>

</div>

@section('after-scripts')
<script type="text/javascript">
   function remove_headerimage(id)
    {
        $.ajax({
        type: 'GET',
        data: {'id':id},
        url: "sitesetting/remove_headerimage",
        success: function(result){
          alert( 'Removed');
            location.reload();
        }});
    }
	function remove_footerimage(id)
    {
        $.ajax({
        type: 'GET',
        data: {'id':id},
        url: "sitesetting/remove_footerimage",
        success: function(result){
          alert( 'Removed');
            location.reload();
        }});
    }
	</script>
@endsection

@endsection