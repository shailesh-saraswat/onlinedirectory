@extends('admin.index')

@section('after-style')

    <link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endsection

@section('content')

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">Manage Categories</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Category List</h3>

                                <a href="{{url('admin/category-add')}}" style="float: right;"><i class="fas fa-plus-square" style="font-size:25px;"></i></a>
                            </div>
                            @if(session('message'))
                                <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
                            @endif
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="category-list" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Category Name</th>                                          
                                            <th>Icon</th>                                          
                                            <th>Parent Category</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($categories) && $categories != null)
                                            <?php $i = 0; ?>
                                            @foreach($categories as $key => $category)
                                                <tr>
                                                    <td><?php $i++;?>{{$i}}</td>
                                                    <td>{{$category->name}}</td>
                                                    <td>
                                                    @if($category->icon)
                                                    <img src="{{asset($category->icon)}}" style="width:50px; height:50px; margin-top:10px;"> 
                                                    @else 
                                                    <img src="{{asset('public/uploads/categoryIcon/default.jpg')}}" style="width:50px; height:50px; margin-top:10px;"> 
                                                    @endif

                                                    </td>
                                                    <td>{{$category->parent_name}}</td>
                                                   
                                                    <td class="status">{{isset($category->status) && $category->status == 1 ? 'Active' : 'In-active'}}</td>
                                                    <td>
                                                        <a data-toggle="modal" id="TargetID" data-target="#categoryInfo" data-url="{{ url('admin/show/category',['id'=>$category->id])}}" href="javascript(0)"><i class="fas fa-eye"></i>&nbsp;&nbsp;</a></a>
                                                        <a href="{{url('admin/category-edit/'.$category->id)}}"><i class="fas fa-edit"></i>&nbsp;&nbsp;</a>
                                                        <a href="{{url('admin/category-delete/'.$category->id)}}" onclick="return confirm('Are you sure you want to delete?')" ><i class="fas fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>

                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
       
        <!-- /.content -->
    
        <div class="modal fade" id="categoryInfo">
            <div class="modal-dialog modal-lg">
                <div class="modal-content user-modal">
                        
                </div>
            </div>
        </div>

    </div>

@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<script>
    $(function () {
        $("#category-list").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });
</script>
<script>
$(document).ready(function(){

    $('td.status').each(function() {
        if ($(this).text() == 'Active') {
            $(this).css('color', 'green');
        }
        else{
            $(this).css('color', 'red');
        } 
    });
});
</script>

@include('admin.partials.customjsModal')

@endsection

