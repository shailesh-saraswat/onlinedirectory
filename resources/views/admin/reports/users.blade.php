@extends('admin.index')

@section('after-style')

<link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
{{-- <link rel="stylesheet" href="{{asset('public/plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('public/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}"> --}}
@endsection

@section('content')

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Users Report</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header">
                        <h3 class="card-title"> Users Report</h3>
                        <div class="card-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <select name="filter" id="filter" class="form-control select2" style="width: 100%;">
                                    <option value="all">All</option>
                                    <option value="today" {{ isset($filtertype) && $filtertype == 'today' ? 'selected' : ''}}>Today</option>
                                    <option value="week" {{ isset($filtertype) && $filtertype == 'week' ? 'selected' : ''}}>Weekly</option>
                                    <option value="month" {{ isset($filtertype) && $filtertype == 'month' ? 'selected' : ''}}>Monthly</option>
                                </select>  

                                 <a class="btn btn-tool btn-sm" style="margin: 0 !important;" href="{{Route('exportUser',['type' => $filtertype])}}" class="btn btn-primary m-2"><i class="fas fa-download"></i></a>
                               
                                    
                                </a>
                            </div>
                        </div>
                    </div>
    
                        <div class="card-body">
                            <table id="user-list" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                         <th>Date</th>
                                        <th>Status</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $i = 1;
                                    @endphp
                                @foreach($result as $res)    
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$res->first_name}}</td>
                                    <td>{{$res->last_name}}</td>
                                    <td>{{$res->email}}</td>
                                    <td>{{$res->created_at}} </td>
                                    <td>
                                    @if($res->status == 1)
                                        <p style="color:green">Active</p>  
                                    @elseif($res->status == 0)
                                        <p style="color:red">In-active</p>   
                                    @elseif($res->status == 2)
                                        <p style="color:red">Pending</p>   
                                    @endif
                                    </td>
                                    
                                </tr>
                                @endforeach       
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

</div>

@endsection
@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script>
$(function() {
    $("#user-list").DataTable({
        "responsive": true,
        "autoWidth": false,
    });
}); 

$('#filter').change(function(){
    var query =[];
    var typename = $('#filter').val();
    if(typename.length && typename!='#'){
		query.push('type='+typename);
        window.location.href = "{{url('/admin/userview')}}?"+query.join();
	}
	
});
</script>

@endsection