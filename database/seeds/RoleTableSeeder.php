<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Role            = new \App\Models\Role();
        $Role->name      = "Admin";
        $Role->role_text = "admin";
        $Role->status    = 1;
        $Role->save();

        $Role            = new \App\Models\Role();
        $Role->name      = "Professional";
        $Role->role_text = "professional";
        $Role->status    = 1;
        $Role->save();

        $Role            = new \App\Models\Role();
        $Role->name      = "User";
        $Role->role_text = "user";
        $Role->status    = 1;
        $Role->save();
	

    }
}
