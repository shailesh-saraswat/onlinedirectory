<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Support\Facades\Redirect;
use Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories     = Category::leftJoin("categories as parent_cat","categories.parent_id" ,'=', 'parent_cat.id')
                        ->select("categories.*", "parent_cat.name as parent_name")
                        ->orderBy('created_at', 'desc')
                        ->get(); 
        return view('admin.category.index')->withCategories($categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('parent_id','=',0)->where('status',1)->get();
        return view('admin.category.create')->withCategories($categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        try{

        $validation = Validator::make($request->all(), [
            
                'name'          => "required|string|max:255",
                'status'        => "required"
		]);
        
        if ($validation->fails()) {
			return  redirect()->back()->withErrors($validation)->withInput();
		} else {
                $category 			    = new Category();
                
                if($request->hasFile('icon')) {
                    $image   = $request->file('icon');
                    $caticon = auth()->user()->id.'_catIcon'.time().'.'.request()->icon->getClientOriginalExtension();
                    $destinationPath = public_path('uploads/categoryIcon');
                    $image->move($destinationPath, $caticon);
                    $category->icon           = "public/uploads/categoryIcon/".$caticon;
                }

                $category->parent_id	= (!empty($request->parent_id))?$request->parent_id:0;
                $category->name 		= $request->name;
                $category->slug 		= strtolower(str_replace(" ","-", $request->name));
                $category->status 	    = $request->status;
                $category->user_id      = auth()->user()->id;
    
			if($category->save()) {
                return Redirect::to('admin/categories-list')->withMessage("Category Added Successfully.");
			}
        }
        }catch(\Exception $e){

                return redirect()->back()
                    ->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
         $categories = Category::leftJoin("categories as parent_cat","categories.parent_id" ,'=', 'parent_cat.id')
            ->select("categories.*", "parent_cat.name as parent_name")
            ->where('categories.id',$id)
            ->orderBy('created_at', 'desc')
            ->first();
        return view('admin.category.view',['category'=>$categories]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {

            $category = Category::findOrfail($id);
            $categories     = Category::where('parent_id','=',0)->where('status',1)->get();

            return view("admin.category.edit",
                [   
                    'categories'    =>  $categories,
                    'category' 	    => 	$category
                ]
            ); 

        }catch(\Exception $e){

            return redirect()->back()
                    ->withErrors($e->getMessage());

        }    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {   

        try {
        
            $validation = Validator::make($request->all(), [
                'name'          => "required|string|max:255",
                'status'        => "required"
		]);
		
		if ($validation->fails()) {
			return  redirect()->back()->withErrors($validation)->withInput();
		} else {

                $categoryId                     = $request->input('id');
                $categoryUpdate                 = Category::query()->where('id', $categoryId)->first();

                if($request->hasFile('icon')){
                    $image = $request->file('icon');
                    $caticon = auth()->user()->id.'_catIcon'.time().'.'.request()->icon->getClientOriginalExtension();
                    $destinationPath = public_path('uploads/categoryIcon');
                    $image->move($destinationPath, $caticon);
                    $categoryUpdate->icon           = "public/uploads/categoryIcon/".$caticon;
            
                } 
                
                $categoryUpdate->parent_id	    = (!empty($request->parent_id))?$request->parent_id:0;
                $categoryUpdate->name 		    = $request->name;
                $categoryUpdate->slug 		    = strtolower(str_replace(" ","-", $request->name));
                $categoryUpdate->status 	    = $request->status;
                $categoryUpdate->user_id        = auth()->user()->id;
            
			if($categoryUpdate->update()) {
                return Redirect::to('admin/categories-list')->withMessage("Category Updated Successfully.");
			}

        }

        }catch(\Exception $e){

            return redirect()->back()
                    ->withErrors($e->getMessage());

        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $getCategory = Category::query()->where('id', $id)->first();

        $getCategory->delete();

        $message = "Category Deleted Successfully!";
        return redirect()->back()->withMessage($message);
    }
}
