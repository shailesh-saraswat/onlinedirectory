<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\{SiteSetting,ContactDetail};
use Session;
use File;

class SiteSettingController extends Controller
{
    public function index($id='')
    {   
		$sitedata = SiteSetting::first(); 
    	return view('admin.website.sitesetting.index',compact('sitedata'));
    }

    public function remove_headerimage(){
		$id       =  $_GET['id'];
		$sitedata = SiteSetting::where('id',$id)->first();
		$image_path=$sitedata->header_logo;
		if(File::exists($image_path)) {
		   File::delete($image_path);
						}
		$sitedata->header_logo='';
		$sitedata->save();
		
	}
    public function update(Request $request,$id='')
	{   
        
		$sitedata=SiteSetting::where('id','1')->first();
		$target='siteimages/site/headerlogo/';
		$header_logo=$request->file('header_logo');
		if(!empty($header_logo))
		{
			$image_path=$sitedata->header_logo;
			if(File::exists($image_path)) {  File::delete($image_path);	}
			$headerImageName=$header_logo->getClientOriginalName();
			$ext1=$header_logo->getClientOriginalExtension();
			$temp1=explode(".",$headerImageName);
			$newHeaderLogo='logo_header'.".".end($temp1);
			$headerTarget=$target.$newHeaderLogo;
			$header_logo->move($target,$newHeaderLogo);
		}
		else
		{
			$headerTarget=$request->input('header');
		}

    		$footer_logo=$request->file('footer_logo');
			$target1='siteimages/site/innerlogo/';
    	if(!empty($footer_logo))
 		{
			$image_path=$sitedata->footer_logo;
				if(File::exists($image_path)) {  File::delete($image_path);	}
			$footerImageName=$footer_logo->getClientOriginalName();
			$ext2=$footer_logo->getClientOriginalExtension();
			$temp2=explode(".",$footerImageName);
			$newFooterLogo='logo_inner'.".".end($temp2);
			$footerTarget=$target1.$newFooterLogo;			
			$footer_logo->move($target1,$newFooterLogo);
		}
		else
		{
			$footerTarget=$request->input('footer');
		}    	
		
    	$sitedata->site_name        =$request->input('site_name');
		$sitedata->header_logo      =$headerTarget;
    	$sitedata->inner_logo       =$footerTarget;
    	$sitedata->site_address     =$request->input('site_address');
		$sitedata->address_city     =$request->input('address_city');
    	$sitedata->site_mobile      =$request->input('site_contact_no');
		$sitedata->site_country     =$request->input('site_country');
    	$sitedata->site_email1      =$request->input('site_email1');
        $sitedata->aboutus          =$request->input('aboutus');   
		$sitedata->contact_mobile   =$request->input('contact_mobile'); 
		$sitedata->days_open        =$request->input('days_open');
		$sitedata->contact_email    =$request->input('contact_email');	
		$sitedata->contact_website  =$request->input('contact_website');
		$sitedata->contact_location =$request->input('contact_location');
		$sitedata->map_link         =$request->input('map_link');
    	$sitedata->fb_link          =$request->input('fb_link');
    	$sitedata->twitter_link     =$request->input('twitter_link');
        $sitedata->instagram_link   =$request->input('instagram_link'); 
		$sitedata->linkedin_link    =$request->input('linkedin_link');
		$sitedata->copy_right       =$request->input('copy_right');     
    	
		/*\$sitedata->youtube_link     =$request->input('youtube_link');
    	$sitedata->pin_link         =$request->input('pin_link');    	
    	$sitedata->apple_link=$request->input('apple_link');
        $sitedata->playstore_link=$request->input('playstore_link');
        $sitedata->account_deactive_time=$request->input('account_deactive_time');*/
    	$sitedata->save();
    	Session::flash('message', 'Site Updated Successfully!');
		return redirect('/admin/sitesetting');
	}

	public function remove_footerimage(){
		$id=$_GET['id'];
		$sitedata=SiteSetting::where('id',$id)->first();
		$image_path=$sitedata->inner_logo;
		if(File::exists($image_path)) {
		   File::delete($image_path);
		}
		$sitedata->inner_logo='';
		$sitedata->save();
		
	}

}
