@extends('admin.index')
@section('after-style')
<link rel="stylesheet" href="{{asset('public/plugins/summernote/summernote-bs4.css')}}">
@endsection
@section('content')

<!-- <div class="wrapper"> -->

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                     
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">FAQ</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10">

                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Create FAQ</strong></h3>
                            </div>
                            {{-- @include('admin.partials.messages') --}}
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form class="form-horizontal" method="post" action="{{url('admin/faq/store')}}" novalidate>
                                @csrf
                            <div class="card-body">
                            <div class="form-group row">
                                <label for="title" class="col-sm-2 col-form-label">Question</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="question" placeholder="Question" name="question" value="{{old('question')}}"/>
                                @error('question')
                                <span class="invalid-feedback" role="alert" style="display:block;">
                                <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="title" class="col-sm-2 col-form-label">Answer *</label>
                            <div class="col-sm-10">
                            <div class="mb-3">
                                <textarea class="textarea" name="answer" id="answer" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{old('answer')}}</textarea>
                                @error('answer')
                                <span class="invalid-feedback" role="alert" style="display:block;">
                                <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            </div>
                            </div>

                                

                            <div class="form-group row">
                                <label for="title" class="col-sm-2 col-form-label">Display Order</label>
                                <div class="col-sm-10">
                                <input type="number" class="form-control" id="display_order" name="display_order" Placeholder="Display Order" value="{{old('display_order')}}"/>
                                @error('display_order')
                                <span class="invalid-feedback" role="alert" style="display:block;">
                                <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                </div>
                            </div>
                                
                            <div class="form-group row">
                                <label for="Status" class="col-sm-2 col-form-label">Status</label>
                            <div class="col-sm-10">
                                <select class="form-control select2" style="width: 100%;" name="status">
                                    <option value="" disabled>Status</option>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select> 
                                @error('status')
                                <span class="invalid-feedback" role="alert" style="display:block;">
                                <strong>{{ $message }}</strong>
                                </span>
                                @enderror   
                                </div>
                            </div>

                            </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                <button type="submit" class="btn btn-info">Save</button>
                                <a href="{{url('admin/faq')}}" class="btn btn-default float-right">Cancel</a>
                                </div>
                                <!-- /.card-footer -->
                            </form>
                            </div>

                    </div>
                    <div class="col-1"></div>
                </div>
            </div>
        </section>

    </div>

<!-- </div> -->
@endsection

@section('after-scripts')

<script src="{{asset('public/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script>

@endsection