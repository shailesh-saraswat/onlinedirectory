<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserSubscription;
use App\Models\User;
use App\Models\Plans;
use Exception;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use App\Libraries\ResponseFactory;


class SubscriptionController extends Controller
{

    public function getPlans(){
        
        try{
            $user = auth()->user();
            
            if(isset($user) && $user != null) {
            
            $plans = Plans::where('status', 1)->where('deleted', '0')->orderBy('id','DESC')->get();
            
            $plandata  = [];
            foreach($plans as $mkey=>$v){
                $plandata[$mkey]['id'] = $v->id;
                $plandata[$mkey]['name'] = $v->name;
                $plandata[$mkey]['amount'] = "$".$v->amount."/".ucwords($v->interval);
                $plandata[$mkey]['description'] = preg_replace("/[\n\r]/"," ",$v->description);
                $plandata[$mkey]['interval']    = $v->interval;
                $plandata[$mkey]['plan_id']     = strtolower($v->plan_id);
            }

            if(isset($plans) && $plans != null){

                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $plandata;
                return response()->json($result);
            }
            else{

                $result['code'] = 404;
                $result['error'] = true;
                $result['result'] = "No plan found!";
                return response()->json($result);
            }
        } else {

                $result['code']    = 401;
                $result['error']   = true;
                $result['message'] = "Unauthorized";
                return response()->json($result);
        }

        }catch(Exception $e){

            return response()->json($e->getMessage());

        }
    }

    /* ========  Subscription Part   ========  */

    public function makeSubscription(Request $request)
    {
        
        try {
            
            $token = $request->header('Authorization');
            if(isset($token)) {
            
            $validation = Validator::make($request->all(), [
                
                "plan_id"        => 'required',
                //"price"          => 'required',
                "transaction_id"  => 'required',
                "google_plan_id" => 'required'
                
            ]);

            
            if ($validation->fails()) {
                
                return response()->json([
                    'code'  => 400,
                    'error'        => true,
                    'message'      => $validation->messages()->first()
                ]);
            } 
            
            $checkPlan = Plans::where(['plan_id' => $request->plan_id, 'status' => 1])->where('deleted', '0')->first();

            if(!isset($checkPlan))
            {
                return response()->json([
                    'code'  => 404,
                    'error'        => true,
                    'message'      => "Sorry! Plan does not found."
                ]);
            }

            $days = 1;
            if($checkPlan->interval == "month"){
                $days = 30;
            }
            if($checkPlan->interval == "year"){
                $days = 365;
            }

            $subscription = UserSubscription::create([
                'cust_id'              => auth()->id(),
                'user_id'              => auth()->id(),
                'transaction_id'       => $request->transaction_id,
                'plan_id'              => $request->plan_id,
                'customer_name'        => auth()->user()->first_name." ".auth()->user()->last_name,
                'google_plan_id'       => $request->google_plan_id,
                'start_date'           => \Carbon\Carbon::now(),
                'renewal_date'         => \Carbon\Carbon::now()->addDays($days),
                'total_amount_received'=> $checkPlan->amount,
                'fee'                  => $checkPlan->amount,
                'device_type'          => $request->device_type
            ]);

            if($subscription){

                auth()->user()->update([
                    'is_subscribed'     => 1,
                    'is_trial'          => 0,
                    'free_trial_end'    => null,   

                ]);

                return response()->json([
                    'code'         => 200,
                    'error'         => false,
                    'message'       => "You have subscribed successfully"
                ]);
                
            }else {
                return response()->json([
                    'code'   => 500,
                    'error'         => true,
                    'message'       => "Something went wrong!"
                ]);
            }
        
        } else {

                $result['code']    = 401;
                $result['error']   = true;
                $result['message'] = "Unauthorized";
                return response()->json($result);
        }

        } catch (Exception $e) {
            return response()->json($e->getMessage());
        } 
    }

    /* ======== Check login subscription status ========  */

    public function SubscriptionLoginStatus(Request $request)
    {
        try {
            
            $token = $request->header('Authorization');
            if(isset($token)) {
            $user = auth()->user();
            $subscribedStatus = User::where('id', auth()->user()->id)->where('status','1')->where(function ($query){
                $query->where(['is_subscribed'=>'0','is_trial' => '0']);
            })->first();
            
            if(isset($subscribedStatus)){
                return response()->json([
                    'code'           => 200,
                    'error'          => false,
                    'is_trial'       => $user->is_trial,
                    'is_subscribed'  => $user->is_subscribed
                ]);
            } else {
                return response()->json([
                    'code'           => 200,
                    'error'          => false,
                    'is_trial'       => $user->is_trial,
                    'is_subscribed'  => $user->is_subscribed
                ]);
            }
            
        } else {

                $result['code']    = 401;
                $result['error']   = true;
                $result['message'] = "Unauthorized";
                return response()->json($result);
        }

        } catch (Exception $e) {
            return response()->json($e->getMessage());
        } 
    }
    

    /* ======== Check Current plan's ========  */

    public function CurrentPlan(Request $request)
    {
        try {
            
        $token = $request->header('Authorization');
        
        if(isset($token)) {

            $user = auth()->user();
            $now = \Carbon\Carbon::now()->format('Y-m-d');
            //return $user->free_trial_end. " " .\Carbon\Carbon::now(); 
            if($user->is_trial == 1 && $user->free_trial_end > $now){
               
                $result['code']    = 200;
                $result['error']   = false;
                $result['result']  = null;
                $result['is_trial']   = $user->is_trial;
                $result['is_subscribed']  = $user->is_subscribed;
                return response()->json($result);
            }
            if($user->is_trial == 1 && $user->free_trial_end < $now){
                
                $user->update([
                    'is_trial' => 0,
                    'free_trial_end'    => null,
                ]);
            }
            $subscription = $user->subscriptions;

            if(isset($subscription) && $subscription != '[]'){
               
                if($subscription->renewal_date < $now){
                    
                    $user->update([
                        'is_subscribed'     => 0
                    ]);
                }
                
                 $currentPlan = UserSubscription::with('plan')->where('user_id', auth()->user()->id)->where(function ($query){
                    $query->where(['status'=>'1']);
                })->orderBy('id', 'DESC')->first();

                 $plans = Plans::where('plan_id',$currentPlan->plan_id)->first();

                $active_plan = [];
                

                return response()->json([

                        'code'       => 200,
                        'error'      => false,
                        'is_trial'   => $user->is_trial,
                        'is_subscribed' => $user->is_subscribed,
                        'result'     => $plans,
                    ]);

            } else {
                
                $result['code']    = 200;
                $result['error']   = false;
                $result['result']  = null;
                $result['is_trial']   = $user->is_trial;
                $result['is_subscribed']  = $user->is_subscribed;
                return response()->json($result);
            }
            
           
        } else {

                $result['code']    = 401;
                $result['error']   = true;
                $result['message'] = "Unauthorized";
                return response()->json($result);
        }

        } catch (Exception $e) {
            return response()->json($e->getMessage());
        } 
    }

    /* check free trial */

    public function checkfreeTrial(Request $request)
    {
        try {
            
            $token            = $request->header('Authorization');
            if(isset($token)) {
            $user             = auth()->user();
            $afterExpireTrail = User::where('id', auth()->user()->id)->where('status','1')->where(function ($query){
                $query->where('free_trial_end', '>=', \Carbon\Carbon::now()->addDays(15));
            })->get(); 
            /* return $afterExpireTrail; */ 
            if(count($afterExpireTrail) > 0) {
                $expireTrail = $user->update(['is_trial'=>2]);
                return response()->json([
                    'code'        => 200,
                    'error'       => false,
                    'is_trial'    => $user->is_trial
                ]); 
            }   
            
            $date             = \Carbon\Carbon::now()->addDays(15);
            /* if(isset($user) && $user != null) { */
                $freetrial = $user->update(['is_trial'=>1,'free_trial_end'=>$date]);
                
                return response()->json([
                    'code'        => 200,
                    'error'       => false,
                    'is_trial'    => $user->is_trial
                ]);
                
            } else {

                    $result['code']    = 401;
                    $result['error']   = true;
                    $result['message'] = "Unauthorized";
                    return response()->json($result);
            }

            } catch (Exception $e) {
                return response()->json($e->getMessage());
            } 
    }
}
