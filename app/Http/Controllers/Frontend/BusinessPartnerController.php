<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\{Country,State,City,BusinessInformation,Role,Template,User,Category,SiteSetting,Visitor,Plans,UserSubscription};
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Rules\MatchOldPassword;
use Hash, Session,DB;
use Illuminate\Support\Carbon;
use Cartalyst\Stripe\Stripe;
use Illuminate\Support\Str;

class BusinessPartnerController extends Controller
{   
    public function __construct() {
        $this->getSiteInfo(); 
    }

    public static function getSiteInfo()
    {
        $siteInfo=SiteSetting::first();  
        Session::put('sitedata',$siteInfo);       
    }

    public function index(){ 
        $today = Visitor::where('userid',auth()->user()->id)->whereDate('created_at', Carbon::today())->count();
        $week  = Visitor::where('userid',auth()->user()->id)->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
        $month = Visitor::where('userid',auth()->user()->id)->whereMonth('created_at', date('m'))
        ->whereYear('created_at', date('Y'))
        ->count();
        return view('frontend.professional.home',compact('today','week','month')); 
    }

    public function businessloginPage(){
        $categories     = Category::where('parent_id','=',0)->where('status',1)->orderBy("id", "asc")->get();
        return view('frontend.auth.businesslogin',compact('categories'));

    }

    public function businessRegisterPage(){
        $countries      = Country::get();
        $categories     = Category::where('parent_id','=',0)->where('status',1)->orderBy('name')->get();
        return view('frontend.auth.businessregister',compact('categories','countries'));
    }

    public function getState(Request $request)
    {
        $data['states'] = State::where("country_id",$request->country_id)
                    ->get(["name","id"]);
        return response()->json($data);
    }

    public function getCity(Request $request)
    {
        $data['cities'] = City::where("state_id",$request->state_id)
                    ->get(["name","id"]);
        return response()->json($data);
    }

    public function businessRegisterStore(Request $request){
            
            //try{
             
            $rules = [
                'first_name' => ['required','regex:/^[a-zA-Z ]+$/u','max:255'],
                'last_name'  => ['required','regex:/^[a-zA-Z ]+$/u','max:255'],
                'password'  => ['required','between:6,15'],
                'confirm_password'  => ['required','between:6,15','same:password'],
                'business_name'  => ['required'],
                'country'  => ['required'],
                'location'  => ['required'],
                'zip_code'  => ['required','min:6'],
                'email' => [
                    'required',
                    'string',
                    'email',
                    'max:255',
                    'unique:users',
                    'regex:/^\w+[-\.\w]*@()\w+[-\.\w]*?\.\w{2,4}$/',\Illuminate\Validation\Rule::unique('users')
                ],
                'mobile' => ['required','between:6,15', \Illuminate\Validation\Rule::unique('users')]
            ];
        
            $messages = [
                'email.regex' => 'We do not accept mails with this domain.',
                'mobile.between' => 'Mobile number must be between 6 to 15 digits.'
            ];

            $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {    
			return  redirect()->back()->withErrors($validation)->withInput(Input::all());
		} else {

                $professional 		         = new User();
                $professional->roles_id 	 =  2;
                $professional->first_name 	 = $request->first_name;
                $professional->last_name 	 = $request->last_name;
                $professional->email 	     = $request->email;
                $professional->password 	 = Hash::make($request->password);
                $professional->mobile 	     = $request->mobile;
                $professional->business_name = $request->business_name;
                $professional->category_id   = $request->business_category;
                $professional->slug          = Str::slug($request->first_name , "-");
                $professional->location 	 = $request->location;
                $professional->country 	     = $request->country;
                $professional->state 	     = $request->state;
                $professional->city 	     = $request->city;
                $professional->latitude 	 = $request->latitude;
                $professional->longitude 	 = $request->longitude;
                $professional->free_trial_end = date('Y-m-d',strtotime('+15 days',strtotime('now')));
                $professional->zip_code 	 = $request->zip_code;
                $professional->template_id 	 = 1;
                $professional->is_trial 	 = 1;
                $professional->is_subscribed = 0;
                $professional->device_type 	 = "";
                $professional->status 	     = 0;

			if($professional->save()) {
                return redirect()->route('business.login')->withMessage("Professional Created Successfully.");
			} 
        }
        /* }catch(\Exception $e){
            return redirect()->back()
            ->withErrors($e->getMessage());
        }  */
    }

    
    public function businessloginStore(Request $request)
    {   
        
        $rules = array(
            'email'    => 'required|email|max:255|exists:users',
            'password' => 'required'
        );
        
        $validator    = Validator::make(Input::all(), $rules);
        
        if($validator->fails()) {
            return redirect()->route('business.login')
            ->withErrors("Email and Password is required to login.")
            ->withInput(Input::except('password'));
            
        }
        
        $userdata = array(
            'email'    => Input::get('email'),
            'password' => Input::get('password'),
        );

        if(Auth::attempt($userdata)) {

        $model  = User::with('roles')->where('email',$request->email)->first();
        if(null != $model){
            if($model->status == 0){
                Auth::logout();
                Session::flush();
                $request->session()->flush();

               return redirect()->route('business.login')
                    ->withErrors("Forbidden! Your Status is Inactive.")
                    ->withInput(Input::except('password'));
            } else if($model->status == 2){
                Auth::logout();
                Session::flush();
                $request->session()->flush();

               return redirect()->route('business.login')
                    ->withErrors("Forbidden! Your Status is Pending.")
                    ->withInput(Input::except('password'));
            } else if($model->roles->role_text == "admin" || $model->roles->role_text == "user"){
                Auth::logout();
                Session::flush();
                $request->session()->flush();

                return redirect()->route('business.login')
                    ->withErrors("Forbidden! Invalid credentials.")
                    ->withInput(Input::except('password'));
            }
            return redirect()->route('business.partner.home');
        }
        } else {

            return redirect()->route('business.login')
                ->withErrors("The credentials do not match with our records.")
                ->withInput(Input::except('password'));

        }     
    
	}

    public function BusinessPartnerProfile(){
        $businessPartnerProfileData = auth()->user();
        return view('frontend.professional.businessPartnerprofile',compact('businessPartnerProfileData',$businessPartnerProfileData));
    }

    public function BusinessPartnerEditProfile(){
        $businessPartnerEditProfileData = auth()->user();
        return view('frontend.professional.businessPartnerEditprofile',compact('businessPartnerEditProfileData',$businessPartnerEditProfileData));
    }

    public function BusinessPartnerUpdateProfile(Request $request){

        try {
            
        $validation = Validator::make($request->all(), [

                'first_name'    => "required|string|regex:/^[a-zA-Z ]+$/u|max:255",
                'last_name'     => "required|string|regex:/^[a-zA-Z ]+$/u|max:255",
                'email'         =>  ['required','max:255','regex:/^\w+[-\.\w]*@(?!(?:)\.com$)\w+[-\.\w]*?\.\w{2,4}$/', \Illuminate\Validation\Rule::unique('users')->ignore($request->input('profileID'))],
                'mobile'        => ['required','between:6,15',\Illuminate\Validation\Rule::unique('users')->ignore($request->input('profileID'))], 
                'business_name' => "required",
                'location'      => "required"
            ],
    
            [
                'email.regex' => 'We do not accept mails with this domain.'
            ],
            [
                'mobile.between' => 'Mobile number must be between 6 to 15 digits.'
            ]
        );  

        if ($validation->fails()) {
			return  redirect()->back()->withErrors($validation)->withInput();
        } else{

            $profileID                       = $request->input('profileID');
            $bpProfileUpdate                 = User::findOrFail($profileID);
            $bpProfileUpdate->first_name     = $request->first_name;
            $bpProfileUpdate->last_name      = $request->last_name;
            $bpProfileUpdate->email 	     = $request->email;
            $bpProfileUpdate->mobile 	     = $request->mobile;
            $bpProfileUpdate->business_name  = $request->business_name;
            $bpProfileUpdate->slug           = Str::slug($request->first_name , "-");
            $bpProfileUpdate->location 	     = $request->location;
            $bpProfileUpdate->latitude 	     = $request->latitude;
            $bpProfileUpdate->longitude 	 = $request->longitude;
            $bpProfileUpdate->zip_code 	     = $request->zip_code;
            $bpProfileUpdate->device_type 	 = "";

            /* if ($request->hasFile('avatar')) {

            try {

                $profile_image = $request->file('avatar');
                $name = $profile_image->getClientOriginalName();
                $destinationFolder = public_path('profile_pictures/professional-' . $bpProfileUpdate->id . '/');

                // Delete folder
                if(file_exists($destinationFolder.$name)){
                    unlink($destinationFolder.$name);
                }
                $profile_image->move($destinationFolder, $name);
                $bpProfileUpdate->profile_image ="public/profile_pictures/professional-".$bpProfileUpdate->id.'/'.$name;

                } catch (\Exception $e) {
                    return redirect()->back()->withErrors($e->getMessage());
                }
            } */

			if($bpProfileUpdate->update()) {
                return Redirect::route('business.partner.profile')->withMessage("Profile updated successfully");
			}
        }
        }catch(\Exception $e){
                return redirect()->back()
                    ->withErrors($e->getMessage());
        } 
    }

    public function BusinessPartnerChangePassword(){
        $bpPartner = auth()->user();
        return view('frontend.professional.businessPartnerChangePassword');
    }

    public function BusinessPartnerUpdatePassword(Request $request){
        
        try {
        
        $validation = Validator::make($request->all(), [
              
            'old_password'     => 'required',
            'new_password'     => 'required|between:8,16',
            'confirm_password' => 'required|between:8,16|same:new_password'
		],
        [
            'old_password.required' => 'Current password does not match'
        ]
        
        );

        if ($validation->fails()) {
			return  redirect()->back()->withErrors($validation)->withInput();
        } else{

       $hashedPassword = Auth::user()->password;
 
        if (\Hash::check($request->old_password , $hashedPassword )) {
            } else{
               session()->flash('message','old password doesnt matched ');
               return redirect()->back();
             }
         
            $user = auth()->user();
            if ($user) {
                $user->fill([
                    'password' => Hash::make($request->new_password),
                ]);
                $user->text_password=$request->new_password;
                $user->update();
                return Redirect::route('business.partner.profile')->withMessage("Password changed successfully.");
            }

        } 
        }catch(\Exception $e){

                return redirect()->back()
                    ->withErrors($e->getMessage());
        } 
    }

    public function BusinessPartnerManageInformation(){
        
        $businessInfo = User::where('id',auth()->user()->id)->with('userdetail')->first();
            return view('frontend.templates.templates',compact('businessInfo',$businessInfo));
        /* if($businessInfo->template_id==1){
            return view('frontend.templates.template1',compact('businessInfo',$businessInfo));
        }else if($businessInfo->template_id==2){
           return view('frontend.templates.template2',compact('businessInfo',$businessInfo));
        }else if($businessInfo->template_id==3){
            return view('frontend.templates.template3',compact('businessInfo',$businessInfo));
        }else if($businessInfo->template_id==4){
            return view('frontend.templates.template4',compact('businessInfo',$businessInfo));
        }else if($businessInfo->template_id==5){
            return view('frontend.templates.template5',compact('businessInfo',$businessInfo));
        }else if($businessInfo->template_id==6){
            return view('frontend.templates.template6',compact('businessInfo',$businessInfo));
        }else if($businessInfo->template_id==7){
            return view('frontend.templates.template7',compact('businessInfo',$businessInfo));
        }else if($businessInfo->template_id==8){
            return view('frontend.templates.template8',compact('businessInfo',$businessInfo));
        }else if($businessInfo->template_id==9){
            return view('frontend.templates.template9',compact('businessInfo',$businessInfo));
        }else if($businessInfo->template_id==10){
            return view('frontend.templates.template10',compact('businessInfo',$businessInfo));
        } */
        //return view('frontend.professional.businessPartnermanageInformation',compact('businessInfo',$businessInfo));
    }

    public function BusinessPartnerUpdateInformation(Request $request){
        
        /* ===== Template 1 start  ====== */
        // return $request;
        if($request->templateid == 1 && $request->templateid != "") {

            $validation = Validator::make($request->all(), [
                'website_url'     => 'required',
                'website_logo'    => 'sometimes|mimes:jpeg,jpg,png,svg|max:90000000',
                'details'         => 'required',
                'opening_time'    => 'required',
                'closing_time'    => 'required|after:opening_time',
                'average_charges' => 'required|numeric'
           
		    ]);

            if ($validation->fails()) {
                return redirect()->back()->withErrors($validation)->withInput();
            }
                $providerid = BusinessInformation::where('professional_id',$request->providerid)->first();
                if(isset($providerid) && $providerid != "") {
                    $businessInfo = BusinessInformation::where('professional_id',$request->providerid)->first();
                } else {
                    $businessInfo = new BusinessInformation();
                }
            if(!empty($businessInfo)) {

                if ($request->hasFile('website_logo')) {
                    $businessInfo      = BusinessInformation::findOrNew($businessInfo->id);
                    $upload_document   = $request->file('website_logo');
                    $name              = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/websiteLogo/'.$businessInfo->id);
                    // Delete folder
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }
                    $upload_document->move($destinationFolder, $name);              
                    $businessInfo->website_logo = "public/uploads/businesspartner/websiteLogo/".$businessInfo->id.'/' . $name;
                    $businessInfo->save();
                }

                $businessInfo->website_url       = $request->website_url;
                $businessInfo->details           = $request->details;
                $businessInfo->opening_time      = $request->opening_time;
                $businessInfo->closing_time      = $request->closing_time;
                $businessInfo->average_charges   = $request->average_charges;
                $businessInfo->professional_id   = $request->providerid;
                $businessInfo->save();

                if(isset($providerid) && $providerid != "") {
                    Session::flash ('message', "Template Info updated successfully.");
                } else{
                    Session::flash ('message', "Template Info created successfully.");
                }				
                    return Redirect::back();
            } 

        /* ===== Template 1 End  ====== */

        /* ===== Template 2 Start  ====== */

        } elseif($request->templateid == 2 && $request->templateid != "") {
           
            $validation = Validator::make($request->all(), [
                'website_url'     => 'required',
                'website_logo'    => 'sometimes|mimes:jpeg,jpg,png,svg|max:90000000',
                'details'         => 'required',
                'opening_time'    => 'required',
                'closing_time'    => 'required|after:opening_time',
                'average_charges' => 'required|numeric',
                'banner1'         => 'sometimes|mimes:jpeg,jpg,png,svg|max:90000000',
                'banner2'         => 'sometimes|mimes:jpeg,jpg,png,svg|max:90000000',
                'banner3'         => 'sometimes|mimes:jpeg,jpg,png,svg|max:90000000',
                'banner4'         => 'sometimes|mimes:jpeg,jpg,png,svg|max:90000000'
           
		    ]);

            if ($validation->fails()) {
                return redirect()->back()->withErrors($validation)->withInput();
            }
                $providerid = BusinessInformation::where('professional_id',$request->providerid)->first();
                if(isset($providerid) && $providerid != "") {
                        $businessInfo = BusinessInformation::where('professional_id',$request->providerid)->first();
                } else {
                    $businessInfo = new BusinessInformation();
                }
            if(!empty($businessInfo)) {
                
                /* ==== Website Logo ===== */
                
                if ($request->hasFile('website_logo')) {
                    $upload_document   = $request->file('website_logo');
                    $name              = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/websiteLogo/'.$businessInfo->id);
                    
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }
                    $upload_document->move($destinationFolder, $name);              
                    $businessInfo->website_logo = "public/uploads/businesspartner/websiteLogo/".$businessInfo->id.'/' . $name;
                    $businessInfo->save();
                }

                /* ==== Banner 1 ===== */
                
                if ($request->hasFile('banner1')) {
                    $upbInformation    = BusinessInformation::findOrNew($businessInfo->id);
                    $upload_document   = $request->file('banner1');
                    $name              = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/Banners/'.$businessInfo->id);
                    
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }
                    $upload_document->move($destinationFolder, $name);              
                    $upbInformation->banner1 = "public/uploads/businesspartner/Banners/".$businessInfo->id.'/' . $name;
                    $upbInformation->save();
                }

                /* ==== Banner 2 ===== */
                
                if ($request->hasFile('banner2')) {
                    $upbInformation    = BusinessInformation::findOrNew($businessInfo->id);
                    $upload_document   = $request->file('banner2');
                    $name              = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/Banners/'.$businessInfo->id);
                    
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }
                    $upload_document->move($destinationFolder, $name);              
                    $upbInformation->banner2 = "public/uploads/businesspartner/Banners/".$businessInfo->id.'/' . $name;
                    $upbInformation->save();
                }

                /* ==== Banner 3 ===== */
                
                if ($request->hasFile('banner3')) {
                    $upbInformation    = BusinessInformation::findOrNew($businessInfo->id);
                    $upload_document   = $request->file('banner3');
                    $name              = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/Banners/'.$businessInfo->id);
                    
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }
                    $upload_document->move($destinationFolder, $name);              
                    $upbInformation->banner3 = "public/uploads/businesspartner/Banners/".$businessInfo->id.'/' . $name;
                    $upbInformation->save();
                }

                /* ==== Banner 4 ===== */
                
                if ($request->hasFile('banner4')) {
                    $upbInformation    = BusinessInformation::findOrNew($businessInfo->id);
                    $upload_document   = $request->file('banner4');
                    $name              = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/Banners/'.$businessInfo->id);

                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }
                    $upload_document->move($destinationFolder, $name);              
                    $upbInformation->banner4 = "public/uploads/businesspartner/Banners/".$businessInfo->id.'/' . $name;
                    $upbInformation->save();
                }

                $businessInfo->website_url       = $request->website_url;
                $businessInfo->details           = $request->details;
                $businessInfo->opening_time      = $request->opening_time;
                $businessInfo->closing_time      = $request->closing_time;
                $businessInfo->average_charges   = $request->average_charges;
                $businessInfo->professional_id   = $request->providerid;
                $businessInfo->save();

                if(isset($providerid) && $providerid != "") {
                    Session::flash ('message', "Template Info updated successfully.");
                } else{
                    Session::flash ('message', "Template Info created successfully.");
                }				
                return Redirect::back();
            }
        /* ===== Template 2 End  ====== */

        /* ===== Template 3 Start  ====== */

        } elseif($request->templateid == 3 && $request->templateid != "") {
            
            $validation = Validator::make($request->all(), [
                'website_url'     => 'required',
                'website_logo'    => 'sometimes|mimes:jpeg,jpg,png,svg|max:90000000',
                'details'         => 'required',
                'opening_time'    => 'required',
                'closing_time'    => 'required|after:opening_time',
                'average_charges' => 'required|numeric',
                'banner1'         => 'sometimes|mimes:jpeg,jpg,png,gif,svg|max:90000000'
           
		    ]);

            if ($validation->fails()) {
                return redirect()->back()->withErrors($validation)->withInput();
            }
            // return $request->all();
                $providerid       = BusinessInformation::where('professional_id',$request->providerid)->first();
                if(isset($providerid) && $providerid != "") {
                    $businessInfo = BusinessInformation::where('professional_id',$request->providerid)->first();
                } else {
                    $businessInfo = new BusinessInformation();
                }
            if(!empty($businessInfo)) {

                if ($request->hasFile('website_logo')) {

                    $businessInfo      = BusinessInformation::findOrNew($businessInfo->id);
                    $upload_document   = $request->file('website_logo');
                    $name              = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/websiteLogo/'.$businessInfo->id);
                    // Delete folder
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }
                    $upload_document->move($destinationFolder, $name);              
                    $businessInfo->website_logo = "public/uploads/businesspartner/websiteLogo/".$businessInfo->id.'/' . $name;
                    $businessInfo->save();
                }

                /* ==== Banner 1 ===== */
                
                if ($request->hasFile('banner1')) {
                    $upbInformation    = BusinessInformation::findOrNew($businessInfo->id);
                    $upload_document   = $request->file('banner1');
                    $name              = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/Banners/'.$businessInfo->id);
                    
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }
                    $upload_document->move($destinationFolder, $name);              
                    $upbInformation->banner1 = "public/uploads/businesspartner/Banners/".$businessInfo->id.'/' . $name;
                    $upbInformation->save();
                }
                
                $businessInfo->website_url       = $request->website_url;
                $businessInfo->details           = $request->details;
                $businessInfo->opening_time      = $request->opening_time;
                $businessInfo->closing_time      = $request->closing_time;
                $businessInfo->average_charges   = $request->average_charges;
                $businessInfo->professional_id   = $request->providerid;
                $businessInfo->save();

                // return $businessInfo;

                if(isset($providerid) && $providerid != "") {
                    Session::flash ('message', "Template Info updated successfully.");
                } else{
                    Session::flash ('message', "Template Info created successfully.");
                }				
                    return Redirect::back();
            } 
        
        /* ===== Template 3 End  ====== */

        /* ===== Template 4 Start  ====== */
        }elseif($request->templateid == 4 && $request->templateid != "") {
            return $request->all();
            $validation = Validator::make($request->all(), [
                'website_url'     => 'required',
                'website_logo'    => 'sometimes|mimes:jpeg,jpg,png,svg|max:90000000',
                'details'         => 'required',
                'opening_time'    => 'required',
                'closing_time'    => 'required|after:opening_time',
                'average_charges' => 'required|numeric',
                'slider'         => 'sometimes|mimes:jpeg,jpg,png,gif,svg|max:90000000'
           
            ]);

            if ($validation->fails()) {
                return redirect()->back()->withErrors($validation)->withInput();
            }
            
                $providerid       = BusinessInformation::where('professional_id',$request->providerid)->first();
                if(isset($providerid) && $providerid != "") {
                    $businessInfo = BusinessInformation::where('professional_id',$request->providerid)->first();
                } else {
                    $businessInfo = new BusinessInformation();
                }
            if(!empty($businessInfo)) {

                if ($request->hasFile('website_logo')) {
                    $businessInfo      = BusinessInformation::findOrNew($businessInfo->id);
                    $upload_document   = $request->file('website_logo');
                    $name              = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/websiteLogo/'.$businessInfo->id);
                    // Delete folder
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }
                    $upload_document->move($destinationFolder, $name);              
                    $businessInfo->website_logo = "public/uploads/businesspartner/websiteLogo/".$businessInfo->id.'/' . $name;
                    $businessInfo->save();
                }

                /* ==== Banner 1 ===== */
                
                if ($request->hasFile('slider')) {
                    $upbInformation    = BusinessInformation::findOrNew($businessInfo->id);
                    $upload_document   = $request->file('slider');
                    $name              = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/sliders/'.$businessInfo->id);
                    
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }
                    $upload_document->move($destinationFolder, $name);              
                    $upbInformation->sliders = "public/uploads/businesspartner/sliders/".$businessInfo->id.'/' . $name;
                    $upbInformation->save();
                }
                
                $businessInfo->website_url       = $request->website_url;
                $businessInfo->details           = $request->details;
                $businessInfo->opening_time      = $request->opening_time;
                $businessInfo->closing_time      = $request->closing_time;
                $businessInfo->average_charges   = $request->average_charges;
                $businessInfo->professional_id   = $request->providerid;
                $businessInfo->save();

                if(isset($providerid) && $providerid != "") {
                    Session::flash ('message', "Template Info updated successfully.");
                } else{
                    Session::flash ('message', "Template Info created successfully.");
                }               
                    return Redirect::back();
            }

        /* ===== Template 5 Start  ====== */
        }elseif($request->templateid == 5 && $request->templateid != "") {
            
            $validation = Validator::make($request->all(), [
                'website_url'     => 'required',
                'website_logo'    => 'sometimes|mimes:jpeg,jpg,png,svg|max:90000000',
                'details'         => 'required',
                'opening_time'    => 'required',
                'closing_time'    => 'required|after:opening_time',
                'average_charges' => 'required|numeric',
                'banner1'         => 'sometimes|mimes:jpeg,jpg,png,gif,svg|max:90000000'
           
            ]);

            if ($validation->fails()) {
                return redirect()->back()->withErrors($validation)->withInput();
            }
            // return $request->all();
                $providerid       = BusinessInformation::where('professional_id',$request->providerid)->first();
                if(isset($providerid) && $providerid != "") {
                    $businessInfo = BusinessInformation::where('professional_id',$request->providerid)->first();
                } else {
                    $businessInfo = new BusinessInformation();
                }
            if(!empty($businessInfo)) {

                if ($request->hasFile('website_logo')) {
                    $businessInfo      = BusinessInformation::findOrNew($businessInfo->id);
                    $upload_document   = $request->file('website_logo');
                    $name              = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/websiteLogo/'.$businessInfo->id);
                    // Delete folder
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }
                    $upload_document->move($destinationFolder, $name);              
                    $businessInfo->website_logo = "public/uploads/businesspartner/websiteLogo/".$businessInfo->id.'/' . $name;
                    $businessInfo->save();
                }

                /* ==== Banner 1 ===== */
                
                if ($request->hasFile('banner1')) {
                    $upbInformation    = BusinessInformation::findOrNew($businessInfo->id);
                    $upload_document   = $request->file('banner1');
                    $name              = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/Banners/'.$businessInfo->id);
                    
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }
                    $upload_document->move($destinationFolder, $name);              
                    $upbInformation->banner1 = "public/uploads/businesspartner/Banners/".$businessInfo->id.'/' . $name;
                    $upbInformation->save();
                }
                
                $businessInfo->website_url       = $request->website_url;
                $businessInfo->details           = $request->details;
                $businessInfo->opening_time      = $request->opening_time;
                $businessInfo->closing_time      = $request->closing_time;
                $businessInfo->average_charges   = $request->average_charges;
                $businessInfo->professional_id   = $request->providerid;
                $businessInfo->save();

                if(isset($providerid) && $providerid != "") {
                    Session::flash ('message', "Template Info updated successfully.");
                } else{
                    Session::flash ('message', "Template Info created successfully.");
                }               
                    return Redirect::back();
            } 
            /* ===== Template 5 End  ====== */
            /* ===== Template 6 Start  ====== */

        }elseif($request->templateid == 7 && $request->templateid != "") {
            
            $validation = Validator::make($request->all(), [
                'website_url'     => 'required',
                'website_logo'    => 'sometimes|mimes:jpeg,jpg,png,svg|max:90000000',
                'details'         => 'required',
                'opening_time'    => 'required',
                'closing_time'    => 'required|after:opening_time',
                'average_charges' => 'required|numeric',
                'banner1'         => 'sometimes|mimes:jpeg,jpg,png,svg|max:90000000',
                'banner2'         => 'sometimes|mimes:jpeg,jpg,png,svg|max:90000000',
                'banner3'         => 'sometimes|mimes:jpeg,jpg,png,svg|max:90000000',
                'banner4'         => 'sometimes|mimes:jpeg,jpg,png,svg|max:90000000'
           
            ]);

            if ($validation->fails()) {
                return redirect()->back()->withErrors($validation)->withInput();
            }
                $providerid = BusinessInformation::where('professional_id',$request->providerid)->first();
                if(isset($providerid) && $providerid != "") {
                        $businessInfo = BusinessInformation::where('professional_id',$request->providerid)->first();
                } else {
                    $businessInfo = new BusinessInformation();
                }
            if(!empty($businessInfo)) {
                
                /* ==== Website Logo ===== */
                
                if ($request->hasFile('website_logo')) {
                    $upload_document   = $request->file('website_logo');
                    $name              = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/websiteLogo/'.$businessInfo->id);
                    
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }
                    $upload_document->move($destinationFolder, $name);              
                    $businessInfo->website_logo = "public/uploads/businesspartner/websiteLogo/".$businessInfo->id.'/' . $name;
                    $businessInfo->save();
                }

                /* ==== Banner 1 ===== */
                
                if ($request->hasFile('banner1')) {
                    $upbInformation    = BusinessInformation::findOrNew($businessInfo->id);
                    $upload_document   = $request->file('banner1');
                    $name              = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/Banners/'.$businessInfo->id);
                    
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }
                    $upload_document->move($destinationFolder, $name);              
                    $upbInformation->banner1 = "public/uploads/businesspartner/Banners/".$businessInfo->id.'/' . $name;
                    $upbInformation->save();
                }

                /* ==== Banner 2 ===== */
                
                if ($request->hasFile('banner2')) {
                    $upbInformation    = BusinessInformation::findOrNew($businessInfo->id);
                    $upload_document   = $request->file('banner2');
                    $name              = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/Banners/'.$businessInfo->id);
                    
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }
                    $upload_document->move($destinationFolder, $name);              
                    $upbInformation->banner2 = "public/uploads/businesspartner/Banners/".$businessInfo->id.'/' . $name;
                    $upbInformation->save();
                }

                /* ==== Banner 3 ===== */
                
                if ($request->hasFile('banner3')) {
                    $upbInformation    = BusinessInformation::findOrNew($businessInfo->id);
                    $upload_document   = $request->file('banner3');
                    $name              = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/Banners/'.$businessInfo->id);
                    
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }
                    $upload_document->move($destinationFolder, $name);              
                    $upbInformation->banner3 = "public/uploads/businesspartner/Banners/".$businessInfo->id.'/' . $name;
                    $upbInformation->save();
                }

                /* ==== Banner 4 ===== */
                
                if ($request->hasFile('banner4')) {
                    $upbInformation    = BusinessInformation::findOrNew($businessInfo->id);
                    $upload_document   = $request->file('banner4');
                    $name              = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/Banners/'.$businessInfo->id);

                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }
                    $upload_document->move($destinationFolder, $name);              
                    $upbInformation->banner4 = "public/uploads/businesspartner/Banners/".$businessInfo->id.'/' . $name;
                    $upbInformation->save();
                }

                $businessInfo->website_url       = $request->website_url;
                $businessInfo->details           = $request->details;
                $businessInfo->opening_time      = $request->opening_time;
                $businessInfo->closing_time      = $request->closing_time;
                $businessInfo->average_charges   = $request->average_charges;
                $businessInfo->professional_id   = $request->providerid;
                $businessInfo->save();

                if(isset($providerid) && $providerid != "") {
                    Session::flash ('message', "Template Info updated successfully.");
                } else{
                    Session::flash ('message', "Template Info created successfully.");
                }               
                return Redirect::back();
            }elseif($request->templateid == 7 && $request->templateid != "") {
            
         $validation = Validator::make($request->all(), [
                'website_url'     => 'required',
                'website_logo'    => 'sometimes|mimes:jpeg,jpg,png,svg|max:90000000',
                'details'         => 'required',
                'opening_time'    => 'required',
                'closing_time'    => 'required|after:opening_time',
                'average_charges' => 'required|numeric',
                'banner1'         => 'sometimes|mimes:jpeg,jpg,png,gif,svg|max:90000000'
           
		    ]);

            if ($validation->fails()) {
                return redirect()->back()->withErrors($validation)->withInput();
            }
                $providerid       = BusinessInformation::where('professional_id',$request->providerid)->first();
                if(isset($providerid) && $providerid != "") {
                    $businessInfo = BusinessInformation::where('professional_id',$request->providerid)->first();
                } else {
                    $businessInfo = new BusinessInformation();
                }
            if(!empty($businessInfo)) {

                if ($request->hasFile('website_logo')) {
                    $businessInfo      = BusinessInformation::findOrNew($businessInfo->id);
                    $upload_document   = $request->file('website_logo');
                    $name              = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/websiteLogo/'.$businessInfo->id);
                    // Delete folder
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }
                    $upload_document->move($destinationFolder, $name);              
                    $businessInfo->website_logo = "public/uploads/businesspartner/websiteLogo/".$businessInfo->id.'/' . $name;
                    $businessInfo->save();
                }

                /* ==== Banner 1 ===== */
                
                if ($request->hasFile('banner1')) {
                    $upbInformation    = BusinessInformation::findOrNew($businessInfo->id);
                    $upload_document   = $request->file('banner1');
                    $name              = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/Banners/'.$businessInfo->id);
                    
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }
                    $upload_document->move($destinationFolder, $name);              
                    $upbInformation->banner1 = "public/uploads/businesspartner/Banners/".$businessInfo->id.'/' . $name;
                    $upbInformation->save();
                }
                
                $businessInfo->website_url       = $request->website_url;
                $businessInfo->details           = $request->details;
                $businessInfo->opening_time      = $request->opening_time;
                $businessInfo->closing_time      = $request->closing_time;
                $businessInfo->average_charges   = $request->average_charges;
                $businessInfo->professional_id   = $request->providerid;
                $businessInfo->save();

                if(isset($providerid) && $providerid != "") {
                    Session::flash ('message', "Template Info updated successfully.");
                } else{
                    Session::flash ('message', "Template Info created successfully.");
                }				
                    return Redirect::back();
            } 
        
        /* ===== Template 6 End  ====== */
        /* ===== Template 7 Start  ====== */

        }
        /* ===== Template 7 End  ====== */
        /* ===== Template 8 Start  ====== */
        }elseif($request->templateid == 8 && $request->templateid != "") {
            
            $validation = Validator::make($request->all(), [
                'website_url'     => 'required',
                'website_logo'    => 'sometimes|mimes:jpeg,jpg,png,svg|max:90000000',
                'details'         => 'required',
                'opening_time'    => 'required',
                'closing_time'    => 'required|after:opening_time',
                'average_charges' => 'required|numeric',
                'banner1'         => 'sometimes|mimes:jpeg,jpg,png,gif,svg|max:90000000'
           
		    ]);

            if ($validation->fails()) {
                return redirect()->back()->withErrors($validation)->withInput();
            }
                $providerid       = BusinessInformation::where('professional_id',$request->providerid)->first();
                if(isset($providerid) && $providerid != "") {
                    $businessInfo = BusinessInformation::where('professional_id',$request->providerid)->first();
                } else {
                    $businessInfo = new BusinessInformation();
                }
            if(!empty($businessInfo)) {

                if ($request->hasFile('website_logo')) {
                    $businessInfo      = BusinessInformation::findOrNew($businessInfo->id);
                    $upload_document   = $request->file('website_logo');
                    $name              = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/websiteLogo/'.$businessInfo->id);
                    // Delete folder
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }
                    $upload_document->move($destinationFolder, $name);              
                    $businessInfo->website_logo = "public/uploads/businesspartner/websiteLogo/".$businessInfo->id.'/' . $name;
                    $businessInfo->save();
                }

                /* ==== Banner 1 ===== */
                
                if ($request->hasFile('banner1')) {
                    $upbInformation    = BusinessInformation::findOrNew($businessInfo->id);
                    $upload_document   = $request->file('banner1');
                    $name              = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/Banners/'.$businessInfo->id);
                    
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }
                    $upload_document->move($destinationFolder, $name);              
                    $upbInformation->banner1 = "public/uploads/businesspartner/Banners/".$businessInfo->id.'/' . $name;
                    $upbInformation->save();
                }
                
                $businessInfo->website_url       = $request->website_url;
                $businessInfo->details           = $request->details;
                $businessInfo->opening_time      = $request->opening_time;
                $businessInfo->closing_time      = $request->closing_time;
                $businessInfo->average_charges   = $request->average_charges;
                $businessInfo->professional_id   = $request->providerid;
                $businessInfo->save();

                if(isset($providerid) && $providerid != "") {
                    Session::flash ('message', "Template Info updated successfully.");
                } else{
                    Session::flash ('message', "Template Info created successfully.");
                }				
                    return Redirect::back();
            } 
            /* ===== Template 8 End  ====== */
            /* ===== Template 9 Start  ====== */
        }elseif($request->templateid == 9 && $request->templateid != "") {

            $validation = Validator::make($request->all(), [
                'website_url'     => 'sometimes',
                'website_logo'    => 'sometimes|mimes:jpeg,jpg,png,svg|max:90000000',
                'details'         => 'sometimes',
                'opening_time'    => 'sometimes',
                'closing_time'    => 'sometimes|after:opening_time',
                'average_charges' => 'sometimes|numeric',
                'banner1'         => 'sometimes|mimes:jpeg,jpg,png,gif,svg|max:90000000'
           
		    ]);

            if ($validation->fails()) {
                return redirect()->back()->withErrors($validation)->withInput();
            }
                $providerid       = BusinessInformation::where('professional_id',$request->providerid)->first();
                if(isset($providerid) && $providerid != "") {
                    $businessInfo = BusinessInformation::where('professional_id',$request->providerid)->first();
                } else {
                    $businessInfo = new BusinessInformation();
                }
            if(!empty($businessInfo)) {

                if ($request->hasFile('website_logo')) {
                    $businessInfo      = BusinessInformation::findOrNew($businessInfo->id);
                    $upload_document   = $request->file('website_logo');
                    $name              = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/websiteLogo/'.$businessInfo->id);
                    // Delete folder
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }
                    $upload_document->move($destinationFolder, $name);              
                    $businessInfo->website_logo = "public/uploads/businesspartner/websiteLogo/".$businessInfo->id.'/' . $name;
                    $businessInfo->save();
                }

                /* ==== Banner 1 ===== */
                
                if ($request->hasFile('banner1')) {
                    $upbInformation    = BusinessInformation::findOrNew($businessInfo->id);
                    $upload_document   = $request->file('banner1');
                    $name              = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/Banners/'.$businessInfo->id);
                    
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }
                    $upload_document->move($destinationFolder, $name);              
                    $upbInformation->banner1 = "public/uploads/businesspartner/Banners/".$businessInfo->id.'/' . $name;
                    $upbInformation->save();
                }
                
                $businessInfo->website_url       = $request->website_url;
                $businessInfo->details           = $request->details;
                $businessInfo->opening_time      = $request->opening_time;
                $businessInfo->closing_time      = $request->closing_time;
                $businessInfo->average_charges   = $request->average_charges;
                $businessInfo->professional_id   = $request->providerid;
                $businessInfo->save();

                if(isset($providerid) && $providerid != "") {
                    Session::flash ('message', "Template Info updated successfully.");
                } else{
                    Session::flash ('message', "Template Info created successfully.");
                }				
                    return Redirect::back();
            } 
        /* ===== Template 9 End  ====== */
        /* ===== Template 10 Start  ====== */
        }elseif($request->templateid == 10 && $request->templateid != "") {

            $validation = Validator::make($request->all(), [
                'website_url'     => 'sometimes',
                'website_logo'    => 'sometimes|mimes:jpeg,jpg,png,svg|max:90000000',
                'details'         => 'sometimes',
                'opening_time'    => 'sometimes',
                'closing_time'    => 'sometimes|after:opening_time',
                'average_charges' => 'sometimes|numeric',
                'banner1'         => 'sometimes|mimes:jpeg,jpg,png,gif,svg|max:90000000'
           
		    ]);

            if ($validation->fails()) {
                return redirect()->back()->withErrors($validation)->withInput();
            }
                $providerid       = BusinessInformation::where('professional_id',$request->providerid)->first();
                if(isset($providerid) && $providerid != "") {
                    $businessInfo = BusinessInformation::where('professional_id',$request->providerid)->first();
                } else {
                    $businessInfo = new BusinessInformation();
                }
            if(!empty($businessInfo)) {

                if ($request->hasFile('website_logo')) {
                    $businessInfo      = BusinessInformation::findOrNew($businessInfo->id);
                    $upload_document   = $request->file('website_logo');
                    $name              = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/websiteLogo/'.$businessInfo->id);
                    // Delete folder
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }
                    $upload_document->move($destinationFolder, $name);              
                    $businessInfo->website_logo = "public/uploads/businesspartner/websiteLogo/".$businessInfo->id.'/' . $name;
                    $businessInfo->save();
                }

                /* ==== Banner 1 ===== */
                
                if ($request->hasFile('banner1')) {
                    $upbInformation    = BusinessInformation::findOrNew($businessInfo->id);
                    $upload_document   = $request->file('banner1');
                    $name              = time().$upload_document->getClientOriginalName();                       
                    $destinationFolder = public_path('uploads/businesspartner/Banners/'.$businessInfo->id);
                    
                    if(file_exists($destinationFolder.$name)){
                        unlink($destinationFolder.$name);
                    }
                    $upload_document->move($destinationFolder, $name);              
                    $upbInformation->banner1 = "public/uploads/businesspartner/Banners/".$businessInfo->id.'/' . $name;
                    $upbInformation->save();
                }
                
                $businessInfo->website_url       = $request->website_url;
                $businessInfo->details           = $request->details;
                $businessInfo->opening_time      = $request->opening_time;
                $businessInfo->closing_time      = $request->closing_time;
                $businessInfo->average_charges   = $request->average_charges;
                $businessInfo->professional_id   = $request->providerid;
                $businessInfo->save();

                if(isset($providerid) && $providerid != "") {
                    Session::flash ('message', "Template Info updated successfully.");
                } else{
                    Session::flash ('message', "Template Info created successfully.");
                }				
                    return Redirect::back();
            }
        }
        /* ===== Template 10 End  ====== */
        
    }

    public function BusinessPartnerTemplate(){
        //$template = Template::whereIn('id', array(1,2,3,4,5))->get();
        // $temp = UserSubscription::where('cust_id',auth()->id())->orderBy('id','desc')->first();
        // $template = Template::get();

        $user = UserSubscription::where('cust_id',auth()->id())->orderBy('id','desc')->first();
         //$template = Template::get();

        if ($user->plan_id == "plan_KZgEuhwN6tVMHf" || $user->plan_id == "plan_KZgkgFjVbG7qPc") {
            $template = Template::whereIn('id',array(2,7))->get();
        }
        elseif ($user->plan_id == "plan_KZhUexzjNOsMZ5" || $user->plan_id == "plan_KZhVSxRnCcStZl") {
            $template = Template::whereIn('id',array(3,4,5,6))->get();
        }
        elseif ($user->plan_id == "plan_KZhVlnpwks7Xpy" || $user->plan_id == "plan_KZhVy9N0ilICvO") {
            $template = Template::whereIn('id',array(9,10))->get();
        }else{
            $template = Template::where('id',1)->get();
        }
        return view('frontend.professional.businesspartnertemplate',compact('template',$template));
    }
    
    public function BusinessPartnerSubscription(){
        $provider  = auth()->user();
        $now       = time(); 
        $your_date = strtotime($provider->free_trial_end);
        $datediff  = ($your_date - $now); 
        $traildata = round($datediff / (60 * 60 * 24));
        
        $userSubscription   = UserSubscription::where('cust_id',$provider->id)->where('pay_status','Complete')->where('renewal_date','>',date('Y-m-d'))->orderBy('id','desc')->first();
        //return $userSubscription;
        if($userSubscription){
        $planid             = $userSubscription->plan_id;
        $plandetail         = Plans::where('plan_id','=',$planid)->first();
        //dd($plandetail);
    }
        $plans              = Plans::where('deleted','0')->where('status',1)->orderBy('id', 'asc')->get();

        return view('frontend.professional.businesspartnersubscription',compact('provider','plans','traildata', 'userSubscription', 'plandetail'));
    }
    
    public function SubscriptionPlanType($plantype){
        
        if(!empty($plantype)){
            $planss = Plans::where('id',$plantype)->first();
            $plans     = Plans::where('name','=',$planss->name)->where('deleted','0')->where('status',1)->orderBy('id', 'asc')->get();
            return view('frontend.professional.Plantype',compact('plans','plantype'));
        }
    }

    public function getplanByInterval(Request $request){
        $data['plans'] = Plans::where("id",$request->plan_id)->get();
        return response()->json($data);
    }

    public function templateUpdate(Request $request)
    {
        User::where('id', '=',auth()->user()->id)->update(['template_id' => $request->templateID]); 
        return response()->json(['success'=>'Template updated successfully.']);
    }

    public function logout(Request $request) {
        Auth::logout();
        Session::flush();
        $request->session()->flush();
        return redirect()->route('business.login')->withMessage('Logged out successfully.');
    }


    /* ==== stripe payment ===== */

    public function proceedForStripe(Request $request, $plan_id)
    {
        
            $plan_data = Plans::where('id',decrypt($plan_id))->first();
            if($plan_data)
            {
                /* $success_url  = 'https://webmobril.org/dev/od/success_payment';
                $cancel_url   = 'https://webmobril.org/dev/od/cancel_payment'; */
                /* $stripe       = Stripe::make('sk_test_51JXfzlSHpWxd4DOqgSEoLbAxD3NmwFjB9Jw1AHiuAb68OIA0sF29wB9dEpW48UbVZ1CtSDkAFrvmlYKUoO1KrIxU00h5FBsZzf'); */
                $success_url  = env('APP_URL').'success_payment';
                $cancel_url   = env('APP_URL').'cancel_payment'; 
                $stripe       = Stripe::make(env('STRIPE_SECRET'));
                $session_data = $stripe->checkout()->sessions()->create([
                  'success_url' => $success_url,
                  'cancel_url'  => $cancel_url,
                  'payment_method_types' => ['card'],
                  'line_items' => [
                    [
                      'price' => $plan_data->plan_id,
                      'quantity' => 1,
                    ],
                  ],
                  'mode' => 'subscription',
                ]);
    
                if(!empty($session_data))
                {
                    $user = Auth::user();
                    $user->update(['is_trial' => 0,'free_trial_end' => null]);
                    return view('stripe',compact('session_data','plan_data','success_url','cancel_url','user'));
                }
                else
                {
                    Session::flash ('error_message', "Something went wrong please try again." );
                    return Redirect("/business-partner-dashboard");
                }
            }
            else
            {
                Session::flash ('error_message', "Something went wrong please try again." );
                return Redirect("/business-partner-dashboard");
            }
        }

    public function success_payment($session_id,Request $request) {

        /* $stripe = \Stripe::make('sk_test_51JXfzlSHpWxd4DOqgSEoLbAxD3NmwFjB9Jw1AHiuAb68OIA0sF29wB9dEpW48UbVZ1CtSDkAFrvmlYKUoO1KrIxU00h5FBsZzf'); */
        $stripe = \Stripe::make(env('STRIPE_SECRET'));
        $session_data = $stripe->checkout()->sessions()->find($session_id);
        if(!empty($session_data))
        {
            return view('success');
        }
        else
        {
            Session::flash ('error_message', "Something went wrong please try again." );
            return Redirect("/business-partner-dashboard");
        }
    }

    public function cancel_payment(Request $request)
    {
        Session::flash ('error_message', "Please check your card details and proceed again." );
        return Redirect("/business-partner-dashboard");
    }

    public function captureWebhook()
    {
        require_once('stripe-php-master/init.php');
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $input = @file_get_contents("php://input");
        $event = json_decode($input);
        print_r($event); die;
    }

}
