<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessInformation extends Model
{
    protected $fillable = ['website_logo', 'website_url', 'details'];

    /* public function userdetail() {
        return $this->belongsTo('App\Models\User', 'professional_id', 'id');
    } */
}
