@extends('frontend.partials.index')
@section('after-style')
<link rel="stylesheet" href="{{asset('public/frontend/assets/css/business_partner.css')}}">
@endsection
@section('content')
<section class="bigHeader" style="background: url('{{asset('public/frontend/assets/image/banner2.jpg')}}') center center;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="tcent">
					Sign Up Business
					<span class="smallBredcm"> <a href="{{route('home')}}"> Home </a> >> Sign Up Business </span>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="login_section">
	<div class="container">
		
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
			<form action="{{route('business.businessregisterstore')}}" method="post">
            @csrf
				<div class="loginBox">
					{{-- @if($errors->any())
						<span class="invalid-feedback" role="alert" style="display:block;">
						<strong>{{$errors->first()}}</strong>
						</span>
					@endif --}}
					
					<div class="">
						<h5 class="login_title" id="exampleModalLabel">Sign Up Business</h5>
					</div>

				   <div class="">
					  <div class="form-group"> 
						<label class="form-label">First Name</label> 
						<input type="text" name="first_name" value="{{old('first_name')}}" class="form-control" placeholder="Enter Your First Name"> 
						@error('first_name')
						<span class="invalid-feedback" role="alert" style="display:block;">
						<strong>{{ $message }}</strong>
						</span>
						@enderror
					</div>
				   <div class="">
					  <div class="form-group"> 
						<label class="form-label">Last Name</label> 
						<input type="text" name="last_name" value="{{old('last_name')}}" class="form-control" placeholder="Enter Your Last Name"> 
						@error('last_name')
						<span class="invalid-feedback" role="alert" style="display:block;">
						<strong>{{ $message }}</strong>
						</span>
						@enderror
					</div>
					<div class="form-group"> 
						<label class="form-label">Email ID</label> 
						<input type="email" name="email" value="{{old('email')}}" class="form-control" placeholder="Enter Your Email" > 
						@error('email')
						<span class="invalid-feedback" role="alert" style="display:block;">
						<strong>{{ $message }}</strong>
						</span>
						@enderror
					</div>
					<div class="form-group"> 
						<label class="form-label">Password</label> 
						<input type="password" name="password" value="{{old('password')}}" class="form-control" placeholder="Enter Your Password"> 
						@error('password')
						<span class="invalid-feedback" role="alert" style="display:block;">
						<strong>{{ $message }}</strong>
						</span>
						@enderror
					</div>
					<div class="form-group"> 
						<label class="form-label">Confirm Password</label> 
						<input type="password" name="confirm_password" value="{{old('confirm_password')}}" class="form-control" placeholder="Enter Your Confirm Password"> 
						@error('confirm_password')
						<span class="invalid-feedback" role="alert" style="display:block;">
						<strong>{{ $message }}</strong>
						</span>
						@enderror
					</div>
					<div class="form-group"> 
						<label class="form-label">Mobile Number</label> 
						<input type="text" name="mobile" class="form-control" value="{{old('mobile')}}" placeholder="Enter Your Mobile Number"> 
						@error('mobile')
						<span class="invalid-feedback" role="alert" style="display:block;">
						<strong>{{ $message }}</strong>
						</span>
						@enderror
					</div>
					<div class="form-group"> 
						<label class="form-label">Business Name</label> 
						<input type="text" name="business_name" value="{{old('business_name')}}" class="form-control" placeholder="Enter Your Business Name"> 
						@error('business_name')
						<span class="invalid-feedback" role="alert" style="display:block;">
						<strong>{{ $message }}</strong>
						</span>
						@enderror
					</div>	
					
					<div class="form-group"> 
						<label class="form-label">Business Type / Category</label> 
						<select name="business_category" id="business_category" class="form-control">
							<option value="">Select Category</option>
							@foreach($categories as $category)   
								@foreach($category->children as $childrenCategory)
								@if($childrenCategory->status == 1)
								<option value="{{$childrenCategory['id']}}" @if (old('business_category') == $childrenCategory['id']) selected="selected" @endif>{{ ucwords($childrenCategory['name']) }}</option>
								@endif
								@endforeach
                            @endforeach
						</select>
						@error('business_category')
						<span class="invalid-feedback" role="alert" style="display:block;">
						<strong>{{ $message }}</strong>
						</span>
						@enderror
					</div>

					<div class="form-group"> 
						<label class="form-label">Country</label> 
						<select name="country" id="country-dropdown" class="form-control">
						<option value="">Select Country</option>
						@foreach($countries as $country)
						<option value="{{$country->id}}" @if(old('country') == $country->id) selected="selected" @endif>{{$country->name}}</option>
						@endforeach
						</select>
						@error('country')
						<span class="invalid-feedback" role="alert" style="display:block;">
						<strong>{{ $message }}</strong>
						</span>
						@enderror
					</div>

					<div class="form-group"> 
						<label class="form-label">State</label> 
						<select class="form-control" name="state" id="state-dropdown">
						</select>
						@error('state')
						<span class="invalid-feedback" role="alert" style="display:block;">
						<strong>{{ $message }}</strong>
						</span>
						@enderror
					</div>

					<div class="form-group"> 
						<label class="form-label">City</label> 
						<select class="form-control" name="city" id="city-dropdown">
						</select>
						@error('city')
						<span class="invalid-feedback" role="alert" style="display:block;">
						<strong>{{ $message }}</strong>
						</span>
						@enderror
					</div>

					<div class="form-group"> 
						<label class="form-label">Zip Code/Postal Code/Regional Code</label> 
						<input type="text" name="zip_code" class="form-control" value="{{old('zip_code')}}" placeholder="Enter Your Zip Code, Regional Code, Postal Code"> 
						@error('zip_code')
						<span class="invalid-feedback" role="alert" style="display:block;">
						<strong>{{ $message }}</strong>
						</span>
						@enderror
					</div>	

					<div class="form-group"> 
						<label class="form-label">Address</label> 
						<input type="text" name="location" value="{{old('location')}}" id="location" class="form-control" placeholder="Enter Your Address"> 
						<input type="hidden" name="latitude" id="latitude" value="{{ old('latitude') }}">
                        <input type="hidden" name="longitude" id="longitude" value="{{ old('longitude') }}">
						@error('location')
						<span class="invalid-feedback" role="alert" style="display:block;">
						<strong>{{ $message }}</strong>
						</span>
						@enderror
					</div>	
										
				   </div>
				   <div class="customLogRegisWrapper">
					<div class="submit"> 
						{{-- <a class="btn btn-primary btn-block" href="#" data-dismiss="modal">Register</a>  --}}
						<button type="submit" class="btn btn-primary btn-block" data-dismiss="modal">Register</button>
					</div>
				   </div>
				   
				   <div class="alreadyHaveAccount">
					Already have an account? <a href="{{route('business.login')}}"> Login </a>
				   </div>
				</div>
			</form>
			</div>
		</div>
	</div>
</section>
@section('after-scripts')
<script>
$(document).ready(function() {
$('#country-dropdown').on('change', function() {
	var country_id = this.value;
	$("#state-dropdown").html('');
	$.ajax({
		url:"{{url('get-states-by-country')}}",
		type: "POST",
		data: {
		country_id: country_id,
		_token: '{{csrf_token()}}'
	},
	dataType : 'json',
	success: function(result){
	$.each(result.states,function(key,value){
		$("#state-dropdown").append('<option value="'+value.id+'">'+value.name+'</option>');
	});
		$('#city-dropdown').html('<option value="">Select State First</option>'); 
	}
	});
});  
$('#country-dropdown').trigger("change");

$('#state-dropdown').on('change', function() {
var state_id = this.value;
$("#city-dropdown").html('');
$.ajax({
url:"{{url('get-cities-by-state')}}",
type: "POST",
data: {
state_id: state_id,
_token: '{{csrf_token()}}'
},
dataType : 'json',
success: function(result){
$.each(result.cities,function(key,value){
$("#city-dropdown").append('<option value="'+value.id+'">'+value.name+'</option>');
});
}
});
});
$('#state-dropdown').trigger("change");

});
</script>

<script src="https://maps.google.com/maps/api/js?key=AIzaSyCR-_1vWp4V7pifLhXGmq5AAGmDy4pB2Tg&libraries=places" type="text/javascript"></script>

<script>
google.maps.event.addDomListener(window, 'load', initialize);
function initialize() {
	/* var options = {
		componentRestrictions: {country: "IN"}
	}; */

	var input = document.getElementById('location');
	var autocomplete = new google.maps.places.Autocomplete(input);
	autocomplete.addListener('place_changed', function() {
		var place = autocomplete.getPlace();
		$('#latitude').val(place.geometry['location'].lat());
		$('#longitude').val(place.geometry['location'].lng());

	// --------- show lat and long ---------------
		$("#lat_area").removeClass("d-none");
		$("#long_area").removeClass("d-none");
	});
}

const input = document.querySelector('.my-input');

/* input.addEventListener('keydown', () => {
    input.setAttribute('type', 'password');
}); */
</script>
@endsection
@endsection