<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('admin/dashboard')}}" class="brand-link">
        <img src="{{asset('public/images/logo.png')}}" alt="Online Directory" class="brand-image" style="opacity: .8;">
        <span class="brand-text font-weight-light">{{--env('APP_NAME') --}}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                @if (Auth::user()->profile_image)
                <img src="{{ asset(auth()->user()->profile_image) }}" class="img-circle elevation-2" alt="User Image">
                @else
                <img src="{{ asset('public/images/user-logo.png') }}" class="img-circle elevation-2" alt="User Image">
                @endif
            </div>
            <div class="info">
                <a href="{{url('admin/dashboard')}}" class="d-block">{{auth()->user()->name ?? ""}}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                <li class="nav-item">
                    <a href="#" class="nav-link {{ CustomHelper::set_active(['admin/profile*']) }} {{ CustomHelper::set_active(['admin/change-password-view*']) }}">
                        <i class="nav-icon fas fa-info"></i>
                        <p>Manage Profile
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('admin/profile')}}" class="nav-link">
                                <i class="fas fa-address-card nav-icon"></i>
                                <p>Profile</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('admin/change-password-view')}}" class="nav-link ">
                                <i class="fas fa-key nav-icon"></i>
                                <p>Change Password</p>
                            </a>
                        </li>

                    </ul>
                </li>
                
                <li class="nav-item">
                    <a href="{{url('admin/users-list')}}" class="nav-link {{ CustomHelper::set_active(['admin/users-list*']) }}">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            Users Management
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{url('admin/professional-list')}}" class="nav-link {{ CustomHelper::set_active(['admin/professional-list*']) }}">
                        <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                        <p>
                            Professionals Management
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="" class="nav-link {{ CustomHelper::set_active(['admin/pages*']) }} {{ CustomHelper::set_active(['admin/about*']) }} {{ CustomHelper::set_active(['admin/faq*']) }} {{ CustomHelper::set_active(['admin/contact/list*']) }}">
                        <i class="nav-icon far fa-file-alt"></i>
                        <p>General Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{url('admin/pages')}}" class="nav-link" class="nav-link">
                                <i class="fa fa-bar-chart nav-icon"></i>
                                <p>Pages</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{url('admin/about')}}" class="nav-link " class="nav-link">
                                <i class="fa fa-bar-chart nav-icon"></i>
                                <p>About</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{url('admin/faq')}}" class="nav-link" class="nav-link">
                                <i class="fa fa-bar-chart nav-icon"></i>
                                <p>Faq</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{url('admin/contact/list')}}" class="nav-link" class="nav-link">
                                <i class="fa fa-bar-chart nav-icon"></i>
                                <p>Enquiry List</p>
                            </a>
                        </li>

                    </ul>
                </li>

                <li class="nav-item">
                    <a href="{{url('admin/categories-list')}}" class="nav-link {{ CustomHelper::set_active(['admin/categories-list*']) }}">
                        <i class="nav-icon fas fa-filter"></i>
                        <p>
                            Manage Categories
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{url('admin/sitesetting')}}" class="nav-link {{ CustomHelper::set_active(['admin/sitesetting*']) }}">
                        <i class="nav-icon fa fa-sticky-note-o"></i>
                        <p>
                            Content Management
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="" class="nav-link {{ CustomHelper::set_active(['admin/userview*']) }} {{ CustomHelper::set_active(['admin/professionalview*']) }}">
                        <i class="nav-icon far fa-file-alt"></i>
                        <p>Reports Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{route('admin.reports.userview')}}" class="nav-link">
                                <i class="fa fa-bar-chart nav-icon"></i>
                                <p>Number of Users</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{route('admin.reports.professionalview')}}" class="nav-link">
                                <i class="fa fa-bar-chart nav-icon"></i>
                                <p>Number of Professionals</p>
                            </a>
                        </li>

                    </ul>
                </li>

                <li class="nav-item">
                    <a href="" class="nav-link {{ CustomHelper::set_active(['admin/plancategory/list*']) }} {{ CustomHelper::set_active(['admin/plans*']) }} {{ CustomHelper::set_active(['admin/subscriber_customers*']) }}">
                        
                        <i class="nav-icon fas fa-dollar-sign"></i>
                        <p>Manage Subscription
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        {{-- <li class="nav-item">
                            <a href="{{url('admin/plancategory/list')}}" class="nav-link">
                                <i class="nav-icon fa fa-list-alt"></i>
                                <p>
                                    Manage Plans Category
                                </p>
                            </a>
                        </li> --}}

                        <li class="nav-item">
                            <a href="{{url('admin/plans')}}" class="nav-link">
                                <i class="nav-icon fa fa-money"></i>
                                <p>
                                    Manage Plans
                                </p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{url('admin/subscriber_customers')}}" class="nav-link">
                                <i class="nav-icon fa fa-money"></i>
                                <p>Subscribers</p>
                            </a>
                        </li>
                    </ul>
                </li>
               
                <li class="nav-item">
                    <a href="{{ route('Admin.Logout') }}" onclick="return logout(event);
                    document.getElementById('logout-form').submit();" class="nav-link">
                    <i class="fas fa-sign-out-alt nav-icon"></i> Logout
                    </a>
                <form id="logout-form" action="{{ route('Admin.Logout') }}" method="POST" style="display: none;">
                        @csrf
                </form>
        
                </li>

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>

<script type="text/javascript">
function logout(event){
    event.preventDefault();
    var check = confirm("Do you really want to logout?");
    if(check){ 
        document.getElementById('logout-form').submit();
    }
}
</script>