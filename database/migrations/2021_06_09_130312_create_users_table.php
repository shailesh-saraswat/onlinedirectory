<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email', 191)->unique();
            $table->string('mobile')->nullable();           
			$table->integer('roles_id')->nullable();
			$table->integer('template_id')->nullable();
			$table->string('password')->default('');
			$table->string('text_password')->default('');
            $table->integer('otp')->nullable();
            $table->tinyInteger('request_otp')->nullable();
			$table->tinyInteger('verified')->default(0);
            $table->timestamp('email_verified_at')->nullable(); 
			$table->string('profile_image')->nullable();
			$table->string('business_name')->nullable();
			$table->string('slug')->nullable();
			$table->string('sms_gateway')->nullable();
			$table->string('address')->nullable();
            $table->string('country')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
			$table->string('zip_code')->nullable();
			$table->string('location')->nullable();
			$table->string('latitude')->nullable();
			$table->string('longitude')->nullable();
            $table->date('free_trial_end')->nullable();
            $table->enum('member_status', ['Silver','Gold','Platinum'])->default('Silver');
            $table->enum('membership_status', ['Active','Active-Blocked','Paused','Canceled','Expired','Disputed','None'])->default('None');
            $table->enum('deleted', ['1','0'])->default('0')->comment('1=>Yes,0=>No');
            $table->bigInteger('category_id')->unsigned()->nullable();
            /* $table->foreign('category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('CASCADE'); */
            $table->string('device_type')->nullable();
            $table->string('device_token')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
