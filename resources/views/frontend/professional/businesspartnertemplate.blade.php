@extends('frontend.professional.partials.index')

@section('content')
<main class="content">
	<div class="container-fluid p-0">
		<div class="row mb-2 mb-xl-3">
			
			<div class="col-auto d-none d-sm-block">
			<h3><strong>You Have Selected : Template {{auth()->user()->template_id ?? ''}} </strong> 
			</h3>
			</div>

			<div class="col-auto ms-auto text-end mt-n1">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb bg-transparent p-0 mt-1 mb-0">
						<li class="breadcrumb-item"><a href="{{route('business.partner.profile')}}">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">Templates</li>
					</ol>
				</nav>
			</div>

		</div>
		<div class="row">
			<div class="col-xl-12 col-xxl-12 d-flex">
				<div class="card w-100 templateThemePDiv">
					<div class="card-body templateThemeCardBody">
						<div class="row">
						@foreach($template as $item)
						<div class="col-sm-6" value="{{ $item->id }}" data-id="{{ $item->id }}">
							{!! $item->content !!}
						</div>
						@endforeach
					
						</div>
					</div>
				</div>
			</div>				
		</div>
	</div>
</main>

@endsection
@section('after-scripts')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
$(document).ready(function(){	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	$(".applybtn").on("click", function(e){
		e.preventDefault();
		var templateID = $(this).parents().closest('.col-sm-6').data('id');
		$.ajax({
			type: "GET",
			dataType: "json",
			url: '{{route('business.partner.templateupdate')}}'+'?_token=' + '{{ csrf_token() }}',
			data: {'templateID': templateID},
		}).done(function(data) {
			swal("Good job!", data.success, "success");
			setTimeout(function () { 
				document.location.reload(true); 
			}, 2000);
		}).fail(function(error){
			 console.log( "Request failed: " + error);
		})
	});

	$(".cancelBtn").on("click", function(e){	
		swal({
            title: "Are you sure?",
            text: "Once clicked, this will cancel!",
            icon: "warning",
            dangerMode: true,
        });
	});
});
</script>
@endsection