<footer class="footer">
	<div class="container-fluid">
		<div class="row text-muted">
			<div class="col-12 text-center">
				<p class="mb-0">
					<a href="{{env('APP_URL')}}" target="_blank" class="text-muted"><strong>Blackbiznetpages</strong></a> &copy; webmobril
				</p>
			</div>
		</div>
	</div>
</footer>
</div>
</div>
<script src="{{asset('public/plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('public/frontend/assets/newBp/js/app.js')}}"></script>
<script src="{{asset('public/frontend/assets/newBp/js/swiper.min.js')}}"></script>
<script src="{{asset('public/frontend/assets/newBp/js/swiper-bundle.min.js')}}"></script>
<script>
	var swiper = new Swiper('.swiper-container', {
		pagination: {
		el: '.swiper-pagination',
		type: 'fraction',
		},
		autoplay: {
		delay: 5000,
		},
		navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev',
		},
	});
</script>
@yield('after-scripts')
</body>
</html>