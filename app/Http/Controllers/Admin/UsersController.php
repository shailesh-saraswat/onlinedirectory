<?php

namespace App\Http\Controllers\Admin;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;
use App\Models\{Category,Visitor};
use Exception, Hash;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Laravel\Passport\Token;
use DB;

class UsersController extends Controller
{   

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users          = User::query()->where('roles_id', "=", 3)->with('roles')->get();
        $categories     = Category::where('parent_id','!=',0)->get();
        return view('admin.users.index',compact('users','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        /* $categories 	= Category::leftJoin("categories as parent_cat","categories.parent_id" ,'=', 'parent_cat.id')
						->select("categories.*", "parent_cat.name as parent_name")
						->orderBy('created_at', 'desc')
						->get();  */
        $categories     = Category::where('parent_id','!=',0)->get();
        return view('admin.users.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //try{
            
        /* $validation = Validator::make($request->all(), [

                'first_name'    => "required|string|regex:/^[a-zA-Z ]+$/u|max:255",
                'last_name'     => "required|string|regex:/^[a-zA-Z ]+$/u|max:255",
                'email'         =>  ['required','max:255','regex:/^\w+[-\.\w]*@(?!(?:)\.com$)\w+[-\.\w]*?\.\w{2,4}$/', \Illuminate\Validation\Rule::unique('users')],
                'mobile'        => ['required', 'between:6,15',\Illuminate\Validation\Rule::unique('users')],
                'password'      => "required|between:8,16",
                'status'        => "required" 
		    ],
    
            [
            'email.regex' => 'We do not accept mails with this domain.'
            ],
            [
            'password.between' => 'Password must be between 8 to 16 characters'
            ],
            [
            'mobile.between' => 'Mobile number must be between 6 to 15 digits.'
            ]

        );  */

        $rules = [
                'first_name' => ['required','regex:/^[a-zA-Z ]+$/u','max:255'],
                'last_name'  => ['required','regex:/^[a-zA-Z ]+$/u','max:255'],
                'password'  => ['required','between:6,15'],
                'status'  => ['required'],
                'email' => [
                    'required',
                    'string',
                    'email',
                    'max:255',
                    'unique:users',
                    'regex:/^\w+[-\.\w]*@(?!(?:outlook|myemail|yahoo|yopmail|red|mailinator|user)\.com$)\w+[-\.\w]*?\.\w{2,4}$/',\Illuminate\Validation\Rule::unique('users')
                ],
                'mobile' => ['required','between:6,15', \Illuminate\Validation\Rule::unique('users')]
            ];
        
        $messages = [
            'email.regex' => 'We do not accept mails with this domain.',
            'password.between' => 'Password must be between 8 to 16 characters',
            'mobile.between' => 'Mobile number must be between 6 to 15 digits.'
        ];

        $validation = Validator::make($request->all(), $rules, $messages);

        if ($validation->fails()) {
			return  redirect()->back()->withErrors($validation)->withInput();
		} else {
            
                $user 			     = new User();
                $slugStr             = strtolower($request->name);
                $user->roles_id      = $request->type;
                $user->first_name    = $request->first_name;
                $user->last_name     = $request->last_name;
                $user->email 	     = $request->email;
                $user->password 	 = Hash::make($request->password);
                $user->mobile 	     = $request->mobile;
                $user->slug          = Str::slug($request->first_name , "-");
                $user->status 	     = $request->status;
                $user->is_trial 	 = 1;
                $user->is_subscribed = 0;
                $user->free_trial_end = date('Y-m-d',strtotime('+15 days',strtotime('now')));

                /*$name      = $user->name;
                $email     = $user->email;
                //$password  = $user->password;
            
                if($request->type == 3){
                    $Usertypes = $types[0]->name;
                }

                $body  = 'Your Email is : ' . $email . ' & your password is : ' . $password . ". You are registered as a ".$Usertypes. " at "  . env('APP_NAME') . ".";
                
                $data = array('name' => $name, 'body' => $body);
                Mail::send('emails.mail', $data, function ($message) use ($name, $email) {
                    $message->to($email, $name)
                        ->subject('User Registration');
                    $message->from(config('mail.username'), env('APP_NAME'));
                });  */

			if($user->save()) {
                return Redirect::to('admin/users-list')->withMessage("User Added Successfully.");
            }
        }
        /* }catch(\Exception $e){

                return redirect()->back()
                    ->withErrors($e->getMessage());
        } */  
    }

    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users          = User::query()->where('id', $id)->first();
        $categories     = Category::where('parent_id','!=',0)->get();
        return view('admin.users.view',['user'=>$users,'categories'=>$categories]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {

            $user           = User::findOrfail($id);
            $categories     = Category::where('parent_id','!=',0)->get();
           
            return view("admin.users.edit",
                [   
                    'categories'    =>  $categories,
                    'users' 	    => 	$user
                ]
            ); 

        }catch(\Exception $e){

            return redirect()->back()
                    ->withErrors($e->getMessage());

        }    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //try{
       
        /* $validation = Validator::make($request->all(), [

                'first_name'    => "required|string|regex:/^[a-zA-Z ]+$/u|max:255",
                'last_name'     => "required|string|regex:/^[a-zA-Z ]+$/u|max:255",
                'email'         =>  ['required','max:255','regex:/^\w+[-\.\w]*@(?!(?:)\.com$)\w+[-\.\w]*?\.\w{2,4}$/', \Illuminate\Validation\Rule::unique('users')->ignore($request->input('id'))],
                'mobile'        => ['required', 'between:6,15',\Illuminate\Validation\Rule::unique('users')->ignore($request->input('id'))],
                'status'        => "required" 
            ],
    
            [
            'email.regex' => 'We do not accept mails with this domain.'
            ],
            [
            'mobile.between' => 'Mobile number must be between 6 to 15 digits.'
            ]
        );   */

        $rules = [
                'first_name' => ['required','regex:/^[a-zA-Z ]+$/u','max:255'],
                'last_name'  => ['required','regex:/^[a-zA-Z ]+$/u','max:255'],
                'email'         =>  ['required','max:255','regex:/^\w+[-\.\w]*@(?!(?:)\.com$)\w+[-\.\w]*?\.\w{2,4}$/', \Illuminate\Validation\Rule::unique('users')->ignore($request->input('id'))],
                'mobile'        => ['required','between:6,15',\Illuminate\Validation\Rule::unique('users')->ignore($request->input('id'))],
                'status'  => ['required']
            ];
        
        $messages = [
            'email.regex' => 'We do not accept mails with this domain.',
            'mobile.between' => 'Mobile number must be between 6 to 15 digits.'
        ];

        $validation = Validator::make($request->all(), $rules, $messages);

        if ($validation->fails()) {
			return  redirect()->back()->withErrors($validation)->withInput();
		} else {
                $userID                     = $request->input('id');
                $userUpdate                 = User::query()->where('id', $userID)->first();
                if($request->status == 0){
                    if(isset($userID) && $userID != ""){
                        DB::table('oauth_access_tokens')->where('user_id', $userID)->delete();
                    }
                }

                $userUpdate->roles_id 		= $request->type;
                $userUpdate->first_name     = $request->first_name;
                $userUpdate->last_name      = $request->last_name;
                $userUpdate->email 	        = $request->email;
                $userUpdate->mobile 	    = $request->mobile;
                $userUpdate->slug           = Str::slug($request->first_name , "-");
                $userUpdate->status 	    = $request->status;
                
                /* if($userUpdate->status == 0){
                    if($request->status==1){
                        $status    = "Verified";  
                        $name      = $userUpdate->name;
                        $email     = $userUpdate->email;
                        //$password  = $userUpdate->password;
        
                        $body = 'Your Account Status  is changed as  : ' . $status ." Contact to admin for more details  at " . env('APP_NAME') . ".";
                        
                        $data = array('name' => $name, 'body' => $body);
                        Mail::send('emails.mail', $data, function ($message) use ($name, $email) {
                            $message->to($email, $name)
                                ->subject('Account Status');
                            $message->from(config('mail.username'), env('APP_NAME'));
                        });  
                    } 
                } */
                
			if($userUpdate->update()) {
                return Redirect::to('admin/users-list')->withMessage("User updated successfully.");
			}
        }
        /* }catch(\Exception $e){
                return redirect()->back()
                    ->withErrors($e->getMessage());
        } */
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        try{

        $getUser = User::query()->where('id', $id)->first();
        
        DB::table('oauth_access_tokens')->where('user_id',$getUser->id)->delete();
        // if ($getUser->tokens()) {
        //     $getUser->tokens()->delete();
        // }
        
        $getUser->delete();
        $message = "User Deleted Successfully!";
        return redirect()->back()->withMessage($message);

        }catch(\Exception $e){

                return redirect()->back()
                    ->withErrors($e->getMessage());
        }
    }


    /*  ====== Professionals Management ========  */

    public function ProfessionalList(){
        
        $professionals   = User::query()->where('roles_id', "=", 2)->with('roles')->get();
        $categories      = Category::where('parent_id','!=',0)->get();
        return view('admin.professional.index',compact('professionals','categories'));
    }

    public function professionalcreate(){
        
        /* $categories  = Category::leftJoin("categories as parent_cat","categories.parent_id" ,'=', 'parent_cat.id')
                    ->select("categories.*", "parent_cat.name as parent_name")
                    ->orderBy('created_at', 'desc')
                    ->get();  */
        $categories     = Category::where('parent_id','!=',0)->where('status',1)->get();

        return view('admin.professional.create',compact('categories'));

    }   

    public function professionalstore(Request $request){
        //try{

        $rules = [
                'first_name' => ['required','regex:/^[a-zA-Z ]+$/u','max:255'],
                'last_name'  => ['required','regex:/^[a-zA-Z ]+$/u','max:255'],
                'password'  => ['required','between:6,15'],
                'location'  => ['required'],
                'category'  => ['required'],
                'status'  => ['required'],
                'zip_code'  => ['required','min:6'],
                'email' => [
                    'required',
                    'string',
                    'email',
                    'max:255',
                    'unique:users',
                    'regex:/^\w+[-\.\w]*@(?!(?:outlook|myemail|yahoo|yopmail|red|mailinator|user)\.com$)\w+[-\.\w]*?\.\w{2,4}$/',\Illuminate\Validation\Rule::unique('users')
                ],
                'mobile' => ['required','between:6,15', \Illuminate\Validation\Rule::unique('users')]
            ];
        
        $messages = [
            'email.regex' => 'We do not accept mails with this domain.',
            'password.between' => 'Password must be between 8 to 16 characters',
            'mobile.between' => 'Mobile number must be between 6 to 15 digits.'
        ];

        $validation = Validator::make($request->all(), $rules, $messages);

        if ($validation->fails()) {
			return  redirect()->back()->withErrors($validation)->withInput();
		} else {

                $professional 			     = new User();
                $professional->roles_id 	 = 2;
                $professional->first_name    = $request->first_name;
                $professional->last_name     = $request->last_name;
                $professional->email 	     = $request->email;
                $professional->password 	 = Hash::make($request->password);
                $professional->mobile 	     = $request->mobile;
                $professional->slug          = Str::slug($request->first_name , "-");
                $professional->location 	 = $request->location;
                $professional->latitude 	 = $request->latitude;
                $professional->longitude 	 = $request->longitude;
                $professional->zip_code 	 = $request->zip_code;
                $professional->category_id 	 = $request->category;
                $professional->template_id 	 = 1;
                $professional->status 	     = $request->status;
                $professional->is_trial 	 = 1;
                $professional->is_subscribed = 0;
                $professional->free_trial_end = date('Y-m-d',strtotime('+15 days',strtotime('now')));

                /*if($request->type == 2){
                    $Usertypes = $types[0]->name;
                }

                $name      = $professional->name;
                $email     = $professional->email;
                //$password  = $professional->password;

                $body  = 'Your Email is : ' . $email . ' & your password is : ' . $password . ". You are registered as a ".$Usertypes. " at "  . env('APP_NAME') . ".";
                
                $data = array('name' => $name, 'body' => $body);
                Mail::send('emails.mail', $data, function ($message) use ($name, $email) {
                    $message->to($email, $name)
                        ->subject('Professional Registration');
                    $message->from(config('mail.username'), env('APP_NAME'));
                });  */

			if($professional->save()) {
                return Redirect::to('admin/professional-list')->withMessage("Professional Added Successfully.");
			}
        }
       /*  }catch(\Exception $e){

                return redirect()->back()
                    ->withErrors($e->getMessage());
        } */
    }

    public function professionaledit($id){
        try {

            $professional   = User::findOrfail($id);
            $categories     = Category::where('parent_id','!=',0)->where('status',1)->get();
           
            return view("admin.professional.edit",
                [   
                    'categories'     =>  $categories,
                    'professionals'  =>  $professional
                ]
            ); 

        }catch(\Exception $e){

            return redirect()->back()
                    ->withErrors($e->getMessage());

        }    
    }

    public function professionalupdate(Request $request){

            $validation = Validator::make($request->all(), [

                'first_name'    => "required|string|regex:/^[a-zA-Z ]+$/u|max:255",
                'last_name'     => "required|string|regex:/^[a-zA-Z ]+$/u|max:255",
                'email'         =>  ['required','max:255','regex:/^\w+[-\.\w]*@(?!(?:)\.com$)\w+[-\.\w]*?\.\w{2,4}$/', \Illuminate\Validation\Rule::unique('users')->ignore($request->input('id'))],
                'mobile'        => ['required','between:6,15',\Illuminate\Validation\Rule::unique('users')->ignore($request->input('id'))],
                'location'      => "required",
                'zip_code'      => "required|min:6",
                'category'      => "required",
                'status'        => "required" 
		    ],
    
            [
            'email.regex' => 'We do not accept mails with this domain.'
            ],
            [
            'mobile.between' => 'Mobile number must be between 6 to 15 digits.'
            ]
        );  

        if ($validation->fails()) {
			return  redirect()->back()->withErrors($validation)->withInput();
		} else {
                $professionalID                     = $request->input('id');
                $professionalUpdate                 = User::query()->where('id', $professionalID)->first();

                if($request->status == 0){
                    if(isset($professionalID) && $professionalID != ""){
                        DB::table('oauth_access_tokens')->where('user_id', $professionalID)->delete();
                    }
                }
                $professionalUpdate->roles_id 		= 2;
                $professionalUpdate->first_name 	= $request->first_name;
                $professionalUpdate->last_name 		= $request->last_name;
                $professionalUpdate->email 	        = $request->email;
                $professionalUpdate->mobile 	    = $request->mobile;
                $professionalUpdate->location 	    = $request->location;
                $professionalUpdate->latitude 	    = $request->latitude;
                $professionalUpdate->longitude 	    = $request->longitude;
                $professionalUpdate->slug           = Str::slug($request->first_name , "-");
                $professionalUpdate->template_id   	= 1;
                $professionalUpdate->category_id 	= $request->category;
                $professionalUpdate->status 	    = $request->status;
                
                if($professionalUpdate->status == 1){
                    $professionalUpdate->status 	= $request->status;
                    if($request->status==1){
                        $status    = "Verified";  
                        $name      = $professionalUpdate->name;
                        $email     = $professionalUpdate->email;
                        $body = 'Your Account Status is changed as : ' . $status ." , You can now login to continue";
                        
                        $data = array('name' => $name, 'body' => $body);
                        Mail::send('emails.mail', $data, function ($message) use ($name, $email) {
                            $message->to($email, $name)
                                ->subject('Account Status');
                            $message->from(config('mail.username'), env('APP_NAME'));
                        });  
                    } 
                }


			if($professionalUpdate->update()) {
                return Redirect::to('admin/professional-list')->withMessage("Professional updated successfully");
			}
        }
       
    }

    public function professionaldestroy($id){
        try{

        $getProfessional = User::query()->where('id', $id)->first();

        DB::table('oauth_access_tokens')->where('user_id',$getProfessional->id)->delete();
        
        // if($getProfessional->tokens())
        // {
        //     $getProfessional->tokens()->delete();
        // }
        $getProfessional->delete();
        $message = "Professional Deleted Successfully!";
        return redirect()->back()->withMessage($message);
        
        }catch(\Exception $e){

                return redirect()->back()
                    ->withErrors($e->getMessage());
        }
    }

    public function ProfessionalShow($id)
    {
        $professionals          = User::query()->where('id', $id)->first();
        $categories             = Category::where('parent_id','!=',0)->where('status',1)->get();
        return view('admin.professional.view',['professional'=>$professionals,'categories'=>$categories]);
    }

    public function VisitorsShow($id)
    {
        $today = Visitor::where('userid',decrypt($id))->whereDate('created_at', Carbon::today())->count();
        $week  = Visitor::where('userid',decrypt($id))->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
        $month = Visitor::where('userid',decrypt($id))->whereMonth('created_at', date('m'))
        ->whereYear('created_at', date('Y'))
        ->count();
        return view('admin.professional.visitor',compact('today','week','month')); 
    }

}
