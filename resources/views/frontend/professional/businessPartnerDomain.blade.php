@extends('frontend.professional.partials.index')

@section('content')

<main class="content">
<div class="container-fluid p-0">

	<div class="row mb-2 mb-xl-3">
		<div class="col-auto d-none d-sm-block">
			<h3><strong> {{auth()->user()->name ?? ""}}</strong> Add Domain</h3>
		</div>
		<div class="col-auto ms-auto text-end mt-n1">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb bg-transparent p-0 mt-1 mb-0">
					<li class="breadcrumb-item"><a href="{{route('business.partner.profile')}}">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Add Domain</li>
				</ol>
			</nav>
		</div>						
	</div>
	<div class="row">
		<div class="col-xl-7 col-xxl-5 d-flex">
			<div class="card w-100">
				<div class="card-body">
				@include('frontend.partials.messages')
				<form action="{{route('business.partner.addupdatedomain')}}" method="post">
				@csrf
				<div class="panel-body">
					<div class="profile-tab profile-content changepwdPT">										
						<div class="form-group">
							<label for="">Add Domain</label>
							<input type="text" name="domain_name" class="form-control" id="" value="{{old('domain_name',(!empty($domain->domain_name)) ? $domain->domain_name:'')}}" placeholder="Enter your domain">
						</div>
														
					</div>
					<button type="submit" class="btn btn-default btnCustom">Submit</button>									
				</div>
				</form>
			</div>
		</div>
	</div>
</div>				
</div>
</main>
@endsection